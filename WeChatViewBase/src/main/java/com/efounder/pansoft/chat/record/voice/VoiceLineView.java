package com.efounder.pansoft.chat.record.voice;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.efounder.chat.R;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by carlos on 2016/1/29.
 * 自定义声音振动曲线view
 */
public class VoiceLineView extends View {
    private final int LINE = 0;
    private final int RECT = 1;

    private int middleLineColor = Color.BLACK;
    private int voiceLineColor = Color.BLACK;
    private float middleLineHeight = 4;
    private Paint paint;
    private Paint paintVoicLine;
    private int mode;

    //是否开始录音
    private boolean startRecord = false;
    /**
     * 灵敏度
     */
    private int sensibility = 4;

    private float maxVolume = 100;


    private float translateX = 0;
    private boolean isSet = false;

    /**
     * 振幅
     */
    private float amplitude = 1;
    /**
     * 音量
     */
    private float volume = 10;
    private float myVolume = 10;

    private int fineness = 1;
    private float targetVolume = 1;


    private long speedY = 50;
    private float rectWidth = 25;
    private float rectSpace = 5;
    private float rectInitHeight = 4;
    private List<Rect> rectList;
    private int currentPosition = 0;
    private int lineSpeed = 0;


    public VoiceLineView(Context context) {
        super(context);
    }

    public VoiceLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAtts(context, attrs);
    }

    public VoiceLineView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAtts(context, attrs);
    }

    private void initAtts(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.voiceView);
        mode = typedArray.getInt(R.styleable.voiceView_viewMode, 0);
        voiceLineColor = typedArray.getColor(R.styleable.voiceView_voiceLine, Color.BLACK);
        maxVolume = typedArray.getFloat(R.styleable.voiceView_maxVolume, 100);
        sensibility = typedArray.getInt(R.styleable.voiceView_sensibility, 4);

        rectWidth = typedArray.getDimension(R.styleable.voiceView_rectWidth, 25);
        rectSpace = typedArray.getDimension(R.styleable.voiceView_rectSpace, 5);
        rectInitHeight = typedArray.getDimension(R.styleable.voiceView_rectInitHeight, 4);
        lineSpeed = typedArray.getInt(R.styleable.voiceView_lineSpeed, 0);
        if (lineSpeed < 0) {
            currentPosition = 9;
        }
        typedArray.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // myVolume = volume;
        drawVoiceRect(canvas);
        run();
    }


    private void drawVoiceRect(Canvas canvas) {
        if (paintVoicLine == null) {
            paintVoicLine = new Paint();
            paintVoicLine.setColor(voiceLineColor);
            paintVoicLine.setAntiAlias(true);
            paintVoicLine.setStyle(Paint.Style.FILL);
            paintVoicLine.setStrokeWidth(2);
        }

        if (rectList == null) {
            rectList = new LinkedList<>();
        }
        rectList.clear();
        int totalWidth = (int) (rectSpace + rectWidth);
        int widthPoint = 0;


        for (int i = 0; i < 10; i++) {
//            Rect rect = new Rect((int) (widthPoint),
//                    (int) (getHeight() / 2 - rectInitHeight / 2 - (volume == 10 ? 0 : volume / 2)),
//                    (int) (widthPoint+rectSpace),
//                    (int) (getHeight() / 2 + rectInitHeight / 2 + (volume == 10 ? 0 : volume / 2)));
            int height = getHeight();
            int left = widthPoint;

            int top = (int) (height / 2 - rectInitHeight / 2);
            int right = (int) (widthPoint + rectWidth);
            int bottom = (int) (height / 2 + rectInitHeight / 2);

//            Log.i("yqs", "totalWidth:" + totalWidth);
//            Log.i("yqs", "widthPoint:" + widthPoint);
//            Log.i("yqs", "height:" + height);
//            Log.i("yqs", "left:" + left);
//            Log.i("yqs", "top:" + top);
//            Log.i("yqs", "right:" + right);
//            Log.i("yqs", "bottom:" + bottom);
            if (i == currentPosition) {
                top = (int) (height / 2 - rectInitHeight / 2 - (myVolume == 10 ? 0 : myVolume / 2));
                bottom = (int) (height / 2 + rectInitHeight / 2 + (myVolume == 10 ? 0 : myVolume / 2));

                Log.i("yqs1", "top:" + top);
                Log.i("yqs1", "bottom:" + bottom);
            }

            Rect rect = new Rect(left, top, right, bottom);
            rectList.add(rect);
            widthPoint = widthPoint + totalWidth;
        }

//        if (lineSpeed<0){
//            Collections.reverse(rectList); // 倒序排列
//        }
//        for (int i = rectList.size() - 1; i >= 0; i--) {
//            canvas.drawRect(rectList.get(i), paintVoicLine);
//        }
//        for (int i = 0; i < rectList.size(); i++) {
//            canvas.drawRect(rectList.get(i), paintVoicLine);
//        }
        if (lineSpeed < 0) {
            for (int i = 9; i >= 0; i--) {
                canvas.drawRect(rectList.get(i), paintVoicLine);
            }
        } else {
            for (int i = 0; i < rectList.size(); i++) {
                canvas.drawRect(rectList.get(i), paintVoicLine);
            }
        }
        // canvas.drawCircle(getWidth() / 2, getHeight() / 2, 200, paintVoicLine);

        rectChange();


    }

    public void setVolume(int volume) {
        if (volume > maxVolume * sensibility / 25) {
            isSet = true;
            this.targetVolume = getHeight() * volume / 2 / maxVolume;
        }
    }


    private void rectChange() {

        Log.i("yqs1", "volume:" + volume);

        if (volume > 10) {
            myVolume = volume;
        }
        if (lineSpeed >= 0) {
            currentPosition++;
            if (currentPosition > 9) {
                currentPosition = 0;
                myVolume = volume;
            }
        } else {
            currentPosition--;
            if (currentPosition < 0) {
                currentPosition = 9;
                myVolume = volume;
            }
        }

        speedY += 6;
        if (volume < targetVolume && isSet) {
            volume += getHeight() / 30;
        } else {
            isSet = false;
            if (volume <= 10) {
                volume = 10;
            } else {
                if (volume < getHeight() / 30) {
                    volume -= getHeight() / 60;
                } else {
                    volume -= getHeight() / 30;
                }
            }
        }
        Log.i("yqs1", "myVolume:" + myVolume);
    }

    public void run() {
        if (mode == RECT) {
            postInvalidateDelayed(40);
        } else {
            invalidate();
        }
    }

    public static class MyRect {
        int left;
        int top;
        int right;
        int bottom;

        public MyRect(int left, int top, int right, int bottom) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }
    }

}