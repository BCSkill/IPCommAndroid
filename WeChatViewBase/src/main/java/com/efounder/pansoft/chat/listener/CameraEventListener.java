package com.efounder.pansoft.chat.listener;


public interface CameraEventListener {
    void onFinishTakePicture();
}
