package com.efounder.pansoft.chat.messageview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * @author lch 5-26
 * 撤回消息 群通知消息 提示消息
 */
public class JFMessageRecallView extends JFMessageBaseView {

    public JFMessageRecallView(Context context) {
        super(context);
        initView(context);
    }

    public JFMessageRecallView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    @SuppressLint("ResourceAsColor")
    private void initView(final Context context) {
        // 消息体区域
        RelativeLayout.LayoutParams chat_item_layout_content_layoutparams = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        chat_item_layout_content_layoutparams
                .addRule(RelativeLayout.ALIGN_PARENT_TOP);
        chat_item_layout_content_layoutparams.leftMargin = 100;
        chat_item_layout_content_layoutparams.rightMargin = 100;
        middleView.addView(rlMessageContentLayout,
                chat_item_layout_content_layoutparams);


    }


}
