package com.efounder.pansoft.chat.photo;

import java.util.LinkedList;

/**
 * Created by will on 18-4-12.
 */

public class PicturePickEvent {
    LinkedList<String> list;
    boolean isRawPic;

    public PicturePickEvent(LinkedList<String> list, boolean isRawPic) {
        this.list = list;
        this.isRawPic = isRawPic;
    }

    public LinkedList<String> getList() {
        return list;
    }

    public void setList(LinkedList<String> list) {
        this.list = list;
    }

    public boolean isRawPic() {
        return isRawPic;
    }

    public void setRawPic(boolean rawPic) {
        isRawPic = rawPic;
    }
}
