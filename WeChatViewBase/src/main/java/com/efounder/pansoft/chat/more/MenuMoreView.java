package com.efounder.pansoft.chat.more;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.efounder.chat.adapter.ExpressionPagerAdapter;
import com.efounder.pansoft.chat.input.JFChatMenuManager;
import com.efounder.pansoft.chat.emoji.widget.AutoHeightLayout;
import com.efounder.chat.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 对话界面 更多按钮的菜单（+号按钮）
 * Created by YQS on 2018/4/11.
 */

public class MenuMoreView extends AutoHeightLayout {
    private Context mContext;
    private LayoutInflater inflater;

    private View rootView;
    private ArrayList<View> menuPoints;
    private ViewPager menuViewPager;
    private JFChatMenuManager qQstyleChatMenuManager;
    private List<View> gridViews;
    private ExpressionPagerAdapter adapter;

    private int oldMenuPoint = 0;// 菜单
    private int currentMenuPoint; // 菜单
    private TextView tipsView;
    private JFChatMenuManager.OnMenuMoreItemClickListener onMenuMoreItemClickListener;


    public MenuMoreView(Context context) {
        this(context, null);
    }

    public MenuMoreView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MenuMoreView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
        mContext = context;
        initView(mContext);
    }

    private void initView(Context mContext) {
        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        this.setGravity(Gravity.CENTER);
        //获取布局
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rootView = inflater.inflate(R.layout.qqstyle_layout_more_menu, this, true);

        menuPoints = new ArrayList<View>();
        menuPoints.add(findViewById(R.id.menu_point_1));
        menuPoints.add(findViewById(R.id.menu_point_2));
        menuPoints.add(findViewById(R.id.menu_point_3));
        menuPoints.add(findViewById(R.id.menu_point_4));


        menuViewPager = findViewById(R.id.menuadater);
        tipsView = findViewById(R.id.tv_tips);
        qQstyleChatMenuManager = new JFChatMenuManager(mContext);

        gridViews = new ArrayList<>();
        adapter = new ExpressionPagerAdapter(gridViews);
        menuViewPager.setAdapter(adapter);

        menuViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                menuPoints.get(oldMenuPoint).setBackgroundResource(
                        R.drawable.dot_normal);
                menuPoints.get(position)
                        .setBackgroundResource(R.drawable.dot_focused);

                oldMenuPoint = position;
                currentMenuPoint = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    public void setMenuUserInfo(int toUserId, int chatType, String chatPermission) {


        List<View> list = qQstyleChatMenuManager.getGridViews(toUserId, chatType, chatPermission);
        menuPoints.get(0).setVisibility(View.GONE);
        menuPoints.get(1).setVisibility(View.GONE);
        if (list != null && list.size() > 0) {
            menuViewPager.setVisibility(VISIBLE);
            tipsView.setVisibility(View.GONE);
            if (list.size() > 1) {
                menuPoints.get(0).setVisibility(View.VISIBLE);
                menuPoints.get(1).setVisibility(View.VISIBLE);
            } else {
                menuPoints.get(0).setVisibility(View.GONE);
                menuPoints.get(1).setVisibility(View.GONE);
            }

            gridViews.clear();
            gridViews.addAll(list);
            adapter = new ExpressionPagerAdapter(gridViews);
            menuViewPager.removeAllViews();
            menuViewPager.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            tipsView.setVisibility(View.VISIBLE);
            menuViewPager.setVisibility(GONE);
        }
    }


    @Override
    public void onSoftKeyboardHeightChanged(int i) {
        Log.d("ddd", "onSoftKeyboardHeightChanged: " + i);
    }


    public void setOnMenuMoreItemClickListener(JFChatMenuManager.OnMenuMoreItemClickListener onMenuMoreItemClickListener) {
        if (qQstyleChatMenuManager != null)
            this.qQstyleChatMenuManager.setOnMenuMoreClickListener(onMenuMoreItemClickListener);
    }


}
