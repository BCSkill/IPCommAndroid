package com.efounder.pansoft.chat.messageview;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.util.EnvSupportManager;
import com.efounder.utils.JfResourceUtil;

import java.util.Map;

/**
 * qq样式 聊天item 2018/04/17
 *
 * @author yqs
 */
public class JFMessageBaseView extends LinearLayout {


    // /TopView 一般用于显示时间戳 1
    protected RelativeLayout topLayout;
    // 默认TopView里面为 显示消息时间
    protected TextView topTimeTextView;

    // MiddleView 中部消息的view 展示消息体 消息发送者等 2
    protected RelativeLayout middleView;
    //消息的内容
    protected RelativeLayout rlMessageContentLayout;

    //头像区域3
    protected RelativeLayout itemAvatarLayout;
    //头像
    protected ImageView avatarImageView;
    //头像下方的背景view
    protected ImageView avatarBelowView;
    //头像上面的
    protected ImageView avatarTopView;


    //消息上方的view 展示用户名,用户名左侧图标  图标左侧的称呼（如qq的相声演员） 4
    protected LinearLayout llMessageTopLayout;
    // 认证图标(用户名旁边的用户符号图标)
    protected ImageView ivUserSymbolView;
    // 用户名TextView
    protected TextView userNameTextView;
    // 用户等级别名textView(相声演员)
    protected TextView userGradeNameTextView;

    // 内容
    protected IMessageItem messageItem;

    //消息体下方消息状态，消息积分等
    protected LinearLayout rlMessageContentBottomLayout;
    //消息状态
    protected TextView chatStateView;
    //消息状态左边的文字（例如 已获取十积分）
    protected TextView chatStateLeftTextView;
    //积分钻石图片
    protected ImageView ivJiFenCountView;
    // /标志区域 用于显示发送进度条(延时1秒显示)/发送失败标志
    protected LinearLayout signViewLayout;

    //加载失败
    protected ImageView loadingErrorImage;
    //语音未读
    protected ImageView voiceUnRead;
    //语音时间
    protected TextView voiceTime;


    //加载中
    protected ProgressBar mprogressbar;

    // /BottomView 一般用于显示信息 如收到红包
    protected RelativeLayout bottomLayout;
    // /默认BottomView里面为Label
    protected TextView bottomTextView;

    // 数据源
    protected Map<String, Object> resultMap;


    private Context mContext;
    //类型 接收还是发送消息
    private int type;

    //是否支持积分等
    protected boolean supportIntergral;


    public JFMessageBaseView(Context context) {
        super(context);
        mContext = context;
        supportIntergral = EnvSupportManager.isSupportShowIntergral();
        initView(context);
    }

    public JFMessageBaseView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup chat_item = (ViewGroup) inflater.inflate(R.layout.qqstyle_messagebaseview_chat_item, this);

        //顶部区域(主要是消息时间)
        topLayout = (RelativeLayout) chat_item.findViewById(R.id.chat_item_topview);
        topTimeTextView = (TextView) chat_item.findViewById(R.id.chat_item_date);

        //中间区域
        middleView = (RelativeLayout) chat_item.findViewById(R.id.chat_item_middlelayout);

        //头像区域
        itemAvatarLayout = (RelativeLayout) inflater.inflate(R.layout.qqstylle_messagebaseview_chat_avatararea, null);
        avatarImageView = (ImageView) itemAvatarLayout.findViewById(R.id.ivUserAvatarView);
        avatarBelowView = (ImageView) itemAvatarLayout.findViewById(R.id.ivUserAvatarBelowView);
        avatarTopView = (ImageView) itemAvatarLayout.findViewById(R.id.ivUserAvatarUpperView);
//        GradientDrawable myGrad = (GradientDrawable)view.getBackground();
//        myGrad.setColor(color);//通过代码设置shape的颜色

        //消息体区域
        rlMessageContentLayout = new RelativeLayout(context);
        rlMessageContentLayout.setGravity(Gravity.CENTER_VERTICAL);

        //消息体上方区域
        llMessageTopLayout = (LinearLayout) inflater.inflate(R.layout.qqstyle_messagebaseview_message_top_layout, null);
        ivUserSymbolView = llMessageTopLayout.findViewById(R.id.ivSymbol);
        userNameTextView = llMessageTopLayout.findViewById(R.id.tvUserNameView);
        userGradeNameTextView = llMessageTopLayout.findViewById(R.id.tvUserAliasView);

        //消息体下方的消息状态 领取积分等
        rlMessageContentBottomLayout = (LinearLayout) inflater.inflate(R.layout.qqstyle_messagebaseview_message_state_layout, null);
        chatStateView = (TextView) rlMessageContentBottomLayout.findViewById(R.id.tvMeesageState);
        chatStateLeftTextView = (TextView) rlMessageContentBottomLayout.findViewById(R.id.tvStateLeft);
        ivJiFenCountView = rlMessageContentBottomLayout.findViewById(R.id.ivJIfenCountView);
        this.setSupplementVisible(true);

        //消息标记区域（失败叹号 load框，语音时间，未读标记等）
        signViewLayout = (LinearLayout) inflater.inflate(R.layout.qqstyle_message_sign_layout, null);
        loadingErrorImage = (ImageView) signViewLayout.findViewById(R.id.failImageView);
        mprogressbar = (ProgressBar) signViewLayout.findViewById(R.id.sendingProgressBar);
        voiceUnRead = (ImageView) signViewLayout.findViewById(R.id.voiceUnRead);
        voiceTime = (TextView) signViewLayout.findViewById(R.id.voiceTime);


        //底部区域
        bottomLayout = (RelativeLayout) chat_item.findViewById(R.id.chat_item_footer);
        bottomTextView = (TextView) chat_item.findViewById(R.id.chat_item_footertext);
        bottomTextView.setVisibility(View.GONE);

    }

    //设置消息状态区域是否显示
    public void setSupplementVisible(boolean isVisible) {
        if (isVisible) {
            rlMessageContentBottomLayout.setVisibility(View.VISIBLE);
        } else {
            rlMessageContentBottomLayout.setVisibility(View.GONE);
        }

    }


    public IMessageItem getMessageItem() {
        return messageItem;
    }

    public void setMessageItem(IMessageItem messageItem) {
        this.messageItem = messageItem;

        // TODO 先从中间layout中删除子view，再将messageItemView（实现IMessageItem接口）加入layout
        rlMessageContentLayout.removeAllViews();

        messageItem.messageView().setId(R.id.qqstyle_chat_messageview_id);
        rlMessageContentLayout.addView(messageItem.messageView());
        //todo 开始头像的帧动画
        Drawable drawable = avatarTopView.getBackground();
        if (drawable != null && drawable instanceof AnimationDrawable) {
            ((AnimationDrawable) drawable).start();
        }
        //todo 开始积分数量的帧动画
        Drawable jifenDrawable = ivJiFenCountView.getBackground();
        if (jifenDrawable != null && jifenDrawable instanceof AnimationDrawable) {
            ((AnimationDrawable) jifenDrawable).start();
        }

        //todo 添加两个时间的view 只有星际通讯支持(支持积分显示的同时也支持这个)
//        if (supportIntergral) {
//            //为了显示时间，需要设置最小宽度
//            rlMessageContentLayout.setMinimumWidth(SizeUtils.dp2px(70f));
//            rlMessageContentLayout.findViewById(R.id.qqstyle_chat_messageview_id).setMinimumWidth(SizeUtils.dp2px(70f));
//            addTimeView(messageItem);
//        }
    }

    /**
     * 加入时间的view
     *
     * @param messageItem
     */
    private void addTimeView(IMessageItem messageItem) {
        //发送者当地时间
        TextView senderLocalTimeView = new TextView(mContext);
        senderLocalTimeView.setId(R.id.qqstyle_chat_left_textview_id);
        senderLocalTimeView.setTextSize(10);
        //接收当地时间
        TextView receiveTimeView = new TextView(mContext);
        receiveTimeView.setId(R.id.qqstyle_chat_right_textview_id);
        receiveTimeView.setTextSize(10);

        //设置时间
        senderLocalTimeView.setText("19:22");
        receiveTimeView.setText("10:58");

        senderLocalTimeView.setTextColor(JfResourceUtil.getSkinColor(R.color.wechatview_chat_tip_item_text_color));
        receiveTimeView.setTextColor(JfResourceUtil.getSkinColor(R.color.wechatview_chat_tip_item_text_color));
        senderLocalTimeView.setBackground(getResources().getDrawable(R.drawable.wechatview_chat_tip_textview_bg));
        receiveTimeView.setBackground(getResources().getDrawable(R.drawable.wechatview_chat_tip_textview_bg));

        RelativeLayout.LayoutParams senderLocalTimeParams = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams receiveParams = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        receiveParams.addRule(RelativeLayout.BELOW, messageItem.messageView().getId());
        receiveParams.addRule(RelativeLayout.ALIGN_RIGHT, messageItem.messageView().getId());
        receiveParams.topMargin = 10;
        rlMessageContentLayout.addView(receiveTimeView, receiveParams);

        senderLocalTimeParams.addRule(RelativeLayout.BELOW, messageItem.messageView().getId());
        senderLocalTimeParams.addRule(RelativeLayout.LEFT_OF, receiveTimeView.getId());
        senderLocalTimeParams.topMargin = 10;
        senderLocalTimeParams.rightMargin = 10;
        rlMessageContentLayout.addView(senderLocalTimeView, senderLocalTimeParams);
    }


    public TextView getSenderTimeView() {
        return rlMessageContentLayout.findViewById(R.id.qqstyle_chat_left_textview_id);
    }

    public TextView getReceiverTimeView() {
        return rlMessageContentLayout.findViewById(R.id.qqstyle_chat_left_textview_id);
    }

    // 上层view
    public ViewGroup getTopLayout() {
        return topLayout;
    }

    // 默认上层textview
    public TextView getTopTimeTextView() {
        return topTimeTextView;
    }

    public RelativeLayout getMiddleView() {
        return middleView;
    }

    //得到头像区域
    public View getItemAvatarLayout() {
        return itemAvatarLayout;
    }

    // 头像
    public ImageView getAvatarImageView() {
        return avatarImageView;
    }

    // 认证
    public ImageView getIvUserSymbolView() {
        return ivUserSymbolView;
    }

    //用户名
    public TextView getUserNameTextView() {
        return userNameTextView;
    }

    //得到消息content布局
    public RelativeLayout getRlMessageContentLayout() {
        return rlMessageContentLayout;
    }

    // 加载中、、、错误、view
    public LinearLayout getSignViewLayout() {
        return signViewLayout;
    }

    //加载失败
    public ImageView getLoadingErrorImage() {
        return loadingErrorImage;
    }

    //语音未读
    public ImageView getVoiceUnReadImage() {
        return voiceUnRead;
    }

    //语音时长
    public TextView getVoiceTimeTextView() {
        return voiceTime;
    }

    //消息状态
    public TextView getChatStateView() {
        return chatStateView;
    }

    //加载中
    public ProgressBar getMprogressbar() {
        return mprogressbar;
    }

    // 下层view
    public ViewGroup getBottomLayout() {
        return bottomLayout;
    }

    // 下层默认textview
    public TextView getBottomTextView() {
        return bottomTextView;
    }

    // 得到数据源
    public Map<String, Object> getResultMap() {
        return resultMap;
    }

    // 设置数据源
    public void setResultMap(Map<String, Object> resultMap) {
        this.resultMap = resultMap;
    }


    public void setItemType(int type) {
        this.type = type;
    }

    public int getItemType() {
        return type;
    }

    //头像背景图
    public ImageView getAvatarBelowView() {
        return avatarBelowView;
    }

    //头像前景图
    public ImageView getAvatarTopView() {
        return avatarTopView;
    }

    //用户等级名字（相声演员）
    public TextView getUserGradeNameTextView() {
        return userGradeNameTextView;
    }

    //消息状态左边的布局
    public TextView getChatStateLeftTextView() {
        return chatStateLeftTextView;
    }

    //积分钻石数量
    public ImageView getIvJiFenCountView() {
        return ivJiFenCountView;
    }


    //用户名所在布局
    public LinearLayout getLlMessageTopLayout() {
        return llMessageTopLayout;
    }

}

