package com.efounder.pansoft.chat.photo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.utils.ResStringUtil;
import com.utilcode.util.SizeUtils;
import com.utilcode.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.LinkedList;


/**
 * Created by will on 18-4-12.
 */

public class JFPicturePickPhotoWallAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<SelectModel> mFiles = null;
    private boolean isWithTakePic;
    private OnPicSelectedChangedListener onPicSelectedChangedListener;
    private OnVideoSelectedListener onVideoSelectedListener;
    private OnTakePicSectedListener onTakePicSectedListener;

    /**
     ** @param isWithTakePic 是否带有拍照按钮
     */
    public JFPicturePickPhotoWallAdapter(Context context, ArrayList<SelectModel> imagePathList,
                                         boolean isWithTakePic) {
        this.context = context;
        this.mFiles = imagePathList;
        this.isWithTakePic = isWithTakePic;
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);

        int width = wm.getDefaultDisplay().getWidth();
        int height = wm.getDefaultDisplay().getHeight();
        // loader = new SDCardImageLoader(ScreenUtils.getScreenW(), ScreenUtils.getScreenH());
    }

    @Override
    public int getCount() {
        if (isWithTakePic) {
            return mFiles == null ? 1 : mFiles.size() + 1;
        } else {
            return mFiles == null ? 0 : mFiles.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (isWithTakePic) {
            if (position == 0) {
                return "";
            } else {
                return mFiles.get(position - 1);
            }
        } else {
            return mFiles.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.qq_style_item_picture_pick_photo_wall, null);
            holder = new ViewHolder();

            holder.imageView =  convertView.findViewById(R.id.photo_wall_item_photo);
            holder.tvNumber =  convertView.findViewById(R.id.tv_number);
            holder.tvNumberTop = convertView.findViewById(R.id.tv_number_top);
            holder.tvVideoTime = convertView.findViewById(R.id.tv_video_time);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //tag镄刱ey蹇呴』浣跨敤id镄勬柟寮忓畾涔変互淇濊瘉鍞竴锛屽惁鍒欎细鍑虹幇IllegalArgumentException.
//        holder.tvNumber.setTag(1, position);
//        holder.tvNumber.setTag(2, holder.imageView);
//        holder.tvNumber.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                Integer position = (Integer) buttonView.getTag(1);
//                ImageView image = (ImageView) buttonView.getTag(2);
//
//                selectionMap.put(position, isChecked);
//                if (isChecked) {
//                    image.setColorFilter(context.getResources().getColor(R.color.trans_black));
//                } else {
//                    image.setColorFilter(null);
//                }
//            }
//        });

        //yqs澧炲姞  鍙互镣瑰向锲剧墖杩涜阃変腑
        holder.imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int newPosition = position;
                if (isWithTakePic) {
                    newPosition = position - 1;
                    if (position == 0 && onTakePicSectedListener != null) {
                        onTakePicSectedListener.onTakePic();
                        return;
                    }
                }
                if (mFiles.get(newPosition).getType() == MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
                    if (JFPicturePickPhotoWallActivity.selectedPics.size() == 0) {
                        if (onVideoSelectedListener != null) {
                            onVideoSelectedListener.onVideoSelected(position);
                            return;
                        }
                    } else {
                        ToastUtils.showShort(R.string.wechatview_image_video_not_selected);
                    }
                }

                Intent intent = new Intent(context, JFPicturePickBigPictureActivity.class);
                //图片太多intent传的值太大会闪退，用EventBus的粘性事件
                EventBus.getDefault().postSticky(new ShowBigPictureEvent(mFiles, newPosition));
//                intent.putExtra("folderList", mFiles);
//                intent.putExtra("curIndex", position);
                ((Activity)context).startActivityForResult(intent, JFMessagePicturePickView.REQUEST_PIC_SELECTE_CODE);
            }
        });

        holder.tvNumberTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int newPosition = position;
                if (isWithTakePic) {
                    newPosition = position - 1;
                    if (position == 0){
                        return;
                    }
                }
                if (mFiles.get(newPosition).getType() == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
                    int index = JFPicturePickPhotoWallActivity.selectedPics.indexOf(mFiles.get(newPosition));
                    if (index == -1) {//不存在则加入数组里
                        if (JFPicturePickPhotoWallActivity.maxNum != 0 && JFPicturePickPhotoWallActivity.selectedPics.size() >= JFPicturePickPhotoWallActivity.maxNum) {
                            ToastUtils.showShort(String.format(ResStringUtil.getString(R.string.bga_pp_toast_photo_picker_max),
                                    JFPicturePickPhotoWallActivity.maxNum));
                            return;
                        }
                        holder.imageView.setColorFilter(context.getResources().getColor(R.color.trans_black));
                        JFPicturePickPhotoWallActivity.selectedPics.add(mFiles.get(newPosition));
                        holder.tvNumber.setText(JFPicturePickPhotoWallActivity.selectedPics.size() + "");
                    } else {
                        JFPicturePickPhotoWallActivity.selectedPics.remove(index);
                        holder.tvNumber.setText("");
                        holder.imageView.setColorFilter(null);
                    }
                    notifyDataSetChanged();
                    onPicSelectedChangedListener.onPicSelectedChanged();
                } else {
                    if (JFPicturePickPhotoWallActivity.selectedPics.size() == 0 &&
                            onVideoSelectedListener != null) {
                        onVideoSelectedListener.onVideoSelected(position);
                        return;
                    }
                    ToastUtils.showShort(R.string.wechatview_image_video_not_selected);
                }
            }
        });
        int newPosition = position;
        if (isWithTakePic) {
            newPosition = position - 1;
        }
        if (isWithTakePic && position == 0) {
            holder.tvNumber.setVisibility(View.GONE);
            holder.tvVideoTime.setVisibility(View.GONE);
            holder.tvNumberTop.setVisibility(View.GONE);
            int padding = SizeUtils.dp2px(40);
            holder.imageView.setPadding(padding, padding, padding, padding);
            holder.imageView.setScaleType(ImageView.ScaleType.CENTER);
            LXGlideImageLoader.getInstance().displayImage(context, holder.imageView,
                    R.drawable.wechat_icon_camera);
        } else {
            String filePath = ((SelectModel) getItem(position)).getPath();
            holder.tvNumber.setVisibility(View.VISIBLE);
            holder.tvVideoTime.setVisibility(View.VISIBLE);
            holder.tvNumberTop.setVisibility(View.VISIBLE);
            holder.imageView.setPadding(0, 0, 0, 0);
            holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            LXGlideImageLoader.getInstance().displayImage(context, holder.imageView,
                    filePath,R.drawable.default_img,R.drawable.default_img);
            int index = JFPicturePickPhotoWallActivity.selectedPics.indexOf(mFiles.get(newPosition));
            if (index != -1){
                holder.tvNumber.setBackground(context.getResources().getDrawable(R.drawable.qq_style_circle_number_bg));
                holder.imageView.setColorFilter(context.getResources().getColor(R.color.trans_black));
                holder.tvNumber.setText(index+1+"");
            }else {
                holder.tvNumber.setText("");
                holder.tvNumber.setBackground(context.getResources().getDrawable(R.drawable.jf_circle_no_number_bg));
                holder.imageView.setColorFilter(null);
            }

            if (mFiles.get(newPosition).getType() == MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
                holder.tvVideoTime.setVisibility(View.VISIBLE);
                holder.tvVideoTime.setText(mFiles.get(newPosition).getSize());
            }else {
                holder.tvVideoTime.setVisibility(View.GONE);
            }
        }

        return convertView;
    }

    private class ViewHolder {
        ImageView imageView;
        TextView tvNumber;
        TextView tvNumberTop;
        TextView tvVideoTime;
    }

    public LinkedList<SelectModel> getSelectionMap() {
        return JFPicturePickPhotoWallActivity.selectedPics;
    }
    public interface OnPicSelectedChangedListener {
        void onPicSelectedChanged();
    }

    public void setOnPicSelectedChangedListener(OnPicSelectedChangedListener onPicSelectedChangedListener) {
        this.onPicSelectedChangedListener = onPicSelectedChangedListener;
    }

    public interface OnVideoSelectedListener{
        void onVideoSelected(int position);
    }

    public void setOnVideoSelectedListener(OnVideoSelectedListener onVideoSelectedListener) {
        this.onVideoSelectedListener = onVideoSelectedListener;
    }

    public interface OnTakePicSectedListener{
        void onTakePic();
    }

    public void setOnTakePicSectedListener(OnTakePicSectedListener onTakePicSectedListener) {
        this.onTakePicSectedListener = onTakePicSectedListener;
    }
}
