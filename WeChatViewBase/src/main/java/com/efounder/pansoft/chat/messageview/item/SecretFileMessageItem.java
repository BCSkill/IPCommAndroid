package com.efounder.pansoft.chat.messageview.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * @author slp
 * 密件消息item
 */
public class SecretFileMessageItem extends LinearLayout implements IMessageItem {

    private JSONObject mJSONObject;
    private Context mContext;
    private IMStruct002 mIMStruct002;
    private ImageView ivSecretBg;

    private MultiTransformation multi;

    public SecretFileMessageItem(Context context) {
        super(context);

        this.mContext = context;
        final LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.chat_item_message_secret_file, this);//注意第二个参数

        ivSecretBg = findViewById(R.id.iv_secret_file_bg);

        int width = DisplayUtil.getMobileWidth(context) * 2 / 4 - 20;
        LayoutParams lp = new LayoutParams(width, LayoutParams.MATCH_PARENT);
        this.setLayoutParams(lp);

        multi = new MultiTransformation(
                new CenterCrop(),
                new RoundedCornersTransformation(10, 0,
                        RoundedCornersTransformation.CornerType.ALL));

        showPic(R.drawable.wechatview_secret_file_bg, ivSecretBg);
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        this.mIMStruct002 = message;
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(mIMStruct002);
            }
        });
    }

    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {

    }

    @Override
    public void prepareForReuse() {

    }

    private void showPic(int resource, ImageView imageView) {
        Glide.with(mContext).load(resource)
                .apply(new RequestOptions()
                        .transform(multi)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(imageView);
    }
}
