package com.efounder.imageselector.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.efounder.chat.R;
import com.efounder.imageselector.bean.Image;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 *  @author yqs
 * 图片Adapter
 */
public class ImageGridAdapter_imageLoader extends BaseAdapter {

	private Context context;
    private LayoutInflater mInflater;
    private boolean showCamera = true;
    private boolean showSelectIndicator = true;

    private List<Image> mImages = new ArrayList<Image>();
    private List<Image> mSelectedImages = new ArrayList<Image>();

    private int mItemSize;
    private GridView.LayoutParams mItemLayoutParams;
    
    
    public ImageGridAdapter_imageLoader(Activity context, boolean showCamera){
    	this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.showCamera = showCamera;
        mItemLayoutParams = new GridView.LayoutParams(GridView.LayoutParams.MATCH_PARENT, GridView.LayoutParams.MATCH_PARENT);
        initImageBeanList();
    }
    
    private void initImageBeanList(){
    	mImages.clear();
    	if (showCamera) {
			Image image = new Image("android_drawable", "asy", 0);
			mImages.add(image);
		}
    }
    
    /**
     * 显示选择指示器
     * @param b
     */
    public void showSelectIndicator(boolean b) {
        showSelectIndicator = b;
    }

    public void setShowCamera(boolean b){
        if(showCamera == b) return;

        showCamera = b;
        notifyDataSetChanged();
    }

    public boolean isShowCamera(){
        return showCamera;
    }

    /**
     * 选择某个图片，改变选择状态
     * @param image
     */
    public void select(Image image) {
        if(mSelectedImages.contains(image)){
            mSelectedImages.remove(image);
        }else{
            mSelectedImages.add(image);
        }
        notifyDataSetChanged();
    }

    /**
     * 通过图片路径设置默认选择
     * @param resultList
     */
    public void setDefaultSelected(ArrayList<String> resultList) {
        for(String path : resultList){
            Image image = getImageByPath(path);
            if(image != null){
                mSelectedImages.add(image);
            }
        }
        if(mSelectedImages.size() > 0){
            notifyDataSetChanged();
        }
    }

    private Image getImageByPath(String path){
        if(mImages != null && mImages.size()>0){
            for(Image image : mImages){
                if(image.path.equalsIgnoreCase(path)){
                    return image;
                }
            }
        }
        return null;
    }

    /**
     * 设置数据集
     * @param images
     */
    public void setData(List<Image> images) {
        mSelectedImages.clear();

        if(images != null && images.size()>0){
        	initImageBeanList();
            mImages.addAll(images);
        }else{
        	initImageBeanList();
        }
        notifyDataSetChanged();
    }

    /**
     * 重置每个Column的Size
     * @param columnWidth
     */
    public void setItemSize(int columnWidth) {

        if(mItemSize == columnWidth){
            return;
        }

        mItemSize = columnWidth;

        mItemLayoutParams = new GridView.LayoutParams(mItemSize, mItemSize);

        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return mImages.size();
    }

    @Override
	public Image getItem(int i) {
		return mImages.get(i);
	}

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        System.out.println("--------getView position:" + i + " view:" + view + " viewHolder:" + (view==null? null:view.getTag()) + " type = " );
        ViewHolde holde;
        if(view == null){
        	view = mInflater.inflate(R.layout.list_item_image, viewGroup, false);
        	holde = new ViewHolde(view);
        }else{
        	holde = (ViewHolde) view.getTag();
        }
        
        holde.bindData(getItem(i));

        /** Fixed View Size */
        GridView.LayoutParams lp = (GridView.LayoutParams) view.getLayoutParams();
        if(lp.height != mItemSize){
            view.setLayoutParams(mItemLayoutParams);
        }
        
        

        return view;
    }

    class ViewHolde {
        ImageView image;
        ImageView indicator;

        ViewHolde(View view){
            image = (ImageView) view.findViewById(R.id.image);
            indicator = (ImageView) view.findViewById(R.id.checkmark);
            view.setTag(this);
        }

        void bindData(final Image data){
            if(data == null) return;
            if (data.path.equals("android_drawable")) {//处理第一个位置：拍照
            	indicator.setVisibility(View.GONE);
            	//R.drawable.asy; com.efounder.chat
            	image.setImageResource(context.getResources().getIdentifier(data.name, "drawable", context.getPackageName()));
            	return;
			}
            
            // 处理单选和多选状态
            if(showSelectIndicator){
                indicator.setVisibility(View.VISIBLE);
                if(mSelectedImages.contains(data)){
                    // 设置选中状态
                    indicator.setImageResource(R.drawable.btn_selected);
                }else{
                    // 未选择
                    indicator.setImageResource(R.drawable.btn_unselected);
                }
            }else{
                indicator.setVisibility(View.GONE);
            }
            File imageFile = new File(data.path);

            if(mItemSize > 0) {
                // 显示图片
//                Picasso.with(mContext)
//                        .load(imageFile)
//                        .placeholder(R.drawable.default_error)
//                                //.error(R.drawable.default_error)
//                        .resize(mItemSize, mItemSize)
//                        .centerCrop()
//                        .into(image);
            	boolean isReloadImage = ! data.path.equals(image.getTag());
            	Log.i("", "-----是否重新加载图片：" + isReloadImage);
            	if (isReloadImage) {
//            		ImageLoader.getInstance().displayImage( "file://" + imageFile.getAbsolutePath(), image);
                    Glide.with(context)
                            .load(imageFile)
                            .apply(new RequestOptions().centerCrop()
                                    .dontAnimate()
                                    .placeholder(R.drawable.default_error)
                                    .error(R.drawable.default_error))
                            .thumbnail(0.5f)
                            //  .override(size/4*3, size/4*3)
                            .into(image);
            		image.setTag(data.path);
				}
            }
        }
    }

}
