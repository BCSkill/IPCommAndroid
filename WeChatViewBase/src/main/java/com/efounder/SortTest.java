package com.efounder;

import java.util.Arrays;

public class SortTest {


    public static void main(String[] args) {
        System.out.println(Arrays.toString(bubbleSort(new int[]{2,8,4,5,9,1,3,8})));
    }

    //冒泡排序
    public  static int [] bubbleSort(int array[]) {
        int t = 0;
        for (int i = 0; i < array.length - 1; i++)
            for (int j = 0; j < array.length - 1 - i; j++)
                if (array[j] > array[j + 1]) {
                    t = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = t;
                }
                return array;
    }
}