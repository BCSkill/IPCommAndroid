package com.efounder.chat.service;

import me.leolin.shortcutbadger.ShortcutBadger;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MsgReceiver extends BroadcastReceiver {
	/**新消息到来*/
	public static final String MSG_NEW_MSG_COMMING  = "new message comming";
	/**新消息已读*/
	public static final String MSG_NEW_MSG_OPEN  = "new message read";
	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if(MSG_NEW_MSG_COMMING.equals(action)){
			intent.getIntExtra("badgernum",-1);
			boolean success = ShortcutBadger.applyCount(context, 10);
		}
	}

}
