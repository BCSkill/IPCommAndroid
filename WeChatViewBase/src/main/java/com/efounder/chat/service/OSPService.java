package com.efounder.chat.service;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.efounder.chat.utils.ServiceUtils;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.util.EnvSupportManager;
import com.efounder.util.StringUtil;

import static com.efounder.frame.utils.Constants.CHAT_PASSWORD;
import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * Created by long on 2016/8/8.
 */

public class OSPService extends Service {
    private static final String TAG = "OSPService";
    //是否支持IM消息服务
    private boolean supportIM = true;
    /**
     * 定时唤醒的时间间隔，5分钟
     */
    private final static int ALARM_INTERVAL = 5 * 60 * 1000;
    private final static int WAKE_REQUEST_CODE = 6666;

    private final static int GRAY_SERVICE_ID = -1001;

    //    PowerManager.WakeLock wakeLock;
    private ForegroundServiceDelegate foregroundServiceDelegate;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "---onCreate---");
//        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
//        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "OSPService");
        supportIM = EnvSupportManager.isSupportIM();
        foregroundServiceDelegate = new ForegroundServiceDelegate(this);
        foregroundServiceDelegate.startForeground();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        if(wakeLock != null && !wakeLock.isHeld()){
//            wakeLock.acquire();
//        }
        Log.i(TAG, "---onStartCommand---");
        if (supportIM && (!ServiceUtils.isServiceRunning(getApplicationContext(), MessageService.class.getCanonicalName())
                || !ServiceUtils.isServiceRunning(getApplicationContext(), SystemInfoService.class.getCanonicalName()))
                && !StringUtil.isEmpty(EnvironmentVariable.getProperty(CHAT_USER_ID)) && !StringUtil.isEmpty(EnvironmentVariable.getProperty(CHAT_PASSWORD))) {
            Log.i(TAG, "---chatUserID:" + EnvironmentVariable.getProperty(CHAT_USER_ID));
            startService(new Intent(getApplicationContext(), SystemInfoService.class));
        }

//        if (Build.VERSION.SDK_INT < 18) {
//            startForeground(GRAY_SERVICE_ID, new Notification());//API < 18 ，此方法能有效隐藏Notification上的图标
//        } else {
//            Intent innerIntent = new Intent(this, GrayInnerService.class);
//            startService(innerIntent);
//            startForeground(GRAY_SERVICE_ID, new Notification());
//        }

        //发送唤醒广播来促使挂掉的UI进程重新启动起来
//        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        Intent alarmIntent = new Intent();
//        alarmIntent.setAction(WakeReceiver.GRAY_WAKE_ACTION);
//        PendingIntent operation = PendingIntent.getBroadcast(this, WAKE_REQUEST_CODE, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), ALARM_INTERVAL, operation);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tryReleaseWakeLock();
//        mediaPlayer.release();
        foregroundServiceDelegate.stopForeground(true);
    }

    protected void tryReleaseWakeLock() {
//        if(wakeLock != null && wakeLock.isHeld() == true){
//            wakeLock.release();
//        }
    }

    /**
     * 给 API >= 18 的平台上用的灰色保活手段
     */
    public static class GrayInnerService extends Service {
        @Override
        public void onCreate() {
            super.onCreate();
        }

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            startForeground(GRAY_SERVICE_ID, new Notification());
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        }

        @Override
        public IBinder onBind(Intent intent) {
            throw new UnsupportedOperationException("Not yet implemented");
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
        }
    }


}
