//package com.efounder.chat.service;
//
//import android.app.Activity;
//import android.app.Service;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.os.IBinder;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.efounder.chat.http.WeChatHttpRequest;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.http.EFHttpRequest;
//import com.efounder.http.EFHttpRequest.HttpRequestListener;
//import com.efounder.message.socket.JFSocketHandler;
//import com.efounder.util.StorageUtil;
//
//import java.io.Serializable;
//
///**
// * 开启服务监听网络状态类
// *
// * @author lch
// *补充 2g、3g、4g  ----http://blog.csdn.net/hknock/article/details/37650917
// */
//public class NetworkStateService extends Service {
//
//	private static final String tag = "NetworkStateService";
//	private ConnectivityManager connectivityManager;
//	private NetworkInfo info;
//	//无网络
//	public static final int NET_UNREACHABLE = 0;
//	//wifi
//	public static final int NET_WIFI = 1;
//	//流量
//	public static final int NET_DATA = 2;
//	private static int NET_STATE = 1;
//
//	private WeChatHttpRequest weChatHttpRequest;
//
//	private boolean isLogining = false;//是否正在登陆
//
//	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
//
//		@Override
//		public void onReceive(Context context, Intent intent) {
//			String action = intent.getAction();
//			if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
//				Log.i(tag, "网络状态已经改变");
//				connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//				info = connectivityManager.getActiveNetworkInfo();
//				if (info != null && info.isAvailable()) {
//					String name = info.getTypeName();
//					Log.i(tag, "当前网络名称：" + name);
//					if (NET_STATE==NET_UNREACHABLE) {
//						Toast.makeText(context, "网络连接已恢复", Toast.LENGTH_LONG).show();
//					}
//					if (name.toLowerCase().equals("wifi")) {
//						NET_STATE = NET_WIFI;
//					} else {
//						NET_STATE = NET_DATA;
//					}
//					//TODO 如果 @link JFSocketHandler isChannelActive = false
//					//1.登录
//					//2.断线重连
//					if (!JFSocketHandler.isChannelActive && !isLogining) {
//						//TODO 当程序退出后 service中 通过EnvironmentVariable 取不到 id 和密码，所以通过storageUtil 取
////						String userId = EnvironmentVariable.getProperty(CHAT_USER_ID);
////						String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
////						storageUtil.putString(CHAT_USER_ID, chatUserID);
////						storageUtil.putString(CHAT_PASSWORD, chatPassword);
//						StorageUtil storageUtil = new StorageUtil(getApplicationContext(), "storage");
//						String userId = storageUtil.getString(CHAT_USER_ID);
//						String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
//						httpLogin(userId, password);
//					}
//
//				} else {
//					Log.i(tag, "没有可用网络");
//					Toast.makeText(context, "网络连接不可用，请检查网络", Toast.LENGTH_LONG).show();
//					NET_STATE = NET_UNREACHABLE;
//				}
//			}
//		}
//	};
//
//	@Override
//	public IBinder onBind(Intent intent) {
//		Log.i(tag,tag + "------onBind");
//		return null;
//	}
//
//	@Override
//	public void onCreate() {
//		super.onCreate();
//		Log.i(tag,tag + "------onCreate");
//		IntentFilter mFilter = new IntentFilter();
//		mFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
//		registerReceiver(mReceiver, mFilter);
//
//		new Thread(){
//			@Override
//			public void run() {
//				super.run();
//				while (true){
//					try {
//						Thread.sleep(3000);
////						Log.i("------","---NetworkStateService is running");
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//				}
//			}
//		}.start();
//
//	}
//
//	@Override
//	public void onDestroy() {
//		super.onDestroy();
//		Log.i(tag,tag + "------onDestroy");
//		unregisterReceiver(mReceiver);
//		if (weChatHttpRequest != null){
//			//TODO 移除监听
//			weChatHttpRequest = null;
//		}
//	}
//
//	@Override
//	public int onStartCommand(Intent intent, int flags, int startId) {
//		Log.i(tag,tag + "------onStartCommand");
//		return super.onStartCommand(intent, flags, startId);
//	}
//
//	private void httpLogin(final String userId, final String password){
//		//TODO 登陆普信
//		Log.i(tag,"断线重连------1.断线重连 begin");
//		isLogining = true;
//		if (weChatHttpRequest == null){//保证单例
//			weChatHttpRequest = WeChatHttpRequest.getInstance(getApplicationContext());
//			weChatHttpRequest.addLoginListener(new WeChatHttpRequest.LoginListener() {
//
//				@Override
//				public void onLoginSuccess() {
//					isLogining = false;
//					Log.i(tag, "断线重连-----2.登录成功");
//				}
//
//				@Override
//				public void onLoginFail(String errorMsg) {
//					Log.e(tag, "断线重连-----2.登录失败 :" + errorMsg);
//					isLogining = false;
//					if (NET_STATE != NET_UNREACHABLE && !JFSocketHandler.isChannelActive) {//只要有网，并且没连上，就一直连
//						new Thread(){
//							@Override
//							public void run() {
//								super.run();
//								try {
//									Thread.sleep(2*1000);
//									httpLogin(userId, password);
//								} catch (InterruptedException e) {
//									e.printStackTrace();
//								}
//							}
//						}.run();
//					}
//				}
//			});
//		}
//
//		weChatHttpRequest.httpLogin(userId,password);
//	}
//
//	public static int getNetState(){
//		return NET_STATE;
//	}
//
//}
