package com.efounder.chat.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Nullable;

/**
 * 启动前台服务并隐藏通知栏图标(android 通知栏显示正在运行)
 *
 * @author YQS
 * service只是起引导作用，干完活就退出了
 */
public class BootstartService extends Service {

    private ForegroundServiceDelegate delegate;

    @Override
    public void onCreate() {
        super.onCreate();

        delegate = new ForegroundServiceDelegate(this);
        delegate.startForeground();
        // stop self to clear the notification  
        //stopSelf();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        delegate.stopForeground(true);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}