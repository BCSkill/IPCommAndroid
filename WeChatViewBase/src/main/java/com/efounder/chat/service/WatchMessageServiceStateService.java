//package com.efounder.chat.service;
//
//import android.app.Service;
//import android.content.Intent;
//import android.os.IBinder;
//import android.support.annotation.Nullable;
//import android.util.Log;
//
//import com.efounder.chat.http.WeChatHttpRequest;
//import com.efounder.message.socket.JFSocketHandler;
//import com.efounder.util.StorageUtil;
//
///**
// * Created by XinQing on 2016/8/8.
// * 监视消息服务连接状态服务
// */
//
//public class WatchMessageServiceStateService extends Service {
//    private static final String TAG = "WatchMessageService";
//
//    private boolean isStopThread = false;
//    private boolean isLogining = false;//是否正在登陆
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        //启动线程，监视
//        new Thread(){
//            @Override
//            public void run() {
//                super.run();
//                while (!isStopThread){
//                    try {
//                        Thread.sleep(1000 * 5);
//                        Log.i(TAG,"----------监视消息服务连接状态服务 运行中。。。");
//                        if (NetworkStateService.getNetState() != NetworkStateService.NET_UNREACHABLE && !JFSocketHandler.isChannelActive && !isLogining) {//只要有网，并且没连上，就一直连
//                            Log.i(TAG,"----------监视消息服务连接状态服务 登录中。。。");
//                            StorageUtil storageUtil = new StorageUtil(getApplicationContext(), "storage");
//                            String userId = storageUtil.getString(CHAT_USER_ID);
//                            String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
//                            httpLogin(userId,password);
//                        }
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }.start();
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        isStopThread = true;
//    }
//
//    private void httpLogin(final String userId, final String password){
//        //TODO 登陆普信
//        Log.i(TAG,"断线重连------1.断线重连 begin");
//        isLogining = true;
//
//        WeChatHttpRequest weChatHttpRequest = WeChatHttpRequest.getInstance(getApplicationContext());
//        weChatHttpRequest.addLoginListener(new WeChatHttpRequest.LoginListener() {
//
//            @Override
//            public void onLoginSuccess() {
//                isLogining = false;
//                Log.i(TAG, "断线重连-----2.登录成功");
//            }
//
//            @Override
//            public void onLoginFail(String errorMsg) {
//                isLogining = false;
//                Log.e(TAG, "断线重连-----2.登录失败 :" + errorMsg);
//                if (NetworkStateService.getNetState() != NetworkStateService.NET_UNREACHABLE && !JFSocketHandler.isChannelActive) {//只要有网，并且没连上，就一直连
//                    new Thread(){
//                        @Override
//                        public void run() {
//                            super.run();
//                            try {
//                                Thread.sleep(2*1000);
//                                httpLogin(userId, password);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }.run();
//                }
//            }
//        });
//        weChatHttpRequest.httpLogin(userId,password);
//    }
//}
