package com.efounder.chat.notify;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import com.efounder.chat.R;
import com.efounder.chat.utils.ImNotificationUtil;

import static com.efounder.chat.utils.ImNotificationUtil.CHANNEL_ID_CONTACTS;


public class MessageNotificationHelper {

    private static final String NOTIFICATION_TAG = "MessageManager";

    public static void notify(final Context context,
                              final String title,final String content) {
        final Resources res = context.getResources();
        final Bitmap picture = BitmapFactory.decodeResource(res, R.drawable.wechat_icon_launcher);
        ImNotificationUtil.createNotificationChannel(context);

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID_CONTACTS)

                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.wechat_icon_launcher)
                .setContentTitle(title)
                .setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setLargeIcon(picture)
                .setTicker(res.getString(R.string.im_new_message_tip))
               // .setNumber(number)
//                .setContentIntent(
//                        PendingIntent.getActivity(
//                                context,
//                                0,
//                                new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.baidu.com")),
//                                PendingIntent.FLAG_UPDATE_CURRENT))

                // Show expanded text content on devices running Android 4.1 or
                // later.
//                .setStyle(new NotificationCompat.BigTextStyle()
//                        .bigText(text)
//                        .setBigContentTitle(title)
//                        .setSummaryText("Dummy summary text"))

//                .addAction(
//                        R.drawable.ic_action_stat_share,
//                        res.getString(R.string.action_share),
//                        PendingIntent.getActivity(
//                                context,
//                                0,
//                                Intent.createChooser(new Intent(Intent.ACTION_SEND)
//                                        .setType("text/plain")
//                                        .putExtra(Intent.EXTRA_TEXT, "Dummy text"), "Dummy title"),
//                                PendingIntent.FLAG_UPDATE_CURRENT))

                .setAutoCancel(true);

        notify(context, builder.build());
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private static void notify(final Context context, final Notification notification) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            nm.notify(NOTIFICATION_TAG, 0, notification);
        } else {
            nm.notify(NOTIFICATION_TAG.hashCode(), notification);
        }
    }

    /**
     * Cancels any notifications of this type previously shown using
     * {@link #(Context, String, int)}.
     */
    @TargetApi(Build.VERSION_CODES.ECLAIR)
    public static void cancel(final Context context) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            nm.cancel(NOTIFICATION_TAG, 0);
        } else {
            nm.cancel(NOTIFICATION_TAG.hashCode());
        }
    }
}
