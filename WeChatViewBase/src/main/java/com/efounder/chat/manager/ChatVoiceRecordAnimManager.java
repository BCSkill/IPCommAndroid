package com.efounder.chat.manager;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.efounder.chat.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by XinQing on 2016/12/22.
 */

public class ChatVoiceRecordAnimManager {

    /**
     * 麦克风动画资源文件,用于录制语音时
     **/
    private static Drawable[] micImages;
    /**
     * 麦克风图片懒加载
     * @return
     */
    public static Drawable[] getMicImages(Context context) {
        if (micImages == null) {
            // 动画资源文件,用于录制语音时
            micImages = new Drawable[]{
                    context.getResources().getDrawable(R.drawable.record_animate_01),
                    context.getResources().getDrawable(R.drawable.record_animate_02),
                    context.getResources().getDrawable(R.drawable.record_animate_03),
                    context.getResources().getDrawable(R.drawable.record_animate_04),
                    context.getResources().getDrawable(R.drawable.record_animate_05),
                    context.getResources().getDrawable(R.drawable.record_animate_06),
                    context.getResources().getDrawable(R.drawable.record_animate_07),
                    context.getResources().getDrawable(R.drawable.record_animate_08),
                    context.getResources().getDrawable(R.drawable.record_animate_09),
                    context.getResources().getDrawable(R.drawable.record_animate_10),
                    context.getResources().getDrawable(R.drawable.record_animate_11),
                    context.getResources().getDrawable(R.drawable.record_animate_12),
                    context.getResources().getDrawable(R.drawable.record_animate_13),
                    context.getResources().getDrawable(R.drawable.record_animate_14),};
        }
        return micImages;
    }

    /**
     * 释放静态资源
     */
    public static void releaseRes(){
        micImages = null;
    }

    /**
     * 获取表情资源
     *
     * @param getSum
     * @return
     */
    public List<String> getExpressionRes(int getSum) {
        List<String> emoticonReslist = new ArrayList<String>();
        for (int x = 1; x <= getSum; x++) {
            String filename = "ee_" + x;
            emoticonReslist.add(filename);
        }
        return emoticonReslist;
    }

}
