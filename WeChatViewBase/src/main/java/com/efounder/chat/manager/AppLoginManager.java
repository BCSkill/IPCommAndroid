//package com.efounder.chat.manager;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.support.v7.app.AlertDialog;
//import android.text.TextUtils;
//
//import com.efounder.chat.R;
//import com.efounder.chat.model.Constant;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.util.AppContext;
//import com.efounder.util.EnvSupportManager;
//import com.efounder.util.PackgeFileCheckUtil;
//import com.efounder.utils.ResStringUtil;
//import com.utilcode.util.AppUtils;
//
//import java.io.File;
//
//import static com.efounder.frame.utils.Constants.CHAT_PASSWORD;
//import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
//
////判断app 是否已经登录
//public class AppLoginManager {
//
//    //判断用户是否已经登录
//    public static boolean isUserLogined() {
//
//        String chatUserID = EnvironmentVariable.getProperty(CHAT_USER_ID);
//        String chatPassword = EnvironmentVariable.getProperty(CHAT_PASSWORD);
//        String pingtaiPassword = EnvironmentVariable.getPassword();
//        String userNumber = EnvironmentVariable.getUserName();
//
//        String path = Constant.appSdcardLocation;
//        File resFile = new File(path);
//        //配置文件是否完整
//        boolean isFileFull = PackgeFileCheckUtil.checkFileIsFull(AppContext.getInstance());
//        //如果没有基本的配置文件
//        boolean isNeedUpdateRES = resFile.exists();
//
//
//        if (!isFileFull || !isNeedUpdateRES) {
//            return false;
//        }
//        if (EnvSupportManager.isSupportIM()) {
//            if (TextUtils.isEmpty(chatUserID) || TextUtils.isEmpty(chatPassword)
//                    || TextUtils.isEmpty(pingtaiPassword)
//                    || TextUtils.isEmpty(userNumber)) {
//                return false;
//
//            }
//        } else {
//            if (TextUtils.isEmpty(pingtaiPassword) || TextUtils.isEmpty(userNumber)) {
//                return false;
//
//            }
//        }
//        return true;
//    }
//
//    //重启app
//    public static void relaunchApp() {
//        AppUtils.relaunchApp();
//    }
//
//    public static void showNotLogInDialog(final Context mcontext) {
//        AlertDialog alertDialog = new AlertDialog.Builder(mcontext)
//                .setTitle(R.string.common_text_hint)
//                .setCancelable(false)
//                .setMessage(ResStringUtil.getString(R.string.wechatview_not_login_please))
//                .setPositiveButton(ResStringUtil.getString(R.string.chat_go_login), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        relaunchApp();
//                    }
//                })
//                .setNegativeButton(R.string.common_text_cancel, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        ((Activity) mcontext).finish();
//                        System.exit(0);
//                    }
//                }).create();
//        alertDialog.show();
//    }
//}
