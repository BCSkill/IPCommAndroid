package com.efounder.chat.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.core.xml.StubObject;
import com.efounder.chat.R;
import com.efounder.chat.activity.RecorderActivity;
import com.efounder.chat.model.ChatMenuModel;
import com.efounder.pansoft.chat.input.ChatInputView;
import com.efounder.util.AbFragmentManager;

import java.util.Hashtable;

/**
 * 聊天界面页面跳转管理
 *
 * @author YQS
 */
public class ChatInterfaceSkipManager {
    private Context mContext;
    //当前聊天的人的id
    private int currentChatUserId;
    //当前聊天类型
    private byte chatType;
    private AbFragmentManager abFragmentManager;


    public ChatInterfaceSkipManager(Context mContext) {
        this.mContext = mContext;
        abFragmentManager = new AbFragmentManager(mContext);
    }

    public ChatInterfaceSkipManager setCurrentChatUserId(int currentChatUserId) {
        this.currentChatUserId = currentChatUserId;
        return this;
    }

    public ChatInterfaceSkipManager setChatType(byte chatType) {
        this.chatType = chatType;
        return this;
    }

    public void onChatMenuClick(ChatMenuModel model) {
        int type = model.getType();
        //跳转activity是否需要startactivityforresult
        boolean forResult = false;
        if (type == ChatMenuModel.VEDIO) {
            // 点击摄像图标
            Intent intent = new Intent(mContext, RecorderActivity.class);
            ((Activity) mContext).startActivityForResult(intent, ChatInputView.REQUEST_CODE_SELECT_VIDEO);
        } else if (type == ChatMenuModel.COMMON) {//通用页面item
            try {
                //todo 处理弹出动画
                StubObject stubObject = model.getStubObject();
                Hashtable<String, String> hashtable = stubObject.getStubTable();
                //默认的进入activity或者fragment的动画
                int inTransition = R.anim.slide_in_from_right;
                int outTransition = R.anim.slide_out_to_left;
                if (hashtable.containsKey("tcms")) {
                    String tcms = hashtable.get("tcms");
                    if (!tcms.equals("") && tcms.equals("present")) {
                        //由下向上弹出
                        inTransition = R.anim.push_bottom_in;
                        outTransition = R.anim.push_top_out;
                    }
                }
                if (hashtable.containsKey("forResult")) {
                    String result = hashtable.get("forResult");
                    if (!"".equals(result) && result.equals("true")) {
                        forResult = true;
                    }
                }
                hashtable.put("toUserId", String.valueOf(currentChatUserId));
                hashtable.put("chatType", String.valueOf(chatType));
                stubObject.setStubTable(hashtable);

                //todo 跳转页面
                Bundle bundle = new Bundle();
                bundle.putSerializable("stubObject", stubObject);
                if (forResult){
                    abFragmentManager.startActivityForResult(model.getStubObject(), inTransition, outTransition);
                }else{
                    abFragmentManager.startActivity(model.getStubObject(), inTransition, outTransition);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (type == ChatMenuModel.FILE) {
            //选择文件
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            ((Activity) mContext).startActivityForResult(intent, ChatInputView.REQUEST_CODE_FILE);
        }
    }
}
