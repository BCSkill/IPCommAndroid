//package com.efounder.chat.manager;
//
//import com.efounder.chat.db.GetDBHelper;
//import com.efounder.chat.db.GroupDBManager;
//import com.efounder.chat.db.UserDBManager;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.message.db.MessageDBHelper;
//import com.efounder.mobilecomps.contacts.User;
//
//import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
//
///**
// * Created by XinQing on 2016/12/29.
// */
//
//public class ChatListAdapterManager {
//    private GroupDBManager groupDBManager;
//    private UserDBManager userDBManager;
//
//    public ChatListAdapterManager(){
//        super();
//        int loginUserId = Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)).intValue();
//        MessageDBHelper helper = GetDBHelper.getInstance().getDBHelper();
//        groupDBManager = new GroupDBManager(helper,loginUserId);
//        userDBManager = new UserDBManager(helper,loginUserId);
//    }
//
//    public User getGroupUser(int groupId, int userId){
//        return groupDBManager.queryGroupUser(groupId,userId);
//    }
//
//    public User getUser(){
//
//        return null;
//    }
//
//}
