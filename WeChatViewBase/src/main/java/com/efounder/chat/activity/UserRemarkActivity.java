
package com.efounder.chat.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.http.GetHttpUtil;
import com.efounder.chat.utils.GroupNameUtil;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.util.ToastUtil;
import com.efounder.utils.ResStringUtil;


/**
 * 更改好友备注名称
 */
public class UserRemarkActivity extends BaseActivity {
    private User user;
    private EditText et_usernick;
    private TextView save;
    private int userId;
    private int groupId;
    private String type = "remark";
    private String newRemark;//新备注
    private String groupRemark;//群组备注


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userId = getIntent().getIntExtra("id", 1);
        if (getIntent().hasExtra("groupId")) {
            groupId = getIntent().getIntExtra("groupId", 1);
            type = "groupRemark";
        }

        setContentView(R.layout.activity_userremark);
        et_usernick = (EditText) findViewById(R.id.et_usernick);

        if (type.equals("groupRemark")) {
            user = GroupNameUtil.getGroupUser(groupId, userId);
            groupRemark = GroupNameUtil.getGroupUserName(user);
            et_usernick.setText(groupRemark);
            if (groupRemark != null) {
                et_usernick.setSelection(groupRemark.length());
            }

        } else {
            user = WeChatDBManager.getInstance().getOneFriendById(userId);
            et_usernick.setText(user.getReMark());
            et_usernick.setSelection(user.getReMark().length());
        }

        save = (TextView) findViewById(R.id.tv_save);
        save.setVisibility(View.VISIBLE);
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(R.string.wrchatview_remarks);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newRemark = et_usernick.getText().toString().trim();

//                if ( newRemark.equals("") || newRemark
//                        .equals("0")) {
//                    et_usernick.setError("备注名不能为空");
//                   // ToastUtil.showToast(UserRemarkActivity.this,"备注名不能为空");
//                    return;
//                }
                if (type.equals("groupRemark")) {//更改群组联系人备注
                    updateGroupUserRemark(newRemark);
                } else {
                    updateRemark(newRemark);//更改好友备注
                }

            }
        });


    }

    private void updateGroupUserRemark(final String newRemark) {

        showLoading(ResStringUtil.getString(R.string.chat_updateing));
        GetHttpUtil.updateOtherGroupUserRemark(this, groupId, userId, newRemark, new GetHttpUtil
                .UpdateUserInfoCallBack() {


            @Override
            public void updateSuccess(boolean isSuccess) {
                if (isSuccess) {
                    dismissLoading();
                    user.setOtherGroupRemark(newRemark);
                    User tempUser = WeChatDBManager.getInstance().getGroupUserInfo(groupId, userId);
                    tempUser.setOtherGroupRemark(newRemark);
                    WeChatDBManager.getInstance().insertOrUpdateMyGroupUser(groupId, tempUser);
                    ToastUtil.showToast(UserRemarkActivity.this, ResStringUtil.getString(R.string.wrchatview_modify_group_success));
                    UserRemarkActivity.this.finish();
                } else {
                    dismissLoading();
                    ToastUtil.showToast(UserRemarkActivity.this, ResStringUtil.getString(R.string.wrchatview_modify_group_fail));
                }
            }
        });
    }


    //更新备注
    private void updateRemark(final String newRemark) {

        showLoading(ResStringUtil.getString(R.string.chat_updateing));
        GetHttpUtil.updateFriendRemark(this, userId, newRemark, new GetHttpUtil
                .UpdateUserInfoCallBack() {


            @Override
            public void updateSuccess(boolean isSuccess) {
                if (isSuccess) {
                    dismissLoading();
                    user.setReMark(newRemark);
                    WeChatDBManager.getInstance().insertOrUpdateUser(user);
                    updateChatList(newRemark); //通知聊天列表刷新
                    ToastUtil.showToast(UserRemarkActivity.this, ResStringUtil.getString(R.string.wrchatview_modify_remark_success));
                    UserRemarkActivity.this.finish();
                } else {
                    dismissLoading();
                    ToastUtil.showToast(UserRemarkActivity.this, ResStringUtil.getString(R.string.wrchatview_modify_remark_fail));
                }
            }
        });
    }

    private void updateChatList(String newRemark) {
        //发送广播
//        Intent intnet = new Intent();
//        intnet.putExtra("id", userId);
//        intnet.putExtra("chatType", StructFactory.TO_USER_TYPE_PERSONAL);
//        intnet.putExtra("userType", user.getType());
//        intnet.putExtra("name", newRemark);
//        intnet.setAction(ChatListFragment.UPDATEBADGNUM);
//        this.sendBroadcast(intnet);
    }
}
