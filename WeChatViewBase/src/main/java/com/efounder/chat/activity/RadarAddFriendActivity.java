package com.efounder.chat.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.widget.RoundImageView;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.mobilecomps.contacts.User;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * Created by Tian Yu on 2016/9/8.
 */

public class RadarAddFriendActivity extends Activity {
    RelativeLayout re_radaraddfriend;
//    private ImageLoader imageLoader;
//    private DisplayImageOptions options;
    float density;

    int x=-60;
    int y=35;

    int MaxX;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //隐藏状态栏
        //定义全屏参数
        int flag= WindowManager.LayoutParams.FLAG_FULLSCREEN;
        //获得当前窗体对象
        Window window=RadarAddFriendActivity.this.getWindow();
        //设置当前窗体为全屏显示
        window.setFlags(flag, flag);
        setContentView(R.layout.activity_radaraddfriend);

        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        MaxX=display.getWidth();

        density = this.getResources().getDisplayMetrics().density;

        Button btn_back= (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadarAddFriendActivity.this.finish();
            }
        });

        RoundImageView my_icon= (RoundImageView) findViewById(R.id.my_icon);

        User user = WeChatDBManager.getInstance().getOneUserById(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        //设置头像
        String avatar=user.getAvatar();
        //initImageLoader();
        showUserAvatar(avatar,my_icon);

        re_radaraddfriend= (RelativeLayout) findViewById(R.id.re_radaraddfriend);

        //测试用
        my_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchfriends();
            }
        });

//        searchfriends();
    }

    private void searchfriends()
    {
        addfriendsicon("1");
    }

    private void addfriendsicon(String userid)
    {
        x+=80;
        if(x*(int)density>=MaxX){
            x=20;
            y+=80;
        }
        Toast.makeText(this,"MaxX="+MaxX+"x="+x*(int)density,Toast.LENGTH_LONG).show();
        RoundImageView friend_icon=new RoundImageView(this);
        friend_icon.setImageResource(R.drawable.default_useravatar);

        re_radaraddfriend.addView(friend_icon);

        ViewGroup.MarginLayoutParams margin=new ViewGroup.MarginLayoutParams(friend_icon.getLayoutParams());
        margin.width=50*(int)density;
        margin.height=50*(int)density;
        margin.setMargins(x,y, x+margin.width, y+margin.height);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(margin);
        friend_icon.setLayoutParams(layoutParams);

//        RoundImageView friend_icon=new RoundImageView(this);
//        friend_icon.setMaxWidth(dip2px(this,50));
//        friend_icon.setMaxHeight(dip2px(this,50));
//        User friend = weChatDBManager.getOneUserById(Integer.valueOf(userid));
//        String avatar=friend.getAvatar();
//        initImageLoader();
//        showUserAvatar(avatar,friend_icon);
    }


    /**
     * 显示用户头像
     */
    private void showUserAvatar(String url,RoundImageView view) {
//        if (url.contains("http")) {
//            imageLoader.displayImage(url,view, options);
//        } else {
//            imageLoader.displayImage("",view, options);
//        }
        LXGlideImageLoader.getInstance().showUserAvatar(this, view, url);

    }
//    /**
//     * 异步加载头像
//     */
//    public void initImageLoader() {
//        // 初始化异步加载图片的类
//        imageLoader = ImageLoader.getInstance();
//        // 设置异步加载图片的配置信息
//        options = ImageUtil.getImageLoaderOptions(R.drawable.default_useravatar);
//
//    }
}
