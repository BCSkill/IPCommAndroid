//package com.efounder.chat.activity;
//
//import android.net.Uri;
//import android.os.Bundle;
//import android.view.BaseView;
//import android.widget.Button;
//import android.widget.MediaController;
//import android.widget.VideoView;
//
//import com.efounder.chat.R;
//
///**
// * 这个用来播放本地视频
// * Created by will on 16-12-23.
// */
//public class MediaControllerTestActivity extends BaseActivity {
//
//
//    private VideoView vv_videoview;
//    private MediaController mController;
//    private Button btn_play;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.wechatview_activity_mediacontroller);
//
//        btn_play = (Button) findViewById(R.id.btn_play);
//        vv_videoview = (VideoView) findViewById(R.id.vv_videoview);
//        mController = new MediaController(this);
//
//        // Button响应事件，播放本地视频
//        btn_play.setOnClickListener(new BaseView.OnClickListener() {
//            @Override
//            public void onClick(BaseView view) {
//                // Uri videoUri = Uri.parse("http://www.androidbegin.com/tutorial/AndroidCommercial.3gp");
//                // vv_videoview.setVideoURI(videoUri);
////                File videoFile = new File("/sdcard/DCIM/Camera/test.mp4");
////                if (videoFile.exists()) {
////                    vv_videoview.setVideoPath(videoFile.getAbsolutePath());
//                vv_videoview.setVideoURI(Uri.parse("http://panserver.solarsource.cn:9692/panserver/files/a991acbb-3fa0-43ca-899c-e1fad8746de1/download"));
//                    vv_videoview.setMediaController(mController);
//                    vv_videoview.start();
//                    vv_videoview.requestFocus();
//              //  }
//            }
//        });
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//    }
//}
