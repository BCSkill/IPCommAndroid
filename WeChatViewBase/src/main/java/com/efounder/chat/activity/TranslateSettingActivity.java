package com.efounder.chat.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.adapter.MultiLanguageAdapter;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.language.LanguageType;
import com.efounder.frame.language.MultiLanguageModel;
import com.efounder.frame.language.MultiLanguageUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * @author : yqs
 * @desc : 翻译设置Activity
 * @version: 1.0
 */
public class TranslateSettingActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView recycler;
    private MultiLanguageAdapter multiLanguageAdapter;
    private List<MultiLanguageModel> multiLanguageModels = new ArrayList<>();

    public static void start(Context context) {
        Intent starter = new Intent(context, TranslateSettingActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wechat_activity_translate_setting);
        iniData();
        initView();
    }

    private void iniData() {
        List<MultiLanguageModel> list = MultiLanguageUtil.getInstance().getLanguageList();
        //不支持亚美尼亚语翻译所以排除掉
        Iterator iterator = list.iterator();
        int currentLanguageType = getCurrentTranlateLanguageType();
        while (iterator.hasNext()) {
            MultiLanguageModel item = (MultiLanguageModel) iterator.next();
            item.setSelect(false);
            if (currentLanguageType == item.getLanguageType()){
                item.setSelect(true);
            }
            if (item.getLanguageType() == LanguageType.LANGUAGE_HY) {
                iterator.remove();
            }
        }

        multiLanguageModels.addAll(list);

    }

    private void initView() {
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(R.string.tab_menu_translate);
        TextView tvSave = (TextView) findViewById(R.id.tv_save);
        tvSave.setVisibility(View.VISIBLE);
        tvSave.setOnClickListener(this);
        recycler = (RecyclerView) findViewById(R.id.recycler);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        multiLanguageAdapter = new MultiLanguageAdapter(this, multiLanguageModels);
        recycler.setAdapter(multiLanguageAdapter);
    }

    @Override
    public void onClick(View v) {
        //保存
        if (v.getId() == R.id.tv_save) {

            for (MultiLanguageModel multiLanguageModel : multiLanguageModels) {
                if (multiLanguageModel.isSelect()) {
                    setCurrentTranlateLanguage(multiLanguageModel.getLanguageType() + "");
                    finish();
                    break;
                }
            }
        }

    }


    /**
     * 获取当前设置的要翻译成的目标语言
     */
    public static String getCurrentTranlateToLanguage() {
        int languageType = getCurrentTranlateLanguageType();
        Locale locale = MultiLanguageUtil.getLocaleByLanguageType(languageType);
        String toLanguage = locale.getLanguage();
        // 多语言代码表 https://blog.csdn.net/qq_40395278/article/details/83269513
        //todo 注意 翻译 印度尼西亚语 in->id 才能用，不支持亚美尼亚语
        if ("in".equals(toLanguage)) {
            toLanguage = "id";
        } else if ("hy".equals(toLanguage)) {
            //直接翻译英语
            toLanguage = "en";
        }
        if (languageType == LanguageType.LANGUAGE_FOLLOW_SYSTEM && MultiLanguageUtil.getInstance().getLanguageType()
                == LanguageType.LANGUAGE_HY) {
            //跟随系统并且当前语言是亚美尼亚语 直接翻译英语
            toLanguage = "en";
        }

        return toLanguage;
    }

    /**
     * 设置要翻译的目标语言
     *
     * @param languageType
     */
    public static void setCurrentTranlateLanguage(String languageType) {
        EnvironmentVariable.setProperty("translateLanguageType", languageType);
    }

    /**
     * 设置要翻译的目标语言
     */
    public static int getCurrentTranlateLanguageType() {
        return Integer.valueOf(EnvironmentVariable.getProperty("translateLanguageType", "0"));
    }
}
