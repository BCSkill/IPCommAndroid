package com.efounder.chat.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.efounder.chat.R;
import com.efounder.chat.adapter.PoiAdapter;
import com.efounder.chat.http.JFCommonRequestManager;
import com.efounder.chat.model.PoiLocation;
import com.efounder.chat.utils.GroupLocationHttpRequest;
import com.efounder.chat.utils.ImageUtil;
import com.efounder.frame.language.MultiLanguageUtil;
import com.efounder.util.LoadingDataUtilBlack;
import com.efounder.util.ToastUtil;
import com.efounder.utils.ResStringUtil;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.pansoft.library.CloudDiskBasicOperation;
import com.utilcode.util.NetworkUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.util.TypedValue.COMPLEX_UNIT_SP;
import static com.efounder.chat.activity.GaoDeLocationActivity.LOCATION_RESULT_CODE;

/**
 * An activity that displays a map showing the place at the device's current location.
 * @author will
 */
public class MapsActivityCurrentPlace extends BaseActivity
        implements OnMapReadyCallback, AdapterView.OnItemClickListener, GoogleMap.OnMyLocationButtonClickListener, View.OnClickListener {

    private static final String TAG = MapsActivityCurrentPlace.class.getSimpleName();
    private String GOOGLE_PLACES_API = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";
    private GoogleMap mMap;
    private CameraPosition mCameraPosition;
    private ListView listView;
    private PoiAdapter adapter;
    private ArrayList<PoiLocation> poiList;//周边建筑物列表
    private boolean isSelectList;
    private String address;//具体地址
    private String title;//位置
    private double latitude = -1;//选中条目的纬度
    private double longitude = -1;//选中条目的经度
    private int PLACE_PICKER_REQUEST = 1;
    private int GPS_REQUEST_CODE = 10;
    private SupportMapFragment mapFragment;

    private int groupId;

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    // A default location (Sydney, Australia) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(-1, -1);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    // Used for selecting the current place.
    private static final int M_MAX_ENTRIES = 5;
    private String[] mLikelyPlaceNames;
    private String[] mLikelyPlaceAddresses;
    private String[] mLikelyPlaceAttributions;
    private LatLng[] mLikelyPlaceLatLngs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Locale locale = MultiLanguageUtil.getSetLanguageLocale(this);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }

        Intent intent = getIntent();
        groupId = intent.getIntExtra("groupid", -1);

        // Retrieve the content view that renders the map.
        setContentView(R.layout.wechat_activity_google_maps);

        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        initView();
        poiList = new ArrayList<>();
        adapter = new PoiAdapter(this, poiList);

        mapFragment.getMapAsync(this);

    }

    private void initView() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        listView = (ListView) findViewById(R.id.lv_poi);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        ImageView backBtn = (ImageView) findViewById(R.id.iv_back);
        TextView title = (TextView) findViewById(R.id.tv_title);
        TextView right = (TextView) findViewById(R.id.tv_save);
        right.setVisibility(View.VISIBLE);
        title.setText(R.string.chat_location);
        right.setText(R.string.common_text_confirm);
        right.setTextSize(COMPLEX_UNIT_SP, 16);
        backBtn.setOnClickListener(this);
        right.setOnClickListener(this);
    }

    /**
     * Saves the state of the map when the activity is paused.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    /**
     * Manipulates the map when it's available.
     * This callback is triggered when the map is ready to be used.
     */
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        // Use a custom info window adapter to handle multiple lines of text in the
        // info window contents.
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.custom_info_contents,
                        (FrameLayout) findViewById(R.id.map), false);

                TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                title.setText(marker.getTitle());

                TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                snippet.setText(marker.getSnippet());

                return infoWindow;
            }
        });

        // Prompt the user for permission.
        getLocationPermission();

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();
    }

    private void startAutocompleteActivity() {
        List<Place.Field> placeFields = new ArrayList<>(Arrays.asList(Place.Field.values()));
        List<TypeFilter> typeFilters = new ArrayList<>(Arrays.asList(TypeFilter.values()));
// Create a RectangularBounds object.
        RectangularBounds bounds = RectangularBounds.newInstance(
                new LatLng(-33.880490, 151.184363),
                new LatLng(-33.858754, 151.229596));
        Intent autocompleteIntent =
                new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .setLocationBias(bounds)
                        .setTypeFilter(typeFilters.get(0))
                        .build(this);
        startActivityForResult(autocompleteIntent, 1001);
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            if (mLastKnownLocation != null) {
                                latitude = mLastKnownLocation.getLatitude();
                                longitude = mLastKnownLocation.getLongitude();
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                        new LatLng(mLastKnownLocation.getLatitude(),
                                                mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                            }
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                        showCurrentPlace();
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            if (!isLocationEnabled()) {
                openGPSSettings();
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    /**
     * fixme 这个方法有时间再改 wwl
     * Prompts the user to select the current place from a list of likely places, and shows the
     * current place on the map - provided the user has granted location permission.
     */
    private void showCurrentPlace() {
        if (mMap == null) {
            return;
        }

        if (mLocationPermissionGranted) {
            // Get the likely places - that is, the businesses and other points of interest that
            // are the best match for the device's current location.
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("key", getString(R.string.google_maps_key));
            hashMap.put("location", latitude + "," + longitude);
            hashMap.put("radius", "100");
            JFCommonRequestManager.getInstance(this).requestGetByAsyn(TAG, GOOGLE_PLACES_API, hashMap,
                    new JFCommonRequestManager.ReqCallBack<String>() {
                        @Override
                        public void onReqSuccess(String result) {
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                if ("OK".equals(jsonObject.optString("status"))) {
                                    JSONArray jsonArray = jsonObject.optJSONArray("results");
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onReqFailed(String errorMsg) {

                        }
                    });
//            @SuppressWarnings("MissingPermission") final Task<PlaceLikelihoodBufferResponse> placeResult =
//                    mPlaceDetectionClient.getCurrentPlace(null);
//            placeResult.addOnCompleteListener
//                    (new OnCompleteListener<PlaceLikelihoodBufferResponse>() {
//                        @Override
//                        public void onComplete(@NonNull Task<PlaceLikelihoodBufferResponse> task) {
//                            if (task.isSuccessful() && task.getResult() != null) {
//                                PlaceLikelihoodBufferResponse likelyPlaces = task.getResult();
//
//                                // Set the count, handling cases where less than 5 entries are returned.
//                                int count;
//                                if (likelyPlaces.getCount() < M_MAX_ENTRIES) {
//                                    count = likelyPlaces.getCount();
//                                } else {
//                                    count = M_MAX_ENTRIES;
//                                }
//
//                                int i = 0;
//                                mLikelyPlaceNames = new String[count];
//                                mLikelyPlaceAddresses = new String[count];
//                                mLikelyPlaceAttributions = new String[count];
//                                mLikelyPlaceLatLngs = new LatLng[count];
//                                Map<Integer, Boolean> map = new HashMap<>();
//                                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
//                                    // Build a list of likely places to show the user.
//                                    mLikelyPlaceNames[i] = (String) placeLikelihood.getPlace().getName();
//                                    mLikelyPlaceAddresses[i] = (String) placeLikelihood.getPlace()
//                                            .getAddress();
//                                    mLikelyPlaceAttributions[i] = (String) placeLikelihood.getPlace()
//                                            .getAttributions();
//                                    mLikelyPlaceLatLngs[i] = placeLikelihood.getPlace().getLatLng();
//                                    PoiLocation pl = new PoiLocation();
//                                    map.put(i + 1, false);
//                                    pl.setTitle(mLikelyPlaceNames[i]);
//                                    pl.setAddress(mLikelyPlaceAddresses[i]);
//                                    pl.setLatitude(mLikelyPlaceLatLngs[i].latitude);
//                                    pl.setLongitude(mLikelyPlaceLatLngs[i].longitude);
//                                    pl.setStateMap(map);
//                                    poiList.add(pl);
//
//                                    i++;
//                                    if (i > (count - 1)) {
//                                        break;
//                                    }
//                                }
//                                if (poiList.size() > 0) {
//                                    title = poiList.get(0).getTitle();
//                                    address = poiList.get(0).getAddress();
//                                    latitude = poiList.get(0).getLatitude();
//                                    longitude = poiList.get(0).getLongitude();
//                                }
//                                adapter.notifyDataSetChanged();
//
//                                // Release the place likelihood buffer, to avoid memory leaks.
//                                likelyPlaces.release();
//
//                            } else {
//                                Log.e(TAG, "Exception: %s", task.getException());
//                            }
//                        }
//                    });
        } else {
            // The user has not granted permission.
            Log.i(TAG, "The user did not grant location permission.");

            // Add a default marker, because the user hasn't selected a place.
            mMap.addMarker(new MarkerOptions()
                    .title(getString(R.string.default_info_title))
                    .position(mDefaultLocation)
                    .snippet(getString(R.string.default_info_snippet)));

            // Prompt the user for permission.
            getLocationPermission();
        }
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * capture screen, save photo , send msg and finish
     */
    public void captureScreen() {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                if (!NetworkUtils.isConnected()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MapsActivityCurrentPlace.this);
                    alertDialogBuilder.setTitle(R.string.wrchatview_prompt).setMessage(ResStringUtil.getString(R.string.network_isnot_available)).setPositiveButton(ResStringUtil.getString(R.string.common_text_confirm), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setNegativeButton(ResStringUtil.getString(R.string.common_text_cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                    return;
                }
                if (null == snapshot) {
                    ToastUtil.showToast(getApplicationContext(), R.string.chat_get_location_fail_retry);
                    return;
                }
                String filePath = null;
                String scale = null;
                try {
                    String path = ImageUtil.chatpath + ".mapImage";
                    File file = new File(path);
                    if (!file.exists()) {
                        file.mkdirs();
                    }

                    filePath = path + File.separator + sdf.format(new Date()) + ".png";
                    FileOutputStream fos = new FileOutputStream(filePath);
                    boolean b = snapshot.compress(Bitmap.CompressFormat.PNG, 70, fos);
                    try {
                        fos.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    StringBuffer buffer = new StringBuffer();
                    if (b)
                        buffer.append(ResStringUtil.getString(R.string.chat_screen_success));
                    else {
                        buffer.append(ResStringUtil.getString(R.string.chat_screen_fail));
                    }


                    scale = ImageUtil.getPicScale(filePath);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("latitude", String.valueOf(latitude));
                    jsonObject.put("longitude", String.valueOf(longitude));
                    jsonObject.put("scale", scale);
                    jsonObject.put("name", title);
                    jsonObject.put("specific", address);
                    Intent data = new Intent();
                    Bundle bundle = new Bundle();

                    bundle.putString("imagePath", filePath);
                    bundle.putString("content", jsonObject.toString());

                    data.putExtras(bundle);
                    setResult(LOCATION_RESULT_CODE, data);
                    finish();
                    overridePendingTransition(R.anim.push_top_in, R.anim.push_bottom_out);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        mMap.snapshot(callback);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GPS_REQUEST_CODE) {
            //做需要做的事情，比如再次检测是否打开GPS了 或者定位
            if (isLocationEnabled()) {
                mapFragment.getMapAsync(this);
            } else {
                openGPSSettings();
            }
        }

        if (requestCode == 1001) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                title = place.getName().toString();
                address = place.getAddress().toString();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                poiList.clear();
                PoiLocation poiLocation = new PoiLocation();
                poiLocation.setAddress(address);
                poiLocation.setTitle(title);
                poiLocation.setLatitude(latitude);
                poiLocation.setLongitude(longitude);
                poiList.add(poiLocation);
                adapter.notifyDataSetChanged();

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(),
                        DEFAULT_ZOOM));
                mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .title(title)
                        .position(place.getLatLng())
                        .snippet(""));
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    /**
     * set group address
     */
    private void setGroupAddress() {
        LoadingDataUtilBlack.show(this);
        Intent data = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("address", address);
        data.putExtras(bundle);
        setResult(RESULT_OK, data);
//            向后台传经纬度
//            new CloudDiskBasicOperation().cloudDiskGetNextLevelPanFileByFileId();
        GroupLocationHttpRequest request = new GroupLocationHttpRequest();
        request.addGroupPosition("" + groupId, "" + longitude, "" + latitude, address, title, new CloudDiskBasicOperation.ReturnBack() {
            @Override
            public void onReturnBack(String result) throws JSONException {
                Log.e("addGroupPosition", result + ",currentThread=" + Thread.currentThread().getName());
                JSONObject object = new JSONObject(result);
                String result1 = object.getString("result");
                String info = "";
                if ("success".equals(result1)) {
                    info = ResStringUtil.getString(R.string.wrchatview_add_group_address_suc);
//                        Toast.makeText(ChatGroupAddressActivity.this, info, Toast.LENGTH_LONG).show();
                } else {
                    String msg = object.getString("msg");
                    if (null != msg) {
                        if ("the position has exist!".equals(msg)) {
                            info = ResStringUtil.getString(R.string.wrchatview_group_address_already);
//                                Toast.makeText(ChatGroupAddressActivity.this, info, Toast.LENGTH_LONG).show();
                        } else {
                            info = ResStringUtil.getString(R.string.wrchatview_add_group_address_fail);
//                                Toast.makeText(ChatGroupAddressActivity.this, info, Toast.LENGTH_LONG).show();
                        }
                    }
                }
                Toast.makeText(MapsActivityCurrentPlace.this, info, Toast.LENGTH_LONG).show();
                LoadingDataUtilBlack.dismiss();
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.iv_back) {
//            取消
            setResult(LOCATION_RESULT_CODE, null);
            finish();
            overridePendingTransition(R.anim.push_top_in, R.anim.push_bottom_out);

        } else if (i == R.id.tv_save) {
            //确定--返回地址
            if (groupId == -1) {
                captureScreen();
            } else {
                setGroupAddress();
            }

        } else if (i == R.id.ll_chat_group_search) {
            //搜索
            try {
                startAutocompleteActivity();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //当前未选中时
        if (adapter.index != position - 1) {
            adapter.index = position - 1;
            isSelectList = true;
            adapter.notifyDataSetChanged();
//            更新定位图标位置
            double latitude = poiList.get(position - 1).getLatitude();
            double longitude = poiList.get(position - 1).getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            // Add a marker for the selected place, with an info window
            // showing information about that place.
            String markerSnippet = mLikelyPlaceAddresses[position - 1];
            if (mLikelyPlaceAttributions[position - 1] != null) {
                markerSnippet = markerSnippet + "\n" + mLikelyPlaceAttributions[position - 1];
            }
            mMap.clear();
            mMap.addMarker(new MarkerOptions()
                    .title(poiList.get(position - 1).getTitle())
                    .position(latLng)
                    .snippet(markerSnippet));

            // Position the map's camera at the location of the marker.
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,
                    DEFAULT_ZOOM));
        }
        title = poiList.get(position - 1).getTitle();
        address = poiList.get(position - 1).getAddress();
        latitude = poiList.get(position - 1).getLatitude();
        longitude = poiList.get(position - 1).getLongitude();
    }


    /**
     * 判断定位服务是否开启
     *
     * @param
     * @return true 表示开启
     */
    public boolean isLocationEnabled() {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    /**
     * 检测GPS是否打开
     *
     * @return
     */
    private boolean checkGPSIsOpen() {
        boolean isOpen;
        LocationManager locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);
        isOpen = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return isOpen;
    }

    /**
     * 跳转GPS设置
     */
    private void openGPSSettings() {
        //没有打开则弹出对话框
        new AlertDialog.Builder(this)
                .setTitle(R.string.wrchatview_prompt)
                .setMessage(R.string.gpsNotifyMsg)
                // 拒绝, 退出应用
                .setNegativeButton(R.string.common_text_cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })

                .setPositiveButton(R.string.setting,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //跳转GPS设置界面
                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivityForResult(intent, GPS_REQUEST_CODE);
                            }
                        })

                .setCancelable(false)
                .show();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        if (!isLocationEnabled()) {
            openGPSSettings();
        }
        return false;
    }
}
