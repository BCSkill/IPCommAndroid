package com.efounder.chat.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.adapter.MultiLanguageAdapter;
import com.efounder.chat.http.GetHttpUtil;
import com.efounder.chat.http.JFCommonRequestManager;
import com.efounder.chat.http.WeChatHttpLoginManager;
import com.efounder.chat.service.MessageService;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.language.MultiLanguageModel;
import com.efounder.frame.language.MultiLanguageUtil;
import com.utilcode.util.ReflectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.efounder.frame.utils.Constants.CHAT_PASSWORD;
import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * @author : zzj
 * @e-mail : zhangzhijun@pansoft.com
 * @date : 2018/12/21 13:55
 * @desc : 多语言Activity
 * @version: 1.0
 */
public class MultiLanguageActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView recycler;
    private MultiLanguageAdapter multiLanguageAdapter;
    private List<MultiLanguageModel> multiLanguageModels = new ArrayList<>();

    public static void start(Context context) {
        Intent starter = new Intent(context, MultiLanguageActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_language);
        iniData();
        initView();
    }

    private void iniData() {

        multiLanguageModels.addAll(MultiLanguageUtil.getInstance().getLanguageList());

    }

    private void initView() {
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(R.string.wechat_multi_language_list_title);
        TextView tvSave = (TextView) findViewById(R.id.tv_save);
        tvSave.setVisibility(View.VISIBLE);
        tvSave.setOnClickListener(this);
        recycler = (RecyclerView) findViewById(R.id.recycler);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        multiLanguageAdapter = new MultiLanguageAdapter(this, multiLanguageModels);
        recycler.setAdapter(multiLanguageAdapter);
    }

    @Override
    public void onClick(View v) {
        //保存
        if (v.getId() == R.id.tv_save) {

            for (MultiLanguageModel multiLanguageModel : multiLanguageModels) {
                if (multiLanguageModel.isSelect()) {
                    MultiLanguageUtil.getInstance().updateLanguage(MultiLanguageActivity.this, multiLanguageModel.getLanguageType());
                }
            }

            //通知服务器语言变化
            notifyServer();
            //"com.pansoft.openplanet.activity.TabBottomActivityForTalkChain"
            try {
                String className = getResources().getString(R.string.from_group_backto_first);
                ReflectUtils reflectUtils = ReflectUtils.reflect(className);
                reflectUtils.method("start", this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 通知服务器本地语言发生变化
     */
    private void notifyServer() {
        // userId passWord  deviceId subAppId  locale
        HashMap<String,String> paramMap = new HashMap<>();
        paramMap.put("userId", EnvironmentVariable.getProperty(CHAT_USER_ID));
        paramMap.put("passWord", EnvironmentVariable.getProperty(CHAT_PASSWORD));
        paramMap.put("deviceId", MessageService.getDeviceId(this.getApplicationContext()));
        paramMap.put("locale", WeChatHttpLoginManager.getSystemLocale());
        paramMap.put("subAppId", EnvironmentVariable.getProperty("subAppId", ""));

        String url =  GetHttpUtil.ROOTURL + "/IMServer/user/changeLocale";
        JFCommonRequestManager.getInstance(this).requestGetByAsyn("notify",url,paramMap, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {

            }

            @Override
            public void onReqFailed(String errorMsg) {

            }
        });
    }
}
