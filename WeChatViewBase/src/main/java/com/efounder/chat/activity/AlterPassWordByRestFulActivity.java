package com.efounder.chat.activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.efounder.chat.R;
import com.efounder.chat.http.GetHttpUtil;
import com.efounder.chat.http.RequestHttpDataUtil;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.utils.Constants;
import com.efounder.util.StringUtil;
import com.efounder.util.ToastUtil;
import com.efounder.utils.ResStringUtil;

public class AlterPassWordByRestFulActivity extends BaseActivity implements OnClickListener {

    private EditText et_oldPassword;
    private EditText et_password;//新密码
    private EditText et_password_corfirm;//确认新密码
    private Button btn_enter;
    private String registrID;//注册号


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_alterpassword);
        initView();
        int myId = Integer.valueOf(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID));
        alterPassword(myId);
        //  WeChatDBManager weChatDBManager = WeChatDBManager.getInstance();
        // User user = weChatDBManager.getOneUserById(myId);
        registrID = EnvironmentVariable.getUserName();

    }

    private void initView() {
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(ResStringUtil.getString(R.string.wrchatview_change_password));
        et_oldPassword = (EditText) findViewById(R.id.et_oldpassword);
        et_password = (EditText) findViewById(R.id.et_password);
        et_password_corfirm = (EditText) findViewById(R.id.et_password_corfirm);
        btn_enter = (Button) findViewById(R.id.btn_enter);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();


    }

    @Override
    public void back(View view) {

        finish();
    }

    private void alterPassword(final int userId) {


        btn_enter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final String pwdEtStr = et_oldPassword.getText().toString().trim();
                final String pwdNewEtStr = et_password.getText().toString().trim();
                final String pwdNewEt2Str = et_password_corfirm.getText().toString().trim();
                if (StringUtil.isEmpty(pwdEtStr)) {
                    et_oldPassword.setError(ResStringUtil.getString(R.string.wrchatview_start_password_not_empty));
                    return;
                }
                if (StringUtil.isEmpty(pwdNewEtStr)) {
                    et_password.setError(ResStringUtil.getString(R.string.wrchatview_new_password_not_empty));
                    return;
                }
                if (pwdNewEtStr.equals(pwdEtStr)) {
                    et_password.setError(ResStringUtil.getString(R.string.wrchatview_new_not_same_start));
                    return;
                }
                if (!pwdNewEtStr.equals(pwdNewEt2Str)) {
                    et_password_corfirm.setError(ResStringUtil.getString(R.string.wrchatview_not_new_same));
                    return;
                }


                showLoading(ResStringUtil.getString(R.string.common_text_please_wait));
                GetHttpUtil.alertPassWordByRestFul(AlterPassWordByRestFulActivity.this, registrID, pwdEtStr, pwdNewEtStr, new RequestHttpDataUtil.AlertPassWordByRestFulCallBack() {
                    @Override
                    public void signResponse(int responsenum, String response) {
                        dismissLoading();

                        // {"result":"success","user":{"imUserId":3275,"imUserPassWord":"123456","passWord":"PNcxc`x^9l`i41^9l^ifcc7i1`5z07","userId":"95598","userName":"åº·æ\u0095°æ\u008D®"}}
                        // {"result":"fail","msg":"用户已经存在"}
                        if (responsenum == 0) {

                            JSONObject jsonObject = JSON.parseObject(response);
                            String result = jsonObject.getString("result");
                            if ("success".equals(result)) {
                                EnvironmentVariable.setPassword(pwdNewEtStr);
                                ToastUtil.showToast(AlterPassWordByRestFulActivity.this, ResStringUtil.getString(R.string.wrchatview_change_succcess));
                                AlterPassWordByRestFulActivity.this.finish();

                            } else {
                                Toast.makeText(AlterPassWordByRestFulActivity.this, result, Toast
                                        .LENGTH_LONG).show();
                            }

                        } else if (responsenum == 1) {
                            //网络出错

                            Toast.makeText(AlterPassWordByRestFulActivity.this, ResStringUtil.getString(R.string.wrchatview_server_request_fail), Toast
                                    .LENGTH_LONG).show();
                        } else if (responsenum == 2) {
                            //未设置注册地址
                            Toast.makeText(AlterPassWordByRestFulActivity.this, ResStringUtil.getString(R.string.wrchatview_not_set_address), Toast
                                    .LENGTH_LONG).show();
                        }
                    }
                });

            }
        });
    }

    private void restartApplication() {
        final Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
