package com.efounder.chat.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.StringRes;

import com.efounder.chat.R;
import com.efounder.chat.http.GetHttpUtil;
import com.efounder.chat.http.JFCommonRequestManager;
import com.efounder.chat.http.OpenEthRequest;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.baseui.EFActivity;
import com.efounder.http.EFHttpRequest;
import com.efounder.utils.EasyPermissionUtils;
import com.utilcode.util.AppUtils;
import com.utilcode.util.LogUtils;

import static com.efounder.frame.utils.Constants.CHAT_PASSWORD;
import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

public class BaseActivity extends EFActivity {
    protected String TAG;
    private static final int notifiId = 11;
    protected NotificationManager notificationManager;
    protected boolean ischeckPermission = true;


    public void setCheckPermission(boolean ischeckPermission) {
        this.ischeckPermission = ischeckPermission;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (ischeckPermission && !EasyPermissionUtils.checkWriteAndPhonePermission(this)) {
            //没有存储权限，重启app
            LogUtils.e("没有存储权限，重启app");
            AppUtils.relaunchApp();
            return;
        }

        super.onCreate(savedInstanceState);
        TAG = getClass().getSimpleName();
        if (savedInstanceState != null) {
            //恢复static中 用户名和密码
            EnvironmentVariable.setProperty(CHAT_USER_ID, savedInstanceState.getString("userId"));
            EnvironmentVariable.setProperty(CHAT_PASSWORD, savedInstanceState.getString("password"));
        }
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            //要结束界面的时候取消网络请求
            GetHttpUtil.cannelRequest();
            EFHttpRequest.cancelRequest(TAG);
            OpenEthRequest.cannelRequest();
            JFCommonRequestManager.getInstance(this).cannelOkHttpRequest(TAG);
            EFHttpRequest.cancelRequest("VolleyRequest");
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        hideEFTitleView();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        hideEFTitleView();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //微信:持久化用户名，密码
        outState.putString("userId", EnvironmentVariable.getProperty(CHAT_USER_ID));
        outState.putString("password", EnvironmentVariable.getProperty(CHAT_PASSWORD));
        Log.i(TAG, "onSaveInstanceState------");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        try {
            super.onRestoreInstanceState(savedInstanceState);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //恢复static中 用户名和密码
        EnvironmentVariable.setProperty(CHAT_USER_ID, savedInstanceState.getString("userId"));
        EnvironmentVariable.setProperty(CHAT_PASSWORD, savedInstanceState.getString("password"));
        Log.i(TAG, "onRestoreInstanceState------");
    }

    /**
     * 返回
     *
     * @param view
     */
    public void back(View view) {
        finish();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressedSupport();
            overridePendingTransition(R.anim.slide_in_from_left,
                    R.anim.slide_out_to_right);
            return true;
        }
        return false;
    }

    public void showLoading(@StringRes int textId) {
        showLoading(getResources().getString(textId));
    }


}
