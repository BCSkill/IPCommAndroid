//package com.efounder.chat.activity;
//
//import android.annotation.SuppressLint;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.app.AlertDialog;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.View.OnTouchListener;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.easemob.util.NetUtils;
//import com.efounder.chat.R;
//import com.efounder.chat.db.WeChatDBManager;
//import com.efounder.chat.http.GetHttpUtil;
//import com.efounder.chat.http.GetHttpUtil.GroupHttpListener;
//import com.efounder.chat.model.ChatListItem;
//import com.efounder.chat.model.Group;
//import com.efounder.chat.model.MessageEvent;
//import com.efounder.chat.struct.StructFactory;
//import com.efounder.chat.utils.GroupNameUtil;
//import com.efounder.chat.utils.ImageUtil;
//import com.efounder.chat.widget.ExpandGridView;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.message.manager.JFMessageManager;
//import com.efounder.mobilecomps.contacts.User;
//import com.efounder.util.ToastUtil;
//import com.efounder.util.URLModifyDynamic;
//import com.nostra13.universalimageloader.core.DisplayImageOptions;
//import com.nostra13.universalimageloader.core.ImageLoader;
//
//import org.greenrobot.eventbus.EventBus;
//import org.json.JSONException;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//
//import static com.efounder.chat.R.id.btn_exit_grp;
//import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
//import static com.efounder.frame.utils.NetStateBroadcastReceiver.isNetActive;
//
//
//public class ChatRoomSettingActivity extends BaseActivity implements
//        OnClickListener, GroupHttpListener, GetHttpUtil.GetHttpUtilListener {
//    private TextView tv_groupname;
//    // 成员总数
//
//    private TextView tv_m_total;
//    // 成员总数
//    int m_total = 0;
//    // 成员列表
//    private ExpandGridView gridview;
//    // 修改群名称、置顶、、、、
//    private RelativeLayout re_change_groupname;
//    private RelativeLayout rl_switch_chattotop;
//    private RelativeLayout rl_switch_block_groupmsg;
//    private RelativeLayout re_clear;//清空聊天记录
//    private RelativeLayout re_myGroupName;
//    private TextView tv_myGroupName;
//
//    // 状态变化
//    private ImageView iv_switch_chattotop;
//    private ImageView iv_switch_unchattotop;
//    private ImageView iv_switch_block_groupmsg;
//    private ImageView iv_switch_unblock_groupmsg;
//    // 删除并退出
//
//    private Button exitBtn;
//
//    // 群名称
//    private String group_name;
//    // 修改后群名称
//    private String newName;
//    // 是否是管理员
//    boolean is_admin = false;
//    List<User> usersList = new ArrayList<User>();
//    String longClickUsername = null;
//
//    private int groupId;
//
//    private Group group;
//    private GridAdapter adapter;
//    private ImageLoader imageLoader;
//    private DisplayImageOptions options;
//
//    public static ChatRoomSettingActivity instance;
//    private ProgressDialog progressDialog;
//    private ProgressDialog deleteDialog;
//    private WeChatDBManager weChatDBManager;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.social_groupchatsetting_activity);
//        GetHttpUtil.setGroupHttpListener(this);
//        GetHttpUtil.setGetHttpUtilListener(this);
//        weChatDBManager = WeChatDBManager.getInstance();
//        instance = this;
//        groupId = getIntent().getIntExtra("id", 1);
//
//        initImageLoader();
//        initView();
//
//
//        // updateGroup();
//    }
//
//    private void initView() {
//
//        progressDialog = new ProgressDialog(ChatRoomSettingActivity.this);
//        tv_groupname = (TextView) findViewById(R.id.tv_groupname);
//        tv_m_total = (TextView) findViewById(R.id.tv_m_total);
//        gridview = (ExpandGridView) findViewById(R.id.gridview);
//        re_change_groupname = (RelativeLayout) findViewById(R.id.re_change_groupname);
//        rl_switch_chattotop = (RelativeLayout) findViewById(R.id.rl_switch_chattotop);
//        rl_switch_block_groupmsg = (RelativeLayout) findViewById(R.id.rl_switch_block_groupmsg);
//        re_clear = (RelativeLayout) findViewById(R.id.re_clear);//清空聊天记录
//        iv_switch_chattotop = (ImageView) findViewById(R.id.iv_switch_chattotop);
//        iv_switch_unchattotop = (ImageView) findViewById(R.id.iv_switch_unchattotop);
//        iv_switch_block_groupmsg = (ImageView) findViewById(R.id.iv_switch_block_groupmsg);
//        iv_switch_unblock_groupmsg = (ImageView) findViewById(R.id.iv_switch_unblock_groupmsg);
//        re_myGroupName = (RelativeLayout) findViewById(R.id.re_mygroupremark);
//        tv_myGroupName = (TextView) findViewById(R.id.tv_mygroupname);//我在本群中的昵称
//        exitBtn = (Button) findViewById(R.id.btn_exit_grp);
//        re_change_groupname.setOnClickListener(this);
//        rl_switch_chattotop.setOnClickListener(this);
//        rl_switch_block_groupmsg.setOnClickListener(this);
//        re_myGroupName.setOnClickListener(this);
//        re_clear.setOnClickListener(this);
//        exitBtn.setOnClickListener(this);
//
//
//    }
//
//    private void initData() {
//
//        tv_groupname.setText(group.getGroupName());//群组名
//
//        User user = GroupNameUtil.getGroupUser(groupId, Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)), weChatDBManager);
//        String myGroupNickName = GroupNameUtil.getGroupUserName(user);
//        tv_myGroupName.setText(myGroupNickName);
//        usersList = group.getUsers();
//        if (usersList == null) {
//            usersList = new ArrayList<User>();
//            Toast.makeText(ChatRoomSettingActivity.this, "获取群组联系人失败",
//                    Toast.LENGTH_LONG).show();
//
//        }
//        m_total = usersList.size();
//        tv_m_total.setText("(" + m_total + ")");
//        // 显示群组成员头像和昵称
//        showMembers(usersList);
//        // 判断是否是群主，是群主有删成员的权限，并显示减号按钮
//        if (group.getCreateId() == Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID))) {
//            is_admin = true;
//        }
//
//
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        group = weChatDBManager.getGroupWithUsers(groupId);
//        if (group.getUsers().size()==0) {
//            if (!isNetActive()) {
//                //ToastUtil.showToast(ChatActivity.this,"初始化数据失败！请检查网络状况");
//                initData();
//                return;
//            }
//            try {
//
//                GetHttpUtil.getGroupUsers(ChatRoomSettingActivity.this, group.getGroupId());
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        } else {
//            initData();
//        }
//
//        initTopAndBother();
//    }
//
//    /**
//     * 设置置顶和免打扰状态
//     */
//    private void initTopAndBother() {
//
//        // 初始化置顶和免打扰的状态
//        if (group.getIsBother() == false) {
//            iv_switch_block_groupmsg.setVisibility(View.INVISIBLE);
//            iv_switch_unblock_groupmsg.setVisibility(View.VISIBLE);
//
//        } else {
//
//            iv_switch_block_groupmsg.setVisibility(View.VISIBLE);
//            iv_switch_unblock_groupmsg.setVisibility(View.INVISIBLE);
//
//        }
//        if (group.getIsTop() == false) {
//            // 当前状态是未置顶
//            iv_switch_chattotop.setVisibility(View.INVISIBLE);
//            iv_switch_unchattotop.setVisibility(View.VISIBLE);
//        } else {
//            // 当前状态是置顶
//            iv_switch_chattotop.setVisibility(View.VISIBLE);
//            iv_switch_unchattotop.setVisibility(View.INVISIBLE);
//        }
//    }
//
//    // 显示群成员头像昵称的gridview
//    @SuppressLint("ClickableViewAccessibility")
//    private void showMembers(List<User> usersList) {
//        adapter = new GridAdapter(this, usersList);
//        gridview.setAdapter(adapter);
//
//        // 设置OnTouchListener,为了让群主方便地推出删除模》
//        gridview.setOnTouchListener(new OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        if (adapter.isInDeleteMode) {
//                            adapter.isInDeleteMode = false;
//                            adapter.notifyDataSetChanged();
//                            return true;
//                        }
//                        break;
//                    default:
//                        break;
//                }
//                return false;
//            }
//        });
//
//
//    }
//
//    @Override
//    public void onClick(View v) {
//        int id = v.getId();
//        // 屏蔽群组
//        if (id == R.id.rl_switch_block_groupmsg) {
//            if (iv_switch_block_groupmsg.getVisibility() == View.VISIBLE) {
//                iv_switch_block_groupmsg.setVisibility(View.INVISIBLE);
//                iv_switch_unblock_groupmsg.setVisibility(View.VISIBLE);
//
//                group.setIsBother(false);
//                weChatDBManager.insertOrUpdateGroup(group);
//            } else {
//                iv_switch_block_groupmsg.setVisibility(View.VISIBLE);
//                iv_switch_unblock_groupmsg.setVisibility(View.INVISIBLE);
//                group.setIsBother(true);
//                weChatDBManager.insertOrUpdateGroup(group);
//            }
//        } else if (id == R.id.re_clear) {
//            progressDialog.setMessage("正在清空群消息...");
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progressDialog.show();
//            clearGroupHistory();
//        } else if (id == R.id.re_change_groupname) {
//            showNameAlert();
//        } else if (id == btn_exit_grp && exitBtn != null) {
//            if (!isNetActive()) {
//                ToastUtil.showToast(ChatRoomSettingActivity.this, "请检查网络状况");
//                return;
//            }
//            deleteDialog = new ProgressDialog(ChatRoomSettingActivity.this);
//            deleteDialog.setMessage("正在退出...");
//            deleteDialog.setCanceledOnTouchOutside(false);
//            deleteDialog.show();
//            try {
//                GetHttpUtil.userQuitGroup(ChatRoomSettingActivity.this, group.getGroupId());
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//        } else if (id == R.id.rl_switch_chattotop) {
//            if (iv_switch_chattotop.getVisibility() == View.VISIBLE) {
//
//                iv_switch_chattotop.setVisibility(View.INVISIBLE);
//                iv_switch_unchattotop.setVisibility(View.VISIBLE);
//                group.setIsTop(false);
//                weChatDBManager.insertOrUpdateGroup(group);
//
//            } else {// 当前状态是未置顶点击后置顶
//                iv_switch_chattotop.setVisibility(View.VISIBLE);
//                iv_switch_unchattotop.setVisibility(View.INVISIBLE);
//                group.setIsTop(true);
//                weChatDBManager.insertOrUpdateGroup(group);
//            }
//        } else if (id == R.id.re_mygroupremark) {
//            showMyGroupRemarkAlert();
//        }
//
//    }
//
//    /**
//     * 清空群聊天记录
//     */
//    public void clearGroupHistory() {
//
//
//        progressDialog.dismiss();
//
//    }
//
//    private void showNameAlert() {
//        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//        LayoutInflater inflater = (LayoutInflater) this
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        RelativeLayout layout = (RelativeLayout) inflater.inflate(
//                R.layout.social_alertdialog, null);
//        dialog.setView(layout);
//        final EditText ed_name = (EditText) layout
//                .findViewById(R.id.ed_name);
//        dialog.setTitle("请输入新的群组名称");
//        dialog.setPositiveButton(R.string.common_text_confirm,
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        newName = ed_name.getText().toString().trim();
//                        if (TextUtils.isEmpty(newName)) {
//                            return;
//                        } else {
//                            try {
//                                GetHttpUtil.updateGroupName(ChatRoomSettingActivity.this, groupId, newName);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        dialog.cancel();
//                    }
//
//
//                });
//
//        dialog.setNegativeButton(R.string.common_text_cancel,
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//
//                });
//        dialog.show();
//
//    }
//
//    private void showMyGroupRemarkAlert() {
//
//        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//        LayoutInflater inflater = (LayoutInflater) this
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        LinearLayout layout = (LinearLayout) inflater.inflate(
//                R.layout.chatroomset_alertdialog, null);
//        dialog.setView(layout);
//        final EditText ed_name = (EditText) layout
//                .findViewById(R.id.ed_name);
//        dialog.setTitle("我在本群中的昵称");
//
//        dialog.setPositiveButton(R.string.common_text_confirm,
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        final String myGroupRemark = ed_name.getText().toString().trim();
//
//                        if (TextUtils.isEmpty(myGroupRemark)) {
//                            return;
//                        } else {
//                            try {
//                                GetHttpUtil.updateOwnGroupUserRemark(ChatRoomSettingActivity.this, groupId, myGroupRemark,
//                                        new GetHttpUtil.UpdateUserInfoCallBack() {
//                                            @Override
//                                            public void updateSuccess(boolean isSuccess) {
//                                                if (isSuccess) {
//                                                    User tempUser = weChatDBManager.getGroupUserInfo(groupId,
//                                                            Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
//                                                    tempUser.setOtherGroupRemark(myGroupRemark);
//                                                    weChatDBManager.insertOrUpdateMyGroupUser(groupId, tempUser);
//                                                    tv_myGroupName.setText(myGroupRemark);
//                                                    ToastUtil.showToast(ChatRoomSettingActivity.this, "修改个人群昵称成功");
//
//                                                } else {
//                                                    ToastUtil.showToast(ChatRoomSettingActivity.this, "修改个人群昵称失败");
//                                                }
//                                            }
//                                        });
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        dialog.cancel();
//                    }
//
//
//                });
//
//        dialog.setNegativeButton(R.string.common_text_cancel,
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//
//                });
//        dialog.show();
//
//
////        // 设置能弹出输入法
////        dlg.getWindow().clearFlags(
////                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
//
//    }
//
//    /**
//     * 管理员删除群成员
//     *
//     * @param id
//     */
//    protected void deleteMembersFromGroup(final int id) {
//        deleteDialog = new ProgressDialog(
//                ChatRoomSettingActivity.this);
//        // 当删除的是自己的时候,意味着就是退群。群主退群是要解散群的，所以要有判断
//        if (group.getCreateId() == id) {
//            deleteDialog.setMessage("正在退出...");
//            deleteDialog.setCanceledOnTouchOutside(false);
//            deleteDialog.show();
//
//            deleteDialog.dismiss();
//            Toast.makeText(ChatRoomSettingActivity.this, "退出成功",
//                    Toast.LENGTH_LONG).show();
//            setResult(100);
//            finish();
//        } else {
//            deleteDialog.setMessage("正在移除群成员...");
//            deleteDialog.setCanceledOnTouchOutside(false);
//            deleteDialog.show();
//            try {
//                GetHttpUtil.AdminDeleteGroupUser(ChatRoomSettingActivity.this, group.getGroupId()
//                        , id
//
//                        , new GroupHttpListener() {
//                            @Override
//                            public void onUserQuitGroupSuccess(boolean isSuccess) {
//                                if (deleteDialog != null && deleteDialog.isShowing()) {
//                                    deleteDialog.dismiss();
//                                }
//                                if (isSuccess) {
//                                    weChatDBManager.quitGroup(id, group.getGroupId());
//                                    Iterator<User> iterator = usersList.iterator();
//                                    while (iterator.hasNext()) {
//                                        User user = iterator.next();
//                                        if (user.getId() == id) {
//                                            iterator.remove();
//                                        }
//                                    }
//                                    adapter.notifyDataSetChanged();
//                                    //生成新的群组名
//                                    // GroupNameUtil.createNewGroupName(ChatRoomSettingActivity.this,groupId);
//                                    ToastUtil.showToast(ChatRoomSettingActivity.this, "移除成功");
//                                } else {
//                                    ToastUtil.showToast(ChatRoomSettingActivity.this, "移除失败");
//                                }
//                            }
//
//                            @Override
//                            public void onUpdateGroupNameSuccess(boolean isSuccess) {
//
//                            }
//                        });
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
////
//        }
//
//    }
//
//
//    public void back(View view) {
//        setResult(RESULT_OK);
//        finish();
//    }
//
//    @Override
//    public void onBackPressed() {
//        setResult(RESULT_OK);
//        finish();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        instance = null;
//    }
//
//    @Override
//    public void onCreateGroupSuccess(boolean isSuccess, Group group) {
//
//    }
//
//    @Override
//    public void onGetGroupUserSuccess(List<User> groupUsers, int groupId) {
//        group = weChatDBManager.getGroupWithUsers(groupId);
//
//        initData();
//    }
//
//    @Override
//    public void onAddUsersSuccess(boolean isSuccess) {
//
//    }
//
//    @Override
//    public void onGetGroupListSuccess(List<Integer> groupIds) {
//
//    }
//
//
//    /**
//     * 群组成员gridadapter
//     *
//     * @author yqs
//     */
//    private class GridAdapter extends BaseAdapter {
//
//        public boolean isInDeleteMode;
//        private List<User> objects;
//        Context context;
//
//        public GridAdapter(Context context, List<User> objects) {
//
//            this.objects = objects;
//            this.context = context;
//            isInDeleteMode = false;
//
//        }
//
//        @Override
//        public View getView(final int position, View convertView,
//                            final ViewGroup parent) {
//            if (convertView == null) {
//                convertView = LayoutInflater.from(context).inflate(
//                        R.layout.social_chatsetting_gridview_item, null);
//            }
//            ImageView iv_avatar = (ImageView) convertView
//                    .findViewById(R.id.iv_avatar);
//            TextView tv_username = (TextView) convertView
//                    .findViewById(R.id.tv_username);
//            ImageView badge_delete = (ImageView) convertView
//                    .findViewById(R.id.badge_delete);
//
//            // 最后一个item，减人按钮
//
//            if (position == getCount() - 1 && is_admin) {
//                tv_username.setText("");
//                badge_delete.setVisibility(View.GONE);
//                iv_avatar.setImageResource(R.drawable.icon_btn_deleteperson);
//
//                if (isInDeleteMode) {
//                    // 正处于删除模式下，隐藏删除按钮
//                    convertView.setVisibility(View.GONE);
//                } else {
//
//                    convertView.setVisibility(View.VISIBLE);
//                }
//
//                iv_avatar.setOnClickListener(new OnClickListener() {
//
//                    @Override
//                    public void onClick(View v) {
//                        isInDeleteMode = true;
//                        notifyDataSetChanged();
//                    }
//
//                });
//
//            } else if ((is_admin && position == getCount() - 2)
//                    || (!is_admin && position == getCount() - 1)) { // 添加群组成员按钮
//                tv_username.setText("");
//                badge_delete.setVisibility(View.GONE);
//                iv_avatar.setImageResource(R.drawable.jy_drltsz_btn_addperson);
//                // 正处于删除模式下,隐藏添加按钮
//                if (isInDeleteMode) {
//                    convertView.setVisibility(View.GONE);
//                } else {
//                    convertView.setVisibility(View.VISIBLE);
//                }
//                iv_avatar.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (group.getUsers() != null) {
//                            Intent intent = new Intent(ChatRoomSettingActivity.this,
//                                    CreatChatRoomActivity.class);
//                            intent.putExtra("chatgroup", (Serializable) group);
//
//                            startActivity(intent);
//                            finish();
//                        } else {
//                            ToastUtil.showToast(ChatRoomSettingActivity.this, "暂时无法进行该操作");
//                        }
//
//
//                    }
//                });
//            } else { // 普通item，显示群组成员
//
//                final User user = objects.get(position);
//                String usernick = null;
//                // User friendUser = weChatDBManager.getOneFriendById(user.getId());
//                User friendUser = GroupNameUtil.getGroupUser(groupId, user.getId(), weChatDBManager);
//                if (!friendUser.getNickName().equals(String.valueOf(user.getId()))) {
//                    //usernick = friendUser.getReMark();
//                    usernick = GroupNameUtil.getGroupUserName(friendUser);
//                } else {
//                    usernick = user.getNickName();
//                }
//                final String userhid = String.valueOf(user.getId());
//                //final String useravatar = user.getAvatar();
//                tv_username.setText(usernick);
//                //TODO yqs 头像暂时用一个
//                if (user.getAvatar().contains("http")) {
//                    imageLoader.displayImage(URLModifyDynamic.getInstance().replace(user.getAvatar()), iv_avatar, options);
//                } else {
//                    imageLoader.displayImage("", iv_avatar, options);
//
//                }
//                iv_avatar.setTag(user.getAvatar());
//                // demo群组成员的头像都用默认头像，需由开发者自己去设置头像
//                if (isInDeleteMode) {
//                    // 如果是删除模式下，显示减人图标
//                    convertView.findViewById(R.id.badge_delete).setVisibility(
//                            View.VISIBLE);
//                } else {
//                    convertView.findViewById(R.id.badge_delete).setVisibility(
//                            View.INVISIBLE);
//                }
//                iv_avatar.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (isInDeleteMode) {
//                            // 如果是删除自己，return
//                            if (EnvironmentVariable.getProperty(CHAT_USER_ID).equals(userhid)) {
//                                Toast.makeText(ChatRoomSettingActivity.this,
//                                        "不能删除自己", Toast.LENGTH_SHORT).show();
//                                return;
//                            }
//                            if (!NetUtils.hasNetwork(getApplicationContext())) {
//                                Toast.makeText(
//                                        getApplicationContext(),
//                                        getString(R.string.network_unavailable),
//                                        Toast.LENGTH_SHORT).show();
//                                return;
//                            } else {
//                                deleteMembersFromGroup(user.getId());
//                            }
//
//
//                        } else {//进入用户信息页
//                            Intent intent = new Intent(ChatRoomSettingActivity.this, GroupUserInfoActivity.class);
//                            intent.putExtra("id", user.getId());
//                            intent.putExtra("groupId", groupId);
//                            startActivity(intent);
//
//
//                        }
//                    }
//
//                });
//
//            }
//            return convertView;
//        }
//
//        @Override
//        public int getCount() {
//            if (is_admin) {
//                return objects.size() + 2;
//            } else {
//
//                return objects.size() + 1;
//
//            }
//
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return objects.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return position;
//        }
//    }
//
//    @Override
//    public void onUserQuitGroupSuccess(boolean isSuccess) {
//        if (deleteDialog != null && deleteDialog.isShowing()) {
//            deleteDialog.dismiss();
//            deleteDialog = null;
//        }
//
//        if (isSuccess) {
//            weChatDBManager.quitGroup(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)),
//                    group.getGroupId());
//
//            //TODO 删除列表数据
//            clearBadgem(group.getGroupId(), StructFactory.TO_USER_TYPE_GROUP);
//
//
//            //跳转到首页
//            try {
//                Class clazz = Class.forName("com.efounder.activity.TabBottomActivity");
//                Intent myIntent = new Intent(this, clazz);
//                startActivity(myIntent);
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            }
//
//            finish();
//
//            ToastUtil.showToast(ChatRoomSettingActivity.this, "退出成功");
//
//        } else {
//            ToastUtil.showToast(ChatRoomSettingActivity.this, "退出失败");
//        }
//
//    }
//
//    private void clearBadgem(int id, byte chatType) {
//
//        //清除当前聊天的角标
//        ChatListItem chatListItem = weChatDBManager.getChatListItem(id, chatType);
//        int unReadCount = JFMessageManager.getInstance().
//                getUnReadCount(id, chatType);
//        JFMessageManager.getInstance().unreadZero(id, chatType);
//
//        if (chatListItem != null) {
//            chatListItem.setBadgernum(-1);
//            weChatDBManager.deleteChatListiItem(chatListItem);
//            //ChatListItemUtil.updateUnreadCount(unReadCount, 0);
//            EventBus.getDefault().post(new MessageEvent(chatListItem, MessageEvent.DELETE));
//
//        }
//
//
//    }
//
//    @Override
//    public void onUpdateGroupNameSuccess(boolean isSuccess) {
//        if (isSuccess) {
//            tv_groupname.setText(newName);
//            group.setGroupName(newName);
//            weChatDBManager.insertOrUpdateGroup(group);
//            notifyChange(group);
//            ToastUtil.showToast(ChatRoomSettingActivity.this, "修改成功");
//        } else {
//            ToastUtil.showToast(ChatRoomSettingActivity.this, "修改失败");
//        }
//
//    }
//
//    /**
//     * 异步加载头像
//     */
//    public void initImageLoader() {
//        // 初始化异步加载图片的类
//        imageLoader = ImageLoader.getInstance();
//        // 设置异步加载图片的配置信息
//        options = ImageUtil.getImageLoaderOptions(R.drawable.default_useravatar);
//
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        notifyChange(group);
//    }
//
//    private void notifyChange(Group group) {
//        ChatListItem chatListItem = weChatDBManager.getChatListItem(group.getGroupId(), 2);
//        if (chatListItem != null)
//            EventBus.getDefault().post(new MessageEvent(chatListItem, MessageEvent.UPDATE));
////        //发送广播
////        Intent unReadIntent = new Intent();
////
////        unReadIntent.putExtra("id", group.getGroupId());
////        unReadIntent.putExtra("chatType", StructFactory.TO_USER_TYPE_GROUP);
////        unReadIntent.putExtra("group",group);
////
////        unReadIntent.setAction(ChatListFragment.UPDATEBADGNUM);
////        this.sendBroadcast(unReadIntent);
//
//
//    }
//}
