
package com.efounder.chat.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.http.GetHttpUtil;
import com.efounder.chat.http.JFCommonRequestManager;
import com.efounder.chat.model.Group;
import com.efounder.chat.utils.GroupNameUtil;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.util.ToastUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.efounder.frame.utils.Constants.CHAT_PASSWORD;
import static com.efounder.frame.utils.Constants.CHAT_USER_ID;


/**
 * 群介绍，群公告界面
 *
 * @author YQS 20180731
 */
public class GroupRecommendActivity extends BaseActivity implements View.OnClickListener {
    private static final int KEY_REQUEST_CODE = 121;

    private RelativeLayout title;
    private ImageView ivBack;
    private ImageView ivClose;
    private TextView tvTitle;
    private ImageView ivAdd;
    private ImageView ivSearch;
    private ImageView ivDetail;
    private TextView tvSave;
    private EditText etInput;

    private int groupId;

    /**
     * @param context
     * @param groupId
     * @param recommend 群介绍
     */
    public static void start(Context context, int groupId, String recommend) {
        Intent starter = new Intent(context, GroupRecommendActivity.class);
        starter.putExtra("groupId", groupId);
        starter.putExtra("recommend", recommend == null ? "" : recommend);
//        ((Activity) context).startActivityForResult(starter, KEY_REQUEST_CODE);
        ((Activity) context).startActivity(starter);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wechatview_activity_group_recommend);
        initView();
        initData();

    }

    private void initData() {
        tvTitle.setText(R.string.wechatview_grouprecommend_title);
        groupId = getIntent().getIntExtra("groupId", 0);
        String recommond = getIntent().getStringExtra("recommend");
        if (GroupNameUtil.userIsManager(groupId)) {
            tvSave.setVisibility(View.VISIBLE);
            tvSave.setText(R.string.save);
            if (recommond != null && !"".equals(recommond)) {
                //recommond = getResources().getString(R.string.wechatview_no_group_recommend);
                etInput.setText(recommond);
                etInput.setSelection(etInput.getText().length());

            }
        } else {
            etInput.setFocusable(false);
            etInput.setEnabled(false);
            if (recommond == null || "".equals(recommond)) {
                recommond = getResources().getString(R.string.wechatview_no_group_recommend);
            }
            etInput.setText(recommond);
        }


    }

    private void initView() {
        title = (RelativeLayout) findViewById(R.id.title);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivClose = (ImageView) findViewById(R.id.iv_close);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        ivAdd = (ImageView) findViewById(R.id.iv_add);
        ivSearch = (ImageView) findViewById(R.id.iv_search);
        ivDetail = (ImageView) findViewById(R.id.iv_detail);
        tvSave = (TextView) findViewById(R.id.tv_save);
        tvSave.setOnClickListener(this);
        etInput = (EditText) findViewById(R.id.et_input);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tv_save) {
            if (etInput.getText().toString().equals("")) {
                ToastUtil.showToast(GroupRecommendActivity.this, R.string.wrchatview_enter_not_empty);
                return;
            }
            updateGroupRecommend(etInput.getText().toString());

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void updateGroupRecommend(final String s) {
        HashMap<String, String> map = new HashMap<>();
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        map.put("groupDesc", s);
        showLoading(R.string.configuring_friend_info);
        JFCommonRequestManager.getInstance(this).requestGetByAsyn(TAG, GetHttpUtil.ROOTURL + "/IMServer/group/updateGroupDesc"
                , map, new JFCommonRequestManager.ReqCallBack<String>() {
                    @Override
                    public void onReqSuccess(String result) {
                        dismissLoading();
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(result);
                            if (jsonObject.optString("result", "").equals("success")) {

                                Group group =  WeChatDBManager.getInstance().getGroupInGroups(groupId, false);
                                group.setRecommond(s);
                                 WeChatDBManager.getInstance().insertOrUpdateGroup(group);
                                ToastUtil.showToast(getApplicationContext(), R.string.chat_set_success);
                                finish();

                            } else {
                                ToastUtil.showToast(getApplicationContext(), R.string.chat_set_fail);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onReqFailed(String errorMsg) {
                        dismissLoading();
                        ToastUtil.showToast(getApplicationContext(), R.string.chat_set_fail);
                    }
                });
    }
}
