//
//package com.efounder.chat.activity;
//
//import android.content.Intent;
//import android.os.Bundle;
//
//import com.efounder.chat.R;
//import com.efounder.chat.fragment.LoginFragment.LoginFragmentCallBack;
//
///**
// * 登陆页面
// *
// */
//public class LoginActivity extends BaseActivity implements LoginFragmentCallBack{
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_we_chat_login);
//
//    }
//
//	@Override
//	public void onLoginSuccess() {
//		Intent intent = new Intent(this, MainActivity.class);
//    	startActivity(intent);
//    	finish();
//	}
//
//	@Override
//	public void onClickRegister() {
//		startActivity(new Intent(this,RegisterActivity.class));
//	}
//
//}
