package com.efounder.chat.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.fragment.SelectGroupContactsFragment;
import com.efounder.chat.fragment.SelectGroupContactsFragment.SelectedCallBack;
import com.efounder.chat.model.Group;
import com.efounder.chat.utils.GroupAvatarHelper;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.utils.ResStringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author YQS选择联系人界面
 * @see SelectFriendActivity
 */
@Deprecated
public class SelectUserActivity extends BaseActivity implements
        SelectedCallBack {

    public static final int SUCCESS = 1;
    private static final int FAILER = -1;
    private TextView tv_checked;
    /**
     * 是否为一个新建的群组
     */
    protected boolean isCreatingNewGroup = true;
    private int groupId;
    private Group group;
    private User user = null;
    private ProgressDialog progressDialog;
    private final String TAG = "CreatChatRoomActivity";
    private GroupAvatarHelper helper;

    private List<User> selectList = new ArrayList<User>();

    // 选中用户总数,右上角显示
    int total = 0;
    private String groupName = "";
    private String navTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectuser);
      //  GetHttpUtil.setGetHttpUtilListener(this);
        helper = new GroupAvatarHelper(this);

        if (getIntent().hasExtra("chatgroup")) {
            isCreatingNewGroup = false;
            group = (Group) getIntent().getSerializableExtra("chatgroup");
            groupId = group.getGroupId();
            groupName = group.getGroupName();
        }
        if (getIntent().hasExtra("selectuser")) {

            isCreatingNewGroup = true;
        }

        String title = getIntent().getStringExtra("title");
        if (!TextUtils.isEmpty(title)) {
            navTitle = title;
        } else {
            navTitle = ResStringUtil.getString(R.string.wrchatview_please_select_contacts);
        }


        // 确定按钮
        tv_checked = (TextView) findViewById(R.id.tv_checked);

        final SelectGroupContactsFragment selectGroupContactsFragment = (SelectGroupContactsFragment)
                getSupportFragmentManager()
                        .findFragmentById(R.id.fragment_contact);
        selectGroupContactsFragment.setSelectedCallBack(this);
        tv_checked.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                //将选中的user返回给调用者,
                if (selectList != null && selectList.size() != 0) {
                    ArrayList<Integer> userIDs = new ArrayList<Integer>(selectList.size());

                    for (User user : selectList) {
                        userIDs.add(user.getId());
                    }

                    Intent data = new Intent();
                    data.putExtra("userIDs", userIDs);
                    setResult(SUCCESS, data);

                } else {
                    setResult(FAILER);
                }
                finish();
            }
        });

    }

    @Override
    public void getSelectUsers(ArrayList<User> selectList) {
        this.selectList = selectList;
        if (selectList != null) {
            total = selectList.size();
            if (isCreatingNewGroup) {
                tv_checked.setText(ResStringUtil.getString(R.string.common_text_confirm)+"(" + total + ")");
            } else {
                // tv_checked.setText("确定(" + (total - 1) + ")");
                tv_checked.setText(ResStringUtil.getString(R.string.common_text_confirm)+"(" + total + ")");
            }

        } else {
            tv_checked.setText(R.string.common_text_confirm);
        }

    }

//    @Override
//    public void onCreateGroupSuccess(boolean isSuccess, Group group) {
//        // 创建群组成功后向群组中添加联系人
//        if (isSuccess) {
//            String ids = EnvironmentVariable.getProperty(CHAT_USER_ID) + ";";
//            this.group = group;
//            for (User user : selectList) {
//                ids = ids + user.getId() + ";";
//            }
//            ids = ids.substring(0, ids.length() - 1);
//            try {
//                GetHttpUtil.addUserToGroup(SelectUserActivity.this,
//                        group.getGroupId(), ids);
//
//            } catch (JSONException e) {
//
//                e.printStackTrace();
//            }
//        } else {
//            if (progressDialog != null && progressDialog.isShowing()) {
//                progressDialog.dismiss();
//                progressDialog = null;
//            }
//            Toast.makeText(SelectUserActivity.this, "创建群组失败",
//                    Toast.LENGTH_SHORT).show();
//        }
//
//
//    }
//
//    @Override
//    public void onAddUsersSuccess(boolean isSuccess) {
//        // 向服务器群组中添加联系人，如果添加成功，返回true
//        if (progressDialog != null && progressDialog.isShowing()) {
//            progressDialog.dismiss();
//            progressDialog = null;
//        }
//        if (isSuccess) {
//            group.setUsers(selectList);
//            //得到登录的用户信息
//            User appLoginUser = WeChatDBManager.getInstance().getOneUserById(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
//            User groupUser = GroupNameUtil.getGroupUser(group.getGroupId(),
//                    Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)), WeChatDBManager.getInstance());
//            int role = User.GROUPCREATOR;
//            if (groupUser != null && groupUser.getGroupUserType() != User.GROUPMEMBER) {
//                role = groupUser.getGroupUserType();
//            }
//            // appLoginUser.setGroupUserType(User.GROUPCREATOR);//设置群组用户属性，群组创建者
//            appLoginUser.setGroupUserType(role);//设置群组用户属性，群组创建者
//
//            selectList.add(appLoginUser);
//            WeChatDBManager.getInstance().insertorUpdateMyGroupUser(group.getGroupId(), selectList);
//            //生成新的群组头像
//            // GroupAvatarUtil.createNewGroupAvatar(CreatChatRoomActivity.this,group.getCreateId());
//            helper.createNewGroupAvatar(group.getGroupId());
//            Intent intent = new Intent(SelectUserActivity.this,
//                    ChatActivity.class);
//            intent.putExtra("id", group.getGroupId());
//            intent.putExtra("chattype", StructFactory.TO_USER_TYPE_GROUP);
//            startActivity(intent);
//            finish();
//            selectList.clear();
//        } else {
//            if (progressDialog != null && progressDialog.isShowing()) {
//                progressDialog.dismiss();
//                progressDialog = null;
//            }
//            Toast.makeText(SelectUserActivity.this, "创建群组失败",
//                    Toast.LENGTH_SHORT).show();
//        }
////        if (group.getGroupName().contains("、"));
////        renameGroup(group.getGroupName());
//
//    }
//
//
//    @Override
//    public void onGetGroupUserSuccess(List<User> groupUsers, int groupId) {
//
//    }
//
//    @Override
//    public void onGetGroupListSuccess(List<Integer> groupIds) {
//
//    }
}
