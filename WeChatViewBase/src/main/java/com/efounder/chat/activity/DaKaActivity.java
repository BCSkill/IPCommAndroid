//package com.efounder.chat.activity;
//
//import android.graphics.Bitmap;
//import android.location.Location;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.support.annotation.NonNull;
//import android.support.v4.widget.SwipeRefreshLayout;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.util.TypedValue;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextClock;
//import android.widget.TextView;
//import android.widget.ZoomControls;
//
//import com.baidu.location.LocationClient;
//import com.baidu.location.LocationClientOption;
//import com.baidu.mapapi.SDKInitializer;
//import com.baidu.mapapi.map.BaiduMap;
//import com.baidu.mapapi.map.MapStatusUpdate;
//import com.baidu.mapapi.map.MapStatusUpdateFactory;
//import com.baidu.mapapi.map.MapView;
//import com.baidu.mapapi.map.UiSettings;
//import com.bumptech.glide.Glide;
//import com.efounder.chat.R;
//import com.efounder.chat.fragment.DatePickerFragment;
//import com.efounder.chat.listener.MyLocationListenner;
//import com.efounder.chat.utils.GPSUtils;
//import com.efounder.chat.view.MaterialDialog;
//import com.efounder.chat.widget.RadarView;
//import com.efounder.imageselector.utils.FileUtils;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//
///**
// * 打卡
// * @author 郑建龙
// */
//@Deprecated
//public class DaKaActivity extends AppCompatActivity implements DatePickerFragment.DataCallBack,View.OnClickListener,SwipeRefreshLayout.OnRefreshListener{
//
//    TextView titleTv;
//    TextView avatarIV;
//    SwipeRefreshLayout swipeRefreshLayout;
//    TextView tv_time;
//    MapView mMapView;
//
//
//    ImageView iv_address;
////    LinearLayout xiawudakalayout;
////    RadarView radar_view2;
//    TextView tv_usr_name;
//
//    BaiduMap baiduMap;
//    private LocationClient mLocClient;// 定位相关
//    public MyLocationListenner myListener;
//
//    String curAddress;
//    String curNetTime;
//    String curLatitude;
//    String curLongtitude;
//    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//    SimpleDateFormat simpleDateFormatHHMM = new SimpleDateFormat("HH:mm");
//    Handler myHandler = new Handler() {
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            Log.i("lcusky","--------msg:"+msg.what);
//            switch (msg.what){
//                case 0://上午打卡
//                    rl_am_dk_content.setVisibility(View.GONE);
//                    rl_am_state_content.setVisibility(View.VISIBLE);
//                    pmUIContent.setVisibility(View.VISIBLE);
//                    amdakalayout.setVisibility(View.VISIBLE);
//                    am_rv.setSearching(false);
//                    am_rv.setVisibility(View.GONE);
//                    showDKResultDialog(0);
//                    break;
//                case 1://下午打卡
//                    rl_pm_n_dk_content.setVisibility(View.GONE);
//                    rl_pm_state_content.setVisibility(View.VISIBLE);
//                    pmdakalayout.setVisibility(View.VISIBLE);
//                    pm_rv.setSearching(false);
//                    pm_rv.setVisibility(View.GONE);
//                    showDKResultDialog(1);
//                    break;
//                case 2://下拉刷新
//                    swipeRefreshLayout.setRefreshing(false);
//                    break;
//                case 3://定位截图
//                    final File file = new File(curLocationAddress);
//                    baiduMap.snapshot(new BaiduMap.SnapshotReadyCallback() {
//                        @Override
//                        public void onSnapshotReady(Bitmap snapshot) {
//                            FileOutputStream out;
//                            try {
//                                out = new FileOutputStream(file);
//                                if (snapshot.compress(Bitmap.CompressFormat.PNG, 100, out)) {
//                                }
//                                if (out!=null){
//                                    out.flush();
//                                    out.close();
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//                    Log.i("lcusky","---lcuksky:"+file.getAbsolutePath());
//                    Glide.with(DaKaActivity.this).load(file).into(iv_address);
//                    swipeRefreshLayout.setRefreshing(false);
//                    break;
//            }
//        }
//    };
//
//    View amUIContent;
//    View pmUIContent;
//
//    //上午打卡 状态部分
//    RelativeLayout rl_am_state_content;
//    //上午打卡部分
//    RelativeLayout rl_am_dk_content;
//    LinearLayout amdakalayout;
//    RadarView am_rv;
//    TextClock am_textclock;
//    TextView tv_am_address;
//    /**上班打卡时间*/
//    TextView tv_amtext;//
//    TextClock pm_textClock1;
//
//    //下午打卡 状态部分
//    RelativeLayout rl_pm_state_content;
//    //下午打卡部分
//    RelativeLayout rl_pm_n_dk_content;
//    LinearLayout pmdakalayout;
//    RadarView pm_rv;
//    TextView tv_pm_address;
//    /**下班打卡时间*/
//    TextView tv_pmtext;
//
//    /**定位图片*/
//     String curLocationAddress;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        SDKInitializer.initialize(getApplicationContext());
//        setContentView(R.layout.lib_wcv_main_ui_da_ka);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setTitle("");
//        setSupportActionBar(toolbar);
////        lib_wcv_toolbar.setNavigationIcon(R.mipmap.nav_icon_back);
//
//        curLocationAddress = FileUtils.getDiskCacheDir(DaKaActivity.this)+ File.separator+"tmp.png";
//        initView();
//        showMapWithLocationClient();
//        initListener();
//        EventBus.getDefault().register(this);
//        mCallBack = new GPSUtils.GPSCallBack() {
//            @Override
//            public void updateView(Location location) {
//                if(location!=null){
//                    tv_usr_name.setText("设备位置信息\n\n经度：");
//                    tv_usr_name.append(String.valueOf(location.getLongitude()));
//                    tv_usr_name.append("\n纬度：");
//                    tv_usr_name.append(String.valueOf(location.getLatitude()));
//                }else{
//                    //清空Text对象
////                    tv_usr_name.getEditableText().clear();
//                }
//            }
//        };
//        GPSUtils.registerGPS(this,mCallBack);
//    }
//
//    GPSUtils.GPSCallBack mCallBack;
//    @Override
//    protected void onResume() {
//        super.onResume();
//        mMapView.onResume();
//    }
//
//    private void initListener(){
//        tv_time.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DatePickerFragment dpf = new DatePickerFragment();
//                dpf.show(getSupportFragmentManager(),"date_picker");
//            }
//        });
//        swipeRefreshLayout.setOnRefreshListener(this);
//    }
//    private void initView() {
//        titleTv = (TextView) findViewById(R.id.tv_title);
//        titleTv.setText(R.string.wrchatview_daka);
//        avatarIV = (TextView) findViewById(R.id.iv_avatar);
//        avatarIV.setOnClickListener(this);
//        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.pb_widget);
//        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark,R.color.colorPrimary);
//        swipeRefreshLayout.setProgressViewOffset(true, 0, (int) TypedValue
//                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources()
//                        .getDisplayMetrics()));
//        swipeRefreshLayout.setRefreshing(true);
//        tv_time = (TextView) findViewById(R.id.tv_time);
//        tv_time.setText(simpleDateFormat.format(new Date()));
//        mMapView = (MapView) findViewById(R.id.mv_location);
//
//        iv_address = (ImageView) findViewById(R.id.iv_address);
////        xiawudakalayout = (LinearLayout) findViewById(xiawudakalayout);
////        xiawudakalayout.setOnClickListener(this);
////        radar_view2 = (RadarView) findViewById(radar_view2);
//        tv_usr_name = (TextView) findViewById(R.id.tv_usr_name);
//        tv_usr_name.setOnClickListener(this);
//
//        amUIContent = findViewById(R.id.v_am_ui);
//        pmUIContent = findViewById(R.id.v_pm_ui);
//
//        rl_am_state_content = (RelativeLayout) findViewById(R.id.rl_am_state_content);
//        rl_am_dk_content = (RelativeLayout) findViewById(R.id.rl_am_dk_content);
//
//        amdakalayout = (LinearLayout) findViewById(R.id.amdakalayout);
//        amdakalayout.setOnClickListener(this);
//        am_rv = (RadarView) findViewById(R.id.am_rv);
//        am_textclock = (TextClock) findViewById(R.id.am_textclock);
//        rl_pm_state_content = (RelativeLayout) findViewById(R.id.rl_pm_state_content);
//        rl_pm_n_dk_content = (RelativeLayout) findViewById(R.id.rl_pm_n_dk_content);
//        pmdakalayout = (LinearLayout) findViewById(R.id.pmdakalayout);
//        pmdakalayout.setOnClickListener(this);
//        pm_rv = (RadarView) findViewById(R.id.pm_rv);
//        pm_textClock1 = (TextClock) findViewById(R.id.pm_textClock1);
//
//        tv_am_address = (TextView) findViewById(R.id.tv_am_address);
//        tv_pm_address = (TextView) findViewById(R.id.tv_pm_address);
//
//        tv_amtext = (TextView) findViewById(R.id.tv_amtext);
//        tv_pmtext = (TextView) findViewById(R.id.tv_pmtext);
//
//        if(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)<12){//上午  TODO
//            amUIContent.setVisibility(View.VISIBLE);
//            pmUIContent.setVisibility(View.GONE);
//        }else{//下午
//            amUIContent.setVisibility(View.VISIBLE);
//            pmUIContent.setVisibility(View.VISIBLE);
//            rl_am_dk_content.setVisibility(View.GONE);
//            rl_am_state_content.setVisibility(View.VISIBLE);
//            rl_pm_state_content.setVisibility(View.GONE);
//            rl_pm_n_dk_content.setVisibility(View.VISIBLE);
//        }
//
//        mMapView.showZoomControls(false);
//        baiduMap = mMapView.getMap();
//        MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(17.0f);
//        baiduMap.setMapStatus(msu);
//        // 隐藏百度logo ZoomControl
//        int count = mMapView.getChildCount();
//        for (int i = 0; i < count; i++) {
//            View child = mMapView.getChildAt(i);
//            if (child instanceof ImageView || child instanceof ZoomControls) {
//                child.setVisibility(View.INVISIBLE);
//            }
//        }
//        // 隐藏比例尺
//        mMapView.showScaleControl(false);
//        UiSettings uiSettings = baiduMap.getUiSettings();
//        uiSettings.setAllGesturesEnabled(false);
//
//        myListener = new MyLocationListenner(baiduMap);
//
////        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//    }
//    /**
//     * 显示当前的位置信息
//     */
//    private void showMapWithLocationClient() {
//        String str1 = "正在定位";
//
//        mLocClient = new LocationClient(this);
//        mLocClient.registerLocationListener(myListener);
//        LocationClientOption option = new LocationClientOption();
////        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
//        option.setOpenGps(true);// 打开gps
//        option.setCoorType("gcj02");
////        option.setCoorType("bd09ll");
////        option.setCoorType("bd09");
//        option.setIsNeedAddress(true);
////        option.setScanSpan(10000);
//        option.setLocationNotify(false);//可选，默认false，设置是否当GPS有效时按照1S/1次频率输出GPS结果
////        option.setEnableSimulateGps(false);//可选，默认false，设置是否需要过滤GPS仿真结果，默认需要
//        mLocClient.setLocOption(option);
//        mLocClient.start();
//    }
//    @Override
//    public void getData(String data) {
//        tv_time.setText(data);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mMapView.onDestroy();
//        mLocClient.unRegisterLocationListener(myListener);
//        if (mLocClient != null)
//            mLocClient.stop();
//        EventBus.getDefault().unregister(this);
//        GPSUtils.unRegisterGPS();
//
//
//
//    }
//
//    @Subscribe
//    public void loadLocationOver(String msg){
//        if(null!=msg&&!"".equals(msg)){
//            String[] str = msg.split(",");
////            tv_time.setText(str[0].substring(0,10));
//            curAddress = str[1];
//            curNetTime = str[0].split(" ")[1];
//            tv_am_address.setText(curAddress);
//            tv_pm_address.setText(curAddress);
////            curLatitude = str[2];
////            curLongtitude = str[3];
//        }
//        Log.i("lcusky","---eventBust result----"+msg+",curAddress:"+curAddress+",curNetTime:"+curNetTime);
////        &center=116.403874,39.914889&markers=116.403874,39.914889";
//        String latAndLong = curLongtitude+","+curLatitude;
//        Message message = new Message();
//        message.what =3;
//         myHandler.sendMessageDelayed(message,1000);
//    }
//
//    public void onAvatarClick(){
////        Intent intent = new Intent(this, KaoQinActivity.class);
////        startActivity(intent);
//    }
//
//    /**
//     * 上下午 雷达扫描
//     * @param time
//     */
//    public void doRadarClick(int time){
//        Log.i("lcusky","---------doRadarClick--"+time);
//        if(time==0){
//            amdakalayout.setVisibility(View.GONE);
//            am_rv.setVisibility(View.VISIBLE);
//            am_rv.setSearching(true);
//            //radar_view2.addPoint();
//        }else{
//            pmdakalayout.setVisibility(View.GONE);
//            pm_rv.setVisibility(View.VISIBLE);
//            pm_rv.setSearching(true);
//        }
//        Message message = new Message();
//        message.what = time;
//        myHandler.sendMessageDelayed(message,3000);
//        showMapWithLocationClient();
//    }
//
//    @Override
//    public void onClick(View view) {
//
//        if(view.getId()==R.id.tv_usr_name){
//
//        }else if(view.getId()==R.id.amdakalayout){
//            doRadarClick(0);
//        }else if(view.getId()==R.id.pmdakalayout){
//        doRadarClick(1);
//        }else if(view.getId()==R.id.iv_avatar){
//        onAvatarClick();
//        }
//    }
//
//
//    private void showDKResultDialog(final int flag){
//       final MaterialDialog mMaterialDialog = new MaterialDialog(this);
//        View view = getLayoutInflater().inflate(R.layout.lib_wcv_dk_result_dialog,null);
//        TextView uporDownTv = (TextView) view.findViewById(R.id.tv_up_down);
//        TextView tvTime = (TextView) view.findViewById(R.id.tv_time);
//        TextView tvTip = (TextView) view.findViewById(R.id.tv_tip);
//        if(flag==0){
//            uporDownTv.setText("上\n班");
//        }else{
//            uporDownTv.setText("下\n班");
//            tvTip.setText("下班时刻 畅享休闲时光");
//        }
//        tvTime.setText(curNetTime);
//        mMaterialDialog.setContentView(R.layout.lib_wcv_dk_result_dialog);
//        mMaterialDialog.setView(view);
////        mMaterialDialog.setContentView(view);
//
//        mMaterialDialog.setPositiveButton("我知道了", new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mMaterialDialog.dismiss();
//                String time =  simpleDateFormatHHMM.format(new Date());
//                switch (flag){
//                    case 0:
//                        tv_amtext.setText("上班打卡时间"+time+" (上班时间8:30)");
//                        break;
//                    case 1:
//                        tv_pmtext.setText("下班打卡时间"+time+" (下班时间17:30)");
//                        break;
//                }
//            }
//        });
//        mMaterialDialog.show();
//    }
//
//    @Override
//    public void onRefresh() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Message msg = new Message();
//                msg.what = 2;
//                myHandler.sendMessageDelayed(msg,3000);
//            }
//        }).start();
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
//}
