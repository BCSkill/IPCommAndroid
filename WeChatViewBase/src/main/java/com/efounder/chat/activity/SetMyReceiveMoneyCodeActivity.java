package com.efounder.chat.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.fragment.SetMoneyCodeFragment;
import com.efounder.chat.http.OpenEthRequest;
import com.efounder.utils.ResStringUtil;
import com.marlonmafra.android.widget.SegmentedTab;

import java.util.ArrayList;
import java.util.List;

/**
 * 设置我的收款码
 */
public class SetMyReceiveMoneyCodeActivity extends BaseActivity implements View.OnClickListener {

    private RelativeLayout title;
    private ImageView ivBack;
    private ImageView ivClose;
    private TextView tvTitle;
    private ImageView ivAdd;
    private ImageView ivSearch;
    private ImageView ivDetail;
    private TextView tvSave;
    private SegmentedTab segmentControl;
    private ViewPager pager;
    private String[] titles1 = new String[]{ResStringUtil.getString(R.string.chat_we_chat), ResStringUtil.getString(R.string.chat_pay_for)};


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wechatview_activity_set_my_money_code1);
        initView();
        initData();

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isFinishing()){
            OpenEthRequest.cannelRequest();
        }
    }

    private void initView() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        title = (RelativeLayout) findViewById(R.id.title);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivClose = (ImageView) findViewById(R.id.iv_close);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        ivAdd = (ImageView) findViewById(R.id.iv_add);
        ivSearch = (ImageView) findViewById(R.id.iv_search);
        ivDetail = (ImageView) findViewById(R.id.iv_detail);
        tvSave = (TextView) findViewById(R.id.tv_save);
        segmentControl = (SegmentedTab) findViewById(R.id.segment_control);
        pager = (ViewPager) findViewById(R.id.pager);


    }

    private void initData() {
        tvTitle.setText(R.string.chat_collection_code);

        pager.setAdapter(new MyAdapter(getSupportFragmentManager()));


        List<String> titles = new ArrayList<>();
        for (int i = 0; i < titles1.length; i++) {
            titles.add(titles1[i]);
        }

        this.segmentControl.setupWithViewPager(this.pager);
        this.segmentControl.setup(titles);
    }

    @Override
    public void onClick(View v) {

    }




    private void refreshView() {
    }

    public class MyAdapter extends FragmentPagerAdapter {


        public MyAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return SetMoneyCodeFragment.newInstance("weixin");
            } else if (position == 1) {
                return SetMoneyCodeFragment.newInstance("zhifubao");
            } else {
                return null;
            }

        }

        @Override
        public int getCount() {
            return 2;
        }


    }
}
