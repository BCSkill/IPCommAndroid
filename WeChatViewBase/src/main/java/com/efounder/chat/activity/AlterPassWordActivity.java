package com.efounder.chat.activity;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.efounder.builder.base.data.EFDataSet;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.chat.R;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.efounder.frame.utils.Constants;
import com.efounder.util.StringUtil;
import com.efounder.util.ToastUtil;
import com.efounder.utils.ResStringUtil;

import java.util.List;

import static com.efounder.frame.utils.Constants.KEY_SIGN;

public class AlterPassWordActivity extends BaseActivity implements OnClickListener {

    private EditText et_oldPassword;
    private EditText et_password;//新密码
    private EditText et_password_corfirm;//确认新密码
    private Button btn_enter;
    private String registrID;//注册号


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_alterpassword);
        initView();
        int myId = Integer.valueOf(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID));
        alterPassword(myId);
        //  WeChatDBManager weChatDBManager = WeChatDBManager.getInstance();
        // User user = weChatDBManager.getOneUserById(myId);
        registrID = EnvironmentVariable.getUserName();

    }

    private void initView() {
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(ResStringUtil.getString(R.string.wrchatview_change_password));
        et_oldPassword = (EditText) findViewById(R.id.et_oldpassword);
        et_password = (EditText) findViewById(R.id.et_password);
        et_password_corfirm = (EditText) findViewById(R.id.et_password_corfirm);
        btn_enter = (Button) findViewById(R.id.btn_enter);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();


    }

    @Override
    public void back(View view) {

        finish();
    }

    private void alterPassword(final int userId) {


        btn_enter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final String pwdEtStr = et_oldPassword.getText().toString().trim();
                final String pwdNewEtStr = et_password.getText().toString().trim();
                final String pwdNewEt2Str = et_password_corfirm.getText().toString().trim();
                if (StringUtil.isEmpty(pwdEtStr)) {
                    et_oldPassword.setError(ResStringUtil.getString(R.string.wrchatview_start_password_not_empty));
                    return;
                }
                if (StringUtil.isEmpty(pwdNewEtStr)) {
                    et_password.setError(ResStringUtil.getString(R.string.wrchatview_new_password_not_empty));
                    return;
                }
                if (pwdNewEtStr.equals(pwdEtStr)) {
                    et_password.setError(ResStringUtil.getString(R.string.wrchatview_new_not_same_start));
                    return;
                }
                if (!pwdNewEtStr.equals(pwdNewEt2Str)) {
                    et_password_corfirm.setError(ResStringUtil.getString(R.string.wrchatview_not_new_same));
                    return;
                }
                //TODO 检查更改密码
                new AsyncTask<String, Integer, JResponseObject>() {
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        showLoading(ResStringUtil.getString(R.string.common_text_please_wait));
                    }

                    @Override
                    protected JResponseObject doInBackground(String... params) {
                        JParamObject PO = JParamObject.Create();

                        PO.SetValueByParamName("loginName", registrID);
                        PO.SetValueByParamName("old_userPassWord", pwdEtStr);
                        PO.SetValueByParamName("userPassWord", pwdNewEtStr);
                        PO.SetValueByParamName("method", "changePassWord");
                        PO.setEnvValue("DBNO", EnvironmentVariable.getProperty(KEY_SIGN));// 数据标示
                        PO.setEnvValue("DataBaseName", EnvironmentVariable.getProperty("DataBaseName"));// 数据标示


                        JParamObject.assign(PO);
                        try {
                            //连接服务器
                            String serverKey = EnvironmentVariable.getProperty("ServerKey");
                            JResponseObject RO = EAI.DAL.SVR(serverKey, PO);

                            return RO;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    }

                    @Override
                    protected void onPostExecute(JResponseObject result) {
                        dismissLoading();
                        if (result == null) {
                            ToastUtil.showToast(AlterPassWordActivity.this, R.string.wrchatview_change_fail);
                            return;
                        }
                        EFDataSet efDataSet = (EFDataSet) result.getResponseObject();
                        if (efDataSet == null) {
                            ToastUtil.showToast(AlterPassWordActivity.this, R.string.wrchatview_change_fail);
                            return;
                        }
                        List<EFRowSet> list = efDataSet.getRowSetArray();
                        if (list != null && list.size() > 0) {
                            EFRowSet efRowSet = list.get(0);
                            if ("0".equals(efRowSet.getString("result", "-1"))) {
//                                EnvironmentVariable.setProperty("PassWord",pwdNewEtStr);
                                EnvironmentVariable.setPassword(pwdNewEtStr);
                                ToastUtil.showToast(AlterPassWordActivity.this, R.string.wrchatview_change_succcess);
                                AlterPassWordActivity.this.finish();
                                // restartApplication();


//                                new AlertDialog.Builder(AlterPassWordActivity.this).
//                                        setMessage("修改密码成功，请重新登录").setTitle(R.string.common_text_hint).setPositiveButton(R.string.common_text_confirm, new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        try {
//                                            Class a = Class.forName("com.efounder.activity.Login_withTitle");
//                                            Intent intent = new Intent();
//                                            intent.setClass(AlterPassWordActivity.this, a);
//                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);  //注意本行的FLAG设置
//                                            startActivity(intent);
//                                        } catch (ClassNotFoundException e) {
//                                            e.printStackTrace();
//                                        }
//
//                                    }
//                                }).show();

                            } else {
                                String tip = efRowSet.getString("tip", ResStringUtil.getString(R.string.wrchatview_change_fail_and));
                                ToastUtil.showToast(AlterPassWordActivity.this, tip);
                            }
                        } else {
                            ToastUtil.showToast(AlterPassWordActivity.this, R.string.wrchatview_change_fail_and);

                        }


                    }
                }.execute();
            }
        });
    }

    private void restartApplication() {
        final Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
