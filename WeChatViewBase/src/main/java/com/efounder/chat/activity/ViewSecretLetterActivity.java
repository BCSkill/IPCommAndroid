package com.efounder.chat.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.utils.AESSecretUtil;
import com.utilcode.util.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class ViewSecretLetterActivity extends BaseActivity {

    private TextView tvSecretLetter;
    private String password;
    private String privateKey;
    private String content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wechatview_activity_view_secret_letter);

        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvSecretLetter = (TextView) findViewById(R.id.tv_secret_letter);

        tvTitle.setText(R.string.wrchatview_secret_letter);

        if (getIntent().getExtras()!=null) {
            //得到消息中的加密文本信息
            try {
                JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("message"));
                content = jsonObject.getString("content");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (content == null) {
                ToastUtils.showShort(R.string.wrchatview_dcryption_failed);
                return;
            }
            //有password则用password解密

            if (getIntent().getExtras().containsKey("password")) {
                password = getIntent().getStringExtra("password");
                passwordDecrypt();
            } else if (getIntent().getExtras().containsKey("privateKey")) {
                //没有password则用私钥方法解密
                privateKey = getIntent().getStringExtra("privateKey");
                privateKeyDecrypt();
            }
        }
    }

    /**
     * 用password方法解密
     */
    private void passwordDecrypt() {
        try {
            AESSecretUtil aesSecretUtil = new AESSecretUtil(password);
            tvSecretLetter.setText(aesSecretUtil.decrypt(content));
        } catch (Exception e) {
            e.printStackTrace();
            ToastUtils.showShort(R.string.wrchatview_dcryption_failed);
        }

    }

    /**
     * 用私钥方法解密
     */
    private void privateKeyDecrypt() {
        // TODO: 18-5-5
    }
}
