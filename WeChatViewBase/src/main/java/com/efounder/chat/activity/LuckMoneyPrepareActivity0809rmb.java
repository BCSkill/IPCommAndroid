//package com.efounder.chat.activity;
//
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.core.xml.StubObject;
//import com.efounder.chat.R;
//import com.efounder.chat.db.WeChatDBManager;
//import com.efounder.chat.model.Group;
//import com.efounder.chat.struct.StructFactory;
//import com.efounder.chat.widget.SecretPassInputDialog;
//import com.efounder.message.manager.JFMessageManager;
//import com.efounder.message.struct.IMStruct002;
//import com.efounder.util.ToastUtil;
//
//import net.sf.json.JSONObject;
//
//import java.text.DecimalFormat;
//
///**
// * 发送红包填写信息界面
// *
// * @author yqs
// */
//
//public class LuckMoneyPrepareActivity0809rmb extends BaseActivity implements View.OnClickListener {
//    //未打开
//    public static final int PACKAGE_NO_OPEN = 0;
//    //以打开
//    public static final int PACKAGE_OPENED = 1;
//    //已领取
//    public static final int PACKAGE_RECEIVED = 2;
//
//    private final String TAG = "LuckMoneyPrepareActivity";
//    private TextView tvTitle;
//    private EditText etMoney;
//    private EditText etLeaveMessage;
//    private TextView tvMoney;
//    private Button butSend;
//
//    private TextView tvFirstTitle;
//    private RelativeLayout rlGroupRpLayput;
//    private TextView tvTips1;
//    private TextView tvTips2;
//    private EditText etCount;
//    private TextView tvTips3;
//
//
//    private int toUserId;
//    private byte chatType;
//    private StubObject mStubObject;
//    private DecimalFormat decimalFormat = new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,
//    private boolean isLuckMode = true;//拼手气红包
//    private boolean isGroup = false;//是否是群组
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.wechatview_activity_luck_money_prepare);
//        initView();
//        initListener();
//        initData();
//    }
//
//    private void initListener() {
//        etMoney.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                String temp = s.toString();
//                if (s.length() == 0) {
//                    butSend.setEnabled(false);
//                    butSend.setClickable(false);
//                    tvMoney.setText(getResources().getString(R.string.im_red_package_money_unit1, "0.00"));
//                    return;
//                } else {
//                    butSend.setEnabled(true);
//                    butSend.setClickable(true);
//                }
//                int posDot = temp.indexOf(".");
//                if (posDot <= 0) {
//                    String text = temp.toString() + ".00";
//                    tvMoney.setText(getResources().getString(R.string.im_red_package_money_unit1, text));
//                    return;
//                }
//                if (temp.length() - posDot - 1 == 0) {
//                    String text = temp.toString() + "00";
//                    tvMoney.setText(getResources().getString(R.string.im_red_package_money_unit1, text));
//                    return;
//                }
//                if (temp.length() - posDot - 1 > 2) {
//                    s.delete(posDot + 3, posDot + 4);
//                }
//                //  String text = String.format("%.2f", temp);
//                String text = null;
//                try {
//                    double money = Double.valueOf(temp.toString());
//                    text = decimalFormat.format(money);
//                } catch (NumberFormatException e) {
//                    e.printStackTrace();
//                }
//                tvMoney.setText(getResources().getString(R.string.im_red_package_money_unit1, text));
//            }
//        });
//    }
//
//
//    private void initView() {
//        //标题
//        tvTitle = (TextView) this.findViewById(R.id.tv_title);
//        etMoney = (EditText) findViewById(R.id.et_money);
//        etLeaveMessage = (EditText) findViewById(R.id.et_leave_message);
//        tvMoney = (TextView) findViewById(R.id.tv_money);
//        butSend = (Button) findViewById(R.id.but_send);
//        butSend.setOnClickListener(this);
//        tvFirstTitle = (TextView) findViewById(R.id.tv_first_title);
//        rlGroupRpLayput = (RelativeLayout) findViewById(R.id.rl_group_rp_layput);
//        tvTips1 = (TextView) findViewById(R.id.tv_tips1);
//        tvTips2 = (TextView) findViewById(R.id.tv_tips2);
//        etCount = (EditText) findViewById(R.id.et_count);
//        tvTips3 = (TextView) findViewById(R.id.tv_tips3);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//
//    }
//
//    private void initData() {
//        tvTitle.setText("发红包");
//        mStubObject = (StubObject) getIntent().getSerializableExtra("stubObject");
//        toUserId = Integer.parseInt(mStubObject.getString("toUserId", "0"));
//        chatType = Byte.parseByte(mStubObject.getString("chatType", ""));
//        tvMoney.setText(getResources().getString(R.string.im_red_package_money_unit1, "0.00"));
//        if (chatType == StructFactory.TO_USER_TYPE_PERSONAL) {
//            initPersonalData();
//        } else {
//            isGroup = true;
//            initGroupRedPackageData();
//        }
//    }
//
//    private void initGroupRedPackageData() {
//        rlGroupRpLayput.setVisibility(View.VISIBLE);
//        updateShowText(true);
//        tvTips2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                isLuckMode = !isLuckMode;
//                updateShowText(isLuckMode);
//            }
//        });
//        Group group = WeChatDBManager.getInstance().getGroupInGroups(toUserId, true);
//        if (group != null && group.getUsers() != null) {
//            tvTips3.setText(getResources().getString(R.string.im_money_prepare_tip_3, group.getUsers().size() + ""));
//        } else {
//            tvTips3.setText(getResources().getString(R.string.im_money_prepare_tip_3, "0"));
//        }
//    }
//
//    private void updateShowText(boolean isLuckMode) {
//        if (isLuckMode) {
//            tvFirstTitle.setText("总金额");
//            tvTips1.setText(R.string.im_money_prepare_tip_1_1);
//            tvTips2.setText(R.string.im_money_prepare_tip_2_1);
//        } else {
//            tvFirstTitle.setText("单个金额");
//            tvTips1.setText(R.string.im_money_prepare_tip_1_2);
//            tvTips2.setText(R.string.im_money_prepare_tip_2_2);
//        }
//
//    }
//
//    private void initPersonalData() {
//        rlGroupRpLayput.setVisibility(View.GONE);
//    }
//
//
//    @Override
//    public void onClick(View v) {
//        int id = v.getId();
//        if (id == R.id.but_send) {
//            //点击发送
//            showVerifyPwdDialog();
//        }
//    }
//
//    private void showVerifyPwdDialog() {
//        try {
//            Double.valueOf(etMoney.getText().toString());
//        } catch (NumberFormatException e) {
//            e.printStackTrace();
//            ToastUtil.showToast(this, "红包金额不合法！");
//            return;
//        }
//        if (isGroup) {
//            if (etCount.getText().length() == 0) {
//                ToastUtil.showToast(this, "请输入红包个数");
//                return;
//            }
//            if ("0".equals(etCount.getText().toString())) {
//                ToastUtil.showToast(this, "红包个数不能为0");
//                return;
//            }
//        }
//
//        SecretPassInputDialog dialog = new SecretPassInputDialog(this, new SecretPassInputDialog.OnEnterClick() {
//            @Override
//            public void passVerifySuccess(String passWord) {
//                //密码正确，开始发送
//                sendMessage();
//            }
//        });
//        dialog.getTips().setVisibility(View.GONE);
//        dialog.show();
//    }
//
//    private void sendMessage() {
//        //组织数据
//        JSONObject jsonObject = new JSONObject();
//        //红包个数
//        int count = etCount.getText().length() == 0 ? 1 : Integer.valueOf(etCount.getText().toString());
//        //红包金额
//        double money = Double.valueOf(etMoney.getText().toString());
//        //留言
//        String leaveMessage = etLeaveMessage.getText().length() == 0 ? "恭喜发财，大吉大利" : etLeaveMessage.getText().toString();
//        //是否是幸运红包 （群组中）
//        boolean isLuckType = false;
//        if (isGroup) {
//            isLuckType = isLuckMode;
//        }
//        //红包状态
//        jsonObject.put("packageState", PACKAGE_NO_OPEN);
//
//        jsonObject.put("count", count);
//        jsonObject.put("money", money);
//        jsonObject.put("leaveMessage", leaveMessage);
//        jsonObject.put("isLuckMode", isLuckType?1:0);
//
//        String message = jsonObject.toString();
//
//        IMStruct002 imStruct002 = StructFactory.getInstance().createRedPackageMessage(message, toUserId, chatType);
//        JFMessageManager.getInstance().sendMessage(imStruct002);
//        finish();
//    }
//}
