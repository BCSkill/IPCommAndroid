package com.efounder.chat.zxing.qrcode;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.efounder.chat.R;
import com.efounder.chat.activity.BaseActivity;

public class OtherResultActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.otherresult);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        final String result = bundle.getString("result");

        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(R.string.wrchatview_recognition_result);

        TextView tv_result = (TextView) this.findViewById(R.id.tv_result);
        tv_result.setText(result);

        ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                OtherResultActivity.this.finish();
            }
        });

        Button btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(result);
                Toast.makeText(OtherResultActivity.this, R.string.wechatview_pasted_cilp, Toast.LENGTH_SHORT).show();
                OtherResultActivity.this.finish();
            }
        });

    }


}
