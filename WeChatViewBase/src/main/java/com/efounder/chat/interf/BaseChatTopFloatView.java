package com.efounder.chat.interf;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.efounder.chat.model.ChatTopFloatModel;

/**
 * 聊天界面上方显示的悬浮窗接口
 *
 * @author wang
 */
public class BaseChatTopFloatView<T> extends FrameLayout implements IChatTopFloatView<T> {

    private View parentView;

    public BaseChatTopFloatView(@NonNull Context context) {
        super(context);
    }

    public BaseChatTopFloatView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseChatTopFloatView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public View getCustomContentView() {
        return this;
    }

    @Override
    public void setParentView(View parentView) {
        this.parentView = parentView;
        ((ViewGroup) parentView).addView(this);
    }

    @Override
    public View getParentView() {
        return parentView;
    }

    @Override
    public void setData(ChatTopFloatModel<T> data) {
        setParentMargin(data.getLeftMargin(), data.getTopMargin(), data.getRightMargin(), data.getBottomMargin());
    }

    @Override
    public void show() {
        ((IChatTopFloatView) parentView).show();
    }

    @Override
    public void hide() {
        ((IChatTopFloatView) parentView).hide();
    }

    @Override
    public void setParentMargin(int left, int top, int right, int bottom) {
        ((IChatTopFloatView) parentView).setParentMargin(left, top, right, bottom);
    }
}
