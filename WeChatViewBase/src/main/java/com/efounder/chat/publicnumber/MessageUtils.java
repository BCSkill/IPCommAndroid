package com.efounder.chat.publicnumber;

//import android.util.Log;
//
//import com.efounder.chat.publicnumber.PublicChatSenderFragment.SendMessageCallback;
//import com.efounder.chat.struct.MessageChildTypeConstant;
//import com.efounder.chat.struct.StructFactory;
//import com.efounder.chat.utils.FileSizeUtil;
//import com.qiniu.android.http.ResponseInfo;
//import com.qiniu.android.storage.UpCancellationSignal;
//import com.qiniu.android.storage.UpCompletionHandler;
//import com.qiniu.android.storage.UpProgressHandler;
//import com.qiniu.android.storage.UploadManager;
//import com.qiniu.android.storage.UploadOptions;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.util.HashMap;

/**
 * 七牛上传工具类
 * 上传视频、图片、语音 返回在线url 并封装struct
 * Created by long on 2016/7/27.
 */

//public class MessageUtils {

//    private static final String TAG ="PublicMessageUtils";
//    public static final String MSG_BASE_URL = "http://oap8mw8pg.bkt.clouddn.com/";
//    private static UploadManager uploadManager = new UploadManager();
//
//    public static void sendMsg(final String msgPath, final String property, final SendMessageCallback sendMessageCallBack, final int messagechildType){
//        final String token = "tyrO2o43Y96uUq5bGwNEkVhZfahYPn2V81XfFEzy:ab1lSJZP_0mvXcXOnp_9cxsEcLQ=:eyJzY29wZSI6InRlc3QiLCJkZWFkbGluZSI6MzI0NzU4MzQ0MX0=";
////         final String token = "tyrO2o43Y96uUq5bGwNEkVhZfahYPn2V81XfFEzy:A8T9tFNql-YYgE30md3bjHlQPU4=:eyJzY29wZSI6InRlc3QiLCJwZXJzaXN0ZW50Tm90aWZ5VXJsIjoiIiwiZGVhZGxpbmUiOjMyNDc2MDgyNTQsInBlcnNpc3RlbnRPcHMiOiJhdnRodW1iL21wNHxzYXZlYXMvY1dKMVkydGxkRHB4YTJWNSJ9";
//        HashMap<String, String> map = new HashMap<String, String>();
//        map.put("x:phone", "88888888");
////        if(MessageChildTypeConstant.subtype_location==messagechildType){
////        	LocationInfo curLocationInfo = new Gson().fromJson(msgPath, LocationInfo.class);
////
////        }
//        Log.d(TAG, "---msgPath---"+msgPath);
//        try {
//            byte[] data = FileSizeUtil.FileToByte(msgPath);
//            if(data==null){
//                Log.i(TAG,"data is null");
//                return;
//            }
//            uploadManager.put(data, null, token,
//                    new UpCompletionHandler() {
//                        @Override
//                        public void complete(String key, ResponseInfo info, JSONObject res) {
//                            if(info.isOK()){
//                                try {
//                                    String resKey =  res.getString("key");
//                                    /**图片、视频、语音等资源路径*/
//									String resUrl =MSG_BASE_URL +resKey;
//									getStruct(resUrl, msgPath, property,sendMessageCallBack, messagechildType);
//                                    Log.i(TAG, resUrl);
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }
//                    },new UploadOptions(map, null, false,
//                            new UpProgressHandler() {
//                                @Override
//                                public void progress(String key, double percent) {
//                                    int progress = (int)(percent*1000);
//                                    if(progress==1000){
//                                    }
//                                }
//                            }, new UpCancellationSignal() {
//                        @Override
//                        public boolean isCancelled() {
//                            return false;
//                        }
//                    }));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    private static void getStruct(String resUrl,String path,String property,SendMessageCallback sendMessageCallBack,int messagechildType){
//    	switch (messagechildType) {
//		case MessageChildTypeConstant.subtype_image:
//			sendMessageCallBack.sendMessage(StructFactory.getInstance().createImageStruct(resUrl,path,property));
//			break;
//		case MessageChildTypeConstant.subtype_voice:
//			sendMessageCallBack.sendMessage(StructFactory.getInstance().createVoiceStruct(resUrl,path,property));
//			break;
//		case MessageChildTypeConstant.subtype_smallVideo:
//			sendMessageCallBack.sendMessage(StructFactory.getInstance().createVideoStruct(resUrl,path,property));
//			break;
//		case MessageChildTypeConstant.subtype_location:
//			sendMessageCallBack.sendMessage(StructFactory.getInstance().createMapStruct(path));
//			break;
//		default:
//			break;
//		}
//    }
//}
