package com.efounder.chat.publicnumber.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;

import java.util.List;

/**
 * Created by XinQing on 2016/12/26.
 */

public class AppAccountFormStateSearchAdapter extends RecyclerView.Adapter<AppAccountFormStateSearchAdapter.MyViewHolder> {
    private Context context;
    private List<DataItem> dataItems;

    public AppAccountFormStateSearchAdapter(Context context,List<DataItem> dataItems) {
        this.context = context;
        this.dataItems = dataItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_app_account_form_state_search, parent,
                false));
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        DataItem dataItem = dataItems.get(position);
        holder.imageView_badge.setSelected(1 == dataItem.getSelectionState());
        holder.imageView_car.setVisibility(1 == dataItem.getSelectionState()? View.VISIBLE : View.INVISIBLE);
        int color = context.getResources().getColor(1 == dataItem.getSelectionState()?R.color.chat_red:R.color.chat_gray_dark);
        holder.textView_time.setText(dataItem.getTime());
        holder.textView_time.setTextColor(color);
        holder.textView_description.setText(dataItem.getDescription());
        holder.textView_description.setTextColor(color);
    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView_badge;
        ImageView imageView_car;
        TextView textView_time;
        TextView textView_description;


        public MyViewHolder(View view){
            super(view);
            imageView_badge = (ImageView) view.findViewById(R.id.imageView_badge);
            imageView_car = (ImageView) view.findViewById(R.id.imageView_car);
            textView_time = (TextView) view.findViewById(R.id.textView_time);
            textView_description = (TextView) view.findViewById(R.id.textView_description);
        }
    }

    public static class DataItem{
        public int selectionState;
        public String time;
        public String description;

        public int getSelectionState() {
            return selectionState;
        }

        public void setSelectionState(int selectionState) {
            this.selectionState = selectionState;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

}
