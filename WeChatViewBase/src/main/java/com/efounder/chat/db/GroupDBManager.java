package com.efounder.chat.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.efounder.chat.model.Group;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.message.db.MessageDBHelper;
import com.efounder.mobilecomps.contacts.ChineseCharacterUtils;
import com.efounder.mobilecomps.contacts.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

public class GroupDBManager {

    private SQLiteDatabase db;
    private MessageDBHelper mHelper;// 创建DBOpenHelper对象
    private SimpleDateFormat dateFormat = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());
    private int loginUserId;
    private static final String TAG = "GroupDBManager";

    /**
     * //群组数据库管理类
     *
     * @param
     * @param
     */
    public GroupDBManager() {
        this.db = GetDBHelper.getInstance().getDb();
        this.loginUserId = Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID, "0")).intValue();
    }

    /**
     * 插入单个群组
     *
     * @param group
     * @return
     */
    public long insertOrUpdate(Group group) {
        ContentValues values = getGroupContentValues(group);
        ContentValues myGroupValues = getMyGroupContentValues(group);
        List<User> users = group.getUsers();
        if (users != null) {
//            for (User user : users) {
//                ContentValues groupUserValue =
//                        getGroupUsersContentValues(group.getGroupId(), user);
//                this.db.insertWithOnConflict("GROUPUSERS", null, groupUserValue,
//                        SQLiteDatabase.CONFLICT_REPLACE);
//            }
            insertOrUpdateUser(group.getGroupId(), users);
        }
        this.db.insertWithOnConflict("mygroup", null, myGroupValues,
                SQLiteDatabase.CONFLICT_REPLACE);
        return this.db.insertWithOnConflict("Groups", null, values,
                SQLiteDatabase.CONFLICT_REPLACE);

    }

    /**
     * 插入单个群组，但是不保存到mygroup数据库中
     */
    public long insertOrUpdateExceptMyGroup(Group group) {
        ContentValues values = getGroupContentValues(group);
        ContentValues myGroupValues = getMyGroupContentValues(group);
        List<User> users = group.getUsers();
        if (users != null) {
            for (User user : users) {
                ContentValues groupUserValue =
                        getGroupUsersContentValues(group.getGroupId(), user);
                this.db.insertWithOnConflict("GROUPUSERS", null, groupUserValue,
                        SQLiteDatabase.CONFLICT_REPLACE);

            }
        }
        return this.db.insertWithOnConflict("Groups", null, values,
                SQLiteDatabase.CONFLICT_REPLACE);
    }

    /**
     * 插入或更新多个群组
     *
     * @param groups
     */
//    public void insertOrUpdate(List<Group> groups) {
//        this.db.beginTransaction();
//        for (Iterator<Group> localIterator = groups.iterator(); localIterator
//                .hasNext(); ) {
//            Group group = (Group) localIterator.next();
//            insertOrUpdate(group);
//        }
//        this.db.setTransactionSuccessful();
//        this.db.endTransaction();
//    }

    /**
     * 插入单个群组
     *
     * @param group
     * @return
     */
    public long insert(Group group) {
        ContentValues values = getGroupContentValues(group);
        ContentValues myGroupValues = getMyGroupContentValues(group);
        List<User> users = group.getUsers();
        if (users != null) {
//            for (User user : users) {
//                ContentValues groupUserValue =
//                        getGroupUsersContentValues(group.getGroupId(), user);
//                this.db.insertWithOnConflict("GROUPUSERS", null, groupUserValue,
//                        SQLiteDatabase.CONFLICT_REPLACE);
//            }
            insertOrUpdateUser(group.getGroupId(), users);
        }
        if (queryByIdWithoutUser(group.getGroupId()) == null) {
            this.db.insert("mygroup", null, myGroupValues);
        }
        return this.db.insertWithOnConflict("Groups", null, values,
                SQLiteDatabase.CONFLICT_REPLACE);

    }


    /**
     * 插入多个群组(登录时获取自己的群组并保存)
     *
     * @param groups
     */
    public void insert(List<Group> groups) {
        long currentTime = System.currentTimeMillis();

        this.db.beginTransaction();
        for (Iterator<Group> localIterator = groups.iterator(); localIterator
                .hasNext(); ) {
            Group group = (Group) localIterator.next();
            insert(group);
        }
        this.db.setTransactionSuccessful();
        this.db.endTransaction();
        Log.i(TAG, "保存我的群组sql执行时间:" + (System.currentTimeMillis() - currentTime) + "ms");
    }


    /**
     * 向群组中插入联系人
     *
     * @param
     * @return
     */
    public void insertOrUpdateUser(int groupId, int userId) {
        User user = new User();
        user.setId(userId);
        user.setOwnGroupRemark("");
        user.setOtherGroupRemark("");
        ContentValues groupUserValues =
                getGroupUsersContentValues(groupId, user);
        this.db.insertWithOnConflict("GROUPUSERS", null, groupUserValues,
                SQLiteDatabase.CONFLICT_REPLACE);

//        this.db.execSQL("insert or replace into GroupUsers values(" + groupId
//                + "," + userId + ",1);");

    }

    /**
     * 向群组中插入联系人
     *
     * @param
     * @return
     */
    public void insertOrUpdateUser(int groupId, User user) {

        ContentValues groupUserValues =
                getGroupUsersContentValues(groupId, user);
        this.db.insertWithOnConflict("GROUPUSERS", null, groupUserValues,
                SQLiteDatabase.CONFLICT_REPLACE);

//        this.db.execSQL("insert or replace into GroupUsers values(" + groupId
//                + "," + userId + ",1);");

    }

    /**
     * 向群组中插入联系人
     *
     * @param
     * @return
     */
    public void insertOrUpdateUser(int groupId, List<User> list) {


        long currentTime = System.currentTimeMillis();
//        this.db.beginTransaction();
//        for (Iterator<User> localIterator = list.iterator(); localIterator
//                .hasNext(); ) {
//            User user = (User) localIterator.next();
//            insertOrUpdateUser(groupId, user);
//        }
//        this.db.setTransactionSuccessful();
//        this.db.endTransaction();

        //sql优化2018.3.5
        String sql = "insert or replace into GROUPUSERS(groupid,userid,groupUserType,ownRemark,otherRemark,enable) " +
                " values(?,?,?,?,?,?)";
        SQLiteStatement stat = db.compileStatement(sql);
        db.beginTransaction();
        for (User user : list) {
            stat.bindLong(1, groupId);
            stat.bindLong(2, user.getId());
            stat.bindLong(3, user.getGroupUserType());
            stat.bindString(4, user.getOwnGroupRemark() == null ? "" : user.getOwnGroupRemark());
            stat.bindString(5, user.getOtherGroupRemark() == null ? "" : user.getOtherGroupRemark());
            stat.bindLong(6, 1);
            stat.executeInsert();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        Log.i(TAG, System.currentTimeMillis() - currentTime + "ms sql执行时间");
    }

    public void deleteGroupUser(int groupId, int userId) {
        this.db.execSQL("update  GroupUsers set enable = 0 where groupId = "
                + groupId + " and userId = " + userId + ";");
    }

    public void deleteAllGroupUser(int groupId) {
        this.db.execSQL("update  groupusers  set enable =0 where groupid =" + groupId);
    }


    /**
     * 通过id查询我的群组是否存在
     *
     * @param id
     * @return
     */
    public boolean queryMyGroupExist(int id) {
        String sql = "select * from MyGroup where  MyGroup.groupId = " + id
                + "  and MyGroup.enable=1;";
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                return true;
            }
            return false;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

    }

    /**
     * 通过id查询群组
     *
     * @param id
     * @return
     */
    public Group queryByIdWithUser(int id) {
        String sql = "select * from Groups,MyGroup where Groups.groupid = "
                + id + " and MyGroup.groupId = " + id
                + " and MyGroup.loginUserId =" + loginUserId +
                " and Groups.enable =1 and MyGroup.enable=1;";

        List<?> list = query(sql);
        if (list.size() > 0) {
//            String sql1 = "select * from user,groupUsers where user.userId = groupusers.userid " +
//                    "and groupid = " + id + " and groupusers.enable = 1;";
            String sql1 = "select * from groupUsers as a left join user as b on  a.userId = b.userid " +
                    " where  a.groupid = " + id + " and a.enable = 1;";
            List<User> users = queryUsers(sql1, null);
            ((Group) list.get(0)).setUsers(users);
        }
        return ((list.size() == 0) ? null : (Group) list.get(0));

    }

    /**
     * 通过id查询群组(不查询群成员)
     *
     * @param id
     * @return
     */
    public Group queryByIdWithoutUser(int id) {
        String sql = "select * from Groups,MyGroup where Groups.groupid = "
                + id + " and MyGroup.groupId = " + id
                + " and MyGroup.loginUserId =" + loginUserId +
                " and Groups.enable =1 and MyGroup.enable=1;";

        List<?> list = query(sql);
        if (list.size() > 0) {
            ((Group) list.get(0)).setUsers(new ArrayList<User>());
        }
        return ((list.size() == 0) ? null : (Group) list.get(0));

    }

    /**
     * 通过关键字查询群组(不查询群成员)
     *
     * @param keyword
     * @return
     */
    public List<Group> queryGroupByKeyword(String keyword) {
        keyword = "%" + keyword + "%";

        String sql = "select a.*,b.* from  MyGroup as a " +
                "left join groups as b on a.groupid =b.groupid" +
                " where  a.enable=1 and (a.groupid like ? "
                + " or b.groupname like ? or b.py_initial like ? or b.py_quanpin like ?  )";

        List<Group> list = query(sql, new String[]{keyword, keyword, keyword, keyword});

        return list;

    }

    /**
     * 通过id查询组，查询Groups数据库，不查询MyGroup数据库
     */
    public Group queryByIdWithUserInGroups(int id, boolean withUser) {
        String sql = "select * from Groups where Groups.groupid = "
                + id + " and Groups.enable =1;";
        List<?> list = queryGroups(sql, null);
        if (list.size() > 0) {

            String sql1 = "select * from groupUsers as a left join user as b on  a.userId = b.userid " +
                    "where a.groupid = " + id + " and a.enable = 1;";
            if (withUser) {
                List<User> users = queryUsers(sql1, null);
                ((Group) list.get(0)).setUsers(users);
            } else {
                ((Group) list.get(0)).setUsers(new ArrayList<User>());
            }
        }
        return ((list.size() == 0) ? null : (Group) list.get(0));

    }

    /**
     * 查询所有群组
     *
     * @param withUser 是否加载人员
     * @return
     */
    public List<Group> queryAll(boolean withUser) {
//        String sql = "select * from Groups,mygroup where  mygroup.loginUserId = " + loginUserId +
//                " and Groups.groupId = mygroup.groupId "
//                + " and mygroup.enable = 1 order by Groups.createTime desc;";
        String sql = "select * from mygroup as a left join  Groups as b  on a.groupid =b.groupid where  a.loginUserId = " + loginUserId
                + " and b.enable = 1 order by b.createTime desc;";
        List<Group> list = query(sql);
        for (Group group : list) {
            if (group.getAvatar() == null) {
                group.setAvatar("");
            }

//            String sql1 = "select * from user,groupUsers where user.userId = groupusers.userid " +
//                    "and groupid = " + group.getGroupId() + " and groupusers.enable = 1;";
            String sql1 = "select * from groupUsers as a left join user as b on  a.userId = b.userid " +
                    "where a.groupid = " + group.getGroupId() + " and a.enable = 1;";
            if (withUser) {
                List<User> users = queryUsers(sql1, null);
                group.setUsers(users);
            } else {
                group.setUsers(new ArrayList<User>());
            }

        }
        return list;

    }

    /**
     * 获取群组中的某个人员信息
     *
     * @param groupId
     * @param userId
     * @return
     */
    public User getGroupUser(int groupId, int userId) {
        String sql1 = "select * from groupUsers as a left join user as b on  b.userId = a.userid " +
                "where a.groupid = " + groupId + " and  a.userid = " + userId + " and a.enable = 1;";

        List<User> users = queryUsers(sql1, null);
        return ((users.size() == 0) ? null : (User) users.get(0));

    }

    /**
     * 获取群组是否包含某个人
     *
     * @param groupId
     * @param userId
     * @return
     */
    public boolean queryGroupContainUser(int groupId, int userId) {
        boolean isContain = false;
        String sql = "select * from groupUsers where userid=? and  groupid=? and groupUsers.enable = ?;";
        String[] selectionArgs = new String[]{userId + "", groupId + "", 1 + ""};
        Cursor cursor = this.db.rawQuery(sql, selectionArgs);
        while (cursor.moveToNext()) {
            isContain = true;
        }
        cursor.close();
        return isContain;


    }

    /**
     * 退群 自己退群，更改两个表， 其他人退群，更改一个表
     *
     * @param userId
     * @param groupId
     */
    public void quitGroup(int userId, int groupId) {
        String sql1 = "update groupUsers set enable = 0 where  groupid =" + groupId + " and " +
                "userid =" + userId + " ;";
//        String sql2 = "update mygroup set enable = 0 where groupId =" + groupId + " and " +
//                "loginUserId=" + userId + ";";
        String sql2 = "delete from mygroup where groupid =" + groupId + " and " +
                "loginUserId=" + userId + " ;";
        db.execSQL(sql1);
        if (userId == loginUserId) {
            db.execSQL(sql2);
        }


    }

    /**
     * 更新群组置顶或者免打扰(单个)
     *
     * @param group
     * @return
     */
    public int updateIsTopOrBother(Group group) {
        ContentValues values = getMyGroupContentValues2(group);
        String whereClause = "groupId=?  and loginUserId = ?";


        String[] whereArgs = {String.valueOf(group.getGroupId()), String.valueOf(group
                .getLoginUserId())};
        return this.db.update("mygroup", values, whereClause, whereArgs);
    }

    // /**
    // * 更新群组(list)
    // *
    // * @param
    // * @return
    // */
    // public void update(List<Group> list) {
    // this.db.beginTransaction();
    // for (Iterator<Group> localIterator = list.iterator(); localIterator
    // .hasNext();) {
    // Group group = (Group) localIterator.next();
    // update(group);
    // }
    // this.db.setTransactionSuccessful();
    // this.db.endTransaction();
    // }

    /**
     * 删除群组（单个）
     *
     * @param groupId
     * @return
     */
    public void delete(int groupId) {
        // String whereClause = "groupId=?";
        // String[] whereArgs = { String.valueOf(group.getGroupId()) };
        // this.db.delete("groupinfo", whereClause, whereArgs);
        // //this.db.execSQL("update group set enable = 0 where groupId ="+group.getGroupId()+";");
        // return this.db.delete("group", whereClause, whereArgs);
        // //return 1;
        this.db.execSQL("update mygroup set enable = 0 where loginUserid="
                + loginUserId + " and groupId=" + groupId + ";");

    }

    public List<Group> query(String sql) {
        return query(sql, null);
    }

    public List<Group> query(String sql, String[] selectionArgs) {
        List<Group> list = new ArrayList<Group>();
        Cursor cursor = this.db.rawQuery(sql, selectionArgs);
        while (cursor.moveToNext()) {
            Group group = new Group();

            group.setGroupId(cursor.getInt(cursor.getColumnIndex("groupId")));
            group.setLoginUserId(cursor.getInt(cursor
                    .getColumnIndex("loginUserId")));
            group.setGroupName(cursor.getString(cursor
                    .getColumnIndex("groupName")));
            group.setCreateId(cursor.getInt(cursor
                    .getColumnIndex("createUserId")));
            group.setAvatar(cursor.getString(cursor.getColumnIndex("avatar")));
            group.setIsBother(cursor.getInt(cursor.getColumnIndex("isBother")) == 1 ? true
                    : false);
            group.setIsTop(cursor.getInt(cursor.getColumnIndex("isTop")) == 1 ? true
                    : false);
            group.setGroupType(cursor.getInt(cursor.getColumnIndex("group_type")));
            group.setBanSpeak(cursor.getInt(cursor.getColumnIndex("no_speak")) == 1);
            group.setEnglihName(cursor.getString(cursor.getColumnIndex("english_name")));
            group.setRecommond(cursor.getString(cursor.getColumnIndex("recommend")));
            try {
                Date date = this.dateFormat.parse(cursor.getString(cursor
                        .getColumnIndex("createTime")));
                group.setCreateTime(date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            list.add(group);

        }

        cursor.close();
        return list;
    }

    public List<Group> queryGroups(String sql, String[] selectionArgs) {
        List<Group> list = new ArrayList<Group>();
        Cursor cursor = this.db.rawQuery(sql, selectionArgs);
        while (cursor.moveToNext()) {
            Group group = new Group();

            group.setGroupId(cursor.getInt(cursor.getColumnIndex("groupId")));
            group.setGroupName(cursor.getString(cursor
                    .getColumnIndex("groupName")));
            group.setCreateId(cursor.getInt(cursor
                    .getColumnIndex("createUserId")));
            group.setAvatar(cursor.getString(cursor.getColumnIndex("avatar")));
            group.setGroupType(cursor.getInt(cursor.getColumnIndex("group_type")));
            group.setBanSpeak(cursor.getInt(cursor.getColumnIndex("no_speak")) == 1);
            group.setEnglihName(cursor.getString(cursor.getColumnIndex("english_name")));
            group.setRecommond(cursor.getString(cursor.getColumnIndex("recommend")));
            try {
                Date date = this.dateFormat.parse(cursor.getString(cursor
                        .getColumnIndex("createTime")));
                group.setCreateTime(date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            list.add(group);

        }

        cursor.close();
        return list;
    }

    public List<User> queryUsers(String sql, String[] selectionArgs) {
        List<User> list = new ArrayList<User>();
        Cursor cursor = this.db.rawQuery(sql, selectionArgs);
        while (cursor.moveToNext()) {
            User user = new User();
            user.setId(cursor.getInt(cursor.getColumnIndex("userId")));
            user.setName(cursor.getString(cursor.getColumnIndex("name")));
            user.setNickName(cursor.getString(cursor.getColumnIndex("nickName")));
            user.setSex(cursor.getString(cursor.getColumnIndex("sex")));
            user.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
            user.setMobilePhone(cursor.getString(cursor.getColumnIndex("mobilephone")));
            user.setAvatar(cursor.getString(cursor.getColumnIndex("avatar")));
            user.setSigNature(cursor.getString(cursor
                    .getColumnIndex("sigNature")));
            user.setEmail(cursor.getString(cursor.getColumnIndex("email")));
            user.setDeptId(cursor.getInt(cursor.getColumnIndex("deptId")));
            user.setType(cursor.getInt(cursor.getColumnIndex("type")));
            user.setOwnGroupRemark(cursor.getString(cursor.getColumnIndex("ownRemark")));
            user.setOtherGroupRemark(cursor.getString(cursor.getColumnIndex("otherRemark")));
            user.setGroupUserType(cursor.getInt(cursor.getColumnIndex("groupUserType")));

            String wechatQrUrl = cursor.getString(cursor.getColumnIndex("wechatQrUrl"));
            String zhifubaoQrUrl = cursor.getString(cursor.getColumnIndex("zhifubaoQrUrl"));
            user.setWeixinQrUrl((wechatQrUrl == null || "".equals(wechatQrUrl)) ? null : wechatQrUrl);
            user.setZhifubaoQrUrl((zhifubaoQrUrl == null || "".equals(zhifubaoQrUrl)) ? null : zhifubaoQrUrl);
            user.setGroupBanSpeak(cursor.getInt(cursor.getColumnIndex("g_user_no_speak")) == 1);
            user.setAllowStrangerChat(cursor.getInt(cursor.getColumnIndex("stranger_chat")) == 1);
            user.setPlanet(cursor.getColumnIndex("planet") == -1 ? "" :
                    cursor.getString(cursor.getColumnIndex("planet")));
            if (user.getAvatar() == null) {
                user.setAvatar("");
            }
            if (user.getId() != 0) {
                list.add(user);
            }
        }

        cursor.close();
        return list;
    }


    /**
     * 查询群组中某个用户的全部信息
     *
     * @param groupId
     * @param userId
     * @return
     */
    public User queryGroupUser(int groupId, int userId) {

//		String sql = "select * from GroupUsers left join User on GroupUsers.userID = User.userID " +
//        "left join Friends on Friends.[userId] = GroupUsers.[userId] " +
//                "where groupID = ? and GroupUsers.userID = ? " +
//                "and (loginUserId=" + loginUserId + " or loginUserId is null)";
        String sql1 = "select * from groupusers where groupId = " + groupId + " and userId =" + userId + ";";
        User user = queryUserAllInfo(sql1);
        return user;

    }

    private User queryUserAllInfo(String sql) {

        Cursor cursor = this.db.rawQuery(sql, null);
        List<User> list = new ArrayList<User>();
        while (cursor.moveToNext()) {
            User user = new User();
            user.setId(cursor.getInt(cursor.getColumnIndex("userId")));
//			user.setName(cursor.getString(cursor.getColumnIndex("name")));
//			user.setNickName(cursor.getString(cursor.getColumnIndex("nickName")));
//			user.setSex(cursor.getString(cursor.getColumnIndex("sex")));
//			user.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
//			user.setMobilePhone(cursor.getString(cursor.getColumnIndex("mobilephone")));
//			user.setAvatar(cursor.getString(cursor.getColumnIndex("avatar")));
//			user.setSigNature(cursor.getString(cursor
//					.getColumnIndex("sigNature")));
//			user.setEmail(cursor.getString(cursor.getColumnIndex("email")));
//			user.setDeptId(cursor.getInt(cursor.getColumnIndex("deptId")));
//			user.setType(cursor.getInt(cursor.getColumnIndex("type")));
            user.setOwnGroupRemark(cursor.getString(cursor.getColumnIndex("ownRemark")));
            user.setOtherGroupRemark(cursor.getString(cursor.getColumnIndex("otherRemark")));
            user.setGroupUserType(cursor.getInt(cursor.getColumnIndex("groupUserType")));
            user.setGroupBanSpeak(cursor.getInt(cursor.getColumnIndex("g_user_no_speak")) == 1);
            //user.setReMark(cursor.getString(cursor.getColumnIndex("remark")));
            list.add(user);

        }

        cursor.close();

        return list.size() == 0 ? null : list.get(0);

    }

    //groups表
    private ContentValues getGroupContentValues(Group group) {
        ContentValues values = new ContentValues();
        Group tempGroup = queryByIdWithUser(group.getGroupId());
        values.put("groupId", group.getGroupId());
        values.put("createUserId", group.getCreateId());
        values.put("groupName", group.getGroupName());
        values.put("py_initial", ChineseCharacterUtils.getJianPin(group.getGroupName()));
        values.put("py_quanpin", ChineseCharacterUtils.getQuanPin(group.getGroupName()));
        values.put("createTime", new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault())
                .format(new Date(group.getCreateTime())));
        if (group.getAvatar().equals("")) {
            if (tempGroup != null && tempGroup.getAvatar() != null && !tempGroup.getAvatar().equals("")) {
                values.put("avatar", tempGroup.getAvatar());
            } else {
                values.put("avatar", group.getAvatar());
            }
        } else {
            values.put("avatar", group.getAvatar());
        }

        values.put("no_speak", group.isBanSpeak() ? 1 : 0);
        values.put("group_type", group.getGroupType());
        values.put("english_name", group.getEnglihName() == null ? "" : group.getEnglihName());
        values.put("recommend", group.getRecommond() == null ? "" : group.getRecommond());
        values.put("enable", 1);

        return values;
    }

    //groupuser表
    private ContentValues getGroupUsersContentValues(int groupId, User user) {
        ContentValues values = new ContentValues();
        values.put("groupId", groupId);
        values.put("userId", user.getId());
        values.put("groupUserType", user.getGroupUserType());
        if (user.getOwnGroupRemark() != null) {
            values.put("ownRemark", user.getOwnGroupRemark());
        }
        if (user.getOtherGroupRemark() != null) {
            values.put("otherRemark", user.getOtherGroupRemark());
        }
        values.put("enable", 1);
        values.put("g_user_no_speak", user.isGroupBanSpeak() ? 1 : 0);
        return values;
    }

    //mygroup表
    private ContentValues getMyGroupContentValues(Group group) {
        ContentValues values = new ContentValues();
        values.put("groupId", group.getGroupId());
        values.put("loginUserId", group.getLoginUserId());
        values.put("isBother", group.getIsBother() == true ? 1 : 0);
        values.put("isTop", group.getIsTop() == true ? 1 : 0);
        values.put("enable", 1);

        return values;
    }

    //mygroup表
    private ContentValues getMyGroupContentValues2(Group group) {
        ContentValues values = new ContentValues();
        values.put("groupId", group.getGroupId());
        values.put("loginUserId", group.getLoginUserId());
        values.put("isBother", group.getIsBother() == true ? 1 : 0);
        values.put("isTop", group.getIsTop() == true ? 1 : 0);
        values.put("enable", 1);

        return values;
    }

    /**
     * 关闭数据库连接
     */
    public void close() {
        if (mHelper != null) {
            mHelper.close();
        }

    }
}