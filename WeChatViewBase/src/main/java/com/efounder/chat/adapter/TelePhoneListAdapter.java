package com.efounder.chat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.model.ChatListItem;
import com.efounder.chat.utils.ChatDateUtils;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.widget.BadgeView;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.message.struct.IMStruct002;
import com.efounder.mobilecomps.contacts.User;
import com.groupimageview.NineGridImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * 聊天列表适配器
 *
 * @author yqs
 */
public class TelePhoneListAdapter extends BaseAdapter {

    private Context mContext;
    private List<ChatListItem> datas = null;


//    private ImageLoader imageLoader;
//    private DisplayImageOptions options;
    private SimpleDateFormat dateFormat;

    public TelePhoneListAdapter(Context context, List<ChatListItem> datas) {

        super();

        if (datas == null) {
            datas = new ArrayList<ChatListItem>(0);
        }
        this.datas = datas;
        mContext = context;
        dateFormat = new SimpleDateFormat("HH:mm",
                Locale.getDefault());// 设置日期格式
    }



    @Override
    public int getItemViewType(int position) {

        //ChatListItem messageItem = datas.get(position);
        return 0;

    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final ChatListItem messageItem = datas.get(position);

        if (convertView == null) {
            LayoutInflater listContainer;   //视图容器工厂

            listContainer = LayoutInflater.from(mContext); //创建视图容器工厂并设置上下文
            convertView = listContainer.inflate(R.layout.chatlist_item, null);   //创建list_item.xml布

            holder = new ViewHolder();

            //item
            //消息item
            String TX_type= EnvironmentVariable.getProperty("TX_type","Square");
            if("Round".equals(TX_type)) {
                holder.icon = (ImageView) convertView.findViewById(R.id.icon1);
            }else{
                holder.icon = (ImageView) convertView.findViewById(R.id.icon2);
            }
//            holder.icon = (ImageView) convertView.findViewById(R.id.icon2);
            holder.icon.setVisibility(ImageView.VISIBLE);
            holder.iconGroup = (NineGridImageView) convertView.findViewById(R.id.icon_group);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.msg = (TextView) convertView.findViewById(R.id.msg);
            holder.time = (TextView) convertView.findViewById(R.id.time);
            holder.sendState = (ImageView) convertView.findViewById(R.id.sending);
            holder.chatListbagLayout = (RelativeLayout) convertView
                    .findViewById(R.id.chatlist_bag);
            holder.infoBother = (ImageView) convertView
                    .findViewById(R.id.infobother);
            holder.iconcontainerforbadage = (LinearLayout) convertView
                    .findViewById(R.id.iconcontainerforbadage);
            holder.badge = new BadgeView(mContext, holder.iconcontainerforbadage);


            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        User user = WeChatDBManager.getInstance().getOneUserById(messageItem.getUserId());

        holder.iconGroup.setVisibility(View.GONE);
        if(holder.icon!=null) {
            holder.icon.setVisibility(View.VISIBLE);
            if (messageItem.getAvatar() != null && messageItem.getAvatar().contains("http")) {
//                imageLoader.displayImage(URLModifyDynamic.getInstance().replace(messageItem.getAvatar()) , holder.icon, options);
                LXGlideImageLoader.getInstance().showUserAvatar(mContext,holder.icon,messageItem.getAvatar());
            } else {
//                imageLoader.displayImage("", holder.icon, options);
                LXGlideImageLoader.getInstance().showUserAvatar(mContext,holder.icon,"");

            }
        }

        if (position ==0 || position ==2 || position == 6){
            holder.infoBother.setImageDrawable(mContext.getResources().getDrawable(R.drawable.icon_phone_out));
        }else {
            holder.infoBother.setImageDrawable(mContext.getResources().getDrawable(R.drawable.icon_phone_enter));
        };



        //FIXME 设置消息状态
        if (messageItem.getStruct002().getState()== IMStruct002.MESSAGE_STATE_SENDING){
            holder.sendState.setVisibility(View.VISIBLE);
        }else{
            holder.sendState.setVisibility(View.GONE);
        }
        //FIXME 设置时间
        holder.time.setText(ChatDateUtils.getTimestampString(new Date(
                messageItem.getStruct002().getTime())));
        //	holder.time.setText(dateFormat.format(messageItem.getStruct002().getTime()));
        holder.sendState.setVisibility(View.GONE);

        holder.title.setText(messageItem.getName());
        if (user.getMobilePhone() != null && !user.getMobilePhone().equals("")){
            holder.msg.setText(user.getMobilePhone());
        }else{
            holder.msg.setText("");
        }


//        // FIXME 根据消息类型设置字体颜色和标题
//        int type = messageItem.getStruct002().getToUserType();
//        switch (type) {
//            case StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT:
//
//                break;
//
//            case StructFactory.TO_USER_TYPE_GROUP:
//                holder.title.setTextColor(mContext.getResources().getColor(
//                        R.color.black));
//                holder.title.setText(messageItem.getName());
//                break;
//            case StructFactory.TO_USER_TYPE_PERSONAL:
//                if (userType == 1) {
//                    holder.title.setTextColor(mContext.getResources().getColor(
//                            R.color.publicnumber_textColor));
//                    holder.title.setText(messageItem.getName());
//                } else {
        //       holder.title.setText(messageItem.getName());
//                    holder.title.setTextColor(mContext.getResources().getColor(
//                            R.color.black));
//                }
//                break;
//        }






        return convertView;
    }



    class ViewHolder {

        public TextView read;
        public TextView delete;
        public Button detail;
        public ImageView icon;
        public NineGridImageView iconGroup;
        public TextView title;
        public TextView msg;
        public TextView time;
        public ImageView infoBother;// 消息免打扰图标
        public ImageView sendState;//消息发送状态
        public LinearLayout iconcontainerforbadage;
        public RelativeLayout chatListbagLayout;
        public BadgeView badge;
        public TextView setTop;

    }


    public void collapse(final View view, Animation.AnimationListener al) {
        final int originHeight = view.getMeasuredHeight();

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime,
                                               Transformation t) {
                if (interpolatedTime == 1.0f) {

                    view.setVisibility(View.GONE);
                } else {
                    view.getLayoutParams().height = originHeight
                            - (int) (originHeight * interpolatedTime);
                    view.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        if (al != null) {
            animation.setAnimationListener(al);
        }
        animation.setDuration(300);
        view.startAnimation(animation);
    }



}
