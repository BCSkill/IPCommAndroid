package com.efounder.chat.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.model.PoiLocation;

import java.util.List;

/**
 * Created by zjk on 2017/6/5.
 * Desc:
 */

public class PoiAdapter extends BaseAdapter {

    private String tag;//入口标识
    private List<PoiLocation> list;
    private Context context;
    private LayoutInflater inflater;
    public int index;

    public PoiAdapter(Context context, List<PoiLocation> list) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public PoiAdapter(Context context, List<PoiLocation> list, String tag) {
        this.tag = tag;
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_chat_group_poi, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.tv_chat_group_title);
            holder.address = (TextView) convertView.findViewById(R.id.tv_chat_group_address);
            holder.selecImg = (ImageView) convertView.findViewById(R.id.iv_chat_group_click);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String title = list.get(position).getTitle();
        String address = list.get(position).getAddress();
        if (position == 0 && null == tag) {
            holder.title.setText(R.string.chat_layout_location);
        } else if (!TextUtils.isEmpty(title)) {
            holder.title.setText(title);
        }

        if (!TextUtils.isEmpty(address))
            holder.address.setText(address);
        if ( null == tag) {
            if (position==index ){
                holder.selecImg.setVisibility(View.VISIBLE);
            }else {
                holder.selecImg.setVisibility(View.INVISIBLE);
            }
        }
        /*if (null == tag)
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.selecImg.setVisibility(View.VISIBLE);
            }
        });*/
        return convertView;
    }

    private class ViewHolder {
        TextView title, address;
        ImageView selecImg;
    }
}

