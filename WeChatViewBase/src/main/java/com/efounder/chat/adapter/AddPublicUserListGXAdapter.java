package com.efounder.chat.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.utils.ResStringUtil;

import java.util.List;

/**
 * 共享专用
 * 添加好友
 * @author kongmeng
 */
public class AddPublicUserListGXAdapter extends BaseAdapter {

    private List<User> users;
//    private ImageLoader imageLoader;
//    private DisplayImageOptions options;

    public AddPublicUserListGXAdapter(List<User> users) {
        super();
        this.users = users;
        //initImageLoader();
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public User getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        User user = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_addfriends_list_item1, null);
            viewHolder = new ViewHolder();
            viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textView_name);
            viewHolder.textViewAdName = (TextView) convertView.findViewById(R.id.textView_adname);
            viewHolder.textViewCompany = (TextView) convertView.findViewById(R.id.textView_company);
            viewHolder.textViewDeptName = (TextView) convertView.findViewById(R.id.textView_deptName);
            viewHolder.textViewTel = (TextView) convertView.findViewById(R.id.textView_tel);
            viewHolder.userAvatarImageView = (ImageView) convertView.findViewById(R.id.imageView1);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //viewHolder.userAvatarImageView.setImageResource(R.drawable.default_useravatar);
        showUserAvatar(viewHolder.userAvatarImageView, user);
        viewHolder.textViewName.setText(ResStringUtil.getString(R.string.wrchatview_name) + user.getNickName());
        viewHolder.textViewAdName.setText(ResStringUtil.getString(R.string.chat_account_number) + user.getId());
        viewHolder.textViewName.setText(user.getNickName());
        viewHolder.textViewAdName.setText( user.getId() + "");
        viewHolder.textViewCompany.setText(user.getCompany());
        viewHolder.textViewDeptName.setText(user.getBmmc());
        viewHolder.textViewTel.setText("");
        return convertView;
    }

    class ViewHolder {
        TextView textViewName;
        TextView textViewAdName;
        TextView textViewTel;
        TextView textViewCompany;
        TextView textViewDeptName;
        ImageView userAvatarImageView;
    }

    /**
     * 显示用户头像
     */

    public void showUserAvatar(ImageView imageView, User user) {
        if ("公众号".equals(user.getNickName())) {
            imageView
                    .setImageResource(R.drawable.icon_official_account);
        } else if ("群聊".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.icon_group_chat);
        } else if ("组织机构".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.icon_organization);
        } else if ("人力资源系统".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.rlzy);
        } else if ("内控系统".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.neikong);
        } else if ("固定资产系统".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.gdzc);
        } else if ("财务核算系统".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.czhs);
        } else if ("生产监控系统".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.scjk2);
        } else if ("生产日报系统".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.bj);
        } else if ("CRM系统".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.crm);
        } else if ("普联餐厅".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.ct);
        } else if ("普联健身".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.jianshen);
        } else if ("奥体中心".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.atzx);
        } else if ("乒乓球室".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.ppq);
        } else if ("篮球场".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.lanqiu);
        } else if ("足球场".equals(user.getNickName())) {
            imageView.setImageResource(R.drawable.zuqiu);
        }
//		else if ("新的朋友".equals(user.getNickName())) {
//			imageView.setImageResource(R.drawable.icon_official_account);
//		}else if ("中油铁工".equals((user.getNickName()))) {
//			imageView.setImageResource(R.drawable.zhongyoutg);
//		}
        else {
            if (user.getAvatar() != null && user.getAvatar().contains("http")) {
//                imageLoader.displayImage(URLModifyDynamic.getInstance().replace(user.getAvatar()), imageView, options);
                LXGlideImageLoader.getInstance().showRoundUserAvatar(imageView.getContext(),imageView,user.getAvatar(),LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_10);

            } else {
//                imageLoader.displayImage("", imageView, options);
                LXGlideImageLoader.getInstance().showRoundUserAvatar(imageView.getContext(),imageView,"",LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_10);

            }
        }

    }

    /**
     * 异步加载头像
     */

//    public void initImageLoader() {
//        // 初始化异步加载图片的类
//        imageLoader = ImageLoader.getInstance();
//        // 设置异步加载图片的配置信息
//        options = ImageUtil.getImageLoaderOptions(R.drawable.default_useravatar);
//
//    }
}
