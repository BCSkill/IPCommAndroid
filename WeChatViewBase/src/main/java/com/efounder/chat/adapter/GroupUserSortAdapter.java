package com.efounder.chat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.model.Group;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.widget.RoundImageView;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.thirdpartycomps.stickylistheaders.StickyListHeadersAdapter;

import java.util.List;
import java.util.Locale;

/**
 * 群组人员列表apdater
 *
 * @author hudq
 */
public class GroupUserSortAdapter extends BaseAdapter implements
        StickyListHeadersAdapter, SectionIndexer {
    private List<User> list = null;
    private Context mContext;
    //    private ImageLoader imageLoader;
//    private DisplayImageOptions options;
    private Group group;


    public GroupUserSortAdapter(Context mContext, List<User> list, Group group) {
        this.mContext = mContext;
        this.list = list;
        this.group = group;
        //initImageLoader();

    }

    /**
     * 当ListView数据发生变化时,调用此方法来更新ListView
     *
     * @param list
     */
    public void updateListView(List<User> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        final User user = list.get(position);
        if (view == null) {
            viewHolder = new ViewHolder();
            view = LayoutInflater.from(mContext).inflate(
                    R.layout.groupuser_item, viewGroup, false);
            viewHolder.text = (TextView) view.findViewById(R.id.title);
            viewHolder.avatar = (RoundImageView) view.findViewById(R.id.avatar);
            viewHolder.userSign = (TextView) view.findViewById(R.id.userSign);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        //todo 设置用户名
        viewHolder.text.setText(user.getOtherGroupRemark());
        //// TODO: 设置头像
        String avatarStr = null;

        try {
            if (user != null && user.getAvatar().contains("http")) {
                avatarStr = user.getAvatar();
            } else {
                avatarStr = "";

            }
        } catch (Exception e) {
            e.printStackTrace();
            viewHolder.avatar.setImageResource(R.drawable.default_useravatar);
        }
//        imageLoader.displayImage(URLModifyDynamic.getInstance().replace(avatarStr) , viewHolder.avatar, options);
        LXGlideImageLoader.getInstance().showUserAvatar(mContext, viewHolder.avatar, avatarStr);
        //todo 设置用户标识
        if (user.getGroupUserType() == User.GROUPCREATOR || group.getCreateId() == user.getId()) {
            viewHolder.userSign.setVisibility(View.VISIBLE);
            viewHolder.userSign.setText(R.string.chat_group_owner);
            viewHolder.userSign.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.textview_bgshape_yellow));
        } else if (user.getGroupUserType() == User.GROUPMANAGER) {
            viewHolder.userSign.setVisibility(View.VISIBLE);
            viewHolder.userSign.setText(R.string.chat_admin);
            viewHolder.userSign.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.textview_bgshape_green));
        } else {
            viewHolder.userSign.setVisibility(View.GONE);
        }
        return view;

    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;

        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.groupuser_header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.header);
            holder.tv_count = (TextView) convertView.findViewById(R.id.tv_count);

            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        // set header text as first char in name
        CharSequence headerChar = list.get(position).getSortLetters().substring(0, 1);
        holder.text.setText(headerChar);
        // 隐藏最顶端headerView
        if ("↑".equals(headerChar)) {
            holder.text.setText(R.string.chat_group_owner);
            holder.text.setVisibility(View.VISIBLE);
        } else if ("☆".equals(headerChar)) {
            holder.text.setText(R.string.chat_admin);
            // holder.text.setVisibility(View.GONE);

        } else {
            holder.text.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    /**
     * 根据ListView的当前位置获取分类的首字母的Char ascii值
     */
    @Override
    public int getSectionForPosition(int position) {
        return list.get(position).getSortLetters().charAt(0);
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    @Override
    public int getPositionForSection(int section) {
        for (int i = 0; i < getCount(); i++) {
            String sortStr = list.get(i).getSortLetters();
            char firstChar = sortStr.toUpperCase(Locale.getDefault()).charAt(0);
            if (firstChar == section) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public Object[] getSections() {
        return list.toArray();
    }

    @Override
    public long getHeaderId(int position) {
        return list.get(position).getSortLetters().charAt(0);
    }

    class HeaderViewHolder {
        TextView text;
        TextView tv_count;
    }

    class ViewHolder {
        TextView text;
        RoundImageView avatar;
        TextView userSign;

    }

    /**
     * 异步加载头像
     */
//    public void initImageLoader() {
//        // 初始化异步加载图片的类
//        imageLoader = ImageLoader.getInstance();
//        // 设置异步加载图片的配置信息
//        options = ImageUtil.getImageLoaderOptions(R.drawable.default_useravatar);
//
//    }

}