package com.efounder.chat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * 可拖拽list
 * Created by kongmeng on 2018/8/31.
 */

public class CommonDragAdapter<T> extends BaseAdapter
{
    protected static final String TAG = CommonDragAdapter.class.getSimpleName();

    private Context context;
    private int layout;
    private Class<? extends CommonViewHolder<T>> viewHolderClazz;
    private List<T> datas;

    private int dragSrcPosition = -1;

    /**
     * @param context
     * @param layout
     * @param datas
     * @param clazz
     */
    public CommonDragAdapter(Context context, int layout, List<T> datas, Class<? extends CommonViewHolder<T>> clazz) {
        super();
        this.context = context;
        this.layout = layout;
        this.datas = datas;
        this.viewHolderClazz = clazz;
    }

    /**
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(layout, parent, false);
        ((TextView) ((ViewGroup) convertView).getChildAt(0)).setText((CharSequence) datas.get(position));

        if (position == dragSrcPosition) {
            convertView.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }
    /**
     * @return
     */
    @Override
    public int getCount() {
        return datas.size();
    }

    /**
     * @param position
     * @return
     */
    @Override
    public Object getItem(int position) {
        return null;
    }

    /**
     * @param position
     * @return
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }


    public static abstract class CommonViewHolder<T> {
        /**
         * @param item
         *
         */
        public abstract void setItem(T item);
    }
    public void moveItem(int srcPosition, int dstPosition) {
        if (srcPosition == dstPosition) {
            notifyDataSetChanged();
            return;
        }
        T movingItem = datas.get(srcPosition);
        datas.remove(srcPosition);
        datas.add(dstPosition, movingItem);
    }

    public void setDragSrcPosition(int dragSrcPosition) {
        this.dragSrcPosition = dragSrcPosition;
    }

}
