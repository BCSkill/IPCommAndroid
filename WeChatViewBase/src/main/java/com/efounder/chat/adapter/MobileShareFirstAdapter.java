package com.efounder.chat.adapter;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.model.ShareBean;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.widget.RoundImageView;

import java.util.List;

import static com.efounder.chat.model.ShareBean.SHARE_TYPE_RECENT;
import static com.efounder.chat.model.ShareBean.SHARE_TYPE_SELECT_FRIEND;

/**
 * @author yqs
 * 分享第一个界面adapter
 */
public class MobileShareFirstAdapter extends RecyclerView.Adapter<MobileShareFirstAdapter.BodyViewHolder> {

    private static final String TAG = "MobileShareFirstAdapter";
//    private static final int TYPE_HEADER = 0;
//
//    private static final int TYPE_BODY = 1;

    private Context context;
    private List<ShareBean> items;
    private OnItemClickListener onitemClickListener;


    public MobileShareFirstAdapter(Context context, List<ShareBean> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public BodyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.wechatview_item_mobile_share, parent, false);
        return new BodyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BodyViewHolder holder, final int position) {
        final ShareBean bean = items.get(position);
        if (SHARE_TYPE_SELECT_FRIEND.equals(bean.getType())
                || ShareBean.SHARE_TYPE_SELECT_GROUP.equals(bean.getType())
                || SHARE_TYPE_SELECT_FRIEND.equals(bean.getType())) {
            holder.ivArrow.setVisibility(View.VISIBLE);
            holder.ivAvatar.setVisibility(View.GONE);
        } else {
            holder.ivArrow.setVisibility(View.INVISIBLE);
            holder.ivAvatar.setVisibility(View.VISIBLE);
        }

        if (SHARE_TYPE_RECENT.equals(bean.getType())) {
            holder.tvRecent.setVisibility(View.VISIBLE);
            holder.llMenu.setVisibility(View.GONE);
        } else {
            holder.tvRecent.setVisibility(View.GONE);
            holder.llMenu.setVisibility(View.VISIBLE);
        }

        holder.tvTitle.setText(bean.getName());
        if (bean.getIcon() == null) {
            holder.ivAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.default_user_avatar));
        } else {
            LXGlideImageLoader.getInstance().showUserAvatar((Activity)context, holder.ivAvatar, bean.getIcon() + "");
        }
        holder.llMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onitemClickListener != null) {
                    onitemClickListener.onItemClick(position, bean);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

//    @Override
//    public int getItemViewType(int position) {
//        if (position == 0) return TYPE_HEADER;
//        return TYPE_BODY;
//    }


    public class BodyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvRecent;
        private LinearLayout llMenu;
        private RoundImageView ivAvatar;
        private TextView tvTitle;
        private ImageView ivArrow;

        public BodyViewHolder(View itemView) {
            super(itemView);
            tvRecent = (TextView) itemView.findViewById(R.id.tv_recent);
            llMenu = (LinearLayout) itemView.findViewById(R.id.ll_menu);
            ivAvatar = (RoundImageView) itemView.findViewById(R.id.iv_avatar);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            ivArrow = (ImageView) itemView.findViewById(R.id.iv_arrow);
        }
    }

    public void setOnitemClickListener(OnItemClickListener onitemClickListener) {
        this.onitemClickListener = onitemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int posotion, ShareBean shareBean);
    }


}
