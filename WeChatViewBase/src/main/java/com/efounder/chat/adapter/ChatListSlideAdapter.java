//package com.efounder.chat.adapter;
//
//import android.content.Context;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.Animation;
//import android.view.animation.Transformation;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.efounder.chat.R;
//import com.efounder.chat.db.WeChatDBManager;
//import com.efounder.chat.fragment.ChatListFragment.UpdateBadageNumInterface;
//import com.efounder.chat.model.ChatListItem;
//import com.efounder.chat.model.Group;
//import com.efounder.chat.struct.MessageChildTypeConstant;
//import com.efounder.chat.struct.StructFactory;
//import com.efounder.chat.utils.ChatDateUtils;
//import com.efounder.chat.utils.GroupNameUtil;
//import com.efounder.chat.utils.LXGlideImageLoader;
//import com.efounder.chat.utils.SmileUtils;
//import com.efounder.chat.widget.BadgeView;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.imageloader.GlideImageLoader;
//import com.efounder.message.manager.JFMessageManager;
//import com.efounder.message.struct.IMStruct002;
//import com.efounder.mobilecomps.contacts.User;
//import com.efounder.utils.ResStringUtil;
//import com.groupimageview.NineGridImageView;
//import com.groupimageview.NineGridImageViewAdapter;
//import com.roamer.slidelistview.SlideBaseAdapter;
//import com.roamer.slidelistview.SlideListView.SlideMode;
//
//import java.io.File;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.Locale;
//
//import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
//
///**
// * 聊天列表swipemenuListView的的适配器
// *
// * @author yqs
// */
//@Deprecated
//public class ChatListSlideAdapter extends SlideBaseAdapter {
//    DeleteListItemListener deleteListItemListener;
//    private Context mContext;
//    private List<ChatListItem> datas = null;
//
//    private final static int ISREAD = -1;// 表示已读，角标不显示
//    private final static int NOREAD = 0;// 表示公众号的未读
//    private final static int NOREADONE = 1;// 表示聊天的未读
//    //    private ImageLoader imageLoader;
////    private DisplayImageOptions options;
////    private DisplayImageOptions options1;
//    private SimpleDateFormat dateFormat;
//    private WeChatDBManager weChatDBManager;
//    private int topIndex;
//
//    private int unReadCount;//设置未读消息
//    private CountCallBack callBack;
//
//    //更新角标接口
//    private UpdateBadageNumInterface updateBadageNumInterface;
//
//    public ChatListSlideAdapter(Context context, List<ChatListItem> datas, UpdateBadageNumInterface _updateBadageNumInterface) {
//
//        super(context);
//        updateBadageNumInterface = _updateBadageNumInterface;
//        initImageLoader();
//        this.topIndex = topIndex;
//        if (datas == null) {
//            datas = new ArrayList<ChatListItem>(0);
//        }
//        this.datas = datas;
//        mContext = context;
//        dateFormat = new SimpleDateFormat("HH:mm",
//                Locale.getDefault());// 设置日期格式
//        weChatDBManager = WeChatDBManager.getInstance();
//    }
//
//    @Override
//    public SlideMode getSlideModeInPosition(int position) {
//        return SlideMode.RIGHT;
//    }
//
//    @Override
//    public int getFrontViewId(int position) {
//        return R.layout.chatlist_item;
//    }
//
//    @Override
//    public int getLeftBackViewId(int position) {
//        return R.layout.row_left_back_view;
//    }
//
//    @Override
//    public int getRightBackViewId(int position) {
//        return R.layout.row_right_back_view;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//
//        //ChatListItem chatListItem = datas.get(position);
//        return 0;
//
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return 1;
//    }
//
//    @Override
//    public int getCount() {
//        return datas.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return datas.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        ViewHolder holder = null;
//        final ChatListItem chatListItem = datas.get(position);
//        int userType = -1;//普通好友或者公众号，group没有此属性
//        User tempUser = null;
//        IMStruct002 imStruct002 = chatListItem.getStruct002();
//        if (imStruct002 == null) {
//            imStruct002 = new IMStruct002();
//            imStruct002.setBody(" ".getBytes());
//            if (chatListItem.getUser() != null) {
//                imStruct002.setToUserType(StructFactory.TO_USER_TYPE_PERSONAL);
//            } else {
//                imStruct002.setToUserType(StructFactory.TO_USER_TYPE_GROUP);
//            }
//
//            imStruct002.setState(IMStruct002.MESSAGE_STATE_SEND);
//            imStruct002.setTime(0);
//            imStruct002.setMessageChildType((byte) -1);
//
//        }
//
//        if (StructFactory.TO_USER_TYPE_GROUP != imStruct002.getToUserType()) {
//            tempUser = chatListItem.getUser();
//            if (tempUser != null) {
//                userType = tempUser.getType();
//            }
//        }
//        if (convertView == null) {
//            convertView = createConvertView(position);
//            holder = new ViewHolder();
//            //左滑按钮
//            holder.title = (TextView) convertView.findViewById(R.id.title);
//            holder.read = (TextView) convertView.findViewById(R.id.read);
//            holder.delete = (TextView) convertView.findViewById(R.id.delete);
//            holder.detail = (Button) convertView.findViewById(R.id.detail);
//            //消息item
//            String TX_type = EnvironmentVariable.getProperty("TX_type", "Square");
//            if ("Round".equals(TX_type)) {
//                holder.icon = (ImageView) convertView.findViewById(R.id.icon1);
//            } else {
//                holder.icon = (ImageView) convertView.findViewById(R.id.icon2);
//            }
////            holder.icon = (ImageView) convertView.findViewById(R.id.icon2);
//            holder.icon.setVisibility(ImageView.VISIBLE);
//
////            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
//            holder.iconGroup = (NineGridImageView) convertView.findViewById(R.id.icon_group);
//            holder.title = (TextView) convertView.findViewById(R.id.title);
//            holder.msg = (TextView) convertView.findViewById(R.id.msg);
//            holder.time = (TextView) convertView.findViewById(R.id.time);
//            holder.sendState = (ImageView) convertView.findViewById(R.id.sending);
//            //显示[草稿]字样
//            holder.editing = (TextView) convertView.findViewById(R.id.editing);
//            //显示[有人@我]
//            holder.mentionHint = convertView.findViewById(R.id.mentionHint);
//            holder.chatListbagLayout = (RelativeLayout) convertView
//                    .findViewById(R.id.chatlist_bag);
//            holder.infoBother = (ImageView) convertView
//                    .findViewById(R.id.infobother);
//            holder.iconcontainerforbadage = (LinearLayout) convertView
//                    .findViewById(R.id.iconcontainerforbadage);
//            holder.badge = new BadgeView(mContext, holder.iconcontainerforbadage);
//            holder.setTop = (TextView) convertView.findViewById(R.id.settop);
//            holder.setTop.setVisibility(View.GONE);
//
//            convertView.setTag(holder);
//
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//        //FIXME 设置头像
//        if (userType == 1 && chatListItem.getAvatar().equals("")) {//公众号
//            holder.iconGroup.setVisibility(View.GONE);
//            holder.icon.setVisibility(View.VISIBLE);
//            if ("人力资源系统".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.rlzy);
//            } else if ("内控系统".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.neikong);
//            } else if ("固定资产系统".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.gdzc);
//            } else if ("财务核算系统".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.czhs);
//            } else if ("生产监控系统".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.scjk2);
//            } else if ("生产日报系统".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.bj);
//            } else if ("CRM系统".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.crm);
//            } else if ("普联餐厅".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.ct);
//            } else if ("普联健身".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.jianshen);
//            } else if ("奥体中心".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.atzx);
//            } else if ("乒乓球室".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.ppq);
//            } else if ("篮球场".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.lanqiu);
//            } else if ("足球场".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.zuqiu);
//            } else if ("新的朋友".equals(tempUser.getNickName())) {
//                holder.icon.setImageResource(R.drawable.icon_official_account);
//            } else {
//                holder.icon.setImageResource(R.drawable.default_useravatar);
//            }
//
//        } else {
//            if (chatListItem.getChatType() == StructFactory.TO_USER_TYPE_PERSONAL
//                    || chatListItem.getChatType() == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT) {//单聊或者群聊
//                holder.iconGroup.setVisibility(View.GONE);
//                holder.icon.setVisibility(View.VISIBLE);
//                if (chatListItem.getAvatar() != null && chatListItem.getAvatar().contains("http")) {
//                    LXGlideImageLoader.getInstance().showUserAvatar(mContext, holder.icon, chatListItem.getAvatar());
////                    imageLoader.displayImage(chatListItem.getAvatar(), holder.icon, options);
//                } else {
//                    LXGlideImageLoader.getInstance().showUserAvatar(mContext, holder.icon, "");
////                    imageLoader.displayImage("", holder.icon, options);
//                }
//            } else {//群聊
//                // holder.icon.setVisibility(View.GONE);
//                // holder.iconGroup.setVisibility(View.VISIBLE);
//                Group group = chatListItem.getGroup();
//                if (group != null && group.getAvatar() != null && !"".equals(group.getAvatar())) {
//                    String filePath = "file://" + group.getAvatar();
//                    File file = new File(group.getAvatar());
//                    if (!file.exists()) {
//                        filePath = "";
//                    }
//                    GlideImageLoader.getInstance().displayImage(mContext, holder.icon, filePath, R.drawable.default_groupavatar);
////                    imageLoader.displayImage(filePath, holder.icon, options1);
//                } else {
//                    LXGlideImageLoader.getInstance().showUserAvatar(mContext, holder.icon, "");
////                    imageLoader.displayImage("", holder.icon, options1);
//                }
//
//                //showGroupAvatar(group,holder.iconGroup);
//            }
//        }
//
//
////		holder.icon.setImageResource(chatListItem.iconRes);
//        /**如果存在草稿，显示[草稿]的TextView，发送状态图标隐藏，发送信息设为草稿信息 **/
//        if (!JFMessageManager.getInstance().getHasAtMeMessage(chatListItem.getUserId(), (byte) chatListItem.getChatType())
//                && EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()) != null &&
//                !EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()).equals("")) {
//            holder.editing.setVisibility(View.VISIBLE);
//            holder.mentionHint.setVisibility(View.GONE);
//            holder.sendState.setVisibility(View.GONE);
//            holder.msg.setText(EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()));
//        } else {
//            if (JFMessageManager.getInstance().getHasAtMeMessage(chatListItem.getUserId(), (byte) chatListItem.getChatType())) {
//                holder.mentionHint.setVisibility(View.VISIBLE);
//            } else {
//                holder.mentionHint.setVisibility(View.GONE);
//            }
//            holder.editing.setVisibility(View.GONE);
//            //FIXME 设置消息内容
//            if (imStruct002 != null && imStruct002.getBody() != null) {
//                setMessageContent(imStruct002, chatListItem, holder.msg);
////            if (imStruct002.getMessageChildType() == MessageChildTypeConstant.subtype_image) {
////                holder.msg.setText("[图片]");
////            } else if (imStruct002.getMessageChildType() ==
////                    MessageChildTypeConstant.subtype_voice) {
////                holder.msg.setText("[语音]");
////            } else if (imStruct002.getMessageChildType() ==
////                    MessageChildTypeConstant.subtype_smallVideo) {
////                holder.msg.setText("[小视频]");
////            } else if (imStruct002.getMessageChildType() ==
////                    MessageChildTypeConstant.subtype_feiyongbaoxiao) {
////                holder.msg.setText("[费用报销]");
////            } else if (imStruct002.getMessageChildType() ==
////                    MessageChildTypeConstant.subtype_oa) {
////                holder.msg.setText("[公文审批]");
////            } else if (imStruct002.getMessageChildType() ==
////                    MessageChildTypeConstant.subtype_task) {
////                holder.msg.setText("[任务]");
////            } else if (imStruct002.getMessageChildType() ==
////                    MessageChildTypeConstant.subtype_form) {
////                holder.msg.setText("[表单]");
////            } else {
////                holder.msg.setText(SmileUtils.getSmiledText(mContext, imStruct002.getMessage()));
////            }
//            } else {
//                holder.msg.setText("");
//            }
//
//            //FIXME 设置消息状态
//            if (imStruct002.getState() == IMStruct002.MESSAGE_STATE_SENDING
//                    || imStruct002.getState() == IMStruct002.MESSAGE_STATE_PRESEND
//                    || imStruct002.getState() == IMStruct002.MESSAGE_STATE_WAITSEND) {
//                holder.sendState.setVisibility(View.VISIBLE);
//                if (imStruct002.getState() == IMStruct002.MESSAGE_STATE_PRESEND) {
//                    //预发送状态，progress为空，或者为-1，则为发送失败
//                    if (imStruct002.getExtra("progress") == null || (int) imStruct002.getExtra("progress") == -1) {
//                        holder.sendState.setImageDrawable(mContext.getResources().getDrawable(R.drawable.msg_state_fail_resend));
//                    } else {
//                        holder.sendState.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chatlist_fragment_sending));
//                    }
//                }
//            } else {
//                holder.sendState.setVisibility(View.GONE);
//            }
//        }
//        //FIXME 设置时间
//        if (imStruct002.getTime() == 0) {
//            holder.time.setText("");
//        } else {
//            holder.time.setText(ChatDateUtils.getTimestampString(new Date(
//                    imStruct002.getTime())));
//
//        }
//        //	holder.time.setText(dateFormat.format(chatListItem.getStruct002().getTime()));
//        // holder.sendState.setVisibility(View.GONE);
//        // if (chatListItem.getBadgernum() == ISREAD) {
//        //  holder.read.setText("标为未读");
//        //  } else {
//        holder.read.setText(R.string.wrchatview_mark_read);
//        //  }
//        if (chatListItem.getIsTop() == true) {
//            holder.chatListbagLayout.setBackgroundColor(mContext.getResources()
//                    .getColor(R.color.chattop_bag));
//            holder.setTop.setText(R.string.wrchatview_cancel_top);
//        } else {
//            holder.chatListbagLayout.setBackgroundColor(mContext.getResources()
//                    .getColor(R.color.transparent));
//            holder.setTop.setText(R.string.wrchatview_top);
//        }
//        // FIXME 根据消息类型设置字体颜色和标题
//        int type = imStruct002.getToUserType();
//        switch (type) {
//            case StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT:
//                holder.title.setTextColor(mContext.getResources().getColor(
//                        R.color.publicnumber_textColor));
//                holder.title.setText(chatListItem.getName());
//                break;
//
//            case StructFactory.TO_USER_TYPE_GROUP:
//                holder.title.setTextColor(mContext.getResources().getColor(
//                        R.color.black));
//                holder.title.setText(chatListItem.getName());
//                break;
//            case StructFactory.TO_USER_TYPE_PERSONAL:
//                if (userType == 1) {
//                    holder.title.setTextColor(mContext.getResources().getColor(
//                            R.color.publicnumber_textColor));
//                    holder.title.setText(chatListItem.getName());
//                } else {
//                    holder.title.setText(chatListItem.getName());
//                    holder.title.setTextColor(mContext.getResources().getColor(
//                            R.color.black));
//                }
//                break;
//        }
//        // FIXME 判断消息免打扰状态
//        if (chatListItem.getIsBother() == true) {
//            holder.infoBother.setVisibility(View.VISIBLE);
//        } else {
//            holder.infoBother.setVisibility(View.INVISIBLE);
//        }
//        // FIXME 设置侧滑已读未读的点击事件
//        holder.read.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setRead(chatListItem, position, false);
//                Toast.makeText(mContext, R.string.wrchatview_mark_success, Toast.LENGTH_SHORT).show();
//            }
//
//        });
//        // FIXME 设置侧滑删除点击事件
//        holder.delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                deleteListItemListener.deleteListItem(v, position);
//                // deletePattern(v, position);
//                //  Toast.makeText(mContext, "删除:" + position, Toast.LENGTH_SHORT)
//                //  .show();
//            }
//        });
//        // FIXME: 2016/9/2 设置是否置顶点击事件
////        holder.setTop.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                if (chatListItem.getIsTop()==true){
////                    chatListItem.setIsTop(false);
////                    datas.remove(chatListItem);
////                    datas.add(topIndex + 1,chatListItem);
////
////                }else{
////                    chatListItem.setIsTop(true);
////                    datas.remove(chatListItem);
////                    datas.add(0,chatListItem);
////                    topIndex++;
////                }
////                weChatDBManager.insertOrUpdateChatList(chatListItem);
////                notifyDataSetChanged();
////            }
////        });
//
//
//        int badgerNum = chatListItem.getBadgernum();
//
//        switch (badgerNum) {
//            // 设置公众号未读角标
//            case NOREAD:
//                holder.badge.setText(" ");
//                holder.badge.setTag("badge");
//                holder.badge.setBadgeMargin(3);
//                holder.badge.setBadgeBackgroundColor(mContext.getResources().getColor(
//                        R.color.chat_badge_color));
//                holder.badge.setTextSize(9);
//                holder.badge.show();
//                break;
//            // 已读，隐藏角标
//            case ISREAD:
//                holder.badge.setVisibility(View.GONE);
//                break;
//            // 设置其他消息未读角标
//            default:
//                holder.badge.setBadgeMargin(3);
//                holder.badge.setTextSize(13);
//                holder.badge.setTag("badge");
//                if (chatListItem.getBadgernum() > 99) {
//                    holder.badge.setText("99+");
//                } else {
//                    holder.badge.setText(chatListItem.getBadgernum() + "");
//                }
//                holder.badge.setBadgeBackgroundColor(mContext.getResources().getColor(
//                        R.color.chat_red));
//                holder.badge.show();
//                break;
//        }
//
//
//        return convertView;
//    }
//
//
//    class ViewHolder {
//
//        public TextView read;
//        public TextView delete;
//        public Button detail;
//        public ImageView icon;
//        public NineGridImageView iconGroup;
//        public TextView title;
//        public TextView msg;
//        public TextView time;
//        public ImageView infoBother;// 消息免打扰图标
//        public ImageView sendState;//消息发送状态
//        public TextView editing;
//        public TextView mentionHint;
//        public LinearLayout iconcontainerforbadage;
//        public RelativeLayout chatListbagLayout;
//        public BadgeView badge;
//        public TextView setTop;
//
//    }
//
//    /**
//     * 设置标记已读未读
//     *
//     * @param chatListItem
//     * @param position     当前的item
//     * @param onlyRead     是否只把消息设置为已读
//     */
//    public void setRead(ChatListItem chatListItem, int position, Boolean onlyRead) {
//        int type = chatListItem.getStruct002().getToUserType();
//        int badgerNum = chatListItem.getBadgernum();
//        switch (type) {
//            // 如果是公众号
//            case StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT:
//                if (badgerNum == ISREAD && onlyRead == false) {
//                    datas.get(position).setBadgernum(NOREAD);
//                    // updateBadageNumInterface.update(WeChatDBManager.UNREADCOUNT += 1);
//                    notifyDataSetChanged();
//                } else {
//                    datas.get(position).setBadgernum(ISREAD);
//                    if (badgerNum == -1) badgerNum = 0;
//
//                    // updateBadageNumInterface.update(WeChatDBManager.UNREADCOUNT-badgerNum);
//                    notifyDataSetChanged();
//                }
//                break;
//
//            default:
//                if (badgerNum == ISREAD && onlyRead == false) {
//                    //datas.get(position).setBadgernum(NOREADONE);
//                    // updateBadageNumInterface.update(WeChatDBManager.UNREADCOUNT+=1);
//                    // JFMessageManager.getInstance().markedAsUnread(chatListItem.getStruct002());
//                    notifyDataSetChanged();
//                } else {
//                    datas.get(position).setBadgernum(ISREAD);
//                    if (badgerNum == -1) badgerNum = 0;
//                    if (chatListItem.getUserId() != Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID))) {
//                        if (chatListItem.getChatType() == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT) {
//                            JFMessageManager.getInstance().unreadZero(chatListItem.getUserId(), (byte) 0);
//                        } else {
//                            JFMessageManager.getInstance().unreadZero(chatListItem.getUserId(), (byte) chatListItem.getChatType());
//                        }
//                        // JFMessageManager.getInstance().unreadZero(chatListItem.getUserId(),(byte)chatListItem.getChatType());
//                    }
//
//                    // WeChatDBManager.UNREADCOUNT = WeChatDBManager.UNREADCOUNT - badgerNum;
//                    //updateBadageNumInterface.update(WeChatDBManager.UNREADCOUNT);
//                    chatListItem.setBadgernum(-1);
//                    if (callBack != null) {
//                        callBack.resetCountNum();
//                    }
//                    //notifyDataSetChanged();
//                }
//                break;
//        }
////        if (badgerNum!= -1){
////            UNREADCOUNT=UNREADCOUNT-badgerNum;
////            updateBadageNumInterface.update(unReadCount);
////
////        }
//        weChatDBManager.insertOrUpdateChatList(datas.get(position));
//
//    }
//
//    /**
//     * 删除item的动画
//     *
//     * @param view
//     * @param position
//     */
//    public void deletePattern(final View view, final int position) {
//
//        Animation.AnimationListener al = new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                //datas.remove(position);
//                notifyDataSetChanged();
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        };
//        collapse(view, al);
//
//    }
//
//    public void collapse(final View view, Animation.AnimationListener al) {
//        final int originHeight = view.getMeasuredHeight();
//
//        Animation animation = new Animation() {
//            @Override
//            protected void applyTransformation(float interpolatedTime,
//                                               Transformation t) {
//                if (interpolatedTime == 1.0f) {
//
//                    view.setVisibility(View.GONE);
//                } else {
//                    view.getLayoutParams().height = originHeight
//                            - (int) (originHeight * interpolatedTime);
//                    view.requestLayout();
//                }
//            }
//
//            @Override
//            public boolean willChangeBounds() {
//                return true;
//            }
//        };
//        if (al != null) {
//            animation.setAnimationListener(al);
//        }
//        animation.setDuration(300);
//        view.startAnimation(animation);
//    }
//
//
//    /**
//     * 异步加载头像
//     */
//    public void initImageLoader() {
//        // 初始化异步加载图片的类
////        imageLoader = ImageLoader.getInstance();
////        // 设置异步加载图片的配置信息
////        options = ImageUtil.getImageLoaderOptions(R.drawable.default_useravatar);
////        options1 = ImageUtil.getImageLoaderOptions(R.drawable.default_groupavatar);
//
//    }
//
//    /**
//     * 删除listview item的回调接口，为了实现删除的动画。。
//     *
//     * @author yqs
//     */
//    public interface DeleteListItemListener {
//        public void deleteListItem(View v, int position);
//    }
//
//    public void setDeleteListItemListener(
//            DeleteListItemListener deleteListItemListener) {
//        this.deleteListItemListener = deleteListItemListener;
//    }
//
//    //Fixme 显示group的头像
//    private void showGroupAvatar(Group group, NineGridImageView iconGroup) {
//        int membersNum = 0;
//        if (group.getUsers() != null) {
//            membersNum = group.getUsers().size();
//        }
//
//        List<String> avatarList = new ArrayList<>();
//        if (membersNum == 0) {
//            for (int i = 0; i < 4; i++) {
//                avatarList.add("");
//            }
//        }
//        for (int i = 0; i < membersNum; i++) {
//            if (i < 9) {
//                String avatar = group.getUsers().get(i).getAvatar();
//                if (avatar == null || !avatar.contains("http")) {
//                    avatar = "";
//                }
//                avatarList.add(avatar);
//
//            }
//        }
//        NineGridImageViewAdapter<String> mAdapter = new NineGridImageViewAdapter<String>() {
//            @Override
//            protected void onDisplayImage(Context context, ImageView imageView, String s) {
////                imageLoader.displayImage(s, imageView, options);
//                GlideImageLoader.getInstance().displayImage(mContext, imageView, s, R.drawable.default_useravatar);
//            }
//
//            @Override
//            protected ImageView generateImageView(Context context) {
//                return super.generateImageView(context);
//            }
//        };
//        iconGroup.setAdapter(mAdapter);
//        iconGroup.setImagesData(avatarList);
//    }
//
//
//    //todo 设置消息文字，群组消息前方加入发送人的姓名
//    private void setMessageContent(IMStruct002 imStruct002, ChatListItem item, TextView msgView) {
//        String frontText = "";
//        if (item.getGroup() != null && item.getGroup().getUsers() != null) {
//            for (User user : item.getGroup().getUsers()) {
//                if (imStruct002.getFromUserId() == user.getId()) {
//                    frontText = GroupNameUtil.getGroupUserName(user) + ":";
//                }
//            }
//        }
//        if (imStruct002.getMessageChildType() == MessageChildTypeConstant.subtype_image) {
//            msgView.setText(frontText + ResStringUtil.getString(R.string.wrchatview_image_kh));
//        } else if (imStruct002.getMessageChildType() ==
//                MessageChildTypeConstant.subtype_voice) {
//            msgView.setText(frontText + ResStringUtil.getString(R.string.wrchatview_voice_kh));
//        } else if (imStruct002.getMessageChildType() ==
//                MessageChildTypeConstant.subtype_smallVideo) {
//            msgView.setText(frontText + ResStringUtil.getString(R.string.wrchatview_small_video_kh));
//        } else if (imStruct002.getMessageChildType() ==
//                MessageChildTypeConstant.subtype_feiyongbaoxiao) {
//            msgView.setText(frontText + ResStringUtil.getString(R.string.wrchatview_reimbursement_kh));
//        } else if (imStruct002.getMessageChildType() ==
//                MessageChildTypeConstant.subtype_oa) {
//            msgView.setText(frontText + ResStringUtil.getString(R.string.wrchatview_document_approval_kh));
//        } else if (imStruct002.getMessageChildType() ==
//                MessageChildTypeConstant.subtype_task) {
//            msgView.setText(frontText + ResStringUtil.getString(R.string.wrchatview_task_kh));
//        } else if (imStruct002.getMessageChildType() ==
//                MessageChildTypeConstant.subtype_form) {
//            msgView.setText(frontText + ResStringUtil.getString(R.string.wrchatview_form_kh));
//        } else if (imStruct002.getMessageChildType() ==
//                MessageChildTypeConstant.subtype_file) {
//            msgView.setText(frontText + ResStringUtil.getString(R.string.wrchatview_file_kh));
//        } else if (imStruct002.getMessageChildType() ==
//                MessageChildTypeConstant.subtype_common) {
//            msgView.setText(frontText + ResStringUtil.getString(R.string.wrchatview_new_message_kh));
//        } else {
//            msgView.setText(frontText + SmileUtils.getSmiledText(mContext, imStruct002.getMessage()));
//        }
//    }
//
//    public interface CountCallBack {
//        public void resetCountNum();
//    }
//
//    public void setCountCallBack(CountCallBack callBack) {
//        this.callBack = callBack;
//    }
//}
