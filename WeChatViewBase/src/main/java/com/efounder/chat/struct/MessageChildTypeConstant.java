package com.efounder.chat.struct;

public class MessageChildTypeConstant {
    /**
     * 文本
     */
    public static final short subtype_text = (short) 0;
    /**
     * 图片
     */
    public static final short subtype_image = (short) 1;
    /**
     * 语音
     */
    public static final short subtype_voice = (short) 2;
    /**
     * 小视频
     */
    public static final short subtype_smallVideo = (short) 3;
    /**
     * 拍摄视频
     */
    public static final short subtype_video = (short) 4;
    /**
     * 文件
     */
    public static final short subtype_file = (short) 5;

    /**
     * 动画
     */
    public static final short subtype_anim = (short) 7;
    /**
     * 转账
     */
    public static final short subtype_transfer = (short) 8;
    /**
     * 红包
     */
    public static final short subtype_red_package = (short) 9;
    /**
     * 语音识别（光速短信）
     */
    public static final short subtype_speechRecognize = (short) 10;
    /**
     * json文本消息类型
     */
    public static final short subtype_format_text = (short) 11;

    /**
     * 密信
     */
    public static final short subtype_secret_text = (short) 20;
    /**
     * 密图
     */
    public static final short subtype_secret_image = (short) 21;
    /**
     * 密件
     */
    public static final short subtype_secret_file = (short) 22;
    /**
     * 通知公告消息类型
     */
    public static final short subtype_notification = (short) 23;
    /**
     * 分享网页的类型
     */
    public static final short subtype_share_web = (short) 24;
    /**
     * 表单
     */
    public static final short subtype_form = (short) 31;
    /**
     * 表单原生
     */
    public static final short subtype_form_native = (short) 32;
    /**
     * 公众号
     */
    public static final short subtype_officalAccount = (short) 41;
    /**
     * 名片
     */
    public static final short subtype_callingCard = (short) 42;
    /**
     * 消费凭证
     */
    public static final short subtype_payCard = (short) 43;
    /**
     * 任务
     */
    public static final short subtype_task = (short) 44;
    /**
     * 阅后即焚文本
     */
    public static final short subtype_bornText = (short) 50;
    /**
     * 阅后即焚图片
     */
    public static final short subtype_bornImage = (short) 51;
    /**
     * 阅后即焚语音
     */
    public static final short subtype_bornVoice = (short) 52;
    /**
     * 阅后即焚小视频
     */
    public static final short subtype_bornSmallVideo = (short) 53;
    /**
     * 阅后即焚拍摄视频
     */
    public static final short subtype_bornVideo = (short) 54;
    /**
     * 位置
     */
    public static final short subtype_location = (short) 55;
    /**
     * 费用报销
     */
    public static final short subtype_feiyongbaoxiao = (short) 56;
    /**
     * oa
     */
    public static final short subtype_oa = (short) 57;
    /**
     * 命令类型的消息（例如：作废单据）
     */
    public static final short subtype_command = (short) 60;

    /**
     * 应用号网址
     */
    public static final short subtype_officalweb = (short) 61;
    /**
     * common原生 rn 网页消息类型
     */
    public static final short subtype_common = (short) 62;
    /**
     * common原生大图片通用item
     */
    public static final short subtype_common_Image = (short) 63;
    /**
     * 糖足消息item
     */
    public static final short subtype_TangZuItem = (short) 64;

    /**
     * 会议消息item
     */
    public static final short subtype_meeting = (short) 67;
    /**
     * 人脸识别消息item
     */
    public static final short subtype_recognition = (short) 68;
    /**
     * 通用评论消息item
     */
    public static final short subtype_subtype_general_common_message = (short) 69;
    /**
     * 新疆油田carditem
     */
    public static final short subtype_xj_item1 = (short) 70;

    /**
     * 中有铁工item 任务
     */
    public static final short subtype_zy_task = (short) 75;

    /**
     * 空间通知类型
     */
    public static final short subtype_mZoneNotification = (short) 80;

    /**
     * 共享任务消息item
     */
    public static final short subtype_gxtask = (short) 81;
    /**
     * 共享消息item 20190730
     */
    public static final short subtype_EFMessageItemCWGXBasic = (short) 87;

    /**
     * 自定义消息item 有头像
     */
    public static final short subtype_custom_with_avatar = (short) 88;
    /**
     * 自定义消息item 无头像，类似于应用号头像
     */
    public static final short subtype_custom_without_avatar = (short) 89;



    /**
     * 聊天过程中的提醒 提示（类似撤回消息的item 文件下载完成）
     */
    public static final short subtype_chat_tips = (short) 97;
    /**
     * 撤回的消息类型
     */
    public static final short subtype_recallMessage = (short) 98;
    /**
     * 用户自定义
     */
    public static final short subtype_custome = (short) 99;
    /**
     * 更新用户信息，更改群组名,推送群组成员
     */
    public static final short subtype_updateUserInfo = (short) 100;
    /**
     * 已送达
     */
    public static final short subtype_send = (short) 101;
    /**
     * 撤回消息时发送的系统消息类型（还可以用来发送系统消息，比如正在输入中,cmd消息）
     * 前台往外发送的系统消息
     */
    public static final short subtype_common_system_info = (short) 103;
    /**
     * 已读
     */
    public static final short subtype_read = (short) 102;

    /**
     * 通用后台向前台发送的系统消息（前台不会发送这个消息的）
     * 消息格式为json,必须含有CMD这个key
     */
    public static final short subtype_common_bg_system_notification = (short) 111;

    /**
     * 直播、点播系统消息唯一指定id
     */
    public static final short live_System_Msg = (short) 251;

    /**
     * 会议系统消息id
     */
    public static final short meeting_System_Msg = (short) 252;
    /**
     * 应用号主数据更新
     */
    public static final short subtype_masterdata = (short) 301;
    /**
     * 应用号号资源更新
     */
    public static final short subtype_officalAccountRes = (short) 302;
    /**
     * app更新
     */
    public static final short subtype_App = (short) 303;
    /**
     * app资源文件更新
     */
    public static final short subtype_AppRes = (short) 304;


}
