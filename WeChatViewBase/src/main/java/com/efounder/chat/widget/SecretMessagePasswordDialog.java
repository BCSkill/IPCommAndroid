package com.efounder.chat.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AlertDialog;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.efounder.chat.R;

/**
 * 显示加密消息的密码
 * @author YQS
 */

public class SecretMessagePasswordDialog {

    private Activity mContext;
    private int mWidth;
    private int mHeight;
    /**
     * 升级的文字信息
     */
    private TextView newVersionInforTv;

    /**
     * 面板占屏幕宽度的百分比
     */
    private static final float WIDTHFRACTION = 0.8f;
    private AlertDialog alertDialog;

    private TextView title;
    private TextView tvTips;
    private TextView tvPassword;
    private Button butEnter;
    private View rootView;
    private OnEnterClick onEnterClick;
    private String pwd;


    private void getWindowWidthAndHeight() {
        WindowManager m = mContext.getWindowManager();
        Display d = m.getDefaultDisplay();
        mWidth = d.getWidth();
        mHeight = d.getHeight();
    }


    public SecretMessagePasswordDialog(Activity context, String pwd, OnEnterClick onEnterClick) {
        this.mContext = context;
        this.onEnterClick = onEnterClick;
        this.pwd = pwd;
        getWindowWidthAndHeight();
        init();
    }


    /**
     * 设置标题
     *
     * @param msg
     */
    public void setTitle(String msg) {
        title.setText(msg);
    }


    private void init() {

        rootView = LayoutInflater.from(mContext).inflate(R.layout.wechatview_dialog_show_password, null);

        title = (TextView) rootView.findViewById(R.id.title);
        tvTips = (TextView) rootView.findViewById(R.id.tv_tips);
        tvPassword = (TextView) rootView.findViewById(R.id.tv_password);
        butEnter = (Button) rootView.findViewById(R.id.but_enter);

        tvPassword.setText(pwd);
        butEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              dismiss();
              onEnterClick.onEnterClick();

            }
        });

        alertDialog = new AlertDialog.Builder(mContext)
                .setCancelable(true).create();
        alertDialog.setView(rootView);
    }


    public void show() {

        alertDialog.show();
        Window window = alertDialog.getWindow();
        //不邪恶这行，圆角无效
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        window.setBackgroundDrawableResource(R.drawable.new_version_round_shape);
//        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.new_version_scale_open);
        setDialogSize(alertDialog, rootView);

//        Timer timer = new Timer();
//        timer.schedule(new TimerTask() {
//
//            @Override
//            public void run() {
//                showKeyboard(editText);
//            }
//        }, 300);

    }

    public void showKeyboard(EditText editText) {
        if (editText != null) {
            //设置可获得焦点
            editText.setFocusable(true);
            editText.setFocusableInTouchMode(true);
            //请求获得焦点
            editText.requestFocus();
            //调用系统输入法
            InputMethodManager inputManager = (InputMethodManager) editText
                    .getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.showSoftInput(editText, 0);
        }
    }

    public void dismiss() {
//        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.new_version_scale_close);
//        newVersionContentView.startAnimation(animation);
        alertDialog.dismiss();

    }

    /**
     * 根据屏幕的宽度来设置面板的宽度
     * 高度自适应
     *
     * @param alertDialog
     * @param view
     */
    private void setDialogSize(AlertDialog alertDialog, View view) {
        //为获取屏幕宽、高
        WindowManager.LayoutParams p = alertDialog.getWindow().getAttributes();  //获取对话框当前的参数值
        p.height = WindowManager.LayoutParams.WRAP_CONTENT;
        p.width = (int) (mWidth * WIDTHFRACTION);
        p.gravity = Gravity.CENTER;
        alertDialog.getWindow().setAttributes(p);
    }



    public interface  OnEnterClick{
        void onEnterClick();
    }
}
