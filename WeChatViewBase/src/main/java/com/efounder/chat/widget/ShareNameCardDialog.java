package com.efounder.chat.widget;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AlertDialog;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.utils.LXGlideImageLoader;

/**
 * 分享名片对话框
 *
 * @author YQS
 */

public class ShareNameCardDialog {

    private Activity mContext;
    private int mWidth;
    private int mHeight;


    /**
     * 面板占屏幕宽度的百分比
     */
    private static final float WIDTHFRACTION = 0.8f;
    private AlertDialog alertDialog;


    private OnEnterClick onEnterClick;

    private View rootView;


    private TextView title;
    private TextView tvContent;
    private TextView tvCancel;
    private TextView tvSend;
    private TextView tvUserName;
    private TextView tvInfo;
    private RoundImageView ivAvatar;



    private void getWindowWidthAndHeight() {
        WindowManager m = mContext.getWindowManager();
        Display d = m.getDefaultDisplay();
        mWidth = d.getWidth();
        mHeight = d.getHeight();
    }


    public ShareNameCardDialog(Activity context, OnEnterClick onEnterClick) {
        this.mContext = context;
        this.onEnterClick = onEnterClick;
        getWindowWidthAndHeight();
        init();
    }


    /**
     * 设置标题
     *
     * @param msg
     */
    public void setTitle(String msg) {
        title.setText(msg);
    }

    public void setContent(String msg) {
        tvContent.setText(msg);
    }

    public void setUserName(String msg) {
        tvUserName.setText(msg);
    }
    public void showAvatar(String msg) {
        LXGlideImageLoader.getInstance().showUserAvatar(mContext,ivAvatar,msg);
    }

    public void setInfo(String msg) {
        tvInfo.setText(msg);
    }

    private void init() {

        rootView = LayoutInflater.from(mContext).inflate(R.layout.wechatview_dialog_share_name_card, null);

        title = (TextView) rootView.findViewById(R.id.title);
        tvContent = (TextView) rootView.findViewById(R.id.tv_content);
        //butEnter = (Button) rootView.findViewById(R.id.but_enter);
        tvUserName = (TextView) rootView.findViewById(R.id.tv_user_name);
        tvInfo = (TextView) rootView.findViewById(R.id.tv_info);

        ivAvatar = (RoundImageView) rootView.findViewById(R.id.iv_avatar);
        tvCancel = (TextView) rootView.findViewById(R.id.tv_cancel);
        tvSend = (TextView) rootView.findViewById(R.id.tv_send);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onEnterClick.buttonClick(false);
            }
        });
        tvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onEnterClick.buttonClick(true);
            }
        });

        alertDialog = new AlertDialog.Builder(mContext)
                .setCancelable(true).create();
        alertDialog.setView(rootView);
    }


    public void show() {

        alertDialog.show();
        Window window = alertDialog.getWindow();
        //不邪恶这行，圆角无效
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        window.setBackgroundDrawableResource(R.drawable.new_version_round_shape);
//        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.new_version_scale_open);
        setDialogSize(alertDialog, rootView);


    }


    public void dismiss() {
//        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.new_version_scale_close);
//        newVersionContentView.startAnimation(animation);
        alertDialog.dismiss();

    }

    /**
     * 根据屏幕的宽度来设置面板的宽度
     * 高度自适应
     *
     * @param alertDialog
     * @param view
     */
    private void setDialogSize(AlertDialog alertDialog, View view) {
        //为获取屏幕宽、高
        WindowManager.LayoutParams p = alertDialog.getWindow().getAttributes();  //获取对话框当前的参数值
        p.height = WindowManager.LayoutParams.WRAP_CONTENT;
        p.width = (int) (mWidth * WIDTHFRACTION);
        p.gravity = Gravity.CENTER;
        alertDialog.getWindow().setAttributes(p);
    }


    public interface OnEnterClick {
        void buttonClick(boolean isSend);
    }
}
