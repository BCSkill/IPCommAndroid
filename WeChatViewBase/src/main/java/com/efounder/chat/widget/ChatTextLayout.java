package com.efounder.chat.widget;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.efounder.chat.R;
import com.efounder.utils.JfResourceUtil;
import com.utilcode.util.SizeUtils;

/**
 * 文本框+翻译布局
 * @autor yqs
 * @date 2019/1/22 10:40
 **/
public class ChatTextLayout extends LinearLayout {

    private Context mContext;
    private ChatTextView textView;
    private ChatTextView translateTextView;
    private View lineView;

    public View getLineView() {
        return lineView;
    }

    //隐藏翻译布局
    public void hideTranslateView() {
        this.getLineView().setVisibility(View.GONE);
        this.getTranslateTextView().setVisibility(View.GONE);

    }

    //显示翻译布局
    public void showTranslateView() {
        this.getLineView().setVisibility(View.VISIBLE);
        this.getTranslateTextView().setVisibility(View.VISIBLE);

    }

    public ChatTextView getTextView() {
        return textView;
    }

    public ChatTextView getTranslateTextView() {
        return translateTextView;
    }

    public ChatTextLayout(Context context) {
        this(context, null);
    }

    public ChatTextLayout(Context context, @Nullable AttributeSet attrs) {
        this(context,attrs,0);
    }

    public ChatTextLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView(mContext);
    }

    private void initView(Context mContext) {
        textView = new ChatTextView(mContext);
        translateTextView = new ChatTextView(mContext);
        lineView = new View(mContext);
        lineView.setBackgroundColor(JfResourceUtil.getSkinColor(R.color.divide_line_color));
        this.setOrientation(VERTICAL);
        LayoutParams textLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        this.addView(textView,textLayoutParams);

        LayoutParams lineLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, SizeUtils.dp2px(0.4f));
        lineLayoutParams.setMargins(0,SizeUtils.dp2px(4f),0,SizeUtils.dp2px(4f));
//        lineView.setPadding(SizeUtils.dp2px(8f),SizeUtils.dp2px(8f),SizeUtils.dp2px(8f),SizeUtils.dp2px(8f));
        this.addView(lineView,lineLayoutParams);
        this.addView(translateTextView,textLayoutParams);

    }
}
