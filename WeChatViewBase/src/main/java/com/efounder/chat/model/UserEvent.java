package com.efounder.chat.model;

import com.efounder.mobilecomps.contacts.User;

/**
 * 当一个用户本地不存在的时候，为了获取用户信息，请求结束后发送事件，方便界面刷新用户信息
 */
public class UserEvent {
    //默认
    public static final int EVENT_DEFAULT = 0;
    //空间请求用户信息
    public static final int EVENT_ZONE = 1;
    //空间与我相关
    public static final int EVENT_ZONE_RELATE_ME = 2;
    //抢到红包的人员
    public static final int EVENT_RED_PACKAGE = 3;

    //聊天界面
    public static final int EVENT_CHAT_ACTIVITY = 4;
    //事件类型
    private int eventType;
    private User user;

    public int getEventType() {
        return eventType;
    }

    public UserEvent setEventType(int eventType) {
        this.eventType = eventType;
        return this;
    }

    public User getUser() {
        return user;
    }

    public UserEvent setUser(User user) {
        this.user = user;
        return this;
    }

    public UserEvent(int eventType) {
        this.eventType = eventType;
    }
}
