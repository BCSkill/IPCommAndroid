package com.efounder.chat.model;

import java.util.List;

/**
 * Created by zjk on 2017/6/7.
 * Desc:办公地点bean
 */

public class GroupPositionsBean {

    /**
     * result : success
     * data : [{"id":2,"groupId":1116,"longitude":"36.676549","latitude":"117.136951","positionname":"美莲广场：山东省济南市历下区新泺大街1166号","shortpositionname":"","createTime":"2017-06-06 16:09:10.0"},{"id":3,"groupId":1116,"longitude":"36.676549","latitude":"117.136951","positionname":"美莲广场：山东省济南市历下区新泺大街1166号","shortpositionname":"","createTime":"2017-06-06 16:31:02.0"},{"id":5,"groupId":1116,"longitude":"117.136951","latitude":"36.676549","positionname":"美莲广场","shortpositionname":"","createTime":"2017-06-06 19:36:17.0"}]
     */

    private String result;
    private List<DataBean> data;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 2
         * groupId : 1116
         * longitude : 36.676549
         * latitude : 117.136951
         * positionname : 美莲广场：山东省济南市历下区新泺大街1166号
         * shortpositionname :
         * createTime : 2017-06-06 16:09:10.0
         */

        private int id;
        private int groupId;
        private String longitude;
        private String latitude;
        private String positionname;
        private String shortpositionname;
        private String createTime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getPositionname() {
            return positionname;
        }

        public void setPositionname(String positionname) {
            this.positionname = positionname;
        }

        public String getShortpositionname() {
            return shortpositionname;
        }

        public void setShortpositionname(String shortpositionname) {
            this.shortpositionname = shortpositionname;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }
    }
}
