package com.efounder.chat.model;

/**
 * 新的好友，群通知的时间
 */
public class BadageNumEvent {
    private int groupNoticeCount;
    private int newFriendCount;

    public BadageNumEvent(int groupNoticeCount, int newFriendCount) {
        this.groupNoticeCount = groupNoticeCount;
        this.newFriendCount = newFriendCount;
    }

    public BadageNumEvent() {

    }

    public int getGroupNoticeCount() {
        return groupNoticeCount;
    }

    public void setGroupNoticeCount(int groupNoticeCount) {
        this.groupNoticeCount = groupNoticeCount;
    }

    public int getNewFriendCount() {
        return newFriendCount;
    }

    public void setNewFriendCount(int newFriendCount) {
        this.newFriendCount = newFriendCount;
    }
}
