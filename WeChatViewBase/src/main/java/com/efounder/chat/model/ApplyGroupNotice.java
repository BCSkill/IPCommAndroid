package com.efounder.chat.model;

import com.efounder.mobilecomps.contacts.User;

import java.io.Serializable;

/**
 * 申请加群的信息（群通知列表）
 * Created by cherise on 2017/1/4.
 */

public class ApplyGroupNotice implements Serializable {

    /**
     * 加入群组申请未接受
     */
    public static final int UNACCEPT = 0;
    /**
     * 加入群组申请已同意
     */
    public static final int ACCEPTED = 1;
    /**
     * 加入群组申请已发送
     */
    public static final int SENT = 2;
    /**
     * 加入群组申请已拒绝
     */
    public static final int REFUSE = 3;
    /**
     * 加入群组申请被拒绝
     */
    public static final int REJECTED = 4;
    private int userId;//用户id
    private int groupId;//群组id
    private int state;//状态
    private long time;//申请发送的时间爱你
    private boolean isRead;//是否已读
    private Group group;
    private User user;
    private String leaveMessage;


    public ApplyGroupNotice() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupid) {
        this.groupId = groupid;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean getRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLeaveMessage() {
        return leaveMessage;
    }

    public ApplyGroupNotice setLeaveMessage(String leaveMessage) {
        this.leaveMessage = leaveMessage;
        return this;
    }
}
