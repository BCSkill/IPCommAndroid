package com.efounder.chat.model;

import java.io.Serializable;

/**
 * Created by will on 17-5-9.
 */

public class ShareBean implements Serializable {

    //最近聊天
    public static final String SHARE_TYPE_CHAT = "chat";
    //选择群聊
    public static final String SHARE_TYPE_SELECT_GROUP = "selectGroup";
    //选择好友
    public static final String SHARE_TYPE_SELECT_FRIEND = "selectFriend";

    //发送动态
    public static final String SHARE_TYPE_ZONE = "zone";

    //最近
    public static final String SHARE_TYPE_RECENT = "recent";

    //菜单用户名
    private String name;
    //图片
    private Object icon;
    //类型
    private String type;
    //接收人id
    private int chatId;
    /**
     * 类型（单聊群聊，公众号）
     **/
    private int chatType;


    public String getName() {
        return name;
    }

    public ShareBean setName(String name) {
        this.name = name;
        return this;
    }

    public Object getIcon() {
        return icon;
    }

    public ShareBean setIcon(Object icon) {
        this.icon = icon;
        return this;
    }

    public String getType() {
        return type;
    }

    public ShareBean setType(String type) {
        this.type = type;
        return this;
    }

    public int getChatId() {
        return chatId;
    }

    public ShareBean setChatId(int chatId) {
        this.chatId = chatId;
        return this;
    }

    public int getChatType() {
        return chatType;
    }

    public ShareBean setChatType(int chatType) {
        this.chatType = chatType;
        return this;
    }

    private ShareBean(Builder builder) {
        name = builder.name;
        icon = builder.icon;
        type = builder.type;
        chatId = builder.chatId;
        chatType = builder.chatType;
    }


    public static final class Builder {
        private String name;
        private Object icon;
        private String type;
        private int chatId;
        private int chatType;

        public Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder icon(Object val) {
            icon = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder chatId(int val) {
            chatId = val;
            return this;
        }

        public Builder chatType(int val) {
            chatType = val;
            return this;
        }

        public ShareBean build() {
            return new ShareBean(this);
        }
    }
}
