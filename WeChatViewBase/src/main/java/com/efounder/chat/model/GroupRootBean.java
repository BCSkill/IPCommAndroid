package com.efounder.chat.model;

/**
 * Created by zjk on 2017/6/22 0022.
 * 群聊设置菜单项
 */

public class GroupRootBean {
    String icon;
    String viewType;
    String show;
    String url;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
