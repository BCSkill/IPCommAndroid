package com.efounder.chat.model;

public class UnReadCountEvent {

    private int count;
    private String type;

    public UnReadCountEvent(int count, String type) {
        this.count = count;
        this.type = type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
