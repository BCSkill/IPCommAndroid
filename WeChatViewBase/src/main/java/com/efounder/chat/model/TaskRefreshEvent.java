package com.efounder.chat.model;

import com.efounder.message.struct.IMStruct002;

/**
 * 用来通知任务刷新的event
 * Created by will on 17-7-12.
 */

public class TaskRefreshEvent {
    public TaskRefreshEvent(IMStruct002 imStruct002) {
        this.imStruct002 = imStruct002;
    }

    IMStruct002 imStruct002;

    public IMStruct002 getImStruct002() {
        return imStruct002;
    }

    public void setImStruct002(IMStruct002 imStruct002) {
        this.imStruct002 = imStruct002;
    }
}
