package com.efounder.chat.model;

import java.util.List;

public class RedPacketHistoryBean {


    /**
     * result : success
     * page : {"allRow":6,"currentPage":1,"pageSize":10,"totalPage":1}
     * getRedPacketRecordList : [{"coinName":"PWR","getPacketUser":"17860596626","imUserId":192205,"packetId":"94db5035-70e9-4b0e-a2ef-a7d737fc1f34","startTime":1534469652000,"timestamp":"2018-08-17 09:34:12","totalMoney":1.0E-5,"userId":"18363825829","userName":"song12"},{"coinName":"PWR","getPacketUser":"17860596626","imUserId":191936,"packetId":"af4cc408-41f4-4d86-9ed9-ec8cba0d1430","startTime":1534463954000,"timestamp":"2018-08-17 07:59:14","totalMoney":0.12257,"userId":"15711160771","userName":"星爵"},{"coinName":"PWR","getPacketUser":"17860596626","imUserId":200348,"packetId":"c1ff8305-3cee-4e11-8fe2-9ba4f531780c","startTime":1534421418000,"timestamp":"2018-08-16 20:10:18","totalMoney":0.26985,"userId":"17860596626","userName":"LAN"},{"coinName":"PWR","getPacketUser":"17860596626","imUserId":192205,"packetId":"9ed8d7a4-0e60-42e3-9760-6965e84339ce","startTime":1534421369000,"timestamp":"2018-08-16 20:09:29","totalMoney":1.0E-5,"userId":"18363825829","userName":"song12"},{"coinName":"PWR","getPacketUser":"17860596626","imUserId":192205,"packetId":"0e762efc-79da-41ee-9a10-4022d944fce8","startTime":1534421341000,"timestamp":"2018-08-16 20:09:01","totalMoney":0.002,"userId":"18363825829","userName":"song12"},{"coinName":"PWR","getPacketUser":"17860596626","imUserId":192205,"packetId":"61850b0e-f640-4c2d-968c-68370f954feb","startTime":1534339053000,"timestamp":"2018-08-15 21:17:33","totalMoney":0.01,"userId":"18363825829","userName":"song12"}]
     */

    private String result;
    private PageBean page;
    private List<GetRedPacketRecordListBean> getRedPacketRecordList;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public PageBean getPage() {
        return page;
    }

    public void setPage(PageBean page) {
        this.page = page;
    }

    public List<GetRedPacketRecordListBean> getGetRedPacketRecordList() {
        return getRedPacketRecordList;
    }

    public void setGetRedPacketRecordList(List<GetRedPacketRecordListBean> getRedPacketRecordList) {
        this.getRedPacketRecordList = getRedPacketRecordList;
    }

    public static class PageBean {
        /**
         * allRow : 6
         * currentPage : 1
         * pageSize : 10
         * totalPage : 1
         */

        private int allRow;
        private int currentPage;
        private int pageSize;
        private int totalPage;

        public int getAllRow() {
            return allRow;
        }

        public void setAllRow(int allRow) {
            this.allRow = allRow;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getTotalPage() {
            return totalPage;
        }

        public void setTotalPage(int totalPage) {
            this.totalPage = totalPage;
        }
    }

    public static class GetRedPacketRecordListBean {
        /**
         * coinName : PWR
         * getPacketUser : 17860596626
         * imUserId : 192205
         * packetId : 94db5035-70e9-4b0e-a2ef-a7d737fc1f34
         * startTime : 1534469652000
         * timestamp : 2018-08-17 09:34:12
         * totalMoney : 1.0E-5
         * userId : 18363825829
         * userName : song12
         */

        private String coinName;
        private String getPacketUser;
        private int imUserId;
        private String packetId;
        private long startTime;
        private String timestamp;
        private double totalMoney;
        private String userId;
        private String userName;
        private int type;

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getCoinName() {
            return coinName;
        }

        public void setCoinName(String coinName) {
            this.coinName = coinName;
        }

        public String getGetPacketUser() {
            return getPacketUser;
        }

        public void setGetPacketUser(String getPacketUser) {
            this.getPacketUser = getPacketUser;
        }

        public int getImUserId() {
            return imUserId;
        }

        public void setImUserId(int imUserId) {
            this.imUserId = imUserId;
        }

        public String getPacketId() {
            return packetId;
        }

        public void setPacketId(String packetId) {
            this.packetId = packetId;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public double getTotalMoney() {
            return totalMoney;
        }

        public void setTotalMoney(double totalMoney) {
            this.totalMoney = totalMoney;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }
    }
}
