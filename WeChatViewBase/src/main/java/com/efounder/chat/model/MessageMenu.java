package com.efounder.chat.model;

import com.efounder.message.struct.IMStruct002;

/**
 * 消息长按后的菜单
 */
public class MessageMenu {
    //复制消息
    public static final int COPY_MESSAGE = 0;
    //删除消息
    public static final int DELETE_MESSAGE = 1;
    //保存图片
    public static final int SAVE_IMAGE = 2;
    //查询表单消息
    public static final int SERACH_FORM_MESSAGE = 3;
    //撤回消息
    public static final int RECALL_MESSAGE = 4;
    //文件本地位置
    public static final int FILE_LOCATION = 5;
    //图片扫码
    public static final int SCAN_IMAGE_QR = 6;
    //分享消息
    public static final int SHARE_MESSAGE = 7;
    //翻译消息
    public static final int TRANSLATE_MESSAGE = 8;
    private IMStruct002 imStruct002;
    private int messageChildType;
    private String menuTitle;
    private int menuType;
    //其他数据可以放这里
    private Object otherResult;

    public MessageMenu(IMStruct002 imStruct002, String menuTitle, int menuType) {
        this.imStruct002 = imStruct002;
        this.menuTitle = menuTitle;
        this.menuType = menuType;
    }

    public MessageMenu(IMStruct002 imStruct002, String menuTitle, int menuType, Object otherResult) {
        this.imStruct002 = imStruct002;
        this.menuTitle = menuTitle;
        this.menuType = menuType;
        this.otherResult = otherResult;
    }

    public IMStruct002 getImStruct002() {
        return imStruct002;
    }

    public MessageMenu setImStruct002(IMStruct002 imStruct002) {
        this.imStruct002 = imStruct002;
        setMessageChildType(imStruct002.getMessageChildType());
        return this;
    }

    public int getMessageChildType() {
        return messageChildType;
    }

    private MessageMenu setMessageChildType(int messageChildType) {
        this.messageChildType = messageChildType;
        return this;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public MessageMenu setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
        return this;
    }

    public int getMenuType() {
        return menuType;
    }

    public MessageMenu setMenuType(int menuType) {
        this.menuType = menuType;
        return this;
    }

    public Object getOtherResult() {
        return otherResult;
    }

    public MessageMenu setOtherResult(Object otherResult) {
        this.otherResult = otherResult;
        return this;
    }
}