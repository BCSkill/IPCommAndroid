package com.efounder.chat.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by will on 17-5-9.
 */

public class Task implements Parcelable{

    private String icon;
    private String id;
    private String title;
    private String info;
    private int todoNum;
    private String status;

    //共享中
    private String product;

    protected Task(Parcel in) {
        icon = in.readString();
        id = in.readString();
        title = in.readString();
        info = in.readString();
        todoNum = in.readInt();
        status = in.readString();
        flow_id = in.readString();
        product = in.readString();
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public String getFlow_id() {
        return flow_id;
    }

    public void setFlow_id(String flow_id) {
        this.flow_id = flow_id;
    }

    private String flow_id;

    public Task() {
    }

    public Task(String icon, String title, String info, int todoNum, String id, String product) {
        this.icon = icon;
        this.title = title;
        this.info = info;
        this.todoNum = todoNum;
        this.id = id;
        this.product = product;
    }
    public Task(String icon, String title, String info, int todoNum, String id) {
        this.icon = icon;
        this.title = title;
        this.info = info;
        this.todoNum = todoNum;
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getTodoNum() {
        return todoNum;
    }

    public void setTodoNum(int todoNum) {
        this.todoNum = todoNum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(icon);
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(info);
        parcel.writeInt(todoNum);
        parcel.writeString(status);
        parcel.writeString(flow_id);
        parcel.writeString(product);
    }
}
