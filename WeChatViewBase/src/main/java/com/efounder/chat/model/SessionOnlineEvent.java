package com.efounder.chat.model;

/**
 *  //消息页面多设备登录通知
 * @author JamesZhang
 *
 */

public class SessionOnlineEvent {
    //应用下线消息
    public static int TYPE_OFFLINE = 1;
    //消息页面多设备登录通知
    public static int TYPE_SESSION_ONLINE = 1;
    //应用登陆过期
    public static int TYPE_LOGIN_OUT_OF_DATE = 2;
    int type;

    public SessionOnlineEvent() {
    }

    public SessionOnlineEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public SessionOnlineEvent setType(int type) {
        this.type = type;
        return this;
    }
}
