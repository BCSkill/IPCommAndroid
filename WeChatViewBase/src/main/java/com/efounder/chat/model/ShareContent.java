package com.efounder.chat.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author will
 */
public class ShareContent implements Serializable{

    private int type;//0 文字 1 图片 2 图文消息  图片文字带url
    private String subject;//主题，标题
    private String text;//文本，内容
    private String url;//网页内容的url
    private String imgUrl;//网页内容的图片url
    //商品内容的url
    private String goodUrl;
    private ArrayList<String> pictureList;//分享的图片

    public ShareContent() {
    }

    public String getGoodUrl() {
        return goodUrl;
    }

    public void setGoodUrl(String goodUrl) {
        this.goodUrl = goodUrl;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ArrayList<String> getPictureList() {
        return pictureList;
    }

    public void setPictureList(ArrayList<String> pictureList) {
        this.pictureList = pictureList;
    }
}
