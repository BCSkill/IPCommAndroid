package com.efounder.chat.model;

import com.efounder.mobilecomps.contacts.User;

import java.io.Serializable;
import java.util.List;

public class Group implements Serializable,Cloneable {

    /**
     * 群聊组id
     */
    private int groupId;
    /**
     * 群组名称
     */
    private String groupName;

    /**
     * 群组名称拼音首字母简写（例如 松1 =>s1）
     **/
    private String groupNamePyInitial;
    /**
     * 群组名称拼音全拼
     **/
    private String groupNameQuanPin;
    /**
     * 群组成员
     */
    private List<User> users;
    /**
     * 创建者id
     */
    private int createId;
    /**
     * 创建时间
     */
    private long createTime;
    /**
     * 是否免打扰
     **/
    private Boolean isBother = false;
    /**
     * 是否置顶
     **/
    private Boolean isTop = false;
    /**
     * 头像文件名
     **/
    private String avatar;
    /**
     * 登录的用户id
     */
    private int loginUserId;
    /**
     * 是否存在
     */
    private boolean isExist = true;
    /**
     * 群组类型 0表示需要管理员同意才能加群 ；1表示随意加群
     */
    private int groupType;

    /**
     * 群是否禁言（禁止群里讲话）
     */
    private boolean isBanSpeak;
    //群英文名
    private String englihName;
    //群介绍 群公告字段
    private String recommond;
    //加群时的验证信息
    private String leaveMessage;

    public Group() {
        super();
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public int getCreateId() {
        return createId;
    }

    public void setCreateId(int createId) {
        this.createId = createId;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public Boolean getIsBother() {
        return isBother;
    }

    public void setIsBother(Boolean isBother) {
        this.isBother = isBother;
    }

    public Boolean getIsTop() {
        return isTop;
    }

    public void setIsTop(Boolean isTop) {
        this.isTop = isTop;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getLoginUserId() {
        return loginUserId;
    }

    public void setLoginUserId(int loginUserId) {
        this.loginUserId = loginUserId;
    }

    public boolean isExist() {
        return isExist;
    }

    public Group setExist(boolean exist) {
        isExist = exist;
        return this;
    }

    public boolean isBanSpeak() {
        return isBanSpeak;
    }

    public Group setBanSpeak(boolean banSpeak) {
        isBanSpeak = banSpeak;
        return this;
    }

    public int getGroupType() {
        return groupType;
    }

    public Group setGroupType(int groupType) {
        this.groupType = groupType;
        return this;
    }

    public String getEnglihName() {
        return englihName;
    }

    public Group setEnglihName(String englihName) {
        this.englihName = englihName;
        return this;
    }

    public String getRecommond() {
        return recommond;
    }

    public Group setRecommond(String recommond) {
        this.recommond = recommond;
        return this;
    }

    public String getLeaveMessage() {
        return leaveMessage;
    }

    public Group setLeaveMessage(String leaveMessage) {
        this.leaveMessage = leaveMessage;
        return this;
    }
    @Override
    public Object clone() throws CloneNotSupportedException {
        Group group = (Group) super.clone();
        return group;
    }

    public String getGroupNamePyInitial() {
        return groupNamePyInitial;
    }

    public Group setGroupNamePyInitial(String groupNamePyInitial) {
        this.groupNamePyInitial = groupNamePyInitial;
        return this;
    }

    public String getGroupNameQuanPin() {
        return groupNameQuanPin;
    }

    public Group setGroupNameQuanPin(String groupNameQuanPin) {
        this.groupNameQuanPin = groupNameQuanPin;
        return this;
    }
}
