package com.efounder.chat.model;

import java.io.Serializable;

/**
 * 聊天界面上方悬浮view传递的数据
 * @author wang
 * @param <T>
 */
public class ChatTopFloatModel<T> implements Serializable {
    /** 处理子view的类名 */
    private String clazzName;
    /** 消息接收方的id */
    private int chatToUserId;
    /** 子view的数据 */
    private T data;
    /** 父view边距 */
    private int topMargin;
    private int leftMargin;
    private int rightMargin;
    private int bottomMargin;

    public ChatTopFloatModel() {
    }

    public String getClazzName() {
        return clazzName;
    }

    public void setClazzName(String clazzName) {
        this.clazzName = clazzName;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getTopMargin() {
        return topMargin;
    }

    public void setTopMargin(int topMargin) {
        this.topMargin = topMargin;
    }

    public int getLeftMargin() {
        return leftMargin;
    }

    public void setLeftMargin(int leftMargin) {
        this.leftMargin = leftMargin;
    }

    public int getRightMargin() {
        return rightMargin;
    }

    public void setRightMargin(int rightMargin) {
        this.rightMargin = rightMargin;
    }

    public int getBottomMargin() {
        return bottomMargin;
    }

    public void setBottomMargin(int bottomMargin) {
        this.bottomMargin = bottomMargin;
    }

    public int getChatToUserId() {
        return chatToUserId;
    }

    public void setChatToUserId(int chatToUserId) {
        this.chatToUserId = chatToUserId;
    }

    public void setMargin(int left, int top, int right, int bottomMargin) {
        this.leftMargin = left;
        this.topMargin = top;
        this.rightMargin = right;
        this.bottomMargin = bottomMargin;
    }
}
