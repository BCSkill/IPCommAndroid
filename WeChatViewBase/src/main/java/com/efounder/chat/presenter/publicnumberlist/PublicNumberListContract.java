package com.efounder.chat.presenter.publicnumberlist;

import android.content.Context;

import com.efounder.frame.baseui.BaseContract;
import com.efounder.mobilecomps.contacts.PinyinComparator;
import com.efounder.mobilecomps.contacts.User;

import java.util.List;

/**
 * author : zzj
 * e-mail : zhangzhijun@pansoft.com
 * date   : 2018/8/28 11:18
 * desc   :  公众号列表view 和 presenter 的契约
 * version: 1.0
 */
public interface PublicNumberListContract {

    //公众号列表的presenter接口
    interface Presenter extends BaseContract.BasePresenter{
        /**
         * 为ListView填充数据
         *
         * @param users
         * @return
         */
        List<User> filledData(List<User> users);

        List<User> filledData(Context context,String[] data);
        /**
         * 根据输入框中的值来过滤数据
         *
         * @param filterStr
         */
        void filterData(String filterStr,List<User> userList,PinyinComparator pinyinComparator);

    }

    //公众号列表的view 接口
    interface View extends BaseContract.BaseView<Presenter> {
        /**
         * 根据输入框中的值来过滤数据成功
         *
         * @param userList  过滤后的集合  在fragment中更新列表
         */
        void filterDataSuccess(List<User> userList);
    }
}
