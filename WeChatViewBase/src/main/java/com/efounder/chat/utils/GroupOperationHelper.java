package com.efounder.chat.utils;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.util.AppContext;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * group操作类，包含对群成员排序等
 *
 * @author YQS
 */

public class GroupOperationHelper {
    private static final String TAG = "GroupOperationHelper";


    /**
     * 对群成员排序（群主管理员优先显示在前面）
     *
     * @param userList
     * @param groupId
     * @return
     */
    public static List<User> sortGroupUser(List<User> userList, int groupId) {
        if (userList == null) {
            return null;
        }

        //群组普通成员= 0; 群组管理员=1;群组创建者=9
        Collections.sort(userList, new Comparator<User>() {
            @Override
            public int compare(User user1, User user2) {
                if (user1.getGroupUserType() > user2.getGroupUserType()) {
                    return -1;
                } else if (user1.getGroupUserType() == user2.getGroupUserType()) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });
        return userList;
    }


    /**
     * 获取邀请进群的群消息
     *
     * @param groupId
     * @param selectUserList
     * @return
     */
    public static String getInviteToGroupMesage(int groupId, List<User> selectUserList) {
        User self = WeChatDBManager.getInstance().getOneUserById(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        String selfName = self.getNickName();

        StringBuffer bufferName = new StringBuffer();

        for (int i = 0; i < selectUserList.size(); i++) {
            if (selectUserList.get(i).getId() == self.getId()) {
                continue;
            }
            bufferName.append(selectUserList.get(i).getNickName()).append("、");

        }
        String menmberName = bufferName.substring(0, bufferName.length() - 1);
//        if (groupName.length() > 30) {
//            groupName = groupName.substring(0, 30);
//        }
        String content = AppContext.getInstance().getResources()
                .getString(R.string.wechatview_invite_to_group, selfName, menmberName);
        return content;
    }

    /**
     * 获取自由进群的群消息
     *
     * @return
     */
    public static String getFreeToGroupMesage() {
        User self = WeChatDBManager.getInstance().getOneUserById(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        String selfName = self.getNickName();
        String content = AppContext.getInstance().getResources()
                .getString(R.string.wechatview_free_to_group, selfName);
        return content;
    }

    /**
     * 获取审批同意进群进群的群消息
     *
     * @param groupId
     * @param user
     * @return
     */
    public static String getAgreeToGroupMesage(int groupId, User user) {
        User self = WeChatDBManager.getInstance().getOneUserById(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        String selfName = self.getNickName();
        String content = AppContext.getInstance().getResources()
                .getString(R.string.wechatview_agree_to_group, selfName, user.getNickName());
        return content;
    }
}
