package com.efounder.chat.utils;

/**
 * 消息时间的一个类
 * @author YQS
 */
public class TimeInfo {
    private long startTime;
    private long endTime;

    public TimeInfo() {
    }

    public long getStartTime() {
        return this.startTime;
    }

    public void setStartTime(long var1) {
        this.startTime = var1;
    }

    public long getEndTime() {
        return this.endTime;
    }

    public void setEndTime(long var1) {
        this.endTime = var1;
    }
}
