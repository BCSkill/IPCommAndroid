package com.efounder.chat.utils;

/**
 * 七牛上传工具类
 * 上传视频、图片、语音 返回在线url 并封装struct
 * Created by long on 2016/7/27.
 */

public class MessageUtils {

    private static final String TAG ="PublicMessageUtils";
    public static final String MSG_BASE_URL = "http://oap8mw8pg.bkt.clouddn.com/";
//    //视频等上传的设置
//    private static Configuration config = new Configuration.Builder()
//            .chunkSize(256*1024)//chunkSize必须为256K的整数倍
//            .putThreshhold(256*1024)
//            .build();
//    //图片上传的设置
//    private static  Configuration picConfig = new Configuration.Builder()
//            .chunkSize(16*1024)
//            .putThreshhold(32*1024)
//            .build();
//    private static UploadManager uploadManager = new UploadManager(config);
//    private static UploadManager picUploadManager = new UploadManager(picConfig);
//    //property为宽高比,时长等信息
//    public static void sendMsg(final String msgPath, final String property,
//                               final ChatSenderFragment.PreSendMessageCallback preSendMessageCallback,
//                               final int messagechildType,byte toUserType){
//        final IMStruct002 imStruct002 = getStruct("%s%",msgPath,property,messagechildType,toUserType);
//        if (preSendMessageCallback != null){
//            preSendMessageCallback.preSendMessage(imStruct002);
//        }
//        //preSend之后,上传,sendPre
//        upload(msgPath,imStruct002,preSendMessageCallback,messagechildType);
//
//    }
//
//    public static void upload(final String msgPath, final IMStruct002 imStruct002, final ChatSenderFragment.PreSendMessageCallback preSendMessageCallback, final int messageChildType){
//        final String token = "tyrO2o43Y96uUq5bGwNEkVhZfahYPn2V81XfFEzy:ab1lSJZP_0mvXcXOnp_9cxsEcLQ=:eyJzY29wZSI6InRlc3QiLCJkZWFkbGluZSI6MzI0NzU4MzQ0MX0=";
//        HashMap<String, String> map = new HashMap<String, String>();
//        map.put("x:phone", "88888888");
//        if (messageChildType==MessageChildTypeConstant.subtype_image){//图片类型，使用图片上传Manager，32K阈值32K分片上传
//            picUploadManager.put(msgPath, null, token,
//                    new UpCompletionHandler() {
//                        @Override
//                        public void complete(String key, ResponseInfo info, JSONObject res) {
//                            if(info.isOK()){
//                                try {
//                                    String resKey =  res.getString("key");
//                                    /**图片、视频、语音等资源路径*/
//                                    String resUrl =MSG_BASE_URL +resKey;
//                                    if (preSendMessageCallback != null){
//                                        String message = imStruct002.getMessage();
//                                        message = message.replaceAll("%s%",resUrl);
//                                        imStruct002.setMessage(message);
//                                        preSendMessageCallback.sendPreMessage(imStruct002);
//                                    }
//                                    Log.i(TAG, resUrl);
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }else {
//                                Log.e("七牛上传失败",info+"");
//                                Toast.makeText(AppContext.getInstance(),"上传失败，请检查文件路径或类型",Toast.LENGTH_SHORT).show();
//                                if (preSendMessageCallback != null){
//                                    imStruct002.putExtra("progress",-1);
//                                    preSendMessageCallback.updateProgress(imStruct002,-1);
//                                }
//                            }
//                        }
//                    },new UploadOptions(map, null, false,
//                            new UpProgressHandler() {
//                                @Override
//                                public void progress(String key, double percent) {
//                                    int progress = (int)(percent*100);
//                                    Log.d("progress",progress+"");
//                                    if (preSendMessageCallback != null){
//                                        imStruct002.putExtra("progress",progress);
//                                        preSendMessageCallback.updateProgress(imStruct002,percent);
//                                    }
//                                }
//                            }, new UpCancellationSignal() {
//                        @Override
//                        public boolean isCancelled() {
//                            return false;
//                        }
//                    }));
//        }else {//其他类型，使用256K阈值256K分片上传
//            uploadManager.put(msgPath, null, token,
//                    new UpCompletionHandler() {
//                        @Override
//                        public void complete(String key, ResponseInfo info, JSONObject res) {
//                            if (info.isOK()) {
//                                try {
//                                    String resKey = res.getString("key");
//                                    /**图片、视频、语音等资源路径*/
//                                    String resUrl = MSG_BASE_URL + resKey;
//                                    if (preSendMessageCallback != null) {
//                                        String message = imStruct002.getMessage();
//                                        message = message.replaceAll("%s%", resUrl);
//                                        imStruct002.setMessage(message);
//                                        preSendMessageCallback.sendPreMessage(imStruct002);
//                                    }
//                                    Log.i(TAG, resUrl);
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            } else {
//                                Log.e("七牛上传失败", info + "");
//                                Toast.makeText(AppContext.getInstance(), "上传失败，请检查文件路径或类型", Toast.LENGTH_SHORT).show();
//                                if (preSendMessageCallback != null) {
//                                    imStruct002.putExtra("progress", -1);
//                                    preSendMessageCallback.updateProgress(imStruct002, -1);
//                                }
//                            }
//                        }
//                    }, new UploadOptions(map, null, false,
//                            new UpProgressHandler() {
//                                @Override
//                                public void progress(String key, double percent) {
//                                    int progress = (int) (percent * 100);
//                                    Log.d("progress", progress + "");
//                                    if (preSendMessageCallback != null) {
//                                        imStruct002.putExtra("progress", progress);
//                                        preSendMessageCallback.updateProgress(imStruct002, percent);
//                                    }
//                                }
//                            }, new UpCancellationSignal() {
//                        @Override
//                        public boolean isCancelled() {
//                            return false;
//                        }
//                    }));
//        }
//    }
//
//    private static IMStruct002 getStruct(String resUrl,String path,String property,int messageChildType,byte toUserType){
//        IMStruct002 imStruct002 = null;
//    	switch (messageChildType) {
//		case MessageChildTypeConstant.subtype_image:
//            imStruct002 = StructFactory.getInstance().createImageStruct(resUrl,path,property, toUserType);
//			break;
//		case MessageChildTypeConstant.subtype_voice:
//            imStruct002 = StructFactory.getInstance().createVoiceStruct(resUrl,path,property, toUserType);
//			break;
//		case MessageChildTypeConstant.subtype_smallVideo:
//            imStruct002 = StructFactory.getInstance().createVideoStruct(resUrl,path,property, toUserType);
//			break;
//		case MessageChildTypeConstant.subtype_location:
//            imStruct002 = StructFactory.getInstance().createMapStruct(resUrl, toUserType);
//			break;
//		default:
//			break;
//		}
//
//        return imStruct002;
//    }
}
