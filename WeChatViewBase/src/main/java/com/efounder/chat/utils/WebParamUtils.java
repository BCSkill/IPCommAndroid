package com.efounder.chat.utils;

import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.utils.Constants;

/**
 * 拼接网页请求的url参数
 * @author will
 */
public class WebParamUtils {

    /**
     * 根据param和value来拼接链接
     */
    public static String dealUrlParams(String param, String value) {
        StringBuilder urlParam = new StringBuilder();
        String[] params = param.split(";");
        String[] values = value.split(";");
        for (int i = 0; i < params.length; i++) {
            urlParam.append(params[i]).append("=");
            //用户名，手机号
            if ("@USERNAME@".equals(values[i])){
                urlParam.append(EnvironmentVariable.getUserName());
                //密码
            }else if ("@PASSWORD@".equals(values[i])) {
                urlParam.append(EnvironmentVariable.getProperty(Constants.CHAT_PASSWORD));
            }else if ("@AD@".equals(values[i])) {
                urlParam.append(EnvironmentVariable.getProperty("isAd", "0"));
            }else if ("@EMAIL@".equals(values[i])) {
                urlParam.append(EnvironmentVariable.getEmail());
            }else if ("@MD5EMAIL@".equals(values[i])) {
                urlParam.append(EnvironmentVariable.getMD5Email());
            }else if ("@MD5USERNAME@".equals(values[i])) {
                urlParam.append(EnvironmentVariable.getMD5UserID());
            }else if ("@MD5PASSWORD@".equals(values[i])) {
                urlParam.append(EnvironmentVariable.getMD5PassWord());
            }else if ("@MD5@".equals(values[i])) {
                urlParam.append(EnvironmentVariable.getMD5());
                //聊天id
            }else if ("@IMUSERID@".equals(values[i])) {
                urlParam.append(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID));
                //钱包账号
            }else if ("@USERID@".equals(values[i])) {
                urlParam.append(EnvironmentVariable.getProperty("tc_ethAddress"));
            }
            urlParam.append("&");
        }
        return urlParam.deleteCharAt(urlParam.length() - 1).toString();
    }

    /**
     * 根据url是否有？来判断要拼接？还是&还是“”空字符
     * @param url url
     * @return
     */
    public static String getAppendChar(String url) {
        if (url.contains("?")) {
            //最后是？，那么不拼任何字符
            if (url.endsWith("?")) {
                return "";
            }
            //其他位置是？，那么url有参数，拼接&+参数
            return "&";
        } else {
            //没有？，拼接？+参数
            return "?";
        }
    }
}
