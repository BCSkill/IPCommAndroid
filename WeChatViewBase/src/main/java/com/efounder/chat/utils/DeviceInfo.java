package com.efounder.chat.utils;

import android.os.Build;

public class DeviceInfo {
    //获取设备厂商
    public static String GetManufacturer()
    {
        return Build.MANUFACTURER;
    }
    //获取设备型号
    public static String GetModel()
    {
        return Build.MODEL;
    }
    //获取设备操作系统版本
    public static String GetOs()
    {
        return Build.VERSION.RELEASE;
    }
}
