package com.efounder.chat.utils;

import androidx.collection.ArrayMap;

/**
 * 接口请求状态,避免多次重复调用接口
 * Created by YQS on 2018/2/28.
 */

public class HttpRequestStatusUtils {
    //请求群组数据
    public final static String GROUP_TYPE = "group";
    //请求用户数据
    public final static String USER_TYPE = "user";
    //创建群头像
    public final static String AVATAR_TYPE = "createAvatar";
    public static final ArrayMap<String, Integer> map = new ArrayMap<>();

    public static void add(String key) {
        map.put(key, 1);
    }

    public static void remove(String key) {
        map.remove(key);
    }

    public static void add(String key, String type) {
        map.put(type + key, 1);
    }

    public static void remove(String key, String type) {
        map.remove(type + key);
    }


    public static boolean isRequest(String key) {
        return map.containsKey(key);
    }

    public static boolean isRequest(String key, String type) {
        return map.containsKey(type + key);
    }
}
