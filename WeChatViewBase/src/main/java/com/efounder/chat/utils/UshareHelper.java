package com.efounder.chat.utils;

import android.content.Context;

/**
 * 友盟分享 辅助 通过反射
 *
 * @author YQS
 */
public interface UshareHelper {
    /**
     * @param context
     * @param url           分享的url
     * @param title         标题
     * @param thumbDrawable 缩略图
     * @param description   描述
     * @param listener      回调
     */
    public void share(Context context, String url, String title, int thumbDrawable
            , String description, ShareListener listener);

    /**
     * @param context
     * @param url           分享的url
     * @param title         标题
     * @param thumbDrawable 缩略图
     * @param description   描述
     * @param objectParam 其他需要传递的参数
     * @param listener      回调
     */
    public void shareWithOtherParams(Context context, String url, String title, int thumbDrawable
            , String description, Object objectParam, ShareListener listener);

    /**
     * 释放
     */
    public void release(Context context);

    public interface ShareListener {
        //void share(String title, int thumbDrawable,String description);
        //1 分享成功 2 分享失败 3 取消分享
        void share(int type);
    }
}
