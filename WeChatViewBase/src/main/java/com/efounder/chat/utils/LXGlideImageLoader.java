package com.efounder.chat.utils;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.efounder.chat.R;
import com.efounder.imageloader.GlideImageLoader;
import com.efounder.util.URLModifyDynamic;

import java.io.File;

/**
 * 联信图片加载
 * Created by YQS on 2018/3/2.
 */

public class LXGlideImageLoader extends GlideImageLoader {

    private volatile static LXGlideImageLoader lxGlideImageLoader;

    //默认头像圆角
    public static final int DEFATLT_AVATAR_RADIUS_12 = 12;
    public static final int DEFATLT_AVATAR_RADIUS_10 = 10;

    public static LXGlideImageLoader getInstance() {
        if (lxGlideImageLoader == null) {
            synchronized (LXGlideImageLoader.class) {
                if (lxGlideImageLoader == null) {
                    lxGlideImageLoader = new LXGlideImageLoader();
                }
            }
        }
        return lxGlideImageLoader;
    }

    /**
     * 显示头像使用
     *
     * @param context
     * @param imageView
     * @param imageUrl
     */
    @Deprecated
    public void showUserAvatar(Context context, ImageView imageView, String imageUrl) {
        displayImage(context, imageView, URLModifyDynamic.getInstance().replace(imageUrl), R.drawable.default_useravatar, R.drawable.default_useravatar);
    }

    public void showUserAvatar(Fragment fragment, ImageView imageView, String imageUrl) {
        displayImage(fragment, imageView, URLModifyDynamic.getInstance().replace(imageUrl), R.drawable.default_useravatar, R.drawable.default_useravatar);
    }

    public void showUserAvatar(Activity activity, ImageView imageView, String imageUrl) {
        displayImage(activity, imageView, URLModifyDynamic.getInstance().replace(imageUrl), R.drawable.default_useravatar, R.drawable.default_useravatar);
    }

    /**
     * 显示圆角用户头像
     *
     * @param context
     * @param imageView
     * @param imageUrl
     * @param rounderCorner 圆角值
     */
    @Deprecated
    public void showRoundUserAvatar(Context context, ImageView imageView, Object imageUrl, int rounderCorner) {
        imageUrl = getImageUrl(imageUrl, R.drawable.default_user_avatar);
        showImageByGlide(context, imageView, imageUrl,
                R.drawable.default_useravatar, R.drawable.default_useravatar, rounderCorner);
    }

    public void showRoundUserAvatar(Fragment fragment, ImageView imageView, Object imageUrl, int rounderCorner) {
        imageUrl = getImageUrl(imageUrl, R.drawable.default_user_avatar);
        if (fragment != null) {
            RequestManager requestManager = Glide.with(fragment);
            showImageByGlide(requestManager, imageView, imageUrl,
                    R.drawable.default_useravatar, R.drawable.default_useravatar, rounderCorner);
        }
    }

    public void showRoundUserAvatar(Activity activity, ImageView imageView, Object imageUrl, int rounderCorner) {
        imageUrl = getImageUrl(imageUrl, R.drawable.default_user_avatar);
        if (activity != null && !activity.isFinishing() && !activity.isDestroyed()) {
            RequestManager requestManager = Glide.with(activity);
            showImageByGlide(requestManager, imageView, imageUrl,
                    R.drawable.default_useravatar, R.drawable.default_useravatar, rounderCorner);
        }
    }

    @NonNull
    private Object getImageUrl(Object imageUrl, int default_user_avatar) {
        if (imageUrl != null && imageUrl instanceof String) {
            imageUrl = URLModifyDynamic.getInstance().replace((String) imageUrl);
        }
        if ("".equals(imageUrl) || imageUrl == null) {
            imageUrl = default_user_avatar;
        }
        return imageUrl;
    }

    /**
     * 显示圆角群组头像
     *
     * @param context
     * @param imageView
     * @param imageUrl
     * @param rounderCorner 圆角值
     */
    @Deprecated
    public void showRoundGrouAvatar(Context context, ImageView imageView, Object imageUrl, int rounderCorner) {
        imageUrl = getImageUrl(imageUrl, R.drawable.default_groupavatar);
        showImageByGlide(context, imageView, imageUrl,
                R.drawable.default_groupavatar, R.drawable.default_groupavatar, rounderCorner);
    }

    public void showRoundGrouAvatar(Fragment fragment, ImageView imageView, Object imageUrl, int rounderCorner) {
        imageUrl = getImageUrl(imageUrl, R.drawable.default_groupavatar);
        if (fragment != null) {
            RequestManager requestManager = Glide.with(fragment);
            showImageByGlide(requestManager, imageView, imageUrl,
                    R.drawable.default_groupavatar, R.drawable.default_groupavatar, rounderCorner);
        }
    }

    public void showRoundGrouAvatar(Activity activity, ImageView imageView, Object imageUrl, int rounderCorner) {
        imageUrl = getImageUrl(imageUrl, R.drawable.default_groupavatar);
        if (activity != null && !activity.isFinishing() && !activity.isDestroyed()) {
            RequestManager requestManager = Glide.with(activity);
            showImageByGlide(requestManager, imageView, imageUrl,
                    R.drawable.default_groupavatar, R.drawable.default_groupavatar, rounderCorner);
        }
    }

    /**
     * 显示头像使用
     *
     * @param context
     * @param imageView
     * @param file
     */
    @Deprecated
    public void showUserAvatar(Context context, ImageView imageView, File file) {
        displayImage(context, imageView, file, R.drawable.default_useravatar, R.drawable.default_useravatar);
    }

    public void showUserAvatar(Fragment fragment, ImageView imageView, File file) {
        displayImage(fragment, imageView, file, R.drawable.default_useravatar, R.drawable.default_useravatar);
    }

    public void showUserAvatar(Activity activity, ImageView imageView, File file) {
        displayImage(activity, imageView, file, R.drawable.default_useravatar, R.drawable.default_useravatar);
    }

    /**
     * 显示群组头像
     *
     * @param context
     * @param imageView
     * @param imageUrl
     */
    @Deprecated
    public void showGroupAvatar(Context context, ImageView imageView, String imageUrl) {
        displayImage(context, imageView, URLModifyDynamic.getInstance().replace(imageUrl), R.drawable.default_groupavatar, R.drawable.default_groupavatar);
    }

    public void showGroupAvatar(Fragment fragment, ImageView imageView, String imageUrl) {
        displayImage(fragment, imageView, URLModifyDynamic.getInstance().replace(imageUrl), R.drawable.default_groupavatar, R.drawable.default_groupavatar);
    }

    public void showGroupAvatar(Activity activity, ImageView imageView, String imageUrl) {
        displayImage(activity, imageView, URLModifyDynamic.getInstance().replace(imageUrl), R.drawable.default_groupavatar, R.drawable.default_groupavatar);
    }

    /**
     * 显示群组头像
     *
     * @param context
     * @param imageView
     * @param file
     */
    @Deprecated
    public void showGroupAvatar(Context context, ImageView imageView, File file) {
        displayImage(context, imageView, file, R.drawable.default_groupavatar, R.drawable.default_groupavatar);
    }

    public void showGroupAvatar(Fragment fragment, ImageView imageView, File file) {
        displayImage(fragment, imageView, file, R.drawable.default_groupavatar, R.drawable.default_groupavatar);
    }

    public void showGroupAvatar(Activity activity, ImageView imageView, File file) {
        displayImage(activity, imageView, file, R.drawable.default_groupavatar, R.drawable.default_groupavatar);
    }
}
