package com.efounder.chat.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * android 原生progressdialog
 * Created by YQS on 2017/11/7.
 */
@Deprecated
public class ProgressDialogUtil {
    private static ProgressDialog dialog;

    /*
     * 显示dialog
     */
    public static void show(Context context, String msg) {
        if (context == null) {
            return;
        }
        if (dialog != null) {
            dialog.dismiss();
        }
        dialog = new ProgressDialog(context);
        dialog.setMessage(msg);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void dismiss() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
