//package com.efounder.chat.utils;
//
//import android.content.Context;
//import android.util.Log;
//
//import com.efounder.chat.db.WeChatDBManager;
//import com.efounder.chat.http.GetHttpUtil;
//import com.efounder.chat.model.ChatListItem;
//import com.efounder.chat.model.Group;
//import com.efounder.chat.struct.StructFactory;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.frame.utils.Constants;
//import com.efounder.message.manager.JFMessageManager;
//import com.efounder.message.struct.IMStruct002;
//import com.efounder.mobilecomps.contacts.User;
//
//import org.json.JSONException;
//
//import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
//
///**
// * Created by yqs on 2016/9/1.
// */
//
//public class ChatListItemUtil {
//    private final static String TAG = "ChatListItemUtil";
//
//    // FIXME: 2016/9/3 收到消息后生成或更新 chatlistitem
//    public static synchronized void createOrUpdateChatListItem(IMStruct002 imstruct002, Context
//            context) {
//        ChatListItem chatListItem = null;
//        WeChatDBManager weChatDBManager = WeChatDBManager.getInstance();
//        byte chatType = imstruct002.getToUserType();
//        //如果是公众号群发的消息 chattype是2 我们需要转为0使用
//        if (chatType == 2) {
//            chatType = 0;
//        }
//        // 给你发消息的那个人的id 可能是个人id 也可能是 groupid
//        int chatUserId = chatType == StructFactory.TO_USER_TYPE_GROUP ? imstruct002
//                .getToUserId() : imstruct002.getFromUserId();
//        int id = chatUserId;//此id有用处
//        if (chatUserId == Integer.parseInt(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID))) {
//            chatUserId = imstruct002.getToUserId();//如果fromid是自己（自己发给被人或者两个设备登陆一个账号，取touserID）
//        }
//        ChatListItem isExistsChatListItem = weChatDBManager.getChatListItem(chatUserId, chatType);
//        if (isExistsChatListItem != null) {
//            chatListItem = isExistsChatListItem;
//            //  chatListItem.setStruct002(imstruct002);
//            int unReadCount = 0;
//            // 如果是单聊或者公众号
//            if (chatType == StructFactory.TO_USER_TYPE_PERSONAL
//                    || chatType == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT) {
//                IMStruct002 im0 = JFMessageManager.getInstance().getLastMessage(chatUserId, (byte) 0);
//                if (im0 == null) {
//                    Log.i(TAG, "---Lastmessage  is  null ----");
//                }
//                // if (im0 != null) {
//                chatListItem.setStruct002(im0);
//                //  }
//                //调用方法获取未读消息条数
//                unReadCount = JFMessageManager.getInstance().getUnReadCount(chatUserId, (byte) 0);
//            } else if (chatType == StructFactory.TO_USER_TYPE_GROUP) {
//                IMStruct002 im1 = JFMessageManager.getInstance().getLastMessage(chatUserId, chatType);
//                if (im1 == null) {
//                    Log.i(TAG, "---Lastmessage  is  null ----");
//                }
//                //  if (im1 != null) {
//                chatListItem.setStruct002(im1);
//                //  }
//                unReadCount = JFMessageManager.getInstance().getUnReadCount(chatUserId, chatType);
//
//
//            }
//
//            if (unReadCount == 0) {
//                chatListItem.setBadgernum(-1);
//            } else {
//                chatListItem.setBadgernum(unReadCount);
//            }
//            Log.i(TAG, "ChatListItemUtil中获取的消息未读数量:" + chatListItem.getBadgernum());
//            weChatDBManager.insertOrUpdateChatList(chatListItem);
//
//        } else {
//            //1对方发给我们的消息
//            int addType = 1;
//            if (id == Integer.parseInt(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID))) {
//                // 如果消息来自自己
//                addType = 0;
//            }
//            addNewItem(imstruct002, addType, context, weChatDBManager);
//        }
//
//
//    }
//
//    /**
//     * 如果聊天列表中不存在,生成新的item
//     *
//     * @param imstruct002
//     * @param type        判断你发给对方还是对方发给你，1表示对方发给你，0表示你发给对方
//     */
//    private static synchronized void addNewItem(final IMStruct002 imstruct002, int type, Context
//            context, final WeChatDBManager weChatDBManager) {
//        Log.i(TAG, "-----生成item----");
//        final ChatListItem chatListItem = new ChatListItem();
//        chatListItem.setLoginUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)).intValue
//                ());
//        chatListItem.setStruct002(imstruct002);
//
//        if (imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_PERSONAL
//                || imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT) {
//            final int userMsgId = type == 0 ? imstruct002.getToUserId()
//                    : imstruct002.getFromUserId();
//
//            User user = weChatDBManager.getOneFriendById(userMsgId);
//            if (user.getNickName().equals(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)))) {
//                user = weChatDBManager.getOneUserById(userMsgId);
//            }
//            if (user == null) {
//                chatListItem.setUserId(imstruct002.getFromUserId());
//                chatListItem.setName(String.valueOf(imstruct002.getFromUserId()));
//                chatListItem.setAvatar("");
//                chatListItem.setIsTop(false);
//                chatListItem.setIsBother(false);
//                chatListItem.setChatType(StructFactory.TO_USER_TYPE_PERSONAL);
//                completeChatListItem(chatListItem, imstruct002, 1, weChatDBManager);
//
//
//            } else {
//                chatListItem.setUserId(userMsgId);
//                chatListItem.setName(user.getReMark());
//                chatListItem.setAvatar(user.getAvatar());
//                chatListItem.setIsTop(user.getIsTop());
//                chatListItem.setIsBother(user.getIsBother());
//                chatListItem.setChatType(ChatTypeUtil.getChatType(user.getId(), imstruct002,
//                        context));
//                completeChatListItem(chatListItem, imstruct002, type, weChatDBManager);
//            }
//
//
//        } else if (imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_GROUP) {
//            Group group = null;
//            int userMsgId = imstruct002.getToUserId();
//            chatListItem.setUserId(userMsgId);
//            group = weChatDBManager.getGroupWithUsers(userMsgId);
//            if (group != null && group.getGroupName().equals(String.valueOf(imstruct002
//                    .getToUserId()))) {
//                try {
//                    GetHttpUtil.getGroupById(context, imstruct002.getToUserId(), new GetHttpUtil
//                            .getGroupInfoCallBack() {
//
//                        @Override
//                        public void onGetGroupInfoSuccess(Group group) {
//
//                        }
//                    }, true);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (group != null) {
//                chatListItem.setName(group.getGroupName());
//                chatListItem.setAvatar(group.getAvatar());
//                chatListItem.setIsTop(group.getIsTop());
//                chatListItem.setIsBother(group.getIsBother());
//                chatListItem.setChatType(StructFactory.TO_USER_TYPE_GROUP);
//                completeChatListItem(chatListItem, imstruct002, type, weChatDBManager);
//            } else {
//                if (imstruct002.getFromUserId() == Integer.valueOf(EnvironmentVariable
//                        .getProperty(Constants.CHAT_USER_ID))) {
//                    type = 0;
//                } else {
//                    type = 1;
//                }
//                chatListItem.setName(String.valueOf(imstruct002.getToUserId()));
//                chatListItem.setAvatar("");
//                chatListItem.setIsTop(false);
//                chatListItem.setIsBother(false);
//                chatListItem.setChatType(StructFactory.TO_USER_TYPE_GROUP);
//
//                completeChatListItem(chatListItem, imstruct002, type, weChatDBManager);
//
//            }
//
//        }
//
//
//    }
//
//    /**
//     * 完成新生成的chatlistitem
//     */
//    private static void completeChatListItem(ChatListItem chatListItem, IMStruct002 imstruct002, int
//            type, WeChatDBManager weChatDBManager) {
//        int unReadCount = 0;
//        if (type == 0) {
//            // 我发送给对方
//            unReadCount = JFMessageManager.getInstance().
//                    getUnReadCount(imstruct002.getToUserId(), imstruct002.getToUserType());
//
//        } else {//对方发给我
//            if (imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_GROUP) {
//                unReadCount = JFMessageManager.getInstance().
//                        getUnReadCount(imstruct002.getToUserId(), imstruct002.getToUserType());
//            } else {
//                unReadCount = JFMessageManager.getInstance().
//                        getUnReadCount(imstruct002.getFromUserId(), (byte) 0);
//            }
//
//        }
//        if (unReadCount == 0) {
//            chatListItem.setBadgernum(-1);
//        } else {
//            chatListItem.setBadgernum(unReadCount);
//        }
//
//        weChatDBManager.insertOrUpdateChatList(chatListItem);
//    }
//
//
//    // FIXME: 2016/9/3 给别人发送消息后生成或更新 chatlistitem
//    public static synchronized void onUpdateOrCreateItem(final IMStruct002 imstruct002, int type,
//                                                         Context
//                                                                 context) {
//        WeChatDBManager weChatDBManager = WeChatDBManager.getInstance();
//        if (imstruct002.getState() == IMStruct002.MESSAGE_STATE_READ
//                || imstruct002.getState() == IMStruct002.MESSAGE_STATE_RECEIVE) {
//            return;
//        }
//
//        ChatListItem chatListItem = null;
//        int toUseId = imstruct002.getToUserId();
//        ChatListItem isExistsChatListItem = weChatDBManager.getChatListItem(toUseId,
//                imstruct002.getToUserType());
//        if (isExistsChatListItem != null) {
//            chatListItem = isExistsChatListItem;
//            chatListItem.setStruct002(imstruct002);
//            weChatDBManager.insertOrUpdateChatList(chatListItem);
//        } else {
//            addNewItem(imstruct002, 0, context, weChatDBManager);
//        }
//
//    }
//
////    public static void updateUnreadCount(int oldCount, int newCount) {
////        int num = 0;
////        num = newCount - oldCount;
////        if (WeChatDBManager.UNREADCOUNT + num >= 0) {
////            WeChatDBManager.UNREADCOUNT = WeChatDBManager.UNREADCOUNT + num;
////        } else {
////            WeChatDBManager.UNREADCOUNT = 0;
////        }
////
////
////    }
//
//
//}
//
//
//
//
//
