package com.efounder.chat.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import androidx.annotation.Nullable;

import com.efounder.chat.R;
import com.efounder.utils.ResStringUtil;
import com.utilcode.util.UriUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

//import javax.annotation.Nullable;

/**
 * 系统分享工具类
 */
public class SystemShareHelper {

    /**
     * 获取所有支持文本分享的app
     *
     * @param context
     * @return
     */
    public  List<ResolveInfo> getShareTextApps(Context context) {
        List<ResolveInfo> mApps = new ArrayList<ResolveInfo>();
        Intent intent = new Intent(Intent.ACTION_SEND, null);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("text/*");
        PackageManager pManager = context.getPackageManager();
        mApps = pManager.queryIntentActivities(intent,
                PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);
        return mApps;
    }

    /**
     * 获取所有支持图片分享的app
     *
     * @param context
     * @return
     */
    public static List<ResolveInfo> getShareImageApps(Context context) {
        List<ResolveInfo> mApps = new ArrayList<ResolveInfo>();
        Intent intent = new Intent(Intent.ACTION_SEND, null);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/*");
        PackageManager pManager = context.getPackageManager();
        mApps = pManager.queryIntentActivities(intent,
                PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);
        return mApps;
    }

    /**
     * 分享文字至所有第三方软件
     *
     * @param context
     * @param text
     * @param subject
     * @param url
     */
    public static void shareText(Context context, String text, @Nullable String subject, @Nullable String url) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (subject != null) {
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        }
        if (url != null) {
            intent.putExtra("url", url);
        }
        intent.setType("text/*");
        //设置分享列表的标题，并且每次都显示分享列表
        context.startActivity(Intent.createChooser(intent, ResStringUtil.getString(R.string.chat_share_to)));
    }

    /**
     * 分享单张图片至所有第三方软件
     *
     * @param context
     * @param imageFilePath
     */
    public static void shareOneImage(Context context, String imageFilePath) {

        //由文件得到uri
        Uri imageUri = Uri.fromFile(new File(imageFilePath));

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.setType("image/*");
        //设置分享列表的标题，并且每次都显示分享列表
        context.startActivity(Intent.createChooser(shareIntent,  ResStringUtil.getString(R.string.chat_share_to)));
    }

    /**
     * 分享多张图片至所有第三方软件
     *
     * @param context
     * @param pathList
     */
    public static void shareMutilImage(Context context, List<String> pathList) {
        if (pathList == null) {
            return;
        }
        ArrayList<Uri> uriList = new ArrayList<>();
        for (int i = 0; i < pathList.size(); i++) {
            uriList.add(UriUtils.getUriForFile(new File(pathList.get(i))));
        }

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uriList);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.setType("image/*");
        //设置分享列表的标题，并且每次都显示分享列表
        context.startActivity(Intent.createChooser(shareIntent,  ResStringUtil.getString(R.string.chat_share_to)));
    }


}
