package com.efounder.chat.utils;

import androidx.annotation.NonNull;

import com.efounder.chat.http.download.DownloadInfo;
import com.efounder.chat.http.download.DownloadManager;
import com.efounder.chat.http.download.State;
import com.efounder.form.util.CloudUtil;
import com.efounder.message.struct.IMStruct002;
import com.efounder.util.JSONUtil;

import net.sf.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by pansoft on 2017/12/22.
 */

public class IMStruct002Util {
    /**
     * 增加判断是否是系统消息，如果是系统消息，则不再聊天列表和聊天界面显示ui
     *
     * @param imStruct002
     * @return
     */
    public static boolean isSystemMessage(IMStruct002 imStruct002) {
//           if(imStruct002.getMessageChildType()==(short) 251){
//               return true;
//           }
        return false;
    }

    @NonNull
    public static IMStruct002 getImStruct002(IMStruct002 iMStruct002, int position) {
        String messageID = iMStruct002.getMessageID();
        String fileID;
        if (iMStruct002.getMessage().contains("Fileid")) {
            fileID = JSONUtil.parseJson(iMStruct002.getMessage()).get("Fileid").getAsString();
        } else if (iMStruct002.getMessage().contains("FileId")) {
            fileID = JSONUtil.parseJson(iMStruct002.getMessage()).get("FileId").getAsString();
        } else {
            fileID = "";
        }
        String downloadUrl = CloudUtil.CLOUD_BASE_URL + "files/" + fileID + "/download";
        DownloadInfo downloadInfo;
        if (null == DownloadManager.DOWNLOAD_INFO_HASHMAP.get(messageID)) {
            downloadInfo = new DownloadInfo();
            downloadInfo.state = State.DOWNLOAD_NOT;//未下载
            downloadInfo.messageID = messageID;
            downloadInfo.fileID = fileID;
            downloadInfo.downloadSize = 0;
            downloadInfo.progress = 0;
            downloadInfo.downloadUrl = downloadUrl;
            downloadInfo.fileName = getFileName(iMStruct002);
            downloadInfo.fileSize = getFileSize(iMStruct002);
            downloadInfo.downloadOnGPRS = false;
            DownloadManager.getInstance().addDownloadInfo(messageID, downloadInfo);
        } else {
            downloadInfo = DownloadManager.DOWNLOAD_INFO_HASHMAP.get(messageID);
        }
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("extraState", downloadInfo.state + "");
        map.put("extraMessageID", downloadInfo.messageID);
        map.put("extraFileID", fileID);
        map.put("extraDownloadSize", downloadInfo.downloadSize + "");
        map.put("extraProgress", downloadInfo.progress + "");
        map.put("extraDownloadUrl", downloadUrl);
        map.put("extraFileName", downloadInfo.fileName);
        map.put("extraFileSize", downloadInfo.fileSize);
        map.put("extraDownloadOnGPRS", downloadInfo.downloadOnGPRS + "");
        //以下为旧的json
        map.put("FileId", fileID);
        if (iMStruct002.getMessage().contains("FileLocalPath")) {
            map.put("FileLocalPath", JSONUtil.parseJson(iMStruct002.getMessage()).get("FileLocalPath").getAsString());
        } else {
            map.put("FileLocalPath", "");
        }
        map.put("FileSize", downloadInfo.fileSize);
        map.put("FileName", downloadInfo.fileName);
        String fileType = null;
        String fileName = downloadInfo.fileName;
        if (null != fileName && fileName.contains(".")) {
            fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
        } else {
            fileType = "";
        }
        map.put("FileType", fileType);

        JSONObject jsonObject = JSONObject.fromObject(map);
        String message = jsonObject.toString();
        IMStruct002 imStruct002 = new IMStruct002();
        imStruct002.setMessage(message);
        imStruct002.setFromUserId(iMStruct002.getFromUserId());
        imStruct002.setToUserId(iMStruct002.getToUserId());
        imStruct002.setToUserType(iMStruct002.getToUserType());
        imStruct002.setMessageChildType(iMStruct002.getMessageChildType());
        imStruct002.putExtra("progress", iMStruct002.getExtra("progress"));
        return imStruct002;
    }

    private static String getFileName(IMStruct002 imStruct002) {
        String fileName;
        String message = imStruct002.getMessage();
        JSONObject jsonObject = JSONObject.fromObject(message);
        fileName = (String) jsonObject.get("FileName");
        return fileName;
    }

    private static String getFileSize(IMStruct002 imStruct002) {
        String fileSize;
        String message = imStruct002.getMessage();
        JSONObject jsonObject = JSONObject.fromObject(message);
        fileSize = "" + jsonObject.get("FileSize");
        if (fileSize.equals("null")) {
            return "0.00B";
        }
        return fileSize;
    }
}

