package com.efounder.chat.utils;

import com.pansoft.library.CloudDiskBasicOperation;
import com.pansoft.library.utils.OkHttpsUtils;

import org.json.JSONException;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by zjk on 2017/6/6.
 * Desc:   群组定位操作类
 */

public class GroupLocationHttpRequest {
    private String baseUrl = "https://espace.solarsource.cn/eattendance/";
    private String queryUrl = "grouppositions?groupid=%s";
    private String addUrl = "grouppositions?";
    private String json;
    private String delUrl="grouppositions/del?";

    public GroupLocationHttpRequest() {
    }

    /**
     * 为群组增加办公地点
     *
     * @param groupId   群组id
     * @param longitude 经度
     * @param latitude  纬度
     * @param address   描述
     * @param back      回调
     */
    public void addGroupPosition(String groupId, String longitude, String latitude, String address, String shortName,final CloudDiskBasicOperation.ReturnBack back) {
        RequestBody rb = new FormBody.Builder()
                .add("groupid", groupId)
                .add("longitude", "" + longitude)
                .add("latitude", "" + latitude)
                .add("positionname", address)
                .add("shortpositionname", shortName)
                .build();
        OkHttpsUtils.initHttps();
        OkHttpsUtils.netRequest("post", baseUrl + addUrl, rb, null, new OkHttpsUtils.DownloadListener() {
            @Override
            public void onSuccess(byte[] bytes) {
                json = new String(bytes);
                if (back != null)
                    try {
                        back.onReturnBack(json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }

            @Override
            public void onFailure(final String exception) {
                json = exception;
                if (back != null)
                    try {
                        back.onReturnBack(json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        });
    }

    /**
     *  查询群组的办公地点
     * @param groupId  用户所属的群组ID
     * @param back
     */
    public void getGroupPositions(String groupId, final CloudDiskBasicOperation.ReturnBack back) {
        String url = String.format(baseUrl + queryUrl, groupId);
        OkHttpsUtils.initHttps();
        OkHttpsUtils.netRequest("get", url, null, null, new OkHttpsUtils.DownloadListener() {
            @Override
            public void onSuccess(byte[] bytes) {
                json = new String(bytes);
                if (back != null)
                    try {
                        back.onReturnBack(json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }

            @Override
            public void onFailure(final String exception) {
                json = exception;
                if (back != null)
                    try {
                        back.onReturnBack(json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        });
    }

    /**
     *  为群组删除办公地点
     * @param id  位置记录的id
     * @param back
     */
    public void delGroupPosition(String id, final CloudDiskBasicOperation.ReturnBack back) {
        RequestBody rb = new FormBody.Builder()
                .add("id", id)
                .build();
        OkHttpsUtils.initHttps();
        OkHttpsUtils.netRequest("post", baseUrl + delUrl, rb, null, new OkHttpsUtils.DownloadListener() {
            @Override
            public void onSuccess(byte[] bytes) {
                json = new String(bytes);
                if (back != null)
                    try {
                        back.onReturnBack(json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }

            @Override
            public void onFailure(final String exception) {
                json = exception;
                if (back != null)
                    try {
                        back.onReturnBack(json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        });
    }

}
