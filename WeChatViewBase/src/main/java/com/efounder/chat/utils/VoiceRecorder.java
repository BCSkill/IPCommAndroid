package com.efounder.chat.utils;

import android.content.Context;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.format.Time;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class VoiceRecorder {
	
	public static final String PREFIX = "voice";
	public static final String EXTENSION = ".amr";
	
	private MediaRecorder recorder;
	private boolean isRecording;
	private long startTime;
	private String voiceFilePath;
	private String voiceFileName;
	private File file;
	private Handler handler;
	private static String path = ImageUtil.amrpath;

	public VoiceRecorder(Handler handler1) {
		isRecording = false;
		voiceFilePath = null;
		voiceFileName = null;
		handler = handler1;
		
	}

	public String startRecording(String s, String s1, Context context) {
		file = null;
		try {
			if (recorder != null) {
				recorder.release();
				recorder = null;
			}
			recorder = new MediaRecorder();
			recorder.setAudioSource(1);
			recorder.setOutputFormat(3);
			recorder.setAudioEncoder(1);
			recorder.setAudioChannels(1);
			recorder.setAudioSamplingRate(8000);
			recorder.setAudioEncodingBitRate(64);
			voiceFileName = getVoiceFileName(s1);
			voiceFilePath = getVoiceFilePath();
			file = new File(voiceFilePath);
			recorder.setOutputFile(file.getAbsolutePath());
			recorder.prepare();
			isRecording = true;
			recorder.start();
		} catch (IOException exception) {
			exception.printStackTrace();
//			EMLog.e("voice", "prepare() failed"); 
		}
		new Thread(new Runnable() {

			public void run() {
				try {
					while (isRecording) {
						Message message = new Message();
						message.what = (recorder.getMaxAmplitude() * 13) / 32767;
						handler.sendMessage(message);
						SystemClock.sleep(100L);
					}
				} catch (Exception exception) {
					exception.printStackTrace();
//					EMLog.e("voice", exception.toString());
				}
			}

		}).start();
		startTime = (new Date()).getTime();
		Log.w("voice", (new StringBuilder("-------------------start voice recording to file:"))
				.append(file.getAbsolutePath()).toString());
		return file != null ? file.getAbsolutePath() : null;
	}

	public void discardRecording() {
		if (recorder != null) {
			try {
				recorder.stop();
				recorder.release();
				recorder = null;
				if (file != null && file.exists() && !file.isDirectory())
					file.delete();
			} catch (IllegalStateException illegalstateexception) {
			} catch (RuntimeException runtimeexception) {
			}
			isRecording = false;
		}
	}

	public int stopRecoding() {
		if (recorder != null) {
			isRecording = false;
			recorder.stop();
			recorder.release();
			recorder = null;
			if (file != null && file.exists() && file.isFile()
					&& file.length() == 0L) {
				file.delete();
				return -1011;
			} else {
				int i = (int) ((new Date()).getTime() - startTime) / 1000;
				Log.w("voice", (new StringBuilder(
						"------------voice recording finished. seconds:")).append(i)
						.append(" file length:").append(file.length())
						.toString());
				return i;
			}
		} else {
			return 0;
		}
	}

	protected void finalize() throws Throwable {
		super.finalize();
		if (recorder != null)
			recorder.release();
	}

	public String getVoiceFileName(String s) {
		Time time = new Time();
		time.setToNow();
		return (new StringBuilder(String.valueOf(s)))
				.append(time.toString().substring(0, 15)).append(".amr")
				.toString();
	}

	public boolean isRecording() {
		return isRecording;
	}

	public String getVoiceFilePath() {
		
		return (new StringBuilder())
				//.append(PathUtil.getInstance().getVoicePath()).append("/")
				.append(path)
				.append(voiceFileName).toString();
	}

	

}
