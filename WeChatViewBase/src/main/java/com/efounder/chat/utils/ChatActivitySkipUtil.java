package com.efounder.chat.utils;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.efounder.chat.R;
import com.efounder.chat.model.ChatTopFloatModel;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.util.EnvSupportManager;
import com.utilcode.util.ActivityUtils;

/**
 * 聊天页面会话跳转 （qq样式或者原来的样式）
 *
 * @author YQS
 */
public class ChatActivitySkipUtil {

    /**
     * 跳转聊天会话
     *
     * @param context
     * @param intent
     */
    public static void startChatActivity(Context context, Intent intent) {
        try {

            //通过主工程配置 可以获取到要跳转的界面
            String className = context.getResources().getString(R.string.chat_actiivity_class);
            Class clazz = Class.forName(className);
            intent.setClass(context, clazz);

            context.startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * 跳转聊天会话
     *
     * @param id       用户id
     * @param chatType 聊天类型
     */
    public static void startChatActivity(int id, byte chatType) {
        EnvironmentVariable.setProperty("currentChatUserId", String.valueOf(id));
        Intent intent = new Intent();
        intent.putExtra("id", id);
        intent.putExtra("chattype", chatType);
        ChatActivitySkipUtil.startChatActivity(ActivityUtils.getTopActivity(), intent);

    }

    /**
     * 跳转聊天会话, 带有设置的上方悬浮view参数
     *
     * @param id       用户id
     * @param chatType 聊天类型
     * @param chatTopFloatModel 上方悬浮view的参数
     */
    public static void startChatActivityWithTopFloatModel(int id, byte chatType, ChatTopFloatModel chatTopFloatModel) {
        EnvironmentVariable.setProperty("currentChatUserId", String.valueOf(id));
        Intent intent = new Intent();
        intent.putExtra("id", id);
        intent.putExtra("chattype", chatType);
        intent.putExtra("chatTopFloatModel", chatTopFloatModel);
        ChatActivitySkipUtil.startChatActivity(ActivityUtils.getTopActivity(), intent);

    }

    /**
     * 跳转用户信息
     *
     * @param context
     * @param intent
     */
    public static void startUserInfoActivity(Context context, Intent intent) {
        try {

            //通过主工程配置 可以获取到要跳转的界面
            String className = context.getResources().getString(R.string.userinfo_actiivity_class);
            Class clazz = Class.forName(className);
            intent.setClass(context, clazz);

            context.startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * 跳转用户信息
     *
     * @param context
     * @param intent
     */
    public static void startUserInfoActivityForResult(Context context, Intent intent, int requestCode) {
        try {

            //通过主工程配置 可以获取到要跳转的界面
            String className = context.getResources().getString(R.string.userinfo_actiivity_class);
            Class clazz = Class.forName(className);
            intent.setClass(context, clazz);
            ((Activity) context).startActivityForResult(intent, requestCode);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * go to map view
     */
    public static void startViewMapDetailActivity(Context context, Intent intent) {
        try {
            String className;
            if (!EnvSupportManager.isSupportGooglePlayUpgrade()) {
                className = "com.efounder.chat.activity.GaoDeLocationItemDetailActivity";
            } else {
                className = "com.efounder.chat.activity.GMapViewLocationActivity";
            }
            className = "com.efounder.chat.activity.GaoDeLocationItemDetailActivity";
            Class clazz = Class.forName(className);
            intent.setClass(context, clazz);
            context.startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * goto set group address activity
     */
    public static void startSetGroupAddressActivity(Context context, Intent intent, int requestCode) {
        try {
            String className;
            if (!EnvSupportManager.isSupportGooglePlayUpgrade()) {
                className = "com.efounder.chat.activity.ChatGroupAddressActivity";
            } else {
                className = "com.efounder.activity.MapsActivityCurrentPlace";
            }
            className = "com.efounder.chat.activity.ChatGroupAddressActivity";
            Class clazz = Class.forName(className);
            intent.setClass(context, clazz);
            ((Activity) context).startActivityForResult(intent, requestCode);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 跳转我的个人信息界面
     *
     * @param context
     */
    public static void startMyUserInfoActivity(Context context) {
        try {
            Intent intent = new Intent(context, Class.forName(context.getResources().getString(R.string.my_userinfo_actiivity_class)));
//           intent.putExtra("id", user.getId());
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
