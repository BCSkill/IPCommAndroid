package com.efounder.chat.utils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    private static SimpleDateFormat DATE = new SimpleDateFormat("yyyy-MM-dd");
	public static String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
	

	public static String getTodayDate(){
		return DATE.format(new Date());
	}
	

	public static String getDateStr(String datePattern,Date date){
		return new SimpleDateFormat(datePattern).format(date);
	}
	

	public static Date parse(String strDate, String pattern)
            throws ParseException {
        return StrUtils.isBlank(strDate) ? null : new SimpleDateFormat(
                pattern).parse(strDate);  
    }  
	

	public static Date parse(String strDate)
            throws ParseException {
        return StrUtils.isBlank(strDate) ? null : DATE.parse(strDate);  
    }  

	public static String getDateStr(Date date){
		return DATE.format(date);
	}
	
	

    public static Date addMonth(Date date, int n)
    {  
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);  
        cal.add(Calendar.MONTH, n);
        return cal.getTime();  
    }  
    
    
    public static String getLastDayOfMonth(String year, String month)
    {  
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.YEAR, Integer.parseInt(year));

        cal.set(Calendar.DATE, 1);

        cal.add(Calendar.MONTH, 1);

        cal.add(Calendar.DATE, -1);
        return String.valueOf(cal.get(Calendar.DAY_OF_MONTH));// �����ĩ�Ǽ���
    }  
  
    public static Date getDate(String year, String month, String day)
            throws ParseException {
        String result = year + "- "
                + (month.length() == 1 ? ("0 " + month) : month) + "- "  
                + (day.length() == 1 ? ("0 " + day) : day);  
        return parse(result);  
    }  
    
    public static Date getDate(int year,int month,int day)
    		throws ParseException {
    	String tempYear = year+"";
    	String tempMonth = month+"";
    	String tempDay = day+"";
    	String result = tempYear + "- "
                 + (tempMonth.length() == 1 ? ("0 " + month) : month) + "- "  
                 + (tempDay.length() == 1 ? ("0 " + day) : day);  
         return parse(result);
    }
	private static class StrUtils{

		static boolean isBlank(String str){
			if(str == null || str.equals("")){
				return true;
			}
			return false;
		}
	}

    // date要转换的date类型的时间
    public static long dateToLong(Date date) {
        return date.getTime();
    }

    public static long dateToLong(String str) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        return simpleDateFormat.parse(str).getTime();
    }

    /**
     * 时间戳转换成日期格式字符串
     * @param seconds 精确到秒的字符串
     * @param format
     * @return
     */
    public static String timeStamp2Date(String seconds,String format) {
        if(seconds == null || seconds.isEmpty() || seconds.equals("null")){
            return "";
        }
        if(format == null || format.isEmpty()){
            format = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        BigDecimal bigDecimal = new BigDecimal(seconds);
        String num = bigDecimal.toPlainString();
        return sdf.format(new Date(Long.valueOf(num)));
    }
}