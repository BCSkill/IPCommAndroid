package com.efounder.chat.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.core.xml.StubObject;
import com.efounder.chat.R;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.message.struct.IMStruct002;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

/**
 * 身份识别信息详情(山大智信中的)
 * Created by slp on 2018/2/5.
 */
@SuppressLint("ValidFragment")
public class IdentityInfoFragment extends BaseFragment {

    private StubObject mStubObject;
    private IMStruct002 mIMStruct002;
    private Hashtable<String, Object> mHashtable;
    private JSONObject mJSONObject;
    private JSONObject mDataJSON;

    //返回键
    private LinearLayout llBack;
    //标题
    private TextView title;
    private ImageView ivHead;
    private ImageView ivSex;
    private TextView tvName;
    private TextView tvSex;
    private TextView tvNation;
    private TextView tvBirth;
    private TextView tvFamily;
    private TextView tvCard;
    private TextView tvPolice;
    private TextView tvValid;

    public IdentityInfoFragment() {

    }

    @SuppressLint("ValidFragment")
    public IdentityInfoFragment(StubObject stubObject) {
        Hashtable<String, String> hashtable = stubObject.getStubTable();
        Bundle bundle = new Bundle();
        bundle.putSerializable("stubObject", stubObject);
        setArguments(bundle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_identity_info, container, false);

        llBack = (LinearLayout) rootView.findViewById(R.id.iv_back);
        title = (TextView) rootView.findViewById(R.id.tv_author);
        ivHead = (ImageView) rootView.findViewById(R.id.iv_iden_head);
        ivSex = (ImageView) rootView.findViewById(R.id.iv_iden_sex);
        tvName = (TextView) rootView.findViewById(R.id.tv_iden_name);
        tvSex = (TextView) rootView.findViewById(R.id.tv_iden_sex);
        tvNation = (TextView) rootView.findViewById(R.id.tv_iden_nation);
        tvBirth = (TextView) rootView.findViewById(R.id.tv_iden_birth);
        tvFamily = (TextView) rootView.findViewById(R.id.tv_iden_family);
        tvCard = (TextView) rootView.findViewById(R.id.tv_iden_card);
        tvPolice = (TextView) rootView.findViewById(R.id.tv_iden_police);
        tvValid = (TextView) rootView.findViewById(R.id.tv_iden_valid);

        title.setText(R.string.wechatview_Identity_Information);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        mStubObject = (StubObject) getArguments().getSerializable("stubObject");
        if (mStubObject != null) {
            mHashtable = mStubObject.getStubTable();
            mIMStruct002 = (IMStruct002) mHashtable.get("imStruct002");
            try {
                String json = new String(mIMStruct002.getBody(), "UTF-8");
                mJSONObject = new JSONObject(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mJSONObject != null) {
            try {
                mDataJSON = mJSONObject.getJSONObject("data");
                if(mDataJSON != null){
                    if (mDataJSON.has("name")) {
                        tvName.setText(mDataJSON.getString("name"));
                    }
                    if (mDataJSON.has("sex")) {
                        if (mDataJSON.getString("sex").equals("M")) {
                            tvSex.setText("男");
                            ivSex.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.iden_man));
                        } else {
                            tvSex.setText("女");
                            ivSex.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.iden_woman));
                        }
                    }
                    if (mDataJSON.has("idNumber")) {
                        tvCard.setText(mDataJSON.getString("idNumber"));
                    }
                    if (mDataJSON.has("idPic")) {
                        setNetPicture(mDataJSON.getString("idPic"), ivHead);
                    }
                    if (mDataJSON.has("birth")) {
                        String strBirth = mDataJSON.getString("birth");
                        String birthYear = strBirth.substring(0, 4);
                        String birthMonth = strBirth.substring(4, 6);
                        String birthDay = strBirth.substring(6, 8);

                        tvBirth.setText(birthYear + "年" + birthMonth + "月" + birthDay + "日");
                    }
                    if (mDataJSON.has("address")) {
                        tvFamily.setText(mDataJSON.getString("address"));
                    }
                    if (mDataJSON.has("nation")) {
                        tvNation.setText(mDataJSON.getString("nation"));
                    }
                    if (mDataJSON.has("policeStation")) {
                        tvPolice.setText(mDataJSON.getString("policeStation"));
                    }
                    if (mDataJSON.has("validityStart") && mDataJSON.has("validityEnd")) {
                        String validStart = mDataJSON.getString("validityStart");
                        String validEnd = mDataJSON.getString("validityEnd");

                        String startYear = validStart.substring(0, 4);
                        String startMonth = validStart.substring(4, 6);
                        String startDay = validStart.substring(6, 8);

                        String endYear = validEnd.substring(0, 4);
                        String endMonth = validEnd.substring(4, 6);
                        String endDay = validEnd.substring(6, 8);

                        tvValid.setText(startYear + "." + startMonth + "." + startDay + " - " + endYear + "." + endMonth + "." + endDay);

                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 加载网络图片
     *
     * @param url
     * @param img
     */
    private void setNetPicture(String url, ImageView img) {
        Glide
                .with(getContext())
                .load(url)
                .transition(new DrawableTransitionOptions().crossFade())
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(img);
    }
}
