//package com.efounder.chat.fragment;
//
//
//import android.content.Context;
//import android.os.Bundle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.efounder.chat.R;
//import com.efounder.frame.baseui.BaseFragment;
//import com.efounder.recycleviewhelper.CommonAdapter;
//import com.efounder.recycleviewhelper.base.ViewHolder;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 动态界面 已经弃用
// *
// * @author YQS
// */
//@Deprecated
//public class ChatUserDynamicListFragment extends BaseFragment {
//    private static final String TAG = "ChatUserDynaticListFrag";
//    private View rootView;
//    private RecyclerView recycleview;
//    private Myadapter myAdapter;
//    private List<String> dataList;
//
//
//    @Override
//    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.wechatview_fragment_chat_user_dynamiclist, container, false);
//        initView();
//        initData();
//        return rootView;
//    }
//
//
//    private void initView() {
//        recycleview = (RecyclerView) rootView.findViewById(R.id.recycleview);
//        recycleview.setLayoutManager(new LinearLayoutManager(getActivity()));
//    }
//
//    private void initData() {
//        dataList = new ArrayList<>();
//        myAdapter = new Myadapter(getActivity(), R.layout.wechatview_item_user_dynamiclist_item, dataList);
//        recycleview.setAdapter(myAdapter);
//        loadData();
//    }
//
//    private void loadData() {
//        for (int i = 0; i < 100; i++) {
//            dataList.add(i + "");
//        }
//        myAdapter.notifyDataSetChanged();
//    }
//
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//    }
//
//
//    private static class Myadapter extends CommonAdapter {
//        public Myadapter(Context context, int layoutId, List datas) {
//            super(context, layoutId, datas);
//        }
//
//        @Override
//        protected void convert(ViewHolder holder, Object o, int position) {
//            TextView tvName = holder.getView(R.id.textview);
//            tvName.setText(o.toString());
//        }
//
//    }
//}
