package com.efounder.chat.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;

import com.efounder.frame.baseui.BaseFragment;
import com.efounder.chat.R;
import com.efounder.tree.bean.TreeBean;
import com.efounder.tree.bean.TreeHelper;
import com.efounder.tree.bean.TreeListViewAdapter;
import com.efounder.tree.bean.TreeListViewAdapter.OnTreeNodeClickListener;
import com.efounder.tree.bean.TreeNode;
import com.efounder.tree.view.SimpleTreeAdapter;

/**
 * 
 * 树Fragment
 * @author hudq
 *
 */
public class TreeFragment extends BaseFragment implements OnClickListener{
	private List<TreeBean> mDatas;
	private ListView mTree;
	private TreeListViewAdapter mAdapter;
	private View view;

	
	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container,  Bundle savedInstanceState) {

		view = inflater.inflate(R.layout.fragment_tree, container, false);

		//1.XXX
		initDatas();
		initView();
	
		return view;
	}

	private void initView() {
		//返回按钮
		view.findViewById(R.id.iv_back).setOnClickListener(this);
		mTree = (ListView) view.findViewById(R.id.id_tree);
		try {
			mAdapter = new SimpleTreeAdapter(mTree, getActivity(), mDatas, 10);
			mAdapter.setOnTreeNodeClickListener(new OnTreeNodeClickListener() {
				@Override
				public void onClick(AdapterView<?> parent, View view,TreeNode node, int position) {
					if (node.isLeaf()) {
						//2.XXX
//						new LoadDataAsyncTask(node,mAdapter).execute();
//						Toast.makeText(getApplicationContext(), node.getName(),Toast.LENGTH_SHORT).show();
						if (node.isHasCheckBox()) {
							boolean isChecked = !node.isChecked();
							node.setChecked(isChecked);
						}
					}
					mAdapter.notifyDataSetChanged();
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		mTree.setAdapter(mAdapter);	}

	private void initDatas() {
		mDatas = new ArrayList<TreeBean>();
		//1.XXX 初始化数据时，parentID 传入 null，表示根节点
//		new LoadDataAsyncTask(null, mAdapter).execute();
		mDatas.add(new TreeBean(1 + "", 0 + "", "中原油田"));
		
		mDatas.add(new TreeBean(10 + "", 1 + "", "油田机关"));
		mDatas.add(new TreeBean(11 + "", 1 + "", "内蒙探区勘探开发指挥部"));
		mDatas.add(new TreeBean(12 + "", 1 + "", "普光分公司"));
		mDatas.add(new TreeBean(13 + "", 1 + "", "勘探局单位"));
		mDatas.add(new TreeBean(14 + "", 1 + "", "分公司单位"));
		
		mDatas.add(new TreeBean(100 + "", 10 + "", "规划计划处"));
		mDatas.add(new TreeBean(101 + "", 10 + "", "企业管理处"));
		mDatas.add(new TreeBean(102 + "", 10 + "", "人力资源处"));
		mDatas.add(new TreeBean(103 + "", 10 + "", "矿区建设工程部"));
		mDatas.add(new TreeBean(104 + "", 10 + "", "投资管理中心"));
		
		TreeBean bean1000 = new TreeBean(1000 + "", 100 + "", "文书科");
		bean1000.setHasCheckBox(true);
		mDatas.add(bean1000);
		mDatas.add(new TreeBean(1001 + "", 100 + "", "秘书科"));
		mDatas.add(new TreeBean(1002 + "", 100 + "", "督查科"));
		mDatas.add(new TreeBean(1003 + "", 100 + "", "接待科"));
		mDatas.add(new TreeBean(1004 + "", 100 + "", "政研科"));
		mDatas.add(new TreeBean(1005 + "", 100 + "", "值班室"));
		
		TreeBean bean10000 = new TreeBean(10000 + "", 1000 + "", "刘海勇");
		bean10000.setHasCheckBox(true);
		mDatas.add(bean10000);
		TreeBean bean10001 = new TreeBean(10001 + "", 1000 + "", "赖艳萍");
		bean10001.setHasCheckBox(true);
		mDatas.add(bean10001);
		TreeBean bean10002 = new TreeBean(10002 + "", 1000 + "", "徐金莲");
		bean10002.setHasCheckBox(true);
		mDatas.add(bean10002);
		TreeBean bean10003 = new TreeBean(10003 + "", 1000 + "", "高丽华");
		bean10003.setHasCheckBox(true);
		mDatas.add(bean10003);
		TreeBean bean10004 = new TreeBean(10004 + "", 1000 + "", "赵江华");
		bean10004.setHasCheckBox(true);
		mDatas.add(bean10004);
	}
	
	private class LoadDataAsyncTask extends AsyncTask<String, Integer, List<TreeBean>>{

		private TreeNode parentNode;
		private TreeListViewAdapter treeListViewAdapter;
		
		public LoadDataAsyncTask(TreeNode parentNode,TreeListViewAdapter treeListViewAdapter) {
			super();
			this.parentNode = parentNode;
			this.treeListViewAdapter = treeListViewAdapter;
		}

		@Override
		protected List<TreeBean> doInBackground(String... params) {
			return null;
		}
		
		@Override
		protected void onPostExecute(List<TreeBean> result) {
			super.onPostExecute(result);
			List<TreeBean> childbBeans = new ArrayList<TreeBean>();
			TreeBean bean1 = new TreeBean(100 + "", parentNode==null? null:parentNode.getId(), "child-100");
			bean1.setHasCheckBox(true);
			childbBeans.add(bean1);
			TreeBean bean2 = new TreeBean(101 + "", parentNode==null? null:parentNode.getId(), "child-101");
			bean2.setHasCheckBox(true);
			childbBeans.add(bean2);
			TreeBean bean3 = new TreeBean(102 + "", parentNode==null? null:parentNode.getId(), "child-102");
			bean3.setHasCheckBox(true);
			childbBeans.add(bean3);
			
			List<TreeNode> visibleNodes = TreeHelper.addChildNodesToTree(treeListViewAdapter.getAllNodes(), parentNode, childbBeans);
			treeListViewAdapter.setVisibleNodes(visibleNodes);
			if (parentNode != null) {//点击时调用，非初始化时调用
				treeListViewAdapter.expandOrCollapse(visibleNodes.indexOf(parentNode));
			}
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.iv_back) {
			getActivity().finish();
		}
		
	}
	


}
