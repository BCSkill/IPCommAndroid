package com.efounder.chat.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.utils.ChatActivitySkipUtil;

/**
 * 联系人详情
 * @author hudq
 *
 */
public class ContactsDetailsFragment extends Fragment implements OnClickListener{

	private static final String TAG_MESSAGE="message";
	private static final String TAG_CALL="call";
	private static final String TEL_NUMBER="tel_number";
	
	private View view;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_contacts_details, container, false);
		initView(view);
		initData();
		return view;
	}
	
	private void initData() {
		Bundle bundle = getArguments();
		String userName = bundle.getString("userName");
		TextView textView_userName = (TextView) view.findViewById(R.id.textView_userName);
		textView_userName.setText(userName);
	}
	
	private void initView(View view){
		Button sendMessageButton = (Button) view.findViewById(R.id.button_send_message);
		sendMessageButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				ChatActivitySkipUtil.startChatActivity(getActivity(), intent);
			}
		});
		
		recurView(view);
	}
	
	private void recurView(View view){
		if (view instanceof ViewGroup) {
			ViewGroup viewGroup = (ViewGroup) view;
			for (int i = 0; i < viewGroup.getChildCount(); i++) {
				View subView = viewGroup.getChildAt(i);
				recurView(subView);
			}
		}else {
			Object tag = view.getTag();
			if (tag != null &&(TAG_MESSAGE.equals(String.valueOf(tag)) || TAG_CALL.equals(String.valueOf(tag)))) {
				view.setOnClickListener(this);
			}
		}
	}
	@Override
	public void onClick(View v) {
		if (v.getTag() == null) {
			return ;
		}
		TextView telTV = (TextView) ((View)v.getParent()).findViewWithTag(TEL_NUMBER);
		String tel;
		if (TextUtils.isEmpty(telTV.getText())) {
			return ;
		}else {
			tel = telTV.getText().toString();
		}
		if (TAG_MESSAGE.equals(v.getTag().toString())) {
			// 发短信
	        Intent intent2 = new Intent();
	        intent2.setAction(Intent.ACTION_SENDTO);
	        intent2.setData(Uri.parse("smsto:"+tel));
	        intent2.putExtra("sms_body", "");
	        startActivity(intent2);
		}else if (TAG_CALL.equals(v.getTag().toString())) {
			// 打电话
	        Intent intent = new Intent();
	        intent.setAction(Intent.ACTION_CALL);
	        intent.setData(Uri.parse("tel:"+tel));
	        startActivity(intent);
		}
		
	}
	
}
