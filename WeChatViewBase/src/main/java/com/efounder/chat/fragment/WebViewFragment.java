package com.efounder.chat.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.core.xml.StubObject;
import com.efounder.chat.R;
import com.efounder.frame.baseui.BaseFragment;
import com.tamic.jswebview.view.ProgressBarWebView;

import java.util.Hashtable;

public class WebViewFragment extends BaseFragment implements View.OnClickListener {

    private Context context;
    private ProgressBarWebView progressBarWebView;
    private String url;


    public WebViewFragment(StubObject stubObject) {
        Hashtable<String, String> hashtable = stubObject.getStubTable();
        Bundle bundle = new Bundle();
        bundle.putString("URL", hashtable.get("url"));
        setArguments(bundle);

    }

    public WebViewFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = (String) getArguments().getSerializable("URL");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_webview, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBarWebView = (ProgressBarWebView) view.findViewById(R.id.my_webView);
        progressBarWebView.loadUrl(url);
    }


    @Override
    public void onClick(View v) {

    }
}
