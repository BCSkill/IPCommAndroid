package com.efounder.chat.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.efounder.frame.baseui.BaseFragment;
import com.efounder.chat.R;

/**
 * 组织机构界面
 * Created by XinQing on 2016/9/3.
 */

public class OrganizationFragment extends BaseFragment{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_organization,container,false);

        return view;
    }
}
