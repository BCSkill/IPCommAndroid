package com.efounder.chat.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.core.xml.StubObject;
import com.efounder.chat.R;
import com.efounder.chat.struct.MessageChildTypeConstant;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.message.manager.JFMessageManager;
import com.efounder.message.struct.IMStruct002;
import com.efounder.util.ToastUtil;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

@Deprecated
public class SendUrlFragment extends BaseFragment implements View.OnClickListener {

    private Context context;

    private LinearLayout inputEdit;
    private EditText edit;
    private Button button;
    private StubObject stubObject;
    private String userId;
    private String chatType;


    public SendUrlFragment() {

    }

    public SendUrlFragment(StubObject stubObject) {
        Hashtable<String, String> hashtable = stubObject.getStubTable();
        Bundle bundle = new Bundle();
        bundle.putSerializable("stubObject", stubObject);

        setArguments(bundle);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sendurl, container, false);

        inputEdit = (LinearLayout) rootView.findViewById(R.id.input_edit);
        edit = (EditText) rootView.findViewById(R.id.edit);
        button = (Button) rootView.findViewById(R.id.button);
        button.setOnClickListener(this);
        stubObject = (StubObject) getArguments().getSerializable("stubObject");
        userId = (String) stubObject.getStubTable().get("toUserId");
        chatType = (String) stubObject.getStubTable().get("chatType");
        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.button) {
         //   sendUrl();
            sendDaily();
        }

    }



    private void sendDaily() {
        if (stubObject == null) {
            ToastUtil.showToast(getActivity(), "发送失败");
            return;
        }

        Map<String, String> map = new HashMap<>();


        map.put("title", "闫庆松的日报");
        // map.put("subTitle", "这是副标题");
        String imageUrl="http://panserver.solarsource.cn:9692/panserver/files/7e58f86f-646e-4206-84bc-3c6d6d62a052/download";
        map.put("image", imageUrl);
        map.put("smallIcon", imageUrl);
        map.put("time", "6月23日 14:48");
        map.put("content", "考勤\n" +
                "html:\n" +
                "1.测试和解决与网页交互出现的定位问题 \n" +
                "2.更改为显示\"定位中\"，并设置定位超时时间\n" +
                "iOS:\n" +
                "1.osp.js添加获取原生用户位置方法，WebComps静态库中添加回调osp.js的原生获取用户位置的类。\n" +
                "2.修改打卡界面清理缓存\n" +
                "\n" +
                "联信iOS\n" +
                "1.联信角标机制优化\n" +
                "2.联信消息界面侧滑\n" +
                "3.iOS联信完成发送原生以及网页消息的item与android通信\n" +
                "联信Android\n" +
                "1.修改群聊界面titlebar群组名无法完整显示时,名称省略，数量完整显示\n" +
                "2.修改页面跳转时根据viewtype跳转到相应页面，根据view值采用隐式意图\n" +
                "3.将xml数据的条目中属性封装成一个对象属性\n" +
                "4.Android联信完成发送原生以及网页消息的ite\n" +
                "\n" +
                "RN\n" +
                "1.调整字体样式\n" +
                "2.修改提交等返回码的判断\n" +
                "3.查看表单页上拉加载数据错误的情况\n" +
                "4.解决RN空间设备工程闪退的bug\n" +
                "5.修改提交退回取回相关调用测试附件拼接url\n");
        map.put("systemName", "工作日志");
      //  map.put("systemName", "工作周报");
       // map.put("systemName", "工作月报");
        map.put("AndroidShow", "com.efounder.chat.fragment.DailyDetailFragment");
        map.put("show", "ShowRNViewController");
        map.put("viewType", "display");

        JSONObject jsonObject = new JSONObject(map);
        String json = jsonObject.toString();


        IMStruct002 imStruct002 = new IMStruct002();
        try {
            imStruct002.setBody(json.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setToUserType(Integer.valueOf(chatType).byteValue());
        imStruct002.setToUserId(Integer.valueOf(userId));
        imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        imStruct002.setMessageChildType(MessageChildTypeConstant.subtype_common);
        JFMessageManager.getInstance().sendMessage(imStruct002);

        getActivity().finish();
    }
    private void sendUrl() {
        if (stubObject == null) {
            ToastUtil.showToast(getActivity(), "发送失败");
            return;
        }
        String url = edit.getText().toString();
        if (url.equals("")) {
            url = "http://www.baidu.com";
        }

        Map<String, String> map = new HashMap<>();
        Hashtable<String, String> hashtable = stubObject.getStubTable();
String imageUrl="http://panserver.solarsource.cn:9692/panserver/files/c1b939b1-7144-4975-82f7-e2c7d6045adc/download";
        map.put("title", "点击打开网页");
        // map.put("subTitle", "这是副标题");
        map.put("image", "http://panserver.solarsource.cn:9692/panserver/files/3df20c10-4f0a-4ae6-ac8d-7b5299c92f1d/download");
           //map.put("image", imageUrl);
        //map.put("smallIcon", imageUrl);
        map.put("smallIcon",
                "http://panserver.solarsource.cn:9692/panserver/files/3df20c10-4f0a-4ae6-ac8d-7b5299c92f1d/download");
        map.put("time", "2017.06.23");
        map.put("content", "这是内容详情，这是内容详情，这是内容详情，这是内容详情，这是内容详情，这是内容详情...");
        map.put("systemName", "网页");

        map.put("url", url);
        map.put("urlState", "online");
        map.put("AndroidShow", "com.efounder.chat.fragment.WebViewFragment");
        map.put("show", "ShowRNViewController");
        map.put("viewType", "display");
        if (hashtable.containsKey("state")) {
            //  map.put("urlState", hashtable.get("state"));
        }
        if (hashtable.containsKey("tcms")) {
            map.put("tcms", hashtable.get("tcms"));
        }
        if (hashtable.containsKey("show")) {
            map.put("show", hashtable.get("show"));
        }
        if (hashtable.containsKey("width")) {
            map.put("width", hashtable.get("width"));
        }
        if (hashtable.containsKey("height")) {
            map.put("height", hashtable.get("width"));
        }
        if (hashtable.containsKey("tcms")) {
            map.put("tcms", hashtable.get("tcms"));
        }
        JSONObject jsonObject = new JSONObject(map);
        String json = jsonObject.toString();


        IMStruct002 imStruct002 = new IMStruct002();
        try {
            imStruct002.setBody(json.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setToUserType(Integer.valueOf(chatType).byteValue());
        imStruct002.setToUserId(Integer.valueOf(userId));
        imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        imStruct002.setMessageChildType(MessageChildTypeConstant.subtype_common);
        JFMessageManager.getInstance().sendMessage(imStruct002);

        getActivity().finish();
    }
}
