package com.efounder.chat.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.efounder.chat.R;
import com.efounder.frame.baseui.BaseFragment;
import com.utilcode.util.LogUtils;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;

/**
 * 联系搜索页面（联系人 群组等）
 *
 * @author YQS 2018/09/02
 */
public class ChatSearchFragment extends BaseFragment {
    private static final String TAG = "ChatSearchFragment";
    private View rootView;
    private SearchView searchView;

    public static ChatSearchFragment newInstance() {

        Bundle args = new Bundle();

        ChatSearchFragment fragment = new ChatSearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.wechatview_fragment_chat_search, container, false);
        initView();
        initListener();
        initData();
        return rootView;
    }


    private void initView() {
        searchView = (SearchView) rootView.findViewById(R.id.searchView);
        //设置是否显示搜索框展开时的提交按钮
        searchView.setSubmitButtonEnabled(true);

    }

    private void initListener() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                LogUtils.e(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                LogUtils.d(newText + "...");
                return false;
            }
        });
    }

    private void initData() {
    }

}
