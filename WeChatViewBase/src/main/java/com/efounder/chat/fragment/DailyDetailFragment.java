package com.efounder.chat.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.message.struct.IMStruct002;
import com.efounder.mobilecomps.contacts.User;

import java.util.Hashtable;

/**
 * 日报，周报，月报详情
 *
 * @author yqs
 */
public class DailyDetailFragment extends BaseFragment implements View.OnClickListener {


    private StubObject stubObject;
    // private String userId;
    // private String chatType;
    private IMStruct002 imStruct002;
    private Hashtable<String, Object> hashtable;
    private ImageView avatar;
    private TextView title;
    private TextView time;
    private EditText detail;
    public DailyDetailFragment() {

    }

//    public DailyDetailFragment(StubObject stubObject) {
//        Hashtable<String, String> hashtable = stubObject.getStubTable();
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("stubObject", stubObject);
//        setArguments(bundle);
//
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dailydetail, container, false);
        View titleView = rootView.findViewById(R.id.title_view);
        stubObject = (StubObject) getArguments().getSerializable("stubObject");
        // userId = (String) stubObject.getStubTable().get("toUserId");
        //chatType = (String) stubObject.getStubTable().get("chatType");
        if (stubObject != null) {
            hashtable = stubObject.getStubTable();
            imStruct002 = (IMStruct002) hashtable.get("imStruct002");
        }

        avatar = (ImageView) rootView.findViewById(R.id.avatar);
        title = (TextView) rootView.findViewById(R.id.title);
        time = (TextView) rootView.findViewById(R.id.time);
        detail = (EditText) rootView.findViewById(R.id.detail);
        detail.setTextIsSelectable(true);


        ImageView  iv_back = (ImageView) rootView.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        TextView tvTitle = (TextView) titleView.findViewById(R.id.tv_title);
        tvTitle.setText(R.string.wrchatview_daka);
        String titles = null;
        if (hashtable.containsKey("systemName")) {
            titles = (String) hashtable.get("systemName");
            tvTitle.setText(titles);
        }
        initData();
        return rootView;
    }

    private void initData() {
        if (imStruct002 != null) {
            int fromUserId = imStruct002.getFromUserId();
            User user = WeChatDBManager.getInstance().getOneUserById(fromUserId);
//            ImageLoader.getInstance().displayImage(URLModifyDynamic.getInstance().replace(user.getAvatar()), avatar,
//                    ImageUtil.getImageLoaderOptions(R.drawable.default_useravatar));
            LXGlideImageLoader.getInstance().showUserAvatar(getActivity(),avatar,user.getAvatar());

        }
        if (hashtable != null) {
            title.setText((String) hashtable.get("title"));
            time.setText((String) hashtable.get("time"));
            detail.setText((String) hashtable.get("content"));
        }

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

    }
}
