package com.efounder.chat.event;

import com.efounder.message.struct.IMStruct002;

/**
 * 会议系统消息
 * Created by slp on 2018/1/20.
 */

public class MeetingSysEvent {

    public MeetingSysEvent(IMStruct002 imStruct002) {
        this.imStruct002 = imStruct002;
    }

    IMStruct002 imStruct002;

    public IMStruct002 getImStruct002() {
        return imStruct002;
    }

    public void setImStruct002(IMStruct002 imStruct002) {
        this.imStruct002 = imStruct002;
    }
}
