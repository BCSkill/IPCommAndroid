package com.efounder.chat.event;

import java.util.HashMap;
import java.util.Map;

/**
 * 通知对话界面刷新的事件
 */
public class NotifyChatUIRefreshEvent {
    /**
     * 默认
     */
    public static int TYPE_DEFAULT = 0;

    /**
     * 退出（解散群组 用户就不能在这里聊天了，退出界面）
     */
    public static int TYPE_EXIT = 1;

    private int notifyType = TYPE_DEFAULT;
    private byte chatType;
    private int chatUserId;
    //放置背的一些参数
    private Map<String,Object> paramMap = new HashMap<>();


    public NotifyChatUIRefreshEvent() {
    }

    public NotifyChatUIRefreshEvent(int notifyType) {
        this.notifyType = notifyType;
    }

    public int getNotifyType() {
        return notifyType;
    }

    public NotifyChatUIRefreshEvent setNotifyType(int notifyType) {
        this.notifyType = notifyType;
        return this;
    }

    public byte getChatType() {
        return chatType;
    }

    public NotifyChatUIRefreshEvent setChatType(byte chatType) {
        this.chatType = chatType;
        return this;
    }


    public int getChatUserId() {
        return chatUserId;
    }

    public NotifyChatUIRefreshEvent setChatUserId(int chatUserId) {
        this.chatUserId = chatUserId;
        return this;
    }

    public NotifyChatUIRefreshEvent(int notifyType, byte chatType, int chatUserId) {
        this.notifyType = notifyType;
        this.chatType = chatType;
        this.chatUserId = chatUserId;
    }

    public Map<String, Object> getParamMap() {
        return paramMap;
    }
}
