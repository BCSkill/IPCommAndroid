package com.efounder.chat.event;

import java.io.Serializable;

/**
 * 推送的token事件
 * @author YQS
 */
public class PushTokenEvent implements Serializable {
    //不仅限于华为，还有小米 谷歌 等
    private String huaWeiPushToken;

    public PushTokenEvent(String huaWeiPushToken) {
        this.huaWeiPushToken = huaWeiPushToken;
    }

    public String getHuaWeiPushToken() {
        return huaWeiPushToken;
    }

    public PushTokenEvent setHuaWeiPushToken(String huaWeiPushToken) {
        this.huaWeiPushToken = huaWeiPushToken;
        return this;
    }
}
