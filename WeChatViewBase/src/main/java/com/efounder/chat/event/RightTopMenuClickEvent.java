package com.efounder.chat.event;

import android.view.View;

import com.core.xml.StubObject;

/**
 * 主界面右上角菜单点击事件
 *
 * @author YQS
 */
public class RightTopMenuClickEvent {
    private View view;
    private StubObject stubObject;

    public RightTopMenuClickEvent(View view, StubObject stubObject) {
        this.view = view;
        this.stubObject = stubObject;
    }

    public View getView() {
        return view;
    }

    public RightTopMenuClickEvent setView(View view) {
        this.view = view;
        return this;
    }

    public StubObject getStubObject() {
        return stubObject;
    }

    public RightTopMenuClickEvent setStubObject(StubObject stubObject) {
        this.stubObject = stubObject;
        return this;
    }
}
