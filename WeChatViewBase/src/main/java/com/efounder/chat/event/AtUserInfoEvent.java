package com.efounder.chat.event;

import com.efounder.mobilecomps.contacts.User;

/**
 * 通知界面新增@人
 * @author YQS
 */
public class AtUserInfoEvent {
    private User user;

    public AtUserInfoEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public AtUserInfoEvent setUser(User user) {
        this.user = user;
        return this;
    }
}
