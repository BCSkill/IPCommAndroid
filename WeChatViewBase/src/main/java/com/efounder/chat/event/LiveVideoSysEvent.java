package com.efounder.chat.event;

import com.efounder.message.struct.IMStruct002;

/**
 * 直播、点播系统消息
 */
public class LiveVideoSysEvent {

    public LiveVideoSysEvent(IMStruct002 imStruct002) {
        this.imStruct002 = imStruct002;
    }

    IMStruct002 imStruct002;

    public IMStruct002 getImStruct002() {
        return imStruct002;
    }

    public void setImStruct002(IMStruct002 imStruct002) {
        this.imStruct002 = imStruct002;
    }
}

