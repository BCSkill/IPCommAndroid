package com.efounder.chat.http.download;

public class FileInfo {

    public String id;
    public String name;
    public String packageName;
    public long size;
    public String downloadUrl;
    public String version;

    @Override
    public String toString() {
        return "FileInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
