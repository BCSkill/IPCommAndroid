package com.efounder.chat.http;

/**
 * Created by yqs on 2017/2/3.
 */

import android.content.Context;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.model.Group;
import com.efounder.chat.model.LogoutEvent;
import com.efounder.chat.utils.CommonThreadPoolUtils;
import com.efounder.chat.utils.ImJsonObjectParseUtil;
import com.efounder.common.BaseRequestManager;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.http.EFHttpRequest;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.util.AppContext;
import com.efounder.util.ToastUtil;
import com.efounder.utils.ResStringUtil;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.efounder.chat.http.GetHttpUtil.FRIENDINITSTR;
import static com.efounder.chat.http.GetHttpUtil.GROUPINITSTR;
import static com.efounder.chat.http.GetHttpUtil.MYSELFINITSTR;
import static com.efounder.chat.http.GetHttpUtil.PUBLCINUMBERINITSTR;
import static com.efounder.chat.http.GetHttpUtil.ROOTURL;
import static com.efounder.frame.utils.Constants.CHAT_PASSWORD;
import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;

/**
 * 通过http请求Json数据
 */
public class RequestHttpDataUtil {
    public static final String TAG = "RequestHttpDataUtil";


    // TODO: 2017/2/3   从服务器请求好友数据
    public static void getFriendData(final Context context, final GetHttpUtil.ReqCallBack callBack) {
        final List<User> users = new ArrayList<User>();
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/user/addresslist";

        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                try {
                    // 获取JSON
                    JSONArray jsonArray = new JSONObject(result).getJSONArray("addressList");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        // 解析JSON
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        User user = new User();
                        user = ImJsonObjectParseUtil.updateUserFromJson(user, jsonObject);
                        //  user.setReMark(null);//备注
                        user.setDeptId(0);
                        user.setIsImportantContacts(false);
//                        user.setIsBother(false);
//                        user.setIsTop(false);
                        users.add(user);
                    }
                    // 插入数据库
                    CommonThreadPoolUtils.execute(new Runnable() {
                        @Override
                        public void run() {
                            WeChatDBManager.getInstance().insertOrUpdateUser(users);
                            GetHttpUtil.changeInitState(FRIENDINITSTR);
                        }
                    });
                    if (callBack != null) {
                        callBack.onReqSuccess("success");
                    }
                    // 处理异常
                } catch (Exception e) {
                    e.printStackTrace();
                    if (callBack != null) {
                        callBack.onReqFailed("fail");
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                ToastUtil.showToast(AppContext.getInstance(), R.string.Network_error);
                if (callBack != null) {
                    callBack.onReqFailed("fail");
                }
            }
        });
    }

    // TODO: 2017/2/3   查找应用号
    public static void searchPublicNumberData(final Context context,
                                              String keywords, final GetHttpUtil.SearchPublicNumberCallBack searchPublicNumberCallBack) {
        final List<User> users = new ArrayList<>();
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("keyWord", keywords);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/public/search";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                try {
                    // 获取JSON
                    JSONArray jsonArray = new JSONObject(result).getJSONArray("publics");

                    //[{"appId":0,"publicType":0,"userId":3005,"userName":"我的空间","userType":1}]
                    for (int i = 0; i < jsonArray.length(); i++) {
                        // 解析JSON
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        User user = new User();
                        user = ImJsonObjectParseUtil.updateUserFromJson(user, jsonObject);
                        user.setDeptId(0);
                        user.setIsImportantContacts(false);
//                        user.setIsBother(false);
//                        user.setIsTop(false);
                        users.add(user);
                    }
                    if (searchPublicNumberCallBack != null) {
                        searchPublicNumberCallBack.getPublicSuccess(users, true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (searchPublicNumberCallBack != null) {
                        searchPublicNumberCallBack.getPublicSuccess(null, false);
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (searchPublicNumberCallBack != null) {
                    searchPublicNumberCallBack.getPublicSuccess(null, false);
                }
                ToastUtil.showToast(AppContext.getInstance(), R.string.Network_error);
            }
        });


    }

    // 查找好友，财务共享
    public static void searchFriendsCWGX(final Context context,
                                         String keywords, final GetHttpUtil.CWGXSearchFriendsCallBack searchPublicNumberCallBack) {
        final List<User> users = new ArrayList<User>();
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        JSONObject requestParam = new JSONObject();
        JSONObject param = new JSONObject();
        JSONObject paramRoot = new JSONObject();
        try {
//            keywords = URLEncoder.encode(keywords, "utf-8");
            param.put("envRoot", new JSONObject());
            paramRoot.put("searchValue", keywords);
            paramRoot.put("Product", "BusinessService");
            param.put("paramRoot", paramRoot);
            requestParam.put("Param", param);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.put("RequestParam", requestParam.toString());
        String requestUrl = EnvironmentVariable.getProperty("SARestfulUrl", "https://csces.openserver.cn/OSPServer");
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl + "/OpenAPIService/SAMobileSearch", map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                try {
                    // 获取JSON
                    JSONArray jsonArray = new JSONObject(result).getJSONArray("users");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        // 解析JSON
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        User user = new User();
                        user = ImJsonObjectParseUtil.updateUserFromJson(user, jsonObject);

                        user.setDeptId(0);
                        if (!"".equals((String) jsonObject.get("userId"))) {
                            user.setId(Integer.parseInt((String) jsonObject.get("userId")));
                            user.setLoginUserId(Integer.parseInt((String) jsonObject.get("userId")));
//                            user.setCompany(jsonObject.getString("zzjgmc"));
//                            user.setBmmc(jsonObject.getString("bmmc"));
                        }
                        user.setIsImportantContacts(false);
                        user.setAvatar(jsonObject.getString("avatar"));
//                        user.setIsBother(false);
//                        user.setIsTop(false);
                        users.add(user);
                    }
                    if (searchPublicNumberCallBack != null) {
                        searchPublicNumberCallBack.getFriendsSuccess(result, true);
                    }

                    // 处理异常
                } catch (Exception e) {
                    e.printStackTrace();
                    if (searchPublicNumberCallBack != null) {
                        searchPublicNumberCallBack.getFriendsSuccess(result, false);
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (searchPublicNumberCallBack != null) {
                    searchPublicNumberCallBack.getFriendsSuccess("", false);
                }
                ToastUtil.showToast(AppContext.getInstance(), R.string.Network_error);

            }
        });

    }

    // 查找好友，财务共享
    public static void searchFriendsGX(final Context context,
                                       String keywords, final GetHttpUtil.SearchFriendsCallBack searchPublicNumberCallBack) {
        final List<User> users = new ArrayList<User>();
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        JSONObject requestParam = new JSONObject();
        JSONObject param = new JSONObject();
        JSONObject paramRoot = new JSONObject();

        try {
//            keywords = URLEncoder.encode(keywords, "utf-8");
            param.put("envRoot", new JSONObject());
            paramRoot.put("searchValue", keywords);
            paramRoot.put("Product", "BusinessService");
            param.put("paramRoot", paramRoot);
            requestParam.put("Param", param);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.put("RequestParam", requestParam.toString());
//        map.put("userId", userid);
//        map.put("passWord", password);
//        map.put("keyWord", keywords);
//        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/user/search";
//        String requestUrl = "https://csces.openserver.cn/OSPServer/OpenAPIService/SAMobileSearch";
        String requestUrl = EnvironmentVariable.getProperty("SARestfulUrl", "https://csces.openserver.cn/OSPServer");
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl + "/OpenAPIService/SAMobileSearch", map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                try {
                    // 获取JSON
                    JSONArray jsonArray = new JSONObject(result).getJSONArray("users");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        // 解析JSON
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        User user = new User();
                        user = ImJsonObjectParseUtil.updateUserFromJson(user, jsonObject);

                        user.setDeptId(0);
//                        if(!"".equals((String) jsonObject.get("userId"))){
//                            user.setId(Integer.parseInt((String) jsonObject.get("userId")));
//                            user.setLoginUserId(Integer.parseInt((String) jsonObject.get("userId")));
//                        }
                        if (jsonObject.get("userId") == null || "".equals(jsonObject.get("userId").toString().trim())) {
                            user.setId(0);
                            user.setLoginUserId(0);
                        } else {
                            user.setId(Integer.parseInt((String) jsonObject.get("userId")));
                            user.setLoginUserId(Integer.parseInt((String) jsonObject.get("userId")));
                        }
                        user.setIsImportantContacts(false);
                        user.setAvatar(jsonObject.getString("avatar"));
//                        user.setCompany(jsonObject.getString("zzjgmc"));
//                        user.setBmmc(jsonObject.getString("bmmc"));
//                        user.setIsBother(false);
//                        user.setIsTop(false);
                        users.add(user);
                    }
                    if (searchPublicNumberCallBack != null) {
                        searchPublicNumberCallBack.getFriendsSuccess(users, true);
                    }

                    // 处理异常
                } catch (Exception e) {
                    e.printStackTrace();
                    if (searchPublicNumberCallBack != null) {
                        searchPublicNumberCallBack.getFriendsSuccess(users, false);
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (searchPublicNumberCallBack != null) {
                    searchPublicNumberCallBack.getFriendsSuccess(users, false);
                }
                ToastUtil.showToast(AppContext.getInstance(), R.string.Network_error);
            }
        });

    }

    // 查找好友，财务共享 分页
    public static void searchFriendsFy(final Context context,
                                       String keywords, int startPage, int pageRow, final GetHttpUtil.SearchFriendsCallBack searchPublicNumberCallBack) {
        final List<User> users = new ArrayList<User>();
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        JSONObject requestParam = new JSONObject();
        JSONObject param = new JSONObject();
        JSONObject paramRoot = new JSONObject();

        try {
//            keywords = URLEncoder.encode(keywords, "utf-8");
            param.put("envRoot", new JSONObject());
            paramRoot.put("searchValue", keywords);
            paramRoot.put("startPage", String.valueOf(startPage));
            paramRoot.put("pageRow", String.valueOf(pageRow));
            paramRoot.put("Product", "BusinessService");
            param.put("paramRoot", paramRoot);
            requestParam.put("Param", param);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.put("RequestParam", requestParam.toString());
//        map.put("userId", userid);
//        map.put("passWord", password);
//        map.put("keyWord", keywords);
//        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/user/search";
//        String requestUrl = "https://csces.openserver.cn/OSPServer/OpenAPIService/SAMobileSearch";
        String requestUrl = EnvironmentVariable.getProperty("SARestfulUrl", "https://csces.openserver.cn/OSPServer");
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl + "/OpenAPIService/SAMobileSearch", map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                try {
                    // 获取JSON
                    JSONArray jsonArray = new JSONObject(result).getJSONArray("users");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        // 解析JSON
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        User user = new User();
                        user = ImJsonObjectParseUtil.updateUserFromJson(user, jsonObject);

                        user.setDeptId(0);
                        if (jsonObject.get("userId") == null || "".equals(jsonObject.get("userId").toString().trim())) {
                            user.setId(0);
                            user.setLoginUserId(0);
                        } else {
                            user.setId(Integer.parseInt((String) jsonObject.get("userId")));
                            user.setLoginUserId(Integer.parseInt((String) jsonObject.get("userId")));
                        }
                        /*if(!"".equals((String) jsonObject.get("userId"))){
                            user.setId(Integer.parseInt((String) jsonObject.get("userId")));
                            user.setLoginUserId(Integer.parseInt((String) jsonObject.get("userId")));
                        }*/
                        user.setIsImportantContacts(false);
                        user.setAvatar(jsonObject.getString("avatar"));
//                        user.setCompany(jsonObject.getString("zzjgmc"));
//                        user.setBmmc(jsonObject.getString("bmmc"));
//                        user.setIsBother(false);
//                        user.setIsTop(false);
                        users.add(user);
                    }
                    if (searchPublicNumberCallBack != null) {
                        searchPublicNumberCallBack.getFriendsSuccess(users, true);
                    }

                    // 处理异常
                } catch (Exception e) {
                    e.printStackTrace();
                    if (searchPublicNumberCallBack != null) {
                        searchPublicNumberCallBack.getFriendsSuccess(users, false);
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (searchPublicNumberCallBack != null) {
                    searchPublicNumberCallBack.getFriendsSuccess(users, false);
                }
                ToastUtil.showToast(AppContext.getInstance(), R.string.Network_error);
            }
        });

    }


    // TODO: 2017/2/3   查找好友
    public static void searchFriends(final Context context,
                                     String keywords, final GetHttpUtil.SearchFriendsCallBack searchPublicNumberCallBack) {
        final List<User> users = new ArrayList<User>();
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("keyWord", keywords);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/user/search";
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                try {
                    // 获取JSON
                    JSONArray jsonArray = new JSONObject(result).getJSONArray("users");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        // 解析JSON
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        User user = new User();
                        user = ImJsonObjectParseUtil.updateUserFromJson(user, jsonObject);
                        user.setDeptId(0);
                        user.setIsImportantContacts(false);
//                        user.setIsBother(false);
//                        user.setIsTop(false);
                        users.add(user);
                    }
                    if (searchPublicNumberCallBack != null) {
                        searchPublicNumberCallBack.getFriendsSuccess(users, true);
                    }

                    // 处理异常
                } catch (Exception e) {
                    e.printStackTrace();
                    if (searchPublicNumberCallBack != null) {
                        searchPublicNumberCallBack.getFriendsSuccess(users, false);
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (searchPublicNumberCallBack != null) {
                    searchPublicNumberCallBack.getFriendsSuccess(users, false);
                }
                ToastUtil.showToast(AppContext.getInstance(), R.string.Network_error);
            }
        });

    }

    // TODO: 2017/2/3   关注应用号
    public static void focussPublicNumber(final Context context,
                                          final String userId,
                                          String password,
                                          int publicId,
                                          final GetHttpUtil.FocusPublicCallBack focusPublicCallBack) {
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userId);
        map.put("passWord", password);
        map.put("publicId", publicId + "");
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/public/userApplyAddPublic";
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                boolean isSuccess = false;
                if ("success".equals(info)) {
                    isSuccess = true;
                } else if ("fail".equals(info)) {
                    isSuccess = false;
                }
                if (focusPublicCallBack != null) {
                    if (isSuccess) {
                        focusPublicCallBack.focusPublicSuccess(true);
                    } else {
                        focusPublicCallBack.focusPublicSuccess(false);
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (focusPublicCallBack != null) {
                    focusPublicCallBack.focusPublicSuccess(false);
                }
                ToastUtil.showToast(AppContext.getInstance(), R.string.Network_error);
            }
        });
    }

    /**
     * todo 取消关注应用号
     */
    public static void cannalFocusPublicNumber(final Context context, final int publicId,
                                               final GetHttpUtil.FocusPublicCallBack focusPublicCallBack) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("publicId", publicId + "");
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/public/userQuitPublic";
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if ("success".equals(info)) {
                    if (focusPublicCallBack != null) {
                        focusPublicCallBack.focusPublicSuccess(true);
                    }
                } else if ("fail".equals(info)) {
                    if (focusPublicCallBack != null) {
                        focusPublicCallBack.focusPublicSuccess(false);
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (focusPublicCallBack != null) {
                    focusPublicCallBack.focusPublicSuccess(false);
                }
                ToastUtil.showToast(AppContext.getInstance(), R.string.Network_error);
            }
        });
    }

    /**
     * todo 获取关注的应用号
     *
     * @param context
     * @
     */
    public static void getAllOfficialNumber(final Context context, final GetHttpUtil.ReqCallBack callBack) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/public/getPublicListByUserId";

        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new BaseRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                try {
                    // 获取JSON
                    JSONArray jsonArray = new JSONObject(result).getJSONArray("publics");
                    final List<User> users = new ArrayList<User>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        // 解析JSON
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        User user = new User();
                        user = ImJsonObjectParseUtil.updateUserFromJson(user, jsonObject);
                        //user.setReMark(null);//备注
                        user.setDeptId(0);
                        user.setIsImportantContacts(false);
//                        user.setIsBother(false);
//                        user.setIsTop(false);
                        users.add(user);
                    }
                    // 插入数据库
                    CommonThreadPoolUtils.execute(new Runnable() {
                        @Override
                        public void run() {
                            WeChatDBManager.getInstance().insertOfficialNumber(users);
                            GetHttpUtil.changeInitState(PUBLCINUMBERINITSTR);
                        }
                    });
                    if (callBack != null) {
                        callBack.onReqSuccess("success");
                    }
                    // 处理异常
                } catch (Exception e) {
                    e.printStackTrace();
                    if (callBack != null) {
                        callBack.onReqFailed("fail");
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                ToastUtil.showToast(context, R.string.Network_error);
                if (callBack != null) {
                    callBack.onReqFailed("fail");
                }
            }
        });
    }

    /**
     * todo  创建群组 new
     *
     * @param context
     * @param groupName
     * @
     */

    public static void createGroup(final Context context, String groupName, final GetHttpUtil.ReqCallBack reqCallBack) {
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        final Group group = new Group();

        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupName", groupName);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/group/createGroup";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new BaseRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                try {
                    // 获取JSON
                    JSONObject jsonObject = new JSONObject(result).getJSONObject("group");

                    int groupId = jsonObject.getInt("groupId");
                    String groupName = jsonObject.getString("groupName");
                    int createUserId = jsonObject.getInt("createUserId");
                    String createTime = jsonObject.getString("createTime") + "000";
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
                    createTime = dateFormat.format(dateFormat1.parse(createTime));

                    group.setGroupId(groupId);
                    group.setLoginUserId(Integer.valueOf(userid));
                    group.setCreateId(createUserId);
                    group.setGroupName(groupName);
                    group.setCreateTime(((dateFormat.parse(createTime))
                            .getTime()));
                    group.setAvatar("");
//                    group.setIsBother(false);
//                    group.setIsTop(false);
                    group.setBanSpeak(false);
                    User user = new User();
                    user.setId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
                    user.setGroupUserType(User.GROUPCREATOR);//创建者group
                    List<User> list = new ArrayList<User>();
                    list.add(user);
                    group.setUsers(list);

                    // 插入数据库
                    WeChatDBManager.getInstance().insertOrUpdateGroup(group);
                    // 4.调用接口
                    if (reqCallBack != null) {
                        reqCallBack.onReqSuccess(group);
                    }
                    // 处理异常
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (reqCallBack != null) {
                    reqCallBack.onReqFailed(errorMsg);
                }
            }
        });
    }

    /**
     * todo 向群组中添加联系人
     *
     * @param context
     * @param groupId
     * @param ids
     * @
     */
    public static void addUserToGroup(final Context context, final int groupId,
                                      final String ids, final GetHttpUtil.ReqCallBack reqCallBack) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);

        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        map.put("addUserId", ids);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/group/addUserToGroup";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new BaseRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if ("success".equals(info) && reqCallBack != null) {
                    reqCallBack.onReqSuccess(true);
                } else if ("fail".equals(info) && reqCallBack != null) {
                    reqCallBack.onReqFailed("");
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (reqCallBack != null) {
                    reqCallBack.onReqFailed("");
                    ToastUtil.showToast(context, R.string.Network_error);
                }
            }
        });


    }

    /**
     * todo 申请加群
     *
     * @param context
     * @param groupId
     * @
     */
    public static void applyToGroup(final Context context, final int groupId, String leaveMessage,
                                    final GetHttpUtil.ApplyToGroupCallBack callBack) {

        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("userId", userid);
        hashMap.put("passWord", password);
        hashMap.put("groupId", groupId + "");
        hashMap.put("leaveMessage", leaveMessage);
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, GetHttpUtil.ROOTURL
                + "/IMServer/group/applyAddGroup", hashMap, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String response) {
                try {
                    String result = new JSONObject(response).optString("result", "");
                    if ("success".equals(result)) {
                        if (callBack != null) {
                            callBack.applyToGroupResult(true);
                        }
                    } else if ("fail".equals(result)) {
                        if (callBack != null) {
                            callBack.applyToGroupResult(false);
                        }
                    }
                } catch (Exception e) {
                    if (callBack != null) {
                        callBack.applyToGroupResult(false);
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (callBack != null) {
                    callBack.applyToGroupResult(false);
                    ToastUtil.showToast(context, R.string.Network_error);
                }
            }
        });
    }

    /**
     * todo 查找群组
     *
     * @param context
     * @param keyWord 关键字
     * @
     */
    public static void searchGroup(final Context context, String keyWord,
                                   final GetHttpUtil.SearchGroupCallback callBack
    ) throws Exception {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);

        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("keyWord", keyWord);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/group/search";

        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new BaseRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String response) {
                String info = null;
                List<Group> groups = new ArrayList<Group>();
                try {
                    JSONObject result = new JSONObject(response);
                    info = result.getString("result");
                    JSONArray groupArray = result.getJSONArray("groups");
                    for (int i = 0; i < groupArray.length(); i++) {
                        Group group = new Group();
                        JSONObject groupObject = (JSONObject) groupArray.get(i);
                        int appId = groupObject.getInt("appId");
                        String createTime = groupObject.getString("createTime");
                        int createUserId = groupObject.getInt("createUserId");
                        int groupId = groupObject.getInt("groupId");
                        String groupName = groupObject.getString("groupName");
                        if (groupObject.has("avatar")) {
                            group.setAvatar(groupObject.getString("avatar"));
                        } else {
                            group.setAvatar("");
                        }
                        group.setCreateId(createUserId);
                        group.setCreateTime(Long.valueOf(createTime));
                        group.setGroupId(groupId);
                        group.setGroupName(groupName);
                        group.setBanSpeak(groupObject.optInt("noSpeak", 0) == 0 ? false : true);
                        group.setGroupType(groupObject.optInt("groupType", 0));
                        groups.add(group);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if ("success".equals(info)) {
                    if (callBack != null) {
                        callBack.getGroupSuccess(groups, true);
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (callBack != null) {
                    callBack.getGroupSuccess(null, false);
                    ToastUtil.showToast(context, R.string.Network_error);
                }
            }
        });
    }

    /**
     * todo 设置群组管理员
     */
    public static void setGroupAdmin(final Context context, int groupId,
                                     int otherUserId, final GetHttpUtil.ReqCallBack callBack) throws Exception {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("otherUserId", otherUserId + "");
        map.put("groupId", groupId + "");
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/group/setGroupMana";

        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new BaseRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if ("success".equals(info) && callBack != null) {
                    callBack.onReqSuccess(true);

                } else if ("fail".equals(info) && callBack != null) {
                    callBack.onReqFailed("");
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (callBack != null) {
                    callBack.onReqFailed("");
                    ToastUtil.showToast(context, R.string.Network_error);
                }
            }
        });
    }

    /**
     * todo 取消设置群组管理员
     */
    public static void cancelGroupAdmin(final Context context, int groupId,
                                        int otherUserId, final GetHttpUtil.ReqCallBack callBack) throws Exception {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);

        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("otherUserId", otherUserId + "");
        map.put("groupId", groupId + "");
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/group/cancelGroupMana";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new BaseRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if ("success".equals(info) && callBack != null) {
                    callBack.onReqSuccess(true);

                } else if ("fail".equals(info) && callBack != null) {
                    callBack.onReqFailed("");
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (callBack != null) {
                    callBack.onReqFailed("");
                    ToastUtil.showToast(context, R.string.Network_error);
                }
            }
        });

    }

    /**
     * todo 得到群组中的联系人
     *
     * @param context
     * @param groupId
     * @
     */

    public static void getGroupUsers(final Context context, final int groupId, final GetHttpUtil.ReqCallBack callBack) {
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
//        //如果正在请求
//        if (HttpRequestStatusUtils.isRequest(groupId + "Users", HttpRequestStatusUtils.GROUP_TYPE)) {
//            Log.e(TAG, "getGroupUsers正在请求，不操作");
//            return;
//        }
//        HttpRequestStatusUtils.add(groupId + "Users", HttpRequestStatusUtils.GROUP_TYPE);

        final HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("userId", userid);
        hashMap.put("passWord", password);
        hashMap.put("groupId", groupId + "");
        String url = ROOTURL + "/IMServer/group/getUserListByGroupId";
        if (EnvironmentVariable.getProperty(KEY_SETTING_APPID, "").equals(AppContext.getInstance()
                .getString(R.string.special_appid))) {
            //星际通讯请求群成员专用
            url = EnvironmentVariable.getProperty("TalkChainUrl") + "/tcserver/user/getUserListByGroupId";
            hashMap.put("access_token", EnvironmentVariable.getProperty("tc_access_token") + "");
        }
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, url, hashMap, new JFCommonRequestManager.ReqCodeCallBack<String>() {


            @Override
            public void onReqSuccess(String result) {
                JSONObject jsonObject1 = null;
                try {
                    jsonObject1 = new JSONObject(result);

                    if (jsonObject1.optString("result", "").equals("success")) {
                        List<User> users = getGroupUserFromJson(result, userid, groupId, context);
                        if (callBack != null) {
                            callBack.onReqSuccess(users);
                        }
                    } else {
                        if (callBack != null) {
                            callBack.onReqFailed("");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {

            }

            @Override
            public void onReqFailed(int errorCode, String errorMsg) {
                if (callBack != null) {
                    callBack.onReqFailed("");
                }
                //todo 针对区块链应用，如果errorcode =400 表示用户登录失效，需要重新登录 yqs
                if (errorCode == 400 && EnvironmentVariable.getProperty(KEY_SETTING_APPID, "").equals(AppContext.getInstance()
                        .getString(R.string.special_appid))) {
                    //发送弹框提示用户
                    EventBus.getDefault().post(new LogoutEvent(LogoutEvent.TYPE_LOGIN_OUT_OF_DATE));

                }
            }
        });
    }

    /**
     * 获取群组成员并存储数据库
     *
     * @param arg1
     * @param userid
     * @param groupId
     * @param context
     * @return
     */
    public static List<User> getGroupUserFromJson(String arg1, String userid, final int groupId, Context context) {
        final List<User> users = new ArrayList<User>();
        try {
            // 获取JSON
            JSONObject jsonObject1 = new JSONObject(arg1);
            JSONArray jsonArray = jsonObject1.getJSONArray("users");
            for (int i = 0; i < jsonArray.length(); i++) {
                // 解析JSON
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                User user = new User();
                user = ImJsonObjectParseUtil.updateUserFromJson(user, jsonObject);

                if (jsonObject.has("mana")) {//群组成员属性0普通用户 1管理员 9 创建者
                    user.setGroupUserType(jsonObject.getInt("mana"));
                } else {
                    user.setGroupUserType(User.GROUPMEMBER);
                }
                //user.setReMark(null);//备注
                user.setDeptId(0);
                user.setIsImportantContacts(false);
//                user.setIsBother(false);
//                user.setIsTop(false);
                user.setGroupBanSpeak(jsonObject.optInt("noSpeak", 0) == 0 ? false : true);
                users.add(user);
            }
            //存入数据库
            if (users.size() > 0) {
                WeChatDBManager.getInstance().removeAllUsserByGroupId(groupId);
                WeChatDBManager.getInstance().insertorUpdateMyGroupUser(groupId, users);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Group group = WeChatDBManager.getInstance().getGroupWithUsers(groupId);
            users.addAll(group.getUsers());
        }
        return users;
    }

    /**
     * todo 根据id得到群
     *
     * @param context
     * @
     */
    public static void getGroupById(final Context context, final int groupId,
                                    final GetHttpUtil.ReqCallBack<Group> callBack, final boolean saveToDB) {
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");

        // String param = JFCommonRequestManager.getRequstParamFromMap(map);
        String url = GetHttpUtil.ROOTURL + "/IMServer/group/getGroupByGroupId";
        //如果正在请求
//        if (HttpRequestStatusUtils.isRequest(groupId + "", HttpRequestStatusUtils.GROUP_TYPE)) {
//            Log.e(TAG, "getGroupById正在请求，不操作");
//            return;
//        }
//        HttpRequestStatusUtils.add(groupId + "", HttpRequestStatusUtils.GROUP_TYPE);
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, url, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                //HttpRequestStatusUtils.remove(groupId + "", HttpRequestStatusUtils.GROUP_TYPE);
                try {
                    Group group = new Group();
                    // 获取JSON
                    JSONObject jsonObject = new JSONObject(result)
                            .getJSONObject("group");

                    int groupId = jsonObject.getInt("groupId");
                    String groupName = jsonObject.getString("groupName");
                    int createUserId = jsonObject.getInt("createUserId");
                    String createTime = jsonObject.getString("createTime")
                            + "000";
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss.SSS");
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat(
                            "yyyyMMddHHmmssSSS");
                    try {
                        createTime = dateFormat.format(dateFormat1
                                .parse(createTime));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (jsonObject.has("avatar")) {
                        group.setAvatar(jsonObject.getString("avatar"));
                    } else {
                        group.setAvatar("");
                    }
                    group.setGroupId(groupId);
                    group.setLoginUserId(Integer.valueOf(userid));
                    group.setCreateId(createUserId);
                    group.setGroupName(groupName);
                    group.setGroupType(jsonObject.optInt("groupType", 0));
                    group.setBanSpeak(jsonObject.optInt("noSpeak", 0) == 1);
                    group.setEnglihName(jsonObject.optString("groupKey", ""));
                    group.setRecommond(jsonObject.optString("groupDesc", ""));
                    try {
                        group.setCreateTime(((dateFormat.parse(createTime))
                                .getTime()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

//                    group.setIsBother(false);
//                    group.setIsTop(false);

                    // 根据saveToDB参数确定是否需要插入数据库
                    if (saveToDB) {
                        WeChatDBManager.getInstance().insertOrUpdateGroup(group);
                    } else {
                        WeChatDBManager.getInstance().insertOrUpdateUSERGroup(group);
                    }
                    if (callBack != null) {
                        callBack.onReqSuccess(group);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (callBack != null) {
                        callBack.onReqFailed("");
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                // HttpRequestStatusUtils.remove(groupId + "", HttpRequestStatusUtils.GROUP_TYPE);
                ToastUtil.showToast(context, R.string.Network_error);
                if (callBack != null) {
                    callBack.onReqFailed(ResStringUtil.getString(R.string.Network_error));
                }
            }

        });
    }

    /**
     * todo 根据groupKey得到群
     *
     * @param context
     */
    public static void getGroupByKeyOrId(final Context context, final String key, final JFCommonRequestManager.ReqCallBack callBack,
                                         final boolean saveToDB) {
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupKey", key);

        // String param = JFCommonRequestManager.getRequstParamFromMap(map);
        String url = GetHttpUtil.ROOTURL + "/IMServer/group/getGroupByGroupIdOrKey";
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, url, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                try {
                    Group group = new Group();
                    // 获取JSON
                    JSONObject jsonObject = new JSONObject(result).getJSONObject("group");

                    int groupId = jsonObject.getInt("groupId");
                    String groupName = jsonObject.getString("groupName");
                    int createUserId = jsonObject.getInt("createUserId");
                    String createTime = jsonObject.getString("createTime")
                            + "000";
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss.SSS");
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat(
                            "yyyyMMddHHmmssSSS");
                    try {
                        createTime = dateFormat.format(dateFormat1
                                .parse(createTime));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (jsonObject.has("avatar")) {
                        group.setAvatar(jsonObject.getString("avatar"));
                    } else {
                        group.setAvatar("");
                    }
                    group.setGroupId(groupId);
                    group.setLoginUserId(Integer.valueOf(userid));
                    group.setCreateId(createUserId);
                    group.setGroupName(groupName);
                    group.setGroupType(jsonObject.optInt("groupType", 0));
                    group.setBanSpeak(jsonObject.optInt("noSpeak", 0) == 1);
                    group.setEnglihName(jsonObject.optString("groupKey", ""));
                    group.setRecommond(jsonObject.optString("groupDesc", ""));

                    try {
                        group.setCreateTime(((dateFormat.parse(createTime))
                                .getTime()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
//                    group.setIsBother(false);
//                    group.setIsTop(false);

                    // 根据saveToDB参数确定是否需要插入数据库
                    if (saveToDB) {
                        WeChatDBManager.getInstance().insertOrUpdateGroup(group);
                    } else {
                        WeChatDBManager.getInstance().insertOrUpdateUSERGroup(group);
                    }
                    if (callBack != null) {
                        callBack.onReqSuccess(group);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Group group = new Group();

                    callBack.onReqFailed("");
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                ToastUtil.showToast(context, R.string.Network_error);
                //callBack.onReqFailed("");
            }
        });
    }

    /**
     * todo 根据登陆的用户id拉取群组
     *
     * @param context
     * @
     */
    public static void getGroupListByLoginId(final Context context, final GetHttpUtil.ReqCallBack callBack) {
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);


        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/group/getGroupListByUserId";
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                try {
                    // 获取JSON
                    JSONArray jsonArray = new JSONObject(result)
                            .getJSONArray("groups");
                    final List<Group> groups = new ArrayList<Group>();
                    final List<Integer> idList = new ArrayList<Integer>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        // 解析JSON
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        Group group = new Group();

                        int groupId = jsonObject.getInt("groupId");
                        if (jsonObject.has("avatar")) {
                            group.setAvatar(jsonObject.getString("avatar"));
                        } else {
                            group.setAvatar("");
                        }
                        String groupName = jsonObject.getString("groupName");
                        int createUserId = jsonObject.getInt("createUserId");
                        String createTime = jsonObject.getString("createTime")
                                + "000";
                        SimpleDateFormat dateFormat = new SimpleDateFormat(
                                "yyyy-MM-dd HH:mm:ss.SSS");
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat(
                                "yyyyMMddHHmmssSSS");
                        createTime = dateFormat.format(dateFormat1
                                .parse(createTime));

                        idList.add(groupId);
                        group.setGroupId(groupId);
                        group.setLoginUserId(Integer.valueOf(userid));
                        group.setCreateId(createUserId);
                        group.setGroupName(groupName);
                        group.setBanSpeak(jsonObject.optInt("noSpeak", 0) == 0 ? false : true);
                        group.setGroupType(jsonObject.optInt("groupType", 0));
                        group.setEnglihName(jsonObject.optString("groupKey", ""));
                        group.setRecommond(jsonObject.optString("groupDesc", ""));

                        try {
                            group.setCreateTime(((dateFormat.parse(createTime))
                                    .getTime()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
//                        group.setIsBother(false);
//                        group.setIsTop(false);
                        groups.add(group);
                    }
                    // 插入数据库
                    CommonThreadPoolUtils.execute(new Runnable() {
                        @Override
                        public void run() {
                            WeChatDBManager.getInstance().insertGroup(groups);
                            GetHttpUtil.changeInitState(GROUPINITSTR);
                        }
                    });
                    if (callBack != null) {
                        callBack.onReqSuccess(idList);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (callBack != null) {
                        callBack.onReqFailed("");
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                ToastUtil.showToast(context, R.string.Network_error);
                if (callBack != null) {
                    callBack.onReqFailed("");
                }
            }
        });
    }

    /**
     * todo 用户自己退出群
     *
     * @param context
     * @param groupId
     * @
     */
    public static void userQuitGroup(final Context context, final int groupId, final GetHttpUtil.ReqCallBack callBack) {

        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);

        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/group/userQuitGroup";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if ("success".equals(info) && callBack != null) {
                    callBack.onReqSuccess(true);
                } else if ("fail".equals(info) && callBack != null) {
                    callBack.onReqFailed("");
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (callBack != null) {
                    callBack.onReqFailed("");
                    ToastUtil.showToast(context, R.string.Network_error);
                }
            }
        });
    }

    /**
     * todo 管理员解散群
     *
     * @param context
     * @param groupId
     * @
     */
    public static void dissolveGroup(final Context context, final int groupId, final GetHttpUtil.ReqCallBack reqCallBack) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);

        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/group/dissolveGroup";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if ("success".equals(info) && reqCallBack != null) {
                    reqCallBack.onReqSuccess(true);

                } else if ("fail".equals(info) && reqCallBack != null) {
                    reqCallBack.onReqFailed(ResStringUtil.getString(R.string.Network_error));
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (reqCallBack != null) {
                    reqCallBack.onReqFailed(ResStringUtil.getString(R.string.Network_error));
                }
            }
        });
    }

    /**
     * todo 管理员删除群成员
     *
     * @param context
     * @param groupId
     * @
     */

    public static void AdminDeleteGroupUser(final Context context, final int groupId,
                                            final int deleteId, final GetHttpUtil.ReqCallBack callBack) {

        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        map.put("deleteUserId", deleteId + "");
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/group/deleteGroupUser";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if ("success".equals(info) && callBack != null) {
                    callBack.onReqSuccess(true);

                } else if ("fail".equals(info) && callBack != null) {
                    callBack.onReqFailed("");
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (callBack != null) {
                    callBack.onReqFailed("");
                    ToastUtil.showToast(context, R.string.Network_error);
                }
            }
        });
    }

    /**
     * todo  更改群组名
     *
     * @param context
     * @param groupId
     */

    public static void updateGroupName(final Context context, int groupId,
                                       String groupName, final GetHttpUtil.ReqCallBack reqCallBack) {

        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        map.put("groupName", groupName);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/group/updateGroup";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if ("success".equals(info) && reqCallBack != null) {
                    reqCallBack.onReqSuccess(true);

                } else if ("fail".equals(info) && reqCallBack != null) {
                    reqCallBack.onReqFailed("");
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                ToastUtil.showToast(context, R.string.Network_error);
                if (reqCallBack != null) {
                    reqCallBack.onReqFailed("");
                }
            }
        });
    }

    /**
     * todo  更改群组头像
     *
     * @param context
     * @param groupId
     */

    public static void updateGroupAvatar(final Context context, int groupId,
                                         String avatar, final GetHttpUtil.UpdateGroupAvatarCallBack callBack) {

        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        map.put("avatar", avatar);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/group/updateGroup";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if ("success".equals(info) && callBack != null) {
                    callBack.updateGroupAvatarSuccess(true);
                } else if ("fail".equals(info) && callBack != null) {
                    callBack.updateGroupAvatarSuccess(false);
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                ToastUtil.showToast(context, R.string.Network_error);
            }
        });
    }

    /**
     * todo 得到用户信息
     *
     * @param uid 用户id
     */
    public static void getUserInfo(int uid, final Context context, final GetHttpUtil.GetUserListener
            getUserListener) {

        HashMap<String, String> map = new HashMap<>();
        map.put("otherUserId", uid + "");
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/user/getOtherUserByUserId";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result).getJSONObject("user");
                    User user = new User();
                    user = ImJsonObjectParseUtil.updateUserFromJson(user, jsonObject);
                    user.setDeptId(0);
                    user.setIsImportantContacts(false);
//                    user.setIsBother(false);
//                    user.setIsTop(false);
                    user.setSortLetters(null);
                    // WeChatDBManager.getInstance().insertOrUpdateUser(user);
                    //todo 更新用户表中的信息
                    WeChatDBManager.getInstance().insertUserTable(user);
                    GetHttpUtil.changeInitState(MYSELFINITSTR);
                    if (getUserListener != null) {
                        getUserListener.onGetUserSuccess(user);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (getUserListener != null) {
                        getUserListener.onGetUserFail();
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (getUserListener != null) {
                    getUserListener.onGetUserFail();
                }
            }
        });
    }

    /**
     * todo  更改个人信息
     */
    public static void updateMyselfInfo(final Context context, final String field,
                                        String value, final GetHttpUtil.UpdateUserInfoCallBack updateUserInfoCallBack
    ) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);

        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put(field, value);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/user/updateUserByUserId";

//        String url = GetHttpUtil.ROOTURL
//                + "/IMServer/user/updateUserByUserId?" + "userId=" + userid + "&passWord=" + password
//                + "&" + field + "=" + value;
        if ("disableStrangers".equals(field)) {
//            url = GetHttpUtil.ROOTURL
//                    + "/IMServer/user/updateUserDisableStrangers?" + "userId=" + userid + "&passWord=" + password
//                    + "&" + field + "=" + value;
            requestUrl = GetHttpUtil.ROOTURL + "/IMServer/user/updateUserDisableStrangers";
        }
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if ("success".equals(info) && updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(true);

                } else if ("fail".equals(info) && updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(false);
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(false);
                }
            }
        });
    }

    /**
     * todo 更改好友备注
     */
    public static void updateFriendRemark(final Context context, int friendUserId,
                                          String value, final GetHttpUtil.UpdateUserInfoCallBack updateUserInfoCallBack
    ) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        try {
            password = URLEncoder.encode(password, "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            value = URLEncoder.encode(value, "utf-8");
        } catch (Exception e1) {

            e1.printStackTrace();
        }

        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("friendUserId", friendUserId + "");
        map.put("note", value);
        String requestUrl = GetHttpUtil.ROOTURL + "/IMServer/user/updateFriend";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, requestUrl, map, new JFCommonRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (JSONException e) {

                    e.printStackTrace();
                }

                if ("success".equals(info)) {
                    if (updateUserInfoCallBack != null) {
                        updateUserInfoCallBack.updateSuccess(true);
                    }
                } else if ("fail".equals(info)) {
                    if (updateUserInfoCallBack != null) {
                        updateUserInfoCallBack.updateSuccess(false);
                    }
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(false);
                }
            }
        });
    }


    /**
     * 更改好友配置信息
     * 是否置顶，是否免打扰等
     */
    public static void updateFriendInfo(Context context, int friendUserId, String property,
                                        String value, JFCommonRequestManager.ReqCallBack<String> callBack) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("friendUserId", String.valueOf(friendUserId));
        map.put(property, value);
        String url = GetHttpUtil.ROOTURL + "/IMServer/user/updateFriend";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, url, map, callBack);
    }

    /**
     * todo 更改群组中成员的备注
     */
    public static void updateOtherGroupUserRemark(final Context context, int groupId, int otherUserId,
                                                  String value, final GetHttpUtil.UpdateUserInfoCallBack updateUserInfoCallBack) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        map.put("otherUserId", otherUserId + "");
        map.put("note", value);
        String url = GetHttpUtil.ROOTURL + "/IMServer/group/updateOtherUserNoteInGroup";

        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, url, map, new BaseRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if ("success".equals(info) && updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(true);
                } else if ("fail".equals(info) && updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(false);
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(false);
                }
            }
        });
    }

    /**
     * todo 更改群组中自己的备注
     */
    public static void updateOwnGroupUserRemark(final Context context, int groupId,
                                                String value, final GetHttpUtil.UpdateUserInfoCallBack updateUserInfoCallBack
    ) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        map.put("note", value);
        String url = GetHttpUtil.ROOTURL + "/IMServer/group/updateOwnerNoteInGroup";

        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, url, map, new BaseRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {

                    e.printStackTrace();
                }

                if ("success".equals(info) && updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(true);

                } else if ("fail".equals(info) && updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(false);
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(false);
                }
            }
        });

    }

    /**
     * todo 删除好友
     *
     * @param context
     * @param friendId
     * @param updateUserInfoCallBack
     */
    public static void deleteFriend(final Context context, final int friendId,
                                    final GetHttpUtil.UpdateUserInfoCallBack updateUserInfoCallBack) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("friendUserId", friendId + "");
        String url = GetHttpUtil.ROOTURL + "/IMServer/user/deleteFriend";

        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, url, map, new BaseRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                String info = null;
                try {
                    info = new JSONObject(result).getString("result");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if ("success".equals(info) && updateUserInfoCallBack != null) {
                    WeChatDBManager.getInstance().deleteFriend(friendId);
                    updateUserInfoCallBack.updateSuccess(true);

                } else if ("fail".equals(info) && updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(false);
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (updateUserInfoCallBack != null) {
                    updateUserInfoCallBack.updateSuccess(false);
                }
            }
        });

    }

    /**
     * 登录
     *
     * @param context
     * @param params
     * @param loginByRestFulCallBack
     */
    public static void loginByRestFul(Context context, HashMap<String, String> params,
                                      final LoginByRestFulCallBack loginByRestFulCallBack) {
        String baseUrl = EnvironmentVariable.getProperty("RestfulInterface", "");
        if (baseUrl.equals("")) {
            loginByRestFulCallBack.loginResponse(3, "请设置登录地址");
            return;
        }

        //todo 星际通讯登录没有login 后缀 ，在这里做兼容。。。
        if (!EnvironmentVariable.getProperty(KEY_SETTING_APPID, "").equals(AppContext.getInstance()
                .getString(R.string.special_appid))) {
            baseUrl = baseUrl + "/login";
        }

        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, baseUrl, params, new BaseRequestManager.ReqCodeCallBack<String>() {
            @Override
            public void onReqFailed(int errorCode, String errorMsg) {
                if (loginByRestFulCallBack != null) {
                    loginByRestFulCallBack.loginResponse(2, ResStringUtil.getString(R.string.wechatview_network_connect_fail));
                }
            }

            @Override
            public void onReqSuccess(String result) {
                if (loginByRestFulCallBack != null) {
                    loginByRestFulCallBack.loginResponse(0, result);
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
               //onReqFailed(int errorCode, String errorMsg) 已经处理,这里不要重复处理
            }
        });
    }


    /**
     * todo 更新群组是否禁言
     *
     * @param context
     * @param groupId
     * @param callBack
     */
    public static void updateGroupNoSpeak(Context context, int groupId, int noSpeak, JFCommonRequestManager.ReqCallBack<String> callBack) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        //noSpeak  1:禁言 0:不禁言
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        map.put("noSpeak", noSpeak + "");
        String param = JFCommonRequestManager.getRequstParamFromMap(map);
        String url = GetHttpUtil.ROOTURL
                + "/IMServer/group/updateGroupNoSpeak";
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, url, map, callBack);
    }

    /**
     * todo 更新群组是否可以自由加群
     *
     * @param context
     * @param groupId
     * @param callBack
     */
    public static void updateAddGroupFreedom(Context context, int groupId, int groupType, JFCommonRequestManager.ReqCallBack<String> callBack) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        //noSpeak  1:禁言 0:不禁言
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        map.put("groupType", groupType + "");
        String param = JFCommonRequestManager.getRequstParamFromMap(map);
        String url = GetHttpUtil.ROOTURL
                + "/IMServer/group/updateGroupType";
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, url, map, callBack);
    }

    /**
     * 设置群组是否免打扰
     *
     * @param context
     * @param groupId
     * @param isBother
     * @param callBack
     */
    public static void updateGroupBother(Context context, int groupId, boolean isBother, JFCommonRequestManager.ReqCallBack<String> callBack) {
        String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
        HashMap<String, String> map = new HashMap<>();
        //noSpeak  1:禁言 0:不禁言
        map.put("userId", userid);
        map.put("passWord", password);
        map.put("groupId", groupId + "");
        //（1是设置免打扰不接收推送  0是允许接收推送）
        map.put("disturb", isBother ? "1" : "0");
        String param = JFCommonRequestManager.getRequstParamFromMap(map);
        String url = GetHttpUtil.ROOTURL + "/IMServer/group/updateGroupUserDisturb";
        JFCommonRequestManager.getInstance().requestGetByAsyn(TAG, url, map, callBack);
    }

    /**
     * 更改个人信息回调
     */
    public interface LoginByRestFulCallBack {

        public void loginResponse(int responsenum, String response);

    }

    /**
     * 注册
     *
     * @param context
     * @param id
     * @param nickName
     * @param passWord
     * @param avator
     * @param sex
     */
    public static void signByRestFul(Context context,
                                     String id, String nickName, String passWord, String avator, String sex,
                                     final RequestHttpDataUtil.SignByRestFulCallBack signByRestFulCallBack) {
        String baseUrl = "";
        //这个判断是数字视觉云的，现在已经不用了 20190430
        if (EnvironmentVariable.getProperty("APPID").equals("OSPMobileSD") || EnvironmentVariable.getProperty("APPID").equals("OSPMobileSDTest")) {
            baseUrl = EnvironmentVariable.getProperty("RestfulInterfaceLogin", "");
        } else {
            baseUrl = EnvironmentVariable.getProperty("RestfulInterface", "");
        }
        if (baseUrl.equals("")) {
            signByRestFulCallBack.signResponse(3, ResStringUtil.getString(R.string.wechatview_please_set_register));
            return;
        }

        String requestUrl = baseUrl + "/register";

        HashMap<String, String> map = new HashMap<>();
        map.put("userId", id);
        map.put("userName", nickName);
        map.put("passWord", passWord + "");
        map.put("avator", avator + "");
        map.put("sex", sex + "");

//        String url = baseUrl + "/register?" + "userId=" + id + "&userName=" + nickName
//                + "&passWord=" + passWord + "&avator=" + avator + "&sex=" + sex;
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, requestUrl, map, new BaseRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                if (signByRestFulCallBack != null) {
                    signByRestFulCallBack.signResponse(0, result);
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (signByRestFulCallBack != null) {
                    signByRestFulCallBack.signResponse(2, ResStringUtil.getString(R.string.wechatview_network_connect_fail));
                }
            }
        });
    }

    /**
     * 注册回调
     */
    public interface SignByRestFulCallBack {
        void signResponse(int responsenum, String response);
    }


    /**
     * 修改密码
     *
     * @param context
     * @param userId
     * @param passWord
     * @param newPassWord
     */
    public static void alertPassWordByRestFul(Context context,
                                              String userId, String passWord, String newPassWord,
                                              final RequestHttpDataUtil.AlertPassWordByRestFulCallBack alertPassWordByRestFulCallBack) {

        String baseUrl = EnvironmentVariable.getProperty("RestfulInterface", "");
        if (baseUrl.equals("")) {
            alertPassWordByRestFulCallBack.signResponse(3, ResStringUtil.getString(R.string.wechatview_please_set_update_password));
            return;
        }

        String requestUrl = baseUrl + "/changePass";
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", userId);
        map.put("passWord", passWord);
        map.put("newPassWord", newPassWord);

//        String url = baseUrl
//                + "/changePass?" + "userId=" + userId + "&passWord=" + passWord
//                + "&newPassWord=" + newPassWord;
        JFCommonRequestManager.getInstance(context).requestGetByAsyn(TAG, requestUrl, map, new BaseRequestManager.ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                if (alertPassWordByRestFulCallBack != null) {
                    alertPassWordByRestFulCallBack.signResponse(0, result);
                }
            }

            @Override
            public void onReqFailed(String errorMsg) {
                if (alertPassWordByRestFulCallBack != null) {
                    alertPassWordByRestFulCallBack.signResponse(2, ResStringUtil.getString(R.string.wechatview_network_connect_fail));
                }
            }
        });
    }

    /**
     * 注册回调
     */
    public interface AlertPassWordByRestFulCallBack {

        public void signResponse(int responsenum, String response);

    }

    //取消请求
    public static void cannelRequest() {
        EFHttpRequest.cancelRequest(TAG);
        JFCommonRequestManager.getInstance(AppContext.getInstance()).cannelOkHttpRequest(TAG);
    }
}