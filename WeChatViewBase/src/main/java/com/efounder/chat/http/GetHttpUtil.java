package com.efounder.chat.http;

import android.annotation.SuppressLint;
import android.content.Context;

import com.efounder.chat.model.Group;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.mobilecomps.contacts.User;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

import static com.efounder.frame.utils.Constants.CHAT_PASSWORD;
import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
import static com.efounder.frame.utils.Constants.KEY_SETTING_IM_BASE_URL;

@SuppressLint("SimpleDateFormat")
public class GetHttpUtil {

    // 用户名,用户密码,URL链接

    public final static String ROOTURL = EnvironmentVariable.getProperty(KEY_SETTING_IM_BASE_URL);
    //public final static String ROOTURL = "http://47.89.191.21:8080";
    public static boolean FRIENDINIT = false;
    public static boolean PUBLCINUMBERINIT = false;
    public static boolean MYSELFINIT = false;
    public static boolean GROUPINIT = false;
    public static String FRIENDINITSTR = "FRIENDINIT";
    public static String PUBLCINUMBERINITSTR = "PUBLCINUMBERINIT";
    public static String MYSELFINITSTR = "MYSELFINIT";
    public static String GROUPINITSTR = "GROUPINIT";


    //public final static String ROOTURL = "http://192.168.0.20:8080";

    // 2.持有接口
    private static InitDataCompleteCallback initDataCompleteCallback;

    /**
     * todo 获取好友数据
     *
     * @param context
     * @throws JSONException
     */
    public static void getUserData(final Context context) throws JSONException {
        RequestHttpDataUtil.getFriendData(context, null);
    }

    public static void getUserData(final Context context, ReqCallBack callBack) throws JSONException {
        RequestHttpDataUtil.getFriendData(context, callBack);
    }

    /**
     * todo 查找应用号
     *
     * @param context
     * @throws JSONException
     */
    public static void searchPublicNumber(final Context context,
                                          String keywords,
                                          final SearchPublicNumberCallBack callBack) throws JSONException {
        RequestHttpDataUtil.searchPublicNumberData(context, keywords, callBack);
    }

    /**
     * todo 查找好友
     *
     * @param context
     * @throws JSONException
     */
    public static void searchFriends(final Context context,
                                     String keywords,
                                     final SearchFriendsCallBack callBack) throws JSONException {
        RequestHttpDataUtil.searchFriends(context, keywords, callBack);
    }

    //共享查找好友
    public static void searchFriendsGX(final Context context,
                                     String keywords,
                                     final SearchFriendsCallBack callBack) throws JSONException {
        RequestHttpDataUtil.searchFriendsGX(context, keywords, callBack);
    }

    //查询好友，分页，共享用
    public static void searchFriendsFY(final Context context,
                                     String keywords,int startPage,int pageRow,
                                     final SearchFriendsCallBack callBack) throws JSONException {
        RequestHttpDataUtil.searchFriendsFy(context, keywords, startPage, pageRow, callBack);
    }

    /**
     * todo 查找好友
     *
     * @param context
     * @throws JSONException
     */
    public static void searchFriendsCWGX(final Context context,
                                     String keywords,
                                     final CWGXSearchFriendsCallBack callBack) throws JSONException {
        RequestHttpDataUtil.searchFriendsCWGX(context, keywords, callBack);
    }

    /**
     * todo  关注应用号
     *
     * @throws JSONException
     */
    public static void focusPublicNumber(final Context context,
                                         final String userId,
                                         final String password,
                                         final int publicId,
                                         final FocusPublicCallBack callBack) throws JSONException {
        RequestHttpDataUtil.focussPublicNumber(context, userId, password, publicId, callBack);

    }

    /**
     * todo 取消关注应用号
     */
    public static void cannalFocusPublicNumber(final Context context,
                                               final int publicId,
                                               final FocusPublicCallBack callBack) throws JSONException {
        RequestHttpDataUtil.cannalFocusPublicNumber(context, publicId, callBack);

    }

    /**
     * todo 获取关注的应用号
     *
     * @param context
     * @throws JSONException
     */
    public static void getAllOfficialNumber(final Context context) throws JSONException {
        RequestHttpDataUtil.getAllOfficialNumber(context, null);
    }

    public static void getAllOfficialNumber(final Context context, ReqCallBack callBack) throws JSONException {
        RequestHttpDataUtil.getAllOfficialNumber(context, callBack);
    }

    /**
     * todo  创建群组new 2017 1211
     *
     * @param context
     * @param groupName
     * @throws JSONException
     */

    public static void createGroup(final Context context, String groupName, ReqCallBack reqCallBack) throws JSONException {
        RequestHttpDataUtil.createGroup(context, groupName, reqCallBack);
    }

    /**
     * todo 向群组中添加联系人
     *
     * @param context
     * @param groupId
     * @param ids
     * @throws JSONException
     */
    public static void addUserToGroup(final Context context,
                                      final int groupId,
                                      final String ids, ReqCallBack reqCallBack) throws JSONException {
        RequestHttpDataUtil.addUserToGroup(context, groupId, ids, reqCallBack);
    }

    /**
     * todo 申请加群
     *
     * @param context
     * @param groupId
     * @throws JSONException
     */
    public static void applyToGroup(final Context context,
                                    final int groupId, final String leaveMessage,
                                    final ApplyToGroupCallBack callBack) throws JSONException {
        RequestHttpDataUtil.applyToGroup(context, groupId, leaveMessage, callBack);
    }

    /**
     * todo 查找群组
     *
     * @param context
     * @param keyWord 关键字
     * @throws JSONException
     */
    public static void searchGroup(final Context context, String keyWord,
                                   final SearchGroupCallback callBack) throws Exception {
        RequestHttpDataUtil.searchGroup(context, keyWord, callBack);
    }

    /**
     * todo 设置群组管理员
     */
    public static void setGroupAdmin(final Context context,
                                     int groupId,
                                     int otherUserId,
                                     final ReqCallBack callBack) throws Exception {
        RequestHttpDataUtil.setGroupAdmin(context, groupId, otherUserId, callBack);
    }

    /**
     * todo 取消设置群组管理员
     */
    public static void cancelGroupAdmin(final Context context,
                                        int groupId,
                                        int otherUserId,
                                        final ReqCallBack callBack) throws Exception {
        RequestHttpDataUtil.cancelGroupAdmin(context, groupId, otherUserId, callBack);
    }

    /**
     * todo 得到群组中的联系人
     *
     * @param context
     * @param groupId
     * @throws JSONException
     */

    public static void getGroupUsers(final Context context,
                                     final int groupId,
                                     final ReqCallBack callBack) {
        RequestHttpDataUtil.getGroupUsers(context, groupId, callBack);

    }

//    public static void getGroupUsers(final Context context,
//                                     final int groupId) throws JSONException {
//        RequestHttpDataUtil.getGroupUsers(context, groupId, mGetHttpUtilListener);
//
//    }

    /**
     * 通过id获取用户姓名
     */
    public static void getUserNameById(final Context context, final int id) {
        final String userid = EnvironmentVariable.getProperty(CHAT_USER_ID);
        String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
    }

    /**
     * todo 根据id得到群
     *
     * @param context
     * @throws JSONException
     */
    public static void getGroupById(final Context context,
                                    final int groupId,
                                    final GetHttpUtil.ReqCallBack<Group> callBack,
                                    final boolean saveToDB)  {
        RequestHttpDataUtil.getGroupById(context, groupId, callBack, saveToDB);
    }

    /**
     * todo 根据groupKey得到群
     *
     * @param context
     * @throws JSONException
     */
    public static void getGroupByKeyOrId(final Context context,
                                         final String key,
                                         final JFCommonRequestManager.ReqCallBack callBack,
                                         final boolean saveToDB) throws JSONException {
        RequestHttpDataUtil.getGroupByKeyOrId(context, key, callBack, saveToDB);
    }

    /**
     * todo 根据登陆的用户id拉取群组
     *
     * @param context
     * @throws JSONException
     */
    public static void getGroupListByLoginId(final Context context, ReqCallBack reqCallBack)
            throws JSONException {
        RequestHttpDataUtil.getGroupListByLoginId(context, reqCallBack);

    }

    /**
     * todo 用户自己退出群
     *
     * @param context
     * @param groupId
     * @throws JSONException
     */
    public static void userQuitGroup(final Context context,
                                     final int groupId, ReqCallBack callBack) throws JSONException {
        RequestHttpDataUtil.userQuitGroup(context, groupId, callBack);
    }

    /**
     * todo 管理员解散群组
     *
     * @param context
     * @param groupId
     * @throws JSONException
     */
    public static void dissolveGroup(final Context context,
                                     final int groupId, ReqCallBack reqCallBack) {

        RequestHttpDataUtil.dissolveGroup(context, groupId, reqCallBack);
    }

    /**
     * todo 管理员删除群成员
     *
     * @param context
     * @param groupId
     * @throws JSONException
     */
    public static void AdminDeleteGroupUser(final Context context,
                                            final int groupId,
                                            final int deleteId,
                                            final ReqCallBack callBack) throws JSONException {
        RequestHttpDataUtil.AdminDeleteGroupUser(context, groupId, deleteId, callBack);

    }

    /**
     * todo  更改群组名
     *
     * @param context
     * @param groupId
     */

    public static void updateGroupName(final Context context,
                                       int groupId,
                                       String groupName, ReqCallBack reqCallBack) throws JSONException {
        RequestHttpDataUtil.updateGroupName(context, groupId, groupName, reqCallBack);
    }

    /**
     * todo  更改群组头像
     *
     * @param context
     * @param groupId
     */

    public static void updateGroupAvatar(final Context context,
                                         int groupId,
                                         String avatar, UpdateGroupAvatarCallBack callBack) {
        RequestHttpDataUtil.updateGroupAvatar(context, groupId, avatar, callBack);
    }

    /**
     * todo 得到用户信息
     *
     * @param uid 用户id
     */
    public static void getUserInfo(int uid,
                                   final Context context,
                                   final GetUserListener getUserListener) {
        RequestHttpDataUtil.getUserInfo(uid, context, getUserListener);
    }

    /**
     * todo  更改个人信息
     */
    public static void updateMyselfInfo(final Context context,
                                        final String field,
                                        String value,
                                        final UpdateUserInfoCallBack callBack) {
        RequestHttpDataUtil.updateMyselfInfo(context, field, value, callBack);
    }

    /**
     * todo 更改好友备注
     */
    public static void updateFriendRemark(final Context context,
                                          int friendUserId,
                                          String value,
                                          final UpdateUserInfoCallBack callBack) {
        RequestHttpDataUtil.updateFriendRemark(context, friendUserId, value, callBack);
    }

    /**
     * todo 更改群组中成员的备注
     */
    public static void updateOtherGroupUserRemark(final Context context,
                                                  int groupId,
                                                  int otherUserId,
                                                  String value,
                                                  final UpdateUserInfoCallBack callBack) {
        RequestHttpDataUtil.updateOtherGroupUserRemark(context, groupId, otherUserId, value, callBack);
    }

    /**
     * todo 更改群组中自己的备注
     */
    public static void updateOwnGroupUserRemark(final Context context,
                                                int groupId,
                                                String value,
                                                final UpdateUserInfoCallBack callBack) {
        RequestHttpDataUtil.updateOwnGroupUserRemark(context, groupId, value, callBack);
    }

    /**
     * 取消/设置好友属性
     * @param context context
     * @param friendUserId 好友id
     * @param property 需改变的属性，是否置顶，是否免打扰
     * @param callBack callback
     */
    public static void updateFriendInfo(final Context context, int friendUserId, String property,
                                         String value,
                                         final JFCommonRequestManager.ReqCallBack<String> callBack) {
        RequestHttpDataUtil.updateFriendInfo(context, friendUserId, property, value, callBack);
    }
    /**
     * 取消/设置好友属性
     * @param context context
     * @param groupId 好友id
     * @param isBother
     * @param callBack callback
     */
    public static void updateGroupBother(final Context context, int groupId, boolean isBother,

                                        final JFCommonRequestManager.ReqCallBack<String> callBack) {
        RequestHttpDataUtil.updateGroupBother(context, groupId, isBother, callBack);
    }
    /**
     * todo 删除好友
     *
     * @param context
     * @param friendId
     * @param callBack
     */
    public static void deleteFriend(final Context context,
                                    final int friendId,
                                    final UpdateUserInfoCallBack callBack) {
        RequestHttpDataUtil.deleteFriend(context, friendId, callBack);
    }

    /**
     * todo 更新群组是否禁言
     *
     * @param context
     * @param groupId
     * @param callBack
     */
    public static void updateGroupNoSpeak(final Context context,
                                          final int groupId,
                                          final int noSpeak,
                                          final JFCommonRequestManager.ReqCallBack<String> callBack) {
        RequestHttpDataUtil.updateGroupNoSpeak(context, groupId, noSpeak, callBack);
    }

    /**
     * todo 更新群组是否允许自由加群
     *
     * @param context
     * @param groupId
     * @param callBack
     * @param groupType 1 表示随意加群
     */
    public static void updateAddGroupFreedom(final Context context,
                                             final int groupId,
                                             final int groupType,
                                             final JFCommonRequestManager.ReqCallBack<String> callBack) {
        RequestHttpDataUtil.updateAddGroupFreedom(context, groupId, groupType, callBack);
    }

    public static synchronized void changeInitState(String name) {
        if (name.equals(FRIENDINITSTR)) {
            FRIENDINIT = true;
        } else if (name.equals(GROUPINITSTR)) {
            GROUPINIT = true;
        } else if (name.equals(MYSELFINITSTR)) {
            MYSELFINIT = true;
        } else if (name.equals(PUBLCINUMBERINITSTR)) {
            PUBLCINUMBERINIT = true;
        }
        if (FRIENDINIT == true && GROUPINIT == true && MYSELFINIT == true && PUBLCINUMBERINIT == true) {
            EnvironmentVariable.setProperty("initcomplete", "true");
            if (initDataCompleteCallback != null) {
                initDataCompleteCallback.onInitDataComplete();
            }
            System.out.printf("init数据-------------------成功");
        }
    }

    /***
     * 初始化完数据的回调
     */
    public interface InitDataCompleteCallback {
        public void onInitDataComplete();
    }


    public interface GetUserListener {
        public void onGetUserSuccess(User user);

        public void onGetUserFail();
    }

    public static InitDataCompleteCallback getInitDataCompleteCallback() {
        return initDataCompleteCallback;
    }

    public static void setInitDataCompleteCallback(InitDataCompleteCallback initDataCompleteCallback) {
        GetHttpUtil.initDataCompleteCallback = initDataCompleteCallback;
    }

    /**
     * 从服务器得到群组信息回调
     */
    public interface getGroupInfoCallBack {

        public void onGetGroupInfoSuccess(Group group);

    }

    /**
     * 更改个人信息回调
     */
    public interface UpdateUserInfoCallBack {

        public void updateSuccess(boolean isSuccess);

    }

    /**
     * 更改群组头像信息回调
     */
    public interface UpdateGroupAvatarCallBack {

        public void updateGroupAvatarSuccess(boolean isSuccess);

    }

    /**
     * 查找应用号回调
     */
    public interface SearchPublicNumberCallBack {

        public void getPublicSuccess(List<User> users, boolean isSuccess);

    }

    /**
     * 查找好友回调
     */
    public interface SearchFriendsCallBack {

        public void getFriendsSuccess(List<User> users, boolean isSuccess);

    }

    /**
     * 财务共享查找好友回调
     */
    public interface CWGXSearchFriendsCallBack {

        public void getFriendsSuccess(String jsonString, boolean isSuccess);

    }

    /**
     * 关注应用号成功回调
     */
    public interface FocusPublicCallBack {

        public void focusPublicSuccess(boolean isSuccess);

    }

    /**
     * 查找群回调
     */

    public interface SearchGroupCallback {
        void getGroupSuccess(List<Group> groups, boolean isSuccess);
    }

    /**
     * 申请加群回调
     */
    public interface ApplyToGroupCallBack {

        public void applyToGroupResult(boolean isSuccess);

    }

    public static void release() {
        initDataCompleteCallback = null;
    }

    public interface ReqCallBack<T> {
        /**
         * 响应成功
         */
        void onReqSuccess(T result);

        /**
         * 响应失败
         */
        void onReqFailed(String errorMsg);
    }
    //*****************************登录*******************************//

    public static void loginByRestFul(Context context,
                                      HashMap<String, String> params,
                                      RequestHttpDataUtil.LoginByRestFulCallBack loginByRestFulCallBack) {
        RequestHttpDataUtil.loginByRestFul(context, params, loginByRestFulCallBack);
    }

    //*****************************注册*******************************//
    public static void signByRestFul(Context context,
                                     String id, String nickName, String passWord, String avator, String sex,
                                     RequestHttpDataUtil.SignByRestFulCallBack signByRestFulCallBack) {
        RequestHttpDataUtil.signByRestFul(context, id, nickName, passWord, avator, sex, signByRestFulCallBack);
    }

    //*****************************修改密码*******************************//
    public static void alertPassWordByRestFul(Context context,
                                              String userId, String passWord, String newPassWord,
                                              RequestHttpDataUtil.AlertPassWordByRestFulCallBack alertPassWordByRestFulCallBack) {
        RequestHttpDataUtil.alertPassWordByRestFul(context, userId, passWord, newPassWord, alertPassWordByRestFulCallBack);
    }

    //取消请求
    public static  void cannelRequest(){
       RequestHttpDataUtil.cannelRequest();
    }
}
