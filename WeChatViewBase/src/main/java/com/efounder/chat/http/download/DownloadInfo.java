package com.efounder.chat.http.download;

/**
 * Created by zhangshunyun on 2017/7/13.
 * <p>
 * 文件下载数据
 */

public class DownloadInfo {
    public String fileID;
    public int state;
    public long downloadSize;
    public long size;
    public String downloadUrl;
    public String fileName;
    public int progress;// 进度
    public String messageID;//messageid
    public String fileSize;
    public boolean downloadOnGPRS;
}
