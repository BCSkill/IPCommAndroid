package com.efounder.chat.http;

import android.content.Context;

import com.efounder.chat.R;
import com.efounder.common.BaseRequestManager;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.utils.ResStringUtil;

import okhttp3.Request;

/**
 * OKHttp上传
 */

public class JFCommonRequestManager extends BaseRequestManager {

    private static volatile JFCommonRequestManager mInstance;

    @Deprecated
    public static JFCommonRequestManager getInstance(Context context) {
        return getInstance();
    }

    public static JFCommonRequestManager getInstance() {

        if (mInstance == null) {
            synchronized (JFCommonRequestManager.class) {
                if (mInstance == null) {
                    mInstance = new JFCommonRequestManager();
                }
            }
        }
        return mInstance;
    }

    private JFCommonRequestManager() {
        super();
    }

    /**
     * 统一为请求添加头信息
     *
     * @return
     */
    @Override
    public Request.Builder addHeaders() {
        Request.Builder builder = super.addHeaders();
        builder.header("UserAgent", System.getProperty("http.agent"));
        //中建项目接口需要
        builder.header("appID", EnvironmentVariable.getProperty("AppToken", "F150078D8F3743D2BF37174139839F98"));
        //这两个是币看接口需要的
        builder.addHeader("Accept-Language", ResStringUtil.getString(R.string.wechat_bk_header_accept_language));
        builder.addHeader("Bk-Currency", ResStringUtil.getString(R.string.wechat_bk_header_bk_currency));
        return builder;
    }
}
