package com.efounder.chat.view.voicedictate;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

import com.efounder.chat.R;
import com.efounder.utils.JfResourceUtil;

import java.util.ArrayList;

/**
 * author : zzj
 * e-mail : zhangzhijun@pansoft.com
 * date   : 2018/9/715:39
 * desc   : 语音听写声波图
 * version: 1.0
 */
public class VoiceDictateWaveView  extends View{

    private int marginRight=0;//波形图绘制距离右边的距离
    private int marginLeft=0;//波形图绘制距离左边的距离
    private float divider = 0.0f;//为了节约绘画时间，每0.2个像素画一个数据
    //中间线颜色
    private int centerLineColor;
    //中间线大小
    private int centerLineSize;
    //波形线大小
    private int waveFromLineSize;
    //波形线的颜色
    private int waveFromLineColor;

    private Paint center;
    private Paint mPaint;
    private int line_off = 70 ;//上下边距的距离
    public int rateX = 100;//控制多少帧取一帧
    public int rateY = 0; //  Y轴缩小的比例 默认为1
    ArrayList<Short> buf = new ArrayList<>();

    public VoiceDictateWaveView(Context context) {
        this(context,null);
    }

    public VoiceDictateWaveView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public VoiceDictateWaveView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.VoiceDictateWaveView);
        centerLineColor = mTypedArray.getColor(R.styleable.VoiceDictateWaveView_wave_center_line_color, Color.parseColor("#C0C0C0"));
        centerLineSize = mTypedArray.getInteger(R.styleable.VoiceDictateWaveView_wave_center_line_size, 1);
        waveFromLineColor = mTypedArray.getColor(R.styleable.VoiceDictateWaveView_wave_from_line_color, Color.parseColor("#80C0C0C0"));
        waveFromLineSize = mTypedArray.getInteger(R.styleable.VoiceDictateWaveView_wave_from_line_size, 3);
        mTypedArray.recycle();
        init();
    }

    /**
     * 设置语音数据
     * @param arrayList
     */
    public void setVoiceList(ArrayList<Short> arrayList){
        buf = arrayList;
        invalidate();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(JfResourceUtil.getSkinColor(R.color.op_float_window_color));
        simpleDraw(canvas,buf,getHeight()/2);
    }

    /**
     * 绘制指定区域
     *
     * @param buf
     *            缓冲区
     * @param baseLine
     *            Y轴基线
     */

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void simpleDraw(Canvas canvas,ArrayList<Short> buf, int baseLine) {
        divider = (float) ((getWidth()-marginRight-marginLeft)/(16000/rateX*20.00));
        rateY = (65535 /2/ (getHeight()-line_off));
//        for (int i = 0; i < buf.size(); i++) {
//            byte bus[] = getBytes(buf.get(i));
//            buf.set(i, (short)((0x0000 | bus[1]) << 8 | bus[0]));//高低位交换
//        }

        float y;

        int height = getHeight()-line_off;
        canvas.drawLine(0, height*0.5f+line_off/2,getWidth() ,height*0.5f+line_off/2, center);//中心线

        for (int i = 0; i < buf.size(); i++) {
            y =buf.get(i)/rateY + baseLine;// 调节缩小比例，调节基准线
            float x=(i) * divider;
            if(getWidth() - (i-1) * divider <= marginRight){
                x = getWidth()+marginRight;
            }
            //画线的方式很多，你可以根据自己要求去画。这里只是为了简单
            float y1 = getHeight() - y;
            if(y<line_off/2){
                y = line_off / 2;
            }
            if(y>getHeight()-line_off/2-1){
                y = getHeight() - line_off / 2 - 1;

            }
            if(y1<line_off/2){
                y1 = line_off / 2;
            }
            if(y1>(getHeight()-line_off/2-1)){
                y1 = (getHeight() - line_off / 2 - 1);
            }
//            LogUtils.e("绘制的y点------》"+y+"----绘制的y1点---"+y1);
            canvas.drawLine(x, y,  x,y1, mPaint);//中间出波形
//            RectF rectF = new RectF(x,);
//            canvas.drawArc(x,y,x+y-getWidth(),getWidth(),180,90,false,mPaint);
        }

    }
    public byte[] getBytes(short s)
    {
        byte[] buf = new byte[2];
        for (int i = 0; i < buf.length; i++)
        {
            buf[i] = (byte) (s & 0x00ff);
            s >>= 8;
        }
        return buf;
    }
    public  void init(){


        center = new Paint();//中心线
        center.setColor(centerLineColor);// 画笔为color
        center.setStrokeWidth(centerLineSize);// 设置画笔粗细
        center.setAntiAlias(true);
        center.setFilterBitmap(true);
        center.setStyle(Paint.Style.FILL);

        mPaint = new Paint();
        mPaint.setColor(waveFromLineColor);// 画笔为color
        mPaint.setStrokeWidth(waveFromLineSize);// 设置画笔粗细
        mPaint.setAntiAlias(true);
//        mPaint.setFilterBitmap(true);
        mPaint.setDither(true);
        mPaint.setStyle(Paint.Style.FILL);
    }

    public int getCenterLineColor() {
        return centerLineColor;
    }

    public void setCenterLineColor(int centerLineColor) {
        this.centerLineColor = centerLineColor;
        invalidate();
    }

    public int getCenterLineSize() {
        return centerLineSize;
    }

    public void setCenterLineSize(int centerLineSize) {
        this.centerLineSize = centerLineSize;
        invalidate();
    }

    public int getWaveFromLineSize() {
        return waveFromLineSize;
    }

    public void setWaveFromLineSize(int waveFromLineSize) {
        this.waveFromLineSize = waveFromLineSize;
        invalidate();
    }

    public int getWaveFromLineColor() {
        return waveFromLineColor;
    }

    public void setWaveFromLineColor(int waveFromLineColor) {
        this.waveFromLineColor = waveFromLineColor;
        invalidate();
    }
}
