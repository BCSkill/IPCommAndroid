package com.efounder.chat.view;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.efounder.chat.R;


/**
 * 通用仿微信popupwindow
 *
 * @author YQS
 */
public class CommonOtherPopWindow extends PopupWindow {
    protected LinearLayout conentView;
   protected LayoutInflater inflater;
    protected Context context;

    public CommonOtherPopWindow(final Activity context) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        conentView = (LinearLayout) inflater.inflate(R.layout.popupwindow_common, null);
        this.context = context;

        // 设置SelectPicPopupWindow的View
        this.setContentView(conentView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        // 刷新状态
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);

        // 设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimationPreview);

    }

    public View addMenuButton(String menuName, int menuIcon) {
       return addMenuButton(menuName, menuIcon, false);
    }

    public View addMenuButton(String menuName, int menuIcon, boolean hasArrow) {
        View menuView = inflater.inflate(R.layout.popupwindow_common_item, null);
        ImageView iv_menuicon = menuView.findViewById(R.id.iv_menuicon);

        ImageView ivTopArrow = (ImageView) menuView.findViewById(R.id.iv_top_arrow);
        if (hasArrow) {
            ivTopArrow.setVisibility(View.VISIBLE);
        }
        if (menuIcon == 0) {
            iv_menuicon.setVisibility(View.GONE);
        } else {
            iv_menuicon.setImageDrawable(context.getResources().getDrawable(menuIcon));
        }
        TextView menuNameView = menuView.findViewById(R.id.tv_menuname);
        menuNameView.setText(menuName);
        conentView.addView(menuView);
        return menuView;

    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent) {
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            this.showAsDropDown(parent, 0, 0);
        } else {
            this.dismiss();
        }
    }
}
