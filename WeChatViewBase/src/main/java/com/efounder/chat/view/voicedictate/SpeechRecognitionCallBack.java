package com.efounder.chat.view.voicedictate;

/**
 * author : zzj
 * e-mail : zhangzhijun@pansoft.com
 * date   : 2018/9/614:03
 * desc   :
 * version: 1.0
 */
public interface SpeechRecognitionCallBack {
    void onStartOfSpeech();

    void onEndOfSpeech();

    void onSuccess(String text, boolean isLast);


    void onFail(String failMsg);

    /**
     * 音量大小监听
     * @param volume
     * @param data
     */
    void onVolumeChanged(int volume, byte[] data);

    /**
     * 录音文件路径回调
     * @param audioPath 文件路径
     */
    void getAudioFilePath(String audioPath);
}
