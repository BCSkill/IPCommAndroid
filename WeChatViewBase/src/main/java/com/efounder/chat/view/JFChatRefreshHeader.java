package com.efounder.chat.view;

import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.efounder.chat.R;
import com.efounder.utils.JfResourceUtil;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshKernel;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.internal.ProgressDrawable;
import com.utilcode.util.SizeUtils;

/**
 * jfchatactivity 下拉加载历史消息的 header
 */
public class JFChatRefreshHeader extends LinearLayout implements RefreshHeader {

    private ImageView imageView;
    private ProgressDrawable progressDrawable;

    public JFChatRefreshHeader(Context context) {
        super(context);
        initView(context);
    }

    public JFChatRefreshHeader(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public JFChatRefreshHeader(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public JFChatRefreshHeader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);

    }


    private void initView(Context context) {
        setGravity(Gravity.CENTER);
        imageView = new ImageView(context);
        setBackgroundColor(JfResourceUtil.getSkinColor(R.color.frame_gray_background_color));
        progressDrawable = new ProgressDrawable();
        progressDrawable.setColor(0xff666666);
        imageView.setImageDrawable(progressDrawable);

        addView(imageView, SizeUtils.dp2px(20), SizeUtils.dp2px(20));

        setMinimumHeight(SizeUtils.dp2px(30));

    }

    @NonNull
    @Override
    public View getView() {
        return this;
    }

    @NonNull
    @Override
    public SpinnerStyle getSpinnerStyle() {
        return SpinnerStyle.Translate;//指定为平移，不能null
    }

    @Override
    public void setPrimaryColors(int... colors) {

    }

    @Override
    public void onInitialized(@NonNull RefreshKernel kernel, int height, int extendHeight) {

    }

    @Override
    public void onMoving(boolean isDragging, float percent, int offset, int height, int extendHeight) {

    }

    @Override
    public void onReleased(@NonNull RefreshLayout refreshLayout, int height, int extendHeight) {

    }

    @Override
    public void onStartAnimator(@NonNull RefreshLayout refreshLayout, int height, int extendHeight) {
        progressDrawable.start();
    }

    @Override
    public int onFinish(@NonNull RefreshLayout refreshLayout, boolean success) {
        progressDrawable.stop();

        return 100;
    }

    @Override
    public void onHorizontalDrag(float percentX, int offsetX, int offsetMax) {

    }

    @Override
    public boolean isSupportHorizontalDrag() {
        return false;
    }

    @Override
    public void onStateChanged(@NonNull RefreshLayout refreshLayout, @NonNull RefreshState oldState, @NonNull RefreshState newState) {
        // 动画是否正在运行


        switch (newState) {
            case None:
            case PullDownToRefresh:
                progressDrawable.start();
                break;
            case Refreshing:
                if (!progressDrawable.isRunning()) {
                    progressDrawable.start();
                }
                break;
            case ReleaseToRefresh:
                if (progressDrawable.isRunning()) {
                    //停止动画播放
                    progressDrawable.stop();
                }
        }

    }
}