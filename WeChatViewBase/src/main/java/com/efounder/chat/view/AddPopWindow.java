package com.efounder.chat.view;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.efounder.chat.R;
import com.efounder.chat.activity.AddFriendsOneActivity;
import com.efounder.chat.activity.CreatChatRoomActivity;
import com.efounder.chat.zxing.qrcode.MipcaActivityCapture;
import com.efounder.constant.EnvironmentVariable;


public class AddPopWindow extends PopupWindow {
    private View conentView;

    public AddPopWindow(final Activity context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        conentView = inflater.inflate(R.layout.popupwindow_add, null);

        // 设置SelectPicPopupWindow的View
        this.setContentView(conentView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        // 刷新状态
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);

        // 设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimationPreview);

        RelativeLayout re_saoyisao = (RelativeLayout) conentView.findViewById(R.id.re_saoyisao);
        RelativeLayout re_addfriends = (RelativeLayout) conentView.findViewById(R.id.re_addfriends);
        RelativeLayout re_chatroom = (RelativeLayout) conentView.findViewById(R.id.re_chatroom);
        RelativeLayout re_help = (RelativeLayout) conentView.findViewById(R.id.re_help);
        RelativeLayout re_energyExchange = (RelativeLayout) conentView.findViewById(R.id.re_energy_exchange);
        if ("1".equals(EnvironmentVariable.getProperty("isShowEnergyExchange", "0"))) {
            re_energyExchange.setVisibility(View.VISIBLE);
        }

//        RelativeLayout re_scanner = (RelativeLayout) conentView.findViewById(R.id.re_saomiaoyi);
        re_addfriends.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//            	Toast.makeText(context,
//						"添加朋友", Toast.LENGTH_LONG).show();
                context.startActivity(new Intent(context, AddFriendsOneActivity.class));
                AddPopWindow.this.dismiss();

            }

        });
        re_chatroom.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//            	Toast.makeText(context,
//						"发起群聊", Toast.LENGTH_LONG).show();
                context.startActivity(new Intent(context, CreatChatRoomActivity.class));
                AddPopWindow.this.dismiss();

            }

        });
        re_help.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(context,
                //"异常测试", Toast.LENGTH_LONG).show();
                //FIXME 模拟异常


            }

        });
        re_saoyisao.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context, MipcaActivityCapture.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
                AddPopWindow.this.dismiss();
            }
        });


        re_energyExchange.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Class activityClass = Class.forName(context.getResources().getString(R.string.activity_energy_exchage_qr));
                    Intent intent = new Intent();
                    intent.setClass(context, activityClass);
                    context.startActivity(intent);
                    AddPopWindow.this.dismiss();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        });
//        re_scanner.setOnClickListener(new OnClickListener() {//// TODO: 17-8-3 共享的扫描
//            @Override
//            public void onClick(View v) {
//                try {
//                    Intent intent = new Intent();
//                    intent.setClass(context, Class.forName("com.pansoft.efounder.custom.EFAppAccountDJZCameraActivityNN"));
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    context.startActivity(intent);
//                    AddPopWindow.this.dismiss();
//                } catch (ClassNotFoundException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
    }


    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent) {
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            this.showAsDropDown(parent, 0, 20);
        } else {
            this.dismiss();
        }
    }
}
