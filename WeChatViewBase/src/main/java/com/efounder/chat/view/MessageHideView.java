package com.efounder.chat.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * 
 * @author lch 5-26
 *  增加系统消息类型
 */
public class MessageHideView extends MessageBaseView {

	public MessageHideView(Context context) {
		super(context);
		initView(context);
	}

	public MessageHideView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}

	@SuppressLint("ResourceAsColor")
	private void initView(final Context context) {
		// 消息体区域
		this.removeAllViews();
		this.setVisibility(View.GONE);


	}
	

}
