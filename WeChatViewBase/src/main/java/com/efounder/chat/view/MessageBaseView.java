package com.efounder.chat.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.constant.EnvironmentVariable;

import java.util.Map;

/**
 * @author hdq
 */
public class MessageBaseView extends LinearLayout implements OnClickListener,
        OnLongClickListener {


    // /BseCell 监听
    private OnMessageClickListener onMessageClickListener;
    private OnMessageLongClickListener onMessageLongClickListener;

    private OnAvatarClickListener onAvatarClickListener;
    private OnAvatarLongClickListener onAvatarLongClickListener;

    // /TopView 一般用于显示时间戳
    protected RelativeLayout topLayout;
    // /默认TopView里面为Label
    protected TextView topTextView;

    // /MiddleView
    protected RelativeLayout middleView;

    //头像、认证、用户名区域
    protected RelativeLayout chat_item_avatar_area;
    // /头像
    protected ImageView avatarImageView;
    // /认证 V
    protected ImageView vImageView;
    // /用户名TextView
    protected TextView userNameTextView;

    /*
     * ///XXX 头像与气泡间的三角 private ImageView angleImageView;
     */
    // /XXX 气泡背景
    //protected Image bubbleImage;
    // /内容
    protected IMessageItem messageItem;
    //消息的内容
    protected RelativeLayout chat_item_layout_content;
    //消息体下方的补充
    protected RelativeLayout chat_item_layout_contentbottom_supplement;


    // /标志区域 用于显示发送进度条(延时1秒显示)/发送失败标志
    protected LinearLayout signView;

    //加载失败
    protected ImageView loadingErrorImage;
    //语音未读
    protected ImageView voiceUnRead;
    //语音时间
    protected TextView voiceTime;
    //消息状态
    protected TextView chatStateView;

    //加载中
    protected ProgressBar mprogressbar;

    // /BottomView 一般用于显示信息 如收到红包
    protected RelativeLayout bottomLayout;
    // /默认BottomView里面为Label
    protected TextView bottomTextView;

    // 数据源
    protected Map<String, Object> resultMap;

    // 个人ID
    private String chatID;

    private Context mContext;
    //类型 接收还是发送消息
    private int type;


    public MessageBaseView(Context context) {
        super(context);
        mContext = context;
        initView(context);
        //this.setVoiceUnReadCallBack
        //new VoiceItem(context).setVoiceUnReadCallBack(this);
    }

    public MessageBaseView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup chat_item = (ViewGroup) inflater.inflate(R.layout.chat_item, this);

        //顶部区域
        topLayout = (RelativeLayout) chat_item.findViewById(R.id.chat_item_topview);
        topTextView = (TextView) chat_item.findViewById(R.id.chat_item_date);
        topTextView.setVisibility(View.VISIBLE);
        topTextView.setText("12:00");

        //中间区域
        middleView = (RelativeLayout) chat_item.findViewById(R.id.chat_item_middlelayout);
        //头像区域
        chat_item_avatar_area = (RelativeLayout) inflater.inflate(R.layout.chat_avatararea, null);

        //根据设置判断显示圆角还是方角头像
        String TX_type = EnvironmentVariable.getProperty("TX_type", "Square");

//        if ("Round".equals(TX_type)) {
            avatarImageView = (ImageView) chat_item_avatar_area.findViewById(R.id.chat_item_avatar1);
//        } else {
//            avatarImageView = (ImageView) itemAvatarLayout.findViewById(R.id.chat_item_avatar2);
//        }
        avatarImageView.setVisibility(ImageView.VISIBLE);

        vImageView = (ImageView) chat_item_avatar_area.findViewById(R.id.vImageView);
        userNameTextView = (TextView) chat_item_avatar_area.findViewById(R.id.chat_item_avatar_username);
        userNameTextView.setVisibility(View.INVISIBLE);
        //消息体区域
        chat_item_layout_content = new RelativeLayout(context);
        chat_item_layout_content.setGravity(Gravity.CENTER_VERTICAL);
        //rlMessageContentLayout = (RelativeLayout) chat_item.findViewById(R.id.rlMessageContentLayout);

        //消息体下方的补充
        chat_item_layout_contentbottom_supplement = new RelativeLayout(context);
        this.setSupplementVisible(false);

        //signview区域
        signView = (LinearLayout) inflater.inflate(R.layout.chat_item_signview, null);
        loadingErrorImage = (ImageView) signView.findViewById(R.id.failImageView);
        mprogressbar = (ProgressBar) signView.findViewById(R.id.sendingProgressBar);
        voiceUnRead = (ImageView) signView.findViewById(R.id.voiceUnRead);
        voiceTime = (TextView) signView.findViewById(R.id.voiceTime);


        //底部区域
        bottomLayout = (RelativeLayout) chat_item.findViewById(R.id.chat_item_footer);
        bottomTextView = (TextView) chat_item.findViewById(R.id.chat_item_footertext);
        bottomTextView.setVisibility(View.GONE);

        //bottomTextView.setVisibility(View.VISIBLE);
        //bottomTextView.setText("我已领取了红包");

    }

    //设置补充区域是否显示
    public void setSupplementVisible(boolean isVisible) {
        if (isVisible) {
            chat_item_layout_contentbottom_supplement.setVisibility(View.VISIBLE);
        } else {
            chat_item_layout_contentbottom_supplement.setVisibility(View.GONE);
        }

    }

//	@Override
//	public void setVoiceRead() {
//		Log.i("messagebaseview","红点消失");
//		getVoiceUnReadImage().setVisibility(View.GONE);
//	}

    /**
     * 消息长按事件
     **/
    public interface OnMessageLongClickListener {
        public void onMessageLongClick(MessageBaseView messageBaseView,
                                       View messageView);
    }

    /**
     * 消息点击事件
     **/
    public interface OnMessageClickListener {
        public void onMessageClick(MessageBaseView messageBaseView,
                                   View messageView);
    }

    /**
     * 头像长按事件
     **/
    public interface OnAvatarLongClickListener {
        public void onAvatarLongClick(MessageBaseView messageBaseView,
                                      View avatarView);
    }

    /**
     * 头像点击事件
     **/
    public interface OnAvatarClickListener {
        public void onAvatarClickListener(MessageBaseView messageBaseView,
                                          View avatarView);
    }

    @Override
    public boolean onLongClick(View v) {

        Toast.makeText(mContext, "chat_item_publicnumber_top click", Toast.LENGTH_SHORT).show();

        if (v.getId() == R.id.chat_item_layout_content
                && onMessageLongClickListener != null) {// 内容长按事件
            onMessageLongClickListener.onMessageLongClick(this, v);
        } else if (v.getId() == R.id.chat_item_avatar
                && onAvatarLongClickListener != null) {// 头像长按事件
            onAvatarLongClickListener.onAvatarLongClick(this, v);
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.chat_item_layout_content
                && onMessageClickListener != null) {// 内容点击事件
            onMessageClickListener.onMessageClick(this, v);
        } else if (v.getId() == R.id.chat_item_avatar
                && onAvatarClickListener != null) {// 头像点击事件
            onAvatarClickListener.onAvatarClickListener(this, v);
        }
    }


    public IMessageItem getMessageItem() {
        return messageItem;
    }

    public void setMessageItem(IMessageItem messageItem) {
        this.messageItem = messageItem;
        // TODO 先从中间layout中删除子view，再将messageItemView（实现IMessageItem接口）加入layout
        chat_item_layout_content.removeAllViews();
        chat_item_layout_content.addView(messageItem.messageView());

        //TODO 判断是否需要添加补充的区域
        if (true) {
            chat_item_layout_contentbottom_supplement.removeAllViews();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            ViewGroup chat_item_middle_supplement = (ViewGroup) inflater.inflate(R.layout.chat_item_middle_supplement, null);
            chatStateView = (TextView) chat_item_middle_supplement.findViewById(R.id.supplementtext);
            chat_item_layout_contentbottom_supplement.addView(chat_item_middle_supplement);
        }


    }

    // 上层view
    public ViewGroup getTopLayout() {
        return topLayout;
    }

    // 默认上层textview
    public TextView getTopTextView() {
        return topTextView;
    }

    public RelativeLayout getMiddleView() {
        return middleView;
    }

    //得到头像区域
    public View getChat_item_avatar_area() {
        return chat_item_avatar_area;
    }

    // 头像
    public ImageView getAvatarImageView() {
        return avatarImageView;
    }

    // 认证
    public ImageView getvImageView() {
        return vImageView;
    }

    public TextView getUserNameTextView() {
        return userNameTextView;
    }

    //得到消息content布局
    public RelativeLayout getChat_item_layout_content() {
        return chat_item_layout_content;
    }

    // 加载中、、、错误、view
    public LinearLayout getSignView() {
        return signView;
    }

    //加载失败
    public ImageView getLoadingErrorImage() {
        return loadingErrorImage;
    }

    //语音未读
    public ImageView getVoiceUnReadImage() {
        return voiceUnRead;
    }

    //语音时长
    public TextView getVoiceTimeTextView() {
        return voiceTime;
    }

    //消息状态
    public TextView getChatStateView() {
        return chatStateView;
    }

    //加载中
    public ProgressBar getMprogressbar() {
        return mprogressbar;
    }

    // 下层view
    public ViewGroup getBottomLayout() {
        return bottomLayout;
    }

    // 下层默认textview
    public TextView getBottomTextView() {
        return bottomTextView;
    }

    // 得到数据源
    public Map<String, Object> getResultMap() {
        return resultMap;
    }

    // 设置数据源
    public void setResultMap(Map<String, Object> resultMap) {
        this.resultMap = resultMap;
    }

    //设置回调接口
    public OnMessageClickListener getOnMessageClickListener() {
        return onMessageClickListener;
    }

    public void setOnMessageClickListener(
            OnMessageClickListener onMessageClickListener) {
        this.onMessageClickListener = onMessageClickListener;
    }

    public OnMessageLongClickListener getOnMessageLongClickListener() {
        return onMessageLongClickListener;
    }

    public void setOnMessageLongClickListener(
            OnMessageLongClickListener onMessageLongClickListener) {
        this.onMessageLongClickListener = onMessageLongClickListener;
    }

    public OnAvatarClickListener getOnAvatarClickListener() {
        return onAvatarClickListener;
    }

    public void setOnAvatarClickListener(OnAvatarClickListener onAvatarClickListener) {
        this.onAvatarClickListener = onAvatarClickListener;
    }

    public OnAvatarLongClickListener getOnAvatarLongClickListener() {
        return onAvatarLongClickListener;
    }

    public void setOnAvatarLongClickListener(
            OnAvatarLongClickListener onAvatarLongClickListener) {
        this.onAvatarLongClickListener = onAvatarLongClickListener;
    }

    public void setItemType(int type) {
        this.type = type;
    }

    public int getItemType() {
        return type;
    }

}

