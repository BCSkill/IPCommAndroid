package com.efounder.chat.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.LinearLayout.LayoutParams;

import com.efounder.chat.R;
import com.efounder.utils.CommonUtils;

/**
 * 
 * @author lch 5-25 接受消息体
 */
public class MessageIncomingView extends MessageBaseView {

	public MessageIncomingView(Context context) {
		super(context);
		initView(context);
	}

	public MessageIncomingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}

	@SuppressLint("ResourceAsColor")
	private void initView(Context context) {

		// 头像区域
		RelativeLayout.LayoutParams chat_item_avatar_area_layoutparams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		chat_item_avatar_area_layoutparams
				.addRule(RelativeLayout.ALIGN_PARENT_LEFT);// 与父容器的左侧对齐
		chat_item_avatar_area_layoutparams
				.addRule(RelativeLayout.ALIGN_PARENT_TOP);// 与父容器的上侧对齐
		chat_item_avatar_area.setId(R.id.chat_item_avatar_area);
		middleView.addView(chat_item_avatar_area,
				chat_item_avatar_area_layoutparams);

		// 消息体区域
		RelativeLayout.LayoutParams chat_item_layout_content_layoutparams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		chat_item_layout_content_layoutparams.addRule(RelativeLayout.RIGHT_OF,
				chat_item_avatar_area.getId());
		chat_item_layout_content_layoutparams
				.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		chat_item_layout_content_layoutparams.leftMargin = 10;
		chat_item_layout_content_layoutparams.topMargin = 2;
		chat_item_layout_content.setMinimumHeight(CommonUtils.dip2px(context,
				40));
		middleView.addView(chat_item_layout_content,
				chat_item_layout_content_layoutparams);
		chat_item_layout_content
				.setBackgroundResource(R.drawable.chat_from_bg_normal);
//		chat_item_layout_content.setPadding(18, 10, 10, 10);
		//chat_item_layout_content.setPadding(16, 1, 1, 1);
		/*
		 * ImageView content = new ImageView(context); LinearLayout.LayoutParams
		 * lp = new LayoutParams(200,11400);
		 * content.setBackgroundColor(R.color.black_deep);
		 */
		// chat_item_layout_content.setBackgroundColor(R.color.black_deep);
		chat_item_layout_content.setId(R.id.chat_item_layout_content);
		// chat_item_layout_content.addView(content,lp);

		// signview区域
		RelativeLayout.LayoutParams signView_layoutparams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		signView_layoutparams.addRule(RelativeLayout.RIGHT_OF,
				chat_item_layout_content.getId());
		signView_layoutparams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		signView_layoutparams.leftMargin = 10;
		// mprogressbar.setVisibility(View.VISIBLE);

		middleView.addView(signView, signView_layoutparams);

		// 消息体下方的补充区域
		RelativeLayout.LayoutParams chat_item_layout_contentbottom_supplement_layoutparams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		chat_item_layout_contentbottom_supplement_layoutparams.addRule(
				RelativeLayout.BELOW, chat_item_layout_content.getId());
		chat_item_layout_contentbottom_supplement_layoutparams.addRule(
				RelativeLayout.ALIGN_RIGHT, chat_item_layout_content.getId());
		chat_item_layout_contentbottom_supplement_layoutparams.leftMargin = 10;
		chat_item_layout_contentbottom_supplement_layoutparams.topMargin = 2;
		// chat_item_layout_contentbottom_supplement.setBackgroundColor(R.color.black);
		chat_item_layout_contentbottom_supplement.setVisibility(View.GONE);
		middleView.addView(chat_item_layout_contentbottom_supplement,
				chat_item_layout_contentbottom_supplement_layoutparams);

		middleView.setPadding(0, 0, CommonUtils.dip2px(context, 42), 0);

	}

	public void setImagePadding() {
		
//		chat_item_layout_content.setPadding(16, 3, 3, 3);
	}
	
}
