package com.efounder.chat.item;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.activity.ChatWebViewActivity;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;
import com.efounder.util.ToastUtil;
import com.efounder.view.RoundFrameLayout;

/**
 * 通告item
 *
 * @author YQS
 */
public class NotificationItem extends RoundFrameLayout implements IMessageItem, OnClickListener {


//    {"imageUrl":"",
//            "imageTitle":"标题",
//            "webUrl":"www.baidu.com",
//            "webTitle","":}

    private static final String TAG = "TempItem";
    private Context mContext;
    private LayoutInflater mInflater;
    private IMStruct002 imStruct002;
    private String webUrl;

    private RoundFrameLayout roundFragmeLayout;
    private ImageView ivTopIcon;
    private TextView tvTopTitle;
    private TextView tvBottomTitle;


    public NotificationItem(Context context) {
        super(context);
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        initView();
        initListener();
    }

    private void initListener() {
        this.setOnClickListener(this);
    }


    private void initView() {
        roundFragmeLayout = (RoundFrameLayout) mInflater.inflate(R.layout.wechatview_message_item_notification, this);
        LayoutParams lp = new LayoutParams(
                DisplayUtil.getMobileWidth(mContext) * 2 / 3 + 80, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setLayoutParams(lp);

        ivTopIcon = (ImageView) findViewById(R.id.iv_top_icon);
        tvTopTitle = (TextView) findViewById(R.id.tv_top_title);
        tvBottomTitle = (TextView) findViewById(R.id.tv_bottom_title);


    }

    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public void onClick(View v) {
        if (webUrl != null && !"".equals(webUrl)) {
            Intent intent = new Intent(mContext, ChatWebViewActivity.class);
            intent.putExtra("url", webUrl);
            mContext.startActivity(intent);
        } else {
            ToastUtil.showToast(mContext, R.string.wechatview_get_url_fail);
        }
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        this.imStruct002 = message;
        String imageUrl = null;
        String imageTitle = null;

        String webTitle = null;
        try {
            net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(imStruct002.getMessage());
            imageUrl = jsonObject.optString("imageUrl", "");
            imageTitle = jsonObject.optString("imageTitle", "");
            webUrl = jsonObject.optString("webUrl", "");
            webTitle = jsonObject.optString("webTitle", "");


        } catch (Exception e) {
            e.printStackTrace();
        }
        tvTopTitle.setText(imageTitle);
        tvBottomTitle.setText(webTitle);
        LXGlideImageLoader.getInstance().displayImage(mContext, ivTopIcon, imageUrl);
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {

    }

    @Override
    public void prepareForReuse() {
    }

}
