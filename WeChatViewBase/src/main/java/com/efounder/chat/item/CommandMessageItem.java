package com.efounder.chat.item;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.chat.utils.SmileUtils;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * 
 * @author lch
 *  支持文本、超链接、打电话
 *
 */
public class CommandMessageItem extends TextView  implements IMessageItem {

	public CommandMessageItem(Context context) {
		super(context);
		//初始化View
		this.setTextSize(TypedValue.COMPLEX_UNIT_SP,16); //16SP
		this.setMaxWidth(DisplayUtil.getMobileWidth(context)*2/3);
		this.setTextColor(getResources().getColor(R.color.ef_gray_dark));
	}

	@Override
	public View messageView() {
		return this;
	}

	@Override
	public ViewSize messageViewSize() {
		return null;
	}

	@Override
	public void setIMStruct002(IMStruct002 message) {
		//{"cmd":"作废","msgID":"0000090C000009110000015A88FA1CC2000026D700","text":"显示内容"}
		String json = message.getMessage();
		try {
			JSONObject jsonObject = new JSONObject(json);
			String text = jsonObject.getString("text");
			this.setText(text);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean getIsInUse() {
		return this.isShown();
	}

	@Override
	public void setIsInUse(boolean isInUse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prepareForReuse() {
		
	}
}
