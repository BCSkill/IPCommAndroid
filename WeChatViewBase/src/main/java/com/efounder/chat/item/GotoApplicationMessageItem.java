package com.efounder.chat.item;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.core.xml.StubObject;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.frame.ViewSize;
import com.efounder.frame.activity.EFTransformFragmentActivity;
import com.efounder.frame.utils.EFFrameUtils;
import com.efounder.frame.xmlparse.EFXmlConstants;
import com.efounder.message.struct.IMStruct002;

/**
 * 应用跳转
 * @author hudq
 *
 */
public class GotoApplicationMessageItem extends LinearLayout  implements IMessageItem {
	private Context mContext;

	public GotoApplicationMessageItem(Context context) {
		super(context);
		mContext = context;
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		inflate.inflate(R.layout.chat_item_goto_application, this);//注意第二个参数
		LinearLayout.LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
		this.setLayoutParams(lp);
		this.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
                //Intent intent = new Intent((Activity) mContext,EFTransformFragmentActivity.class);
				StubObject stubObject = new StubObject();
				stubObject.setString(EFXmlConstants.ATTR_FORM,"chaiyou.xml");
				Bundle bundle = new Bundle();
				bundle.putSerializable("stubObject", stubObject);
				bundle.putString(EFTransformFragmentActivity.EXTRA_TITLE_NAME,"柴油");
				bundle.putInt(EFTransformFragmentActivity.EXTRA_TITLE_RIGHT_VISIBILITY,View.INVISIBLE);

				try {
					EFFrameUtils.pushFragment((Class<? extends BaseFragment>) Class.forName("com.efounder.chat.fragment.EFAppAccountFormFragment"),bundle);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}


				//mContext.startActivity(intent);
			}
		});
	}

	@Override
	public View messageView() {
		return this;
	}

	@Override
	public ViewSize messageViewSize() {
		return null;
	}

	@Override
	public void setIMStruct002(IMStruct002 message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getIsInUse() {
		return this.isShown();
	}

	@Override
	public void setIsInUse(boolean isInUse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prepareForReuse() {
		
	}
}
