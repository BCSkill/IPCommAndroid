package com.efounder.chat.item;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.core.xml.StubObject;
import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.frame.ViewSize;
import com.efounder.imageloader.GlideImageLoader;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;
import com.efounder.util.AbFragmentManager;
import com.pansoft.xmlparse.FormatSet;
import com.pansoft.xmlparse.FormatTable;
import com.pansoft.xmlparse.MobileFormatUtil;

import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

/**
 * Created by apple on 2017/11/17.
 * <p>
 * 糖足中的消息item
 */

public class TangZuMessageItem extends LinearLayout implements IMessageItem {

    private Context mContext;
//    private ImageLoader imgLoader;
    private AbFragmentManager abFragmentManager;
    private TextView tvDetail;
    private ImageView icSmallIcon;
    private TextView tvSystemname;
    private TextView tvTime;
    private TextView tvState;
    private IMStruct002 message;
    private JSONObject jsonObject;

    public TangZuMessageItem(Context context) {
        super(context);
        mContext = context;
//        imgLoader = ImageLoader.getInstance();
        abFragmentManager = new AbFragmentManager(context);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.chat_item_tangzu_view, this);
        tvDetail = (TextView) findViewById(R.id.tv_detail);
        icSmallIcon = (ImageView) findViewById(R.id.ic_smallIcon);
        tvSystemname = (TextView) findViewById(R.id.tv_systemname);
        tvTime = (TextView) findViewById(R.id.tv_time);
        tvState = (TextView) findViewById(R.id.tv_state);

        int width = DisplayUtil.getMobileWidth(context) * 2 / 3 - 20;
        LayoutParams lp = new LayoutParams(width, LayoutParams.WRAP_CONTENT);
        this.setLayoutParams(lp);
        this.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String json = null;
                boolean isActivity = false;
                try {
                    json = new String(message.getBody(), "UTF-8");
                    net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(json);
                    Map<String, Object> map = jsonObject;

                    //默认的进入activity或者fragment的动画
                    int inTransition = R.anim.slide_in_from_right;
                    int outTransition = R.anim.slide_out_to_left;
                    if (jsonObject.has("tcms")) {
                        String tcms = jsonObject.getString("tcms");
                        if (!tcms.equals("") && tcms.equals("present")) {
                            //由下向上弹出
                            inTransition = R.anim.push_bottom_in;
                            outTransition = R.anim.push_top_out;
                        } else if (!tcms.equals("") && tcms.equals("alert")) {

                        }
                    }

                    StubObject stubObject = new StubObject();
                    Hashtable<String, Object> hashtable = new Hashtable<String, Object>();
                    stubObject.setStubTable(hashtable);
                    // Bundle bundle = new Bundle();
                    for (String key : map.keySet()) {
                        hashtable.put(key, map.get(key).toString());
                    }
                    //String AndroidShow =map.get("AndroidShow");//com.efounder.RNTaskDetailActivity"

                    hashtable.put("imStruct002", message);
                    if (jsonObject.has("AndroidShow")) {
                        String showClass = jsonObject.getString("AndroidShow");
                        if (showClass.equals("com.efounder.RNTaskDetailActivity")) {
                            //如果没有配置相应的类型，则弹框不跳转activity
                            FormatSet formatSet = MobileFormatUtil.getInstance().getFormatSet();
                            String djID = "";
                            net.sf.json.JSONObject taskData = jsonObject.getJSONObject("TaskData");
                            if (null != taskData) {
                                String PFLOW_ID = taskData.optString("PFLOW_ID");
                                if (PFLOW_ID != null && !PFLOW_ID.equals("")) {
                                    djID = PFLOW_ID;
                                } else {
                                    djID = taskData.optString("FLOW_ID");
                                }
                            }
                            if (null != djID && !djID.equals("")) {
                                FormatTable hfmt = formatSet.getFormatTableById(djID);

                                if (hfmt == null) {
                                    Toast.makeText(getContext(), "当前业务资源文件尚未配置，请联系管理员", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            }

                        }
                    }
                    abFragmentManager.startActivity(stubObject, inTransition, outTransition);

//                    if (!isActivity) {
//                       // bundle.putString(CommonTransformFragmentActivity.EXTRA_TITLE_NAME, jsonObject.getString("title"));
//                        EFFrameUtils.startActivity(CommonTransformFragmentActivity.class, bundle, inTransition, outTransition);
//                    } else {
//                       // bundle.putString(EFTransformFragmentActivity.EXTRA_TITLE_NAME, jsonObject.getString("title"));
//                        Class activity = abFragmentManager.getActivity(stubObject);
//                        EFFrameUtils.startActivity(activity, bundle, inTransition, outTransition);
//
//                    }


//                    EFFrameUtils.pushFragment((Class<? extends BaseFragment>) fragment.getClass(),
//                            bundle, inTransition, outTransition);
                    // bundle.putString(EFTransformFragmentActivity.EXTRA_THEME, "dialog");com.efounder.chat.fragment.WebViewFragment
//                    EFFrameUtils.pushFragment((Class<? extends BaseFragment>) Class.forName(jsonObject.getString("AndroidShow")),
//                            bundle, inTransition, outTransition);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        this.message = message;

        try {
            String json = new String(message.getBody(), "UTF-8");
            jsonObject = new JSONObject(json);

            // FIXME: 2017/6/19 设置详情
            if (jsonObject.has("content")) {
                tvDetail.setText(jsonObject.getString("content"));
                tvDetail.setVisibility(View.VISIBLE);
            } else {
                tvDetail.setVisibility(View.GONE);
            }
            // FIXME: 2017/6/19 设置系统名称
            if (jsonObject.has("systemName")) {
                tvSystemname.setText(jsonObject.getString("systemName"));
                tvSystemname.setVisibility(View.VISIBLE);
            } else {
                tvSystemname.setVisibility(View.GONE);
            }
            // FIXME: 2017/6/19 设置左下角小图片
            if (jsonObject.has("smallIcon")) {
                String imageString = jsonObject.getString("smallIcon");
                if (imageString.equals("")) {
                    icSmallIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.wechat_icon_launcher));
                } else {
                    GlideImageLoader.getInstance().displayImage(mContext,icSmallIcon,imageString);
//                    imgLoader.displayImage(imageString, icSmallIcon);
                }
            } else {
                icSmallIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.wechat_icon_launcher));
            }
            // FIXME: 2017/6/19  设置时间
            if (jsonObject.has("time")) {
                String time = jsonObject.getString("time");
                if (!time.equals("")) {
                    tvTime.setVisibility(VISIBLE);
                    tvTime.setText(time);
                } else {
                    tvTime.setVisibility(GONE);
                }
            } else {
                tvTime.setVisibility(GONE);
            }
            // FIXME: 2017/6/19  设置状态
            if (jsonObject.has("state")) {
                String formState = jsonObject.getString("state");
                if (!formState.equals("")) {
                    tvState.setVisibility(VISIBLE);
                    tvState.setText(formState);
                } else {
                    tvState.setVisibility(INVISIBLE);
                }
            } else {
                tvState.setVisibility(INVISIBLE);
            }

            // FIXME: 2017/6/19  设置状态颜色
            if (jsonObject.has("stateColor")) {
                String stateLevel = jsonObject.getString("stateColor");
                if ("green".equals(stateLevel)) {//绿色
                    tvState.setBackgroundResource(R.drawable.form_textviewboard_green);
                    tvState.setTextColor(Color.parseColor("#4db239"));
                } else if ("blue".equals(stateLevel)) {//蓝色
                    tvState.setBackgroundResource(R.drawable.form_textviewboard_blue);
                    tvState.setTextColor(Color.parseColor("#108ee9"));
                } else if ("yellow".equals(stateLevel)) {//黄色
                    tvState.setBackgroundResource(R.drawable.form_textviewboard_yellow);
                    tvState.setTextColor(Color.parseColor("#fcaf0b"));
                } else if ("red".equals(stateLevel)) {//红色
                    tvState.setBackgroundResource(R.drawable.form_textviewboard_red);
                    tvState.setTextColor(Color.parseColor("#ff6666"));
                } else {//默认

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {

    }

    @Override
    public void prepareForReuse() {

    }
}
