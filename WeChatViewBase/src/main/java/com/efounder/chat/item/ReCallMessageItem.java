package com.efounder.chat.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.mobilecomps.contacts.User;

/**
 * 撤回消息
 * @author yqs
 *
 */
public class ReCallMessageItem extends LinearLayout  implements IMessageItem {
	private Context mContext;
	private TextView textView;

	public ReCallMessageItem(Context context) {
		super(context);
		mContext = context;
		this.setBackgroundResource(R.color.transparent);
		LinearLayout.LayoutParams mLineaLayoutParams = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		this.setLayoutParams(mLineaLayoutParams);
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view =inflate.inflate(R.layout.chat_item_recallmessage, this);//注意第二个参数
		textView = (TextView) view.findViewById(R.id.recallmessage);


	}

	@Override
	public View messageView() {
		return this;
	}

	@Override
	public ViewSize messageViewSize() {
		return null;
	}

	@Override
	public void setIMStruct002(IMStruct002 message) {
		User currentUser = null;
		if (message.getpUserId()==0){
			currentUser = WeChatDBManager.getInstance().getOneUserById(message.getFromUserId());
		}else{
			currentUser = WeChatDBManager.getInstance().getOneUserById(message.getpUserId());
		}
		textView.setText(getResources().getString(R.string.wechatview_recallmessage_tip,currentUser.getNickName()));
		
	}

	@Override
	public boolean getIsInUse() {
		return this.isShown();
	}

	@Override
	public void setIsInUse(boolean isInUse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prepareForReuse() {
		
	}
}
