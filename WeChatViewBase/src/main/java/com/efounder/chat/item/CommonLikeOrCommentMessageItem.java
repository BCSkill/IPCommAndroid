package com.efounder.chat.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;
import com.efounder.util.AbFragmentManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Map;

/**
 * @author : zzj
 * @e-mail : zhangzhijun@pansoft.com
 * @date : 2018/10/1613:57
 * @desc : 通用评论 点赞item
 * @version: 1.0
 *
 * // 点赞
 * {
 * "title":"某某点赞了我",
 * "content":"文章标题",
 * "images":["这里是图片链接数组","这里是图片链接数组"],
 *
 * "viewType":"display",
 * "AndroidShow":"com.efounder.news.activity.DetailNewsActivity",
 * "show":"NewsController",
 *
 *  "newsBean",{newJson}
 * }
 *
 *
 *
 * // 评论
 * {
 * "title":"某某评论了我的文章",
 * "content":"真好笑(评论内容)",
 * "images":["这里是图片链接数组","这里是图片链接数组"],
 *
 * "viewType":"display",
 * "AndroidShow":"com.efounder.news.activity.DetailNewsActivity",
 * "show":"NewsController",
 *
 *  "newsBean",{newJson}
 * }
 *
 *
 *
 * // 回复
 * {
 * "title":"某某回复了我",
 * "content":"真好笑(回复内容)",
 * "images":["这里是图片链接数组","这里是图片链接数组"],
 *
 * "viewType":"display",
 * "AndroidShow":"com.efounder.news.activity.DetailNewsActivity",
 * "show":"NewsController",
 *  "newsBean",{newJson}
 * }
 */

public class CommonLikeOrCommentMessageItem extends LinearLayout implements IMessageItem {

    /**
     * title
     */
    private TextView tvLikeorcommentTitle;
    /**
     * content
     */
    private TextView tvLikeorcommentContent;
    /**
     * image
     */
    private ImageView ivLikeorcommentPic;
    private JSONObject jsonObject;
    private IMStruct002 message;
    private Context mContext;
    private AbFragmentManager abFragmentManager;
    public CommonLikeOrCommentMessageItem(Context context) {
        super(context);
        mContext = context;
//        imgLoader = ImageLoader.getInstance();
        abFragmentManager = new AbFragmentManager(context);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.wechatview_common_likeorcomment_layout, this);
        tvLikeorcommentTitle = findViewById(R.id.tv_likeorcomment_title);
        tvLikeorcommentContent = findViewById(R.id.tv_likeorcomment_content);
        ivLikeorcommentPic = findViewById(R.id.iv_likeorcomment_pic);
        //param
        int width = DisplayUtil.getMobileWidth(context) * 2 / 3 - 20;
        LayoutParams lp = new LayoutParams(width, LayoutParams.WRAP_CONTENT);
        this.setLayoutParams(lp);
        this.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String json = null;
                boolean isActivity = false;
                try {
                    //message.setExtMap(null);
                    json = message.getMessage();
                    net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(json);
                    Map<String, Object> map = jsonObject;

                    //默认的进入activity或者fragment的动画
                    int inTransition = R.anim.slide_in_from_right;
                    int outTransition = R.anim.slide_out_to_left;

                    StubObject stubObject = new StubObject();
                    Hashtable<String, Object> hashtable = new Hashtable<String, Object>();
                    stubObject.setStubTable(hashtable);
                    // Bundle bundle = new Bundle();
                    for (String key : map.keySet()) {
                        hashtable.put(key, map.get(key).toString());
                    }
                    //String AndroidShow =map.get("AndroidShow");//com.efounder.RNTaskDetailActivity"

                    hashtable.put("imStruct002", message);
                    if (jsonObject.has("AndroidShow")) {
                        String showClass = jsonObject.getString("AndroidShow");
                    }
                    abFragmentManager.startActivity(stubObject, inTransition, outTransition);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        this.message = message;
        String json = null;
        try {
            json = new String(message.getBody(), "UTF-8");
            jsonObject = new JSONObject(json);
            tvLikeorcommentTitle.setText(jsonObject.optString("title", ""));
            tvLikeorcommentContent.setText(jsonObject.optString("content", ""));
            JSONArray imageArray = jsonObject.optJSONArray("images");
            if(imageArray!=null&&imageArray.length()>0){
               String imageUrl = (String) imageArray.optString(0);
                LXGlideImageLoader.getInstance().displayRoundCornerImage(mContext,ivLikeorcommentPic,imageUrl,R.drawable.unknow, R.drawable.unknow, 20);
            }else {
                ivLikeorcommentPic.setVisibility(GONE);
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {

    }

    @Override
    public void prepareForReuse() {

    }
}
