package com.efounder.chat.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.util.AppContext;

import net.sf.json.JSONObject;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * 群通知，提示等消息
 *
 * @author yqs
 */
public class ChatTipsMessageItem extends LinearLayout implements IMessageItem {
    //通用通知
    public static final String TYPE_NOTIFY = "notification";
    //接收文件成功的消息类型
    public static final String TYPE_NOTIFY_RECEIVED_FILE = "receivedFile";


    private Context mContext;
    private TextView textView;

    public ChatTipsMessageItem(Context context) {
        super(context);
        mContext = context;
        this.setBackgroundResource(R.color.transparent);
        LayoutParams mLineaLayoutParams = new LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        mLineaLayoutParams.setMargins(100, 0, 100, 0);
        this.setLayoutParams(mLineaLayoutParams);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflate.inflate(R.layout.wechatview_chat_item_chat_tips, this);//注意第二个参数
        textView = (TextView) view.findViewById(R.id.tv_content);


    }

    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        String json = message.getMessage();

        try {
//            JSONObject jsonObject = JSONObject.fromObject(json);
//            String type = jsonObject.optString("type", "");
//            if (TYPE_NOTIFY.equals(type)) {
//                //进入群组的提醒
//                textView.setText(jsonObject.optString("content", ""));
//            } else if (TYPE_NOTIFY_RECEIVED_FILE.equals(type)) {
//                //接收文件成功的提醒
//                textView.setText(jsonObject.optString("content", ""));
//                if (message.getFromUserId() == Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID))) {
//                    textView.setText("您已成功接收对方发送的文件");
//                } else {
//                    textView.setText("对方已成功接收您发送的文件");
//                }
//     }
            String text = getNotificationText(message);
            textView.setText(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        textView.setText(getResources().getString(R.string.wechatview_recallmessage_tip,currentUser.getNickName()));

    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {
        // TODO Auto-generated method stub

    }

    @Override
    public void prepareForReuse() {

    }


    /**
     * 从消息中获取文本
     *
     * @param imStruct002
     * @return
     */
    public static String getNotificationText(IMStruct002 imStruct002) {
        String json = imStruct002.getMessage();
        JSONObject jsonObject = JSONObject.fromObject(json);

        String type = jsonObject.optString("type", "");
        if (TYPE_NOTIFY.equals(type)) {
            //进入群组的提醒
            return jsonObject.optString("content", "");
        } else if (TYPE_NOTIFY_RECEIVED_FILE.equals(type)) {
            //接收文件成功的提醒
            if (imStruct002.getFromUserId() == Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID))) {
                return AppContext.getInstance().getResources().getString(R.string.wechatview_file_received_1,
                        jsonObject.optString("fileName", ""));
            } else {
                return AppContext.getInstance().getResources().getString(R.string.wechatview_file_received_2,
                        jsonObject.optString("fileName", ""));
            }
        }
        return json;
    }
}
