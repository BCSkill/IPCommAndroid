package com.efounder.chat.item;

import java.util.Timer;
import java.util.TimerTask;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.interf.DismissCallbacks;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.chat.widget.BadgeView;
import com.efounder.chat.widget.ChatListView;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

public class TextMessageItemForDes extends RelativeLayout implements IMessageItem,OnClickListener{

	private static final String TAG = "TextMessageItemForDes";
	private  final int REMOVE_VIEW = 0;
	private  final int TIME_VIEW = 1;
	long mAnimationTime = 200;
	Object mToken;
	boolean dismissRight = false;
	int mViewWidth;
	
	private TextView contentTv;
	private TextView showTv;
	BadgeView badge ;
	Spannable text;
	private DismissCallbacks mCallbacks;
	View rootView;
	ChatListView chatListView;
	private Context mContext;
	/**使用该控件  最后设置为false*/
	public static boolean isRunning;
	
	private IMStruct002 iMStruct002;
	LayoutInflater inflater;
	
	public TextMessageItemForDes(Context context,ChatListView chatListView,View rootView,Spannable text,DismissCallbacks mCallbacks) {
		super(context);
		this.text = text;
		this.mCallbacks = mCallbacks;
		this.rootView = rootView;
		this.chatListView = chatListView;
		this.mContext = context;
		inflater = LayoutInflater.from(context);
	}

	private void init() {
		RelativeLayout.LayoutParams mLineaLayoutParams = new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		this.setLayoutParams(mLineaLayoutParams);
		 
		View rootView ;
		if (Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID))==
		iMStruct002.getFromUserId()) {
			rootView	= inflater.inflate(R.layout.item_in_destroy_send, this);
			dismissRight = true;
		}else{
			rootView	= inflater.inflate(R.layout.item_in_destroy, this);
			dismissRight = false;
		}
		
		this.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				mViewWidth = TextMessageItemForDes.this.getWidth();
				TextMessageItemForDes.this.getViewTreeObserver().removeGlobalOnLayoutListener(this);
			}
		});
		
		contentTv = (TextView) rootView.findViewById(R.id.anim_tv);
		showTv = (TextView) rootView.findViewById(R.id.sc_tv);
		contentTv.setTextSize(15);
		contentTv.setTextColor(getResources().getColor(R.color.black_deep));
		
		badge = new BadgeView(mContext, contentTv);
		badge.setBadgeMargin(3);
		badge.setTextSize(13);
		badge.setTag("badge");
		if (Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID))==
				iMStruct002.getFromUserId()) {
			badge.setBadgePosition(BadgeView.POSITION_TOP_LEFT); //默认值
		}else{
			badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT); //默认值
		}
		badge.setText("5");
		badge.setBadgeBackgroundColor(mContext.getResources().getColor(R.color.red));
		
		this.setOnClickListener(this);
	}

	@Override
	public View messageView() {
		return this;
	}

	@Override
	public ViewSize messageViewSize() {
		return null;
	}

	@Override
	public void onClick(View v) {
		if(v==this){
			if(!isRunning){
				isRunning = true;
				showTv.setVisibility(View.GONE);
				contentTv.setVisibility(View.VISIBLE);
				contentTv.setText(text);
				badge.show();
				timeAction(rootView);
			}else{
//				Toast.makeText(mContext, "请稍后再试", 0).show();
			}
		}
	}

	//计时器
		int i = 5;
		private void timeAction(final View view){
			final Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					i--;
					if(i==0){
						timer.cancel();
						mHandler.sendEmptyMessage(REMOVE_VIEW);
					}else{
						Message msg = new Message();
						msg.obj = i;
						msg.what = TIME_VIEW;
						mHandler.sendMessage(msg);
					}
				}
			}, 0, 1000);
		}
		int curI=0;
		private Handler mHandler = new Handler(){
			public void handleMessage(android.os.Message msg) {
				switch (msg.what) {
				case REMOVE_VIEW:
					performAnim(rootView);
					break;
				case TIME_VIEW:
					curI = (Integer) msg.obj;
					badge.setText(i+"");
					break;
				}
			};
		};
		private void performAnim(final View mView){
			mView.animate()
	        .translationX(dismissRight ? mViewWidth : -mViewWidth)
	        .alpha(0)
	        .setDuration(mAnimationTime)
	        .setListener(new AnimatorListenerAdapter() {
	            @Override
	            public void onAnimationEnd(Animator animation) {
	                performDismiss(mView);
	            }
	        });
		}
		private void performDismiss(final View mView) {
	        final ViewGroup.LayoutParams lp = mView.getLayoutParams();
	        final int originalHeight = mView.getHeight();

	        ValueAnimator animator = ValueAnimator.ofInt(originalHeight, 1).setDuration(mAnimationTime);

	        animator.addListener(new AnimatorListenerAdapter() {
	            @Override
	            public void onAnimationEnd(Animator animation) {
	                mCallbacks.onDismiss(mView, mToken);
	                isRunning = false;
//	            	ViewGroup view = (ViewGroup) mView.getParent();
//	                view.removeView(mView);
	                // Reset view presentation
	                mView.setAlpha(1f);
	                mView.setTranslationX(0);
	                lp.height = originalHeight;
	                mView.setLayoutParams(lp);
	            }
	        });

	        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
	            @Override
	            public void onAnimationUpdate(ValueAnimator valueAnimator) {
	                lp.height = (Integer) valueAnimator.getAnimatedValue();
	                mView.setLayoutParams(lp);
	            }
	        });

	        animator.start();
	    }

		@Override
		public void setIMStruct002(IMStruct002 message) {
			// TODO Auto-generated method stub
		  iMStruct002 = message;
		  init();
		}

		@Override
		public boolean getIsInUse() {
			return this.isShown();
		}

		@Override
		public void setIsInUse(boolean isInUse) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void prepareForReuse() {
			
		}
}
