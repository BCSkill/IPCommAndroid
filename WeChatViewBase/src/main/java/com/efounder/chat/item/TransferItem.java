package com.efounder.chat.item;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;
import com.efounder.utils.ResStringUtil;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * 转账界面
 *{
 "tradingTime":"转账时间",
 "money":"转账金额",
 "moneyUnit":"单位BTC、TFT等",
 "txFee":"手续费，可为空",
 "sendIMUserId":"发送方id",
 "receiveIMUserId":"接收方id",
 "tradingID":"交易号，交易Hash",
 "receiveWalletAddress":"接收地址",
 "sendWalletAddress":"发送地址",
 "androidShow":"com.pansoft.openplanet.activity.TransactionResultActivity",
 "iosShow":"WalletTradeResultController"
 "chainId":"链编号"
 }
 * @author yqs
 */
public class
TransferItem extends LinearLayout implements IMessageItem {
    private static final String TAG = "TransferItem";

    private Context mContext;
    private ImageView iconImageView;
    private LinearLayout parentLayout;
    private TextView moneyTextView;
    private TextView messageTextView;


    private IMStruct002 imStruct002;
    private int chainId;

//	private Bitmap mBitmap;//每个view中持有一个bitmap（如果放到imSruct中，会内存占用过多）


    public TransferItem(Context context) {
        super(context);
        mContext = context;
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        parentLayout = (LinearLayout) inflater.inflate(R.layout.chat_item_transfer, this);
        iconImageView = (ImageView) parentLayout.findViewById(R.id.imageview);
        moneyTextView = parentLayout.findViewById(R.id.tvMoney);
        messageTextView = parentLayout.findViewById(R.id.tvMessage);

        int width = DisplayUtil.getMobileWidth(context) * 2 / 3 - 20;
        LayoutParams lp = new LayoutParams(width, LayoutParams.MATCH_PARENT);
        this.setLayoutParams(lp);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageBody = imStruct002.getMessage();

                try {
                    net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(messageBody);
                    if (jsonObject != null) {
                        Intent intent = new Intent();
                        try {
                            if (jsonObject.containsKey("androidShow")) {
                                intent.setClass(mContext, Class.forName(jsonObject.optString("androidShow")));
                            } else {
                                intent.setClass(mContext, Class.forName("com.pansoft.openplanet.activity.TransferMessageDetailActivity"));
                            }
                            try {
                                chainId = jsonObject.optInt("chainId");
                            } catch (Exception e) {
                                chainId = Integer.valueOf(jsonObject.optString("chainId"));
                            }

                            intent.putExtra("receiveUserId", jsonObject.optString("receiveUserId", ""));
                            intent.putExtra("receiveWalletAddress", jsonObject.optString("receiveWalletAddress", ""));
                            intent.putExtra("money", jsonObject.optString("money", "0"));
                            intent.putExtra("sendWalletAddress", jsonObject.optString("sendWalletAddress", ""));
                            intent.putExtra("sendUserId", jsonObject.optString("sendUserId", ""));
                            intent.putExtra("tradingTime", jsonObject.optString("tradingTime", ""));

                            intent.putExtra("sendIMUserId", jsonObject.optString("sendIMUserId",  ""));
                            intent.putExtra("receiveIMUserId", jsonObject.optString("receiveIMUserId",  ""));
                            intent.putExtra("tradingID", jsonObject.optString("tradingID", ""));
                            intent.putExtra("txFee", jsonObject.optString("txFee", ""));
                            intent.putExtra("moneyUnit", jsonObject.optString("moneyUnit", ""));
                            intent.putExtra("chainId", chainId);

                            intent.putExtra("block", jsonObject.optString("block", ""));
                            mContext.startActivity(intent);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        imStruct002 = message;
        String messageBody = imStruct002.getMessage();

        try {
            net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(messageBody);
            if (jsonObject != null) {
                String money = jsonObject.optString("money", "0");
                moneyTextView.setText(money + ResStringUtil.getString(R.string.wechatview_energy));
                if (jsonObject.containsKey("moneyUnit")) {
                    try {
                        chainId = jsonObject.optInt("chainId");
                    } catch (Exception e) {
                        chainId = Integer.valueOf(jsonObject.optString("chainId"));
                    }
                    moneyTextView.setText(money + " " + jsonObject.optString("moneyUnit"));
                    if (chainId == 1) {
                        iconImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chat_item_transfer_btc));
                    } else if (chainId == 2) {
                        iconImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chat_item_transfer_eth));
                    } else if (chainId == 3 || chainId == 5) {
                        iconImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chat_item_transfer_eos));
                    }else if(chainId == 0x88){
                        //ete
                        iconImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chat_item_transfer_ete1));
                    } else{
                        iconImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chat_item_transfer_icon));
                    }
                } else {
                    iconImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chat_item_transfer_icon));
                }

                //发送方id等于我的id，是我发出的
                if (jsonObject.optString("sendIMUserId",  "").equals(EnvironmentVariable.getProperty(CHAT_USER_ID))) {
                    moneyTextView.setText(R.string.wechatview_transaction_out);
                    messageTextView.setText(R.string.wechatview_transaction_other_base);
                } else {
                    //发送方id不等于我的id，对方转入我的账户
                    if (!jsonObject.optString("sendIMUserId",  "").equals("")) {
                        moneyTextView.setText(R.string.wechatview_transaction_transfer);
                        messageTextView.setText(R.string.wechatview_transaction_you_base);
                    }
                }
                //若果接收方id等于我的，那么是我接收的
                if (jsonObject.optString("receiveIMUserId",  "").equals(EnvironmentVariable.getProperty(CHAT_USER_ID))){
                    moneyTextView.setText(R.string.wechatview_transaction_transfer);
                    messageTextView.setText(R.string.wechatview_transaction_you_base);
                } else {
                    //接收方id不是我的，那么是我发出的
                    if (!jsonObject.optString("receiveIMUserId", "").equals("")) {
                        moneyTextView.setText(R.string.wechatview_transaction_out);
                        messageTextView.setText(R.string.wechatview_transaction_other_base);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {
    }

    @Override
    public void prepareForReuse() {

    }

//    public static String formatPrice(double price, boolean halfUp, int digits) {
//        DecimalFormat formater = new DecimalFormat();
//        // keep 2 decimal places
//        formater.setMaximumFractionDigits(digits);
//        formater.setMinimumFractionDigits(digits);
//        formater.setGroupingSize(3);
//        formater.setRoundingMode(halfUp ? RoundingMode.HALF_UP : RoundingMode.FLOOR);
//        return formater.format(price);
//    }


}
