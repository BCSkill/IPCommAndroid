package com.efounder.chat.item;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;

public class ShareMessageItem extends LinearLayout implements IMessageItem {

    private Context mContext;
    private LinearLayout rootView;
    private ImageView imageView;
    private TextView tvTitle, tvMessage;

    private IMStruct002 imStruct002;

    public ShareMessageItem(Context context) {
        super(context);
        this.mContext = context;
        initView(context);
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        imStruct002 = message;
        String messageBody = imStruct002.getMessage();

        try {
            net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(messageBody);
            if (jsonObject != null) {
                tvTitle.setText(jsonObject.optString("subject", ""));
                tvMessage.setText(jsonObject.optString("text", ""));
                if (jsonObject.optString("imgUrl") != null && jsonObject.optString("imgUrl").equals("https://m.yxlady.com/favicon.ico")) {
                    LXGlideImageLoader.getInstance().displayImage(mContext, imageView,
                            R.drawable.share_web, R.drawable.share_web,R.drawable.share_web);
                }else {
                    LXGlideImageLoader.getInstance().displayImage(mContext, imageView,
                            jsonObject.optString("imgUrl"), R.drawable.share_web, R.drawable.share_web);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {

    }

    @Override
    public void prepareForReuse() {

    }

    private void initView(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        rootView = (LinearLayout) inflater.inflate(R.layout.chat_item_share, this);
        imageView = rootView.findViewById(R.id.imageview);
        tvTitle = rootView.findViewById(R.id.tvTitle);
        tvMessage = rootView.findViewById(R.id.tvMessage);

        int width = DisplayUtil.getMobileWidth(context) * 2 / 3 - 20;
        LayoutParams lp = new LayoutParams(width, LayoutParams.MATCH_PARENT);
        this.setLayoutParams(lp);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageBody = imStruct002.getMessage();

                try {
                    net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(messageBody);
                    if (jsonObject != null) {
                        Intent intent = new Intent();
                        try {
                            intent.setClass(context, Class.forName
                                    ("com.efounder.chat.activity.ChatWebViewActivity"));
                            intent.putExtra("url",
                                    jsonObject.optString("url", ""));
                            context.startActivity(intent);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
