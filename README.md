<p align="center">
<img src="./assets/ipcomm_icon.png" width="150" />
</p>
<h1 align="center"><a href="https://ipcom.io">IPComm星际通讯</a></h1>
<p align="center">
    <a >
        <img src="https://img.shields.io/badge/Licence-GPL3.0-green.svg?style=flat" />
    </a>
    </p>
<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>

### 项目介绍
星际通讯是基于区块链的价值共享互联网即时通讯应用平台，是一个去中心化的任何人都可以使用的通讯网络，是一款基于区块链的价值共享互联网即时通讯APP。星际通讯系统为人与设备、人与人、人与服务、服务与设备等提供高效、稳定、即时的网络通讯服务。通过区块链加密存储技术帮助您管理数字资产，支持即时消息通讯、离线消息；并支持文字、图片、语音、视频、表单及自定义消息类型。
完全由原生代码实现了单聊、群聊、公众号等聊天功能。聊天格式支持文字、表情、图片、视频、文件等常规内容，更拓展支持加密文本、加密图片等加密内容。

### 下载试用
<p><img src="./assets/android.png" width="40" style="vertical-align: middle;" />
			<a href="https://gitee.com/IPComm/IPCommAndroid/attach_files/451733/download">Android</a></p>
<p><img src="./assets/ios.png" width="40" style="vertical-align: middle;" />
			<a href="https://gitee.com/IPComm/IPCommAndroid/attach_files/344175/download">iOS</a></p>
<p><img src="./assets/html5.png" width="40" style="vertical-align: middle;" />
			<a href="https://tc.ipcom.io/tcserver/html/OpenPlanet/index.html#/login">Web</a></p>
<p><img src="./assets/windows.png" width="40" style="vertical-align: middle;" />
			<a href="https://ipcomefile.openserver.cn/ipcom/windows/ipcom1.1.48.exe">Windows</a></p>
<p><img src="./assets/macos.png" width="40" style="vertical-align: middle;" />
			<a href="https://ipcomefile.openserver.cn/ipcom/mac/ipcom.dmg">MacOS</a></p>
<p><img src="./assets/linux.png" width="40" style="vertical-align: middle;" />
			<a href="https://ipcomefile.openserver.cn/ipcom/linux/ipcom_ubuntu.AppImage">Ubuntu</a></p>

### IPComm全平台仓库

<!--- [Android仓库](https://gitee.com/IPComm/IPCommAndroid)-->
<!--- [iOS仓库](https://gitee.com/IPComm/IPCommiOS)-->
<!--- [Web仓库](https://gitee.com/IPComm/IPCommWeb)-->
<!--- [Desktop仓库](https://gitee.com/IPComm/IPCommDesktop)-->
<p><img src="./assets/repo_android.png" width="40" style="vertical-align: middle;"/>
			<a href="https://gitee.com/IPComm/IPCommAndroid" >Android仓库</a></p>
		<p><img src="./assets/repo_ios.png" width="40" style="vertical-align: middle;" />
			<a href="https://gitee.com/IPComm/IPCommiOS">iOS仓库</a></p>
		<p><img src="./assets/repo_web.png" width="40" style="vertical-align: middle;" />
			<a href="https://gitee.com/IPComm/IPCommWeb">Web仓库</a></p>
		<p><img src="./assets/repo_desktop.png" width="40" style="vertical-align: middle;" />
			<a href="https://gitee.com/IPComm/IPCommDesktop">Desktop仓库</a></p>

### 主要特点
#### 聊天
- 支持单聊、群聊、公众号等聊天功能；
- 支持文字、表情、图片、视频、文件等普通聊天格式；
- 支持加密文字、加密图片等加密内容；
- 支持语音实时输入发送（“光速短信”）；
- 数字货币红包；
- 支持公众号消息的推送服务；
#### 其他
- 空间（“引力场”）动态的发送、展示、评论互动功能；
- 数字货币的资产管理、转账功能；
- 数字货币的行情、K线图实时展示；
- 新闻资讯服务；
- 商城系统；
- 支持多语言
- 支持换肤

### 运行环境
- compileSdkVersion = 28
- minSdkVersion = 20
- gradle版本5.4.1

### 如何使用
##### 项目使用git的submodule来管理多个库工程依赖
-  AndroidStudio使用 `vcs`->`Checkout from Version Control`->`Git` 输入`https://gitee.com/IPComm/IPCommAndroid.git`导入工程
 - 也可以在新的目录中使用命令行执行`git clone  --recurse https://gitee.com/IPComm/IPCommAndroid.git`导入工程

- 如果clone下来的库工程没有代码，可以执行命令：`git submodule init`  和 ` git submodule update `来拉取

- 在子模块上提交等操作详见 [git submodule文档](https://git-scm.com/book/zh/v2/Git-%E5%B7%A5%E5%85%B7-%E5%AD%90%E6%A8%A1%E5%9D%97)


### 工程结构
- IPComm：主工程
- WeChatViewBase：聊天库
- IPAssetLibAndroid：钱包库
- NewsLibAndroid：新闻资讯库
- ThirdPartyCompsAndroid：三方工具库
- UShareAndroid：分享库
- ZoneLibAndroid：空间库
- XunFeiLibAndroid：语音识别库
- MallLibAndroid：商城库
- PayLibAndroid：商城支付库
- NewsLibAndroid：资讯库
- PushLibAndroid：推送库
- ChartLibAndroid：图表、K线库

### 界面展示

<img src="./assets/assets_home.jpg" width="300" />

<img src="./assets/mall.jpg" width="300" />

<img src="./assets/market.jpg" width="300" />

<img src="./assets/zone_home.jpg" width="300" />

<img src="./assets/news.jpg" width="300" />

<img src="./assets/chat_list.jpg" width="300" />

<img src="./assets/chat.jpg" width="300" />

<img src="./assets/chat_group.jpg" width="300" />

<img src="./assets/friends_list.jpg" width="300" />

<img src="./assets/user_info.jpg" width="300" />

<img src="./assets/add_friend.jpg" width="300" />


### 版权声明
本软件使用 GPL3.0 协议，请严格遵照协议内容!

### 合作及联系
- QQ交流群: 976048137

- 联系邮箱：app@turbochain.ai
<img src="./assets/qq_ipcomm.jpg" width="300" />

### End
